unit main;

{$mode delphi}{$H+}

interface

uses
    {$IFDEF LINUX}
    cwstring,
    {$ENDIF}
    {$IFDEF WINDOWS}
    Windows,
    {$ENDIF}
    {$IFDEF THREADREADER}input_reader,{$ENDIF}
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    StdCtrls, ExtDlgs, ActnList, pipes, Contnrs, math;

type

    TLCanvasException = exception;
    TPointFloat = record x,y: real; end;
    TMatrix = array[1..3,1..3] of real;

    { TGO }

    TGO = class
        fcolor: TColor;
        ftransform: TMatrix;
        fdisplacement: TPoint;
        ps: TPenStyle;
        pm: TPenMode;
        pw: integer;
        procedure paint; virtual; abstract;
    end;


    { TGAxis }

    TGAxis = class (TGO)
        step: real;
        pos: integer;
        manual_pos: boolean;
        name: unicodestring;
    end;

    { TGAxisX }

    TGAxisX = class (TGAxis)
        procedure paint; override;
    end;

    { TGAxisY }

    TGAxisY = class (TGAxis)
        procedure paint; override;
    end;

    { TGGrid }

    TGGrid = class (TGO)
        stepX, stepY: real;
        procedure paint; override;
    end;

    { TGMark }

    TGMark = class (TGO)
        fp: TPointFloat;
        symbol: char;
        text: unicodestring;
        procedure paint; override;
    end;

    { TGPoint }

    TGPoint = class (TGO)
        x, y: real;
        procedure paint; override;
    end;

    { TGPolyline }

    TGPolyline = class (TGO)
        points: array of TPointFloat;
        ps: TPenStyle;
        pm: TPenMode;
        procedure paint; override;
        procedure add_point(x,y: real);
    end;

    { TGPlot }

    TGPlot = class (TGPolyline)
    end;


    { TGLegend }

    TGLegend = class (TGO)
        x,y: real;
        limit: integer;
        mode: (up, down, bottom, mTop);
        lines: array of record sym: char; cl: TColor; s: unicodestring; end;
        procedure paint; override;
        procedure add_line(sym: char; cl: TColor; s: unicodestring);
    end;

    { TForm1 }

    TForm1 = class(TForm)
        Action_font_size: TAction;
        Action_SaveAs: TAction;
        ActionList1: TActionList;
        SavePictureDialog1: TSavePictureDialog;
        Timer1: TTimer;
        objects: TObjectList;
        cmdl: TStringList;
        procedure Action_font_sizeExecute(Sender: TObject);
        procedure Action_SaveAsExecute(Sender: TObject);
        procedure FormCreate(Sender: TObject);
        procedure FormDestroy(Sender: TObject);
        procedure FormPaint(Sender: TObject);
        procedure FormResize(Sender: TObject);
        procedure FormShow(Sender: TObject);
        procedure Timer1Timer(Sender: TObject);
    private
        procedure on_command;
        procedure cmd_size;
        procedure cmd_area;
        procedure cmd_axis;
        procedure cmd_background;
        procedure cmd_circle;
        procedure cmd_color;
        procedure cmd_displacement;
        procedure cmd_equal_scale;
        procedure cmd_grid;
        procedure cmd_legend;
        procedure cmd_legend_line;
        procedure cmd_mark;
        procedure cmd_new_plot;
        procedure cmd_pen;
        procedure cmd_plot;
        procedure cmd_point;
        procedure cmd_polyline;
        procedure cmd_polyline_binary;
        procedure cmd_position;
        procedure cmd_save_as;
        procedure cmd_translate;
        procedure cmd_rotate;
        procedure cmd_scale;
        procedure ExecuteFile(fn: unicodestring);
        procedure SaveAs(fn: unicodestring);
        function tail(n: integer): unicodestring;
        { private declarations }
    public
        { public declarations }
    end;

const
    identity: TMatrix = ((1,0,0),(0,1,0),(0,0,1));

var
    Form1: TForm1;
    stdin: TInputPipeStream;
    stdout: TOutputPipeStream;
    backbuffer: TBitmap;
    cmd: TCommand;


    lo_x: real = -1;
    hi_x: real = 1;
    a_x: real = 2;
    lo_y: real = -1;
    hi_y: real = 1;
    a_y: real = 2;
    cw,ch: integer;
    frame: integer;
    cnv: TCanvas;
    legend: TGLegend;
    background_color: TColor = clWhite;
    equal_scale: boolean = false;
    color: TColor = clBlack;
    penstyle: TPenStyle = psSolid;
    penmode: TPenMode = pmCopy;
    penwidth: integer = 1;
    displacement: TPoint = (x:0; y:0);
    dp: TPoint = (x:0; y:0);
    transform: TMatrix = ((1,0,0),(0,1,0),(0,0,1));
    tm: TMatrix = ((1,0,0),(0,1,0),(0,0,1));


implementation


{$R *.lfm}

procedure error(const msg: unicodestring);
begin
    raise TLCanvasException.Create(msg);
end;

function StrToFloat(s: string): real;
var fs: TFormatSettings;
begin
    fs.DecimalSeparator:='.';
    if not TryStrToFloat(s, result, fs) then begin
        fs.DecimalSeparator:=',';
        result := SysUtils.StrToFloat(s, fs);
    end;
end;

function mul(const p: TPointFloat; const m: TMatrix): TPointFloat; overload;
begin
    result.x := p.x*m[1,1]+p.y*m[2,1]+1*m[3,1];
    result.y := p.x*m[1,2]+p.y*m[2,2]+1*m[3,2];
end;

function mul(const m1: TMatrix; const m2: TMatrix): TMatrix; overload;
begin
    result[1,1] := m1[1,1]*m2[1,1]+m1[1,2]*m2[2,1]+m1[1,3]*m2[3,1];
    result[1,2] := m1[1,1]*m2[1,2]+m1[1,2]*m2[2,2]+m1[1,3]*m2[3,2];
    result[1,3] := m1[1,1]*m2[1,3]+m1[1,2]*m2[2,3]+m1[1,3]*m2[3,3];

    result[2,1] := m1[2,1]*m2[1,1]+m1[2,2]*m2[2,1]+m1[2,3]*m2[3,1];
    result[2,2] := m1[2,1]*m2[1,2]+m1[2,2]*m2[2,2]+m1[2,3]*m2[3,2];
    result[2,3] := m1[2,1]*m2[1,3]+m1[2,2]*m2[2,3]+m1[2,3]*m2[3,3];

    result[3,1] := m1[3,1]*m2[1,1]+m1[3,2]*m2[2,1]+m1[3,3]*m2[3,1];
    result[3,2] := m1[3,1]*m2[1,2]+m1[3,2]*m2[2,2]+m1[3,3]*m2[3,2];
    result[3,3] := m1[3,1]*m2[1,3]+m1[3,2]*m2[2,3]+m1[3,3]*m2[3,3];
end;

function translate(const v: TPointFloat): TMatrix;
begin
    result := identity;
    result[1,3]:=v.x;
    result[2,3]:=v.y;
end;

procedure set_area(lx,hx,ly,hy: real);
begin
    lo_x := lx;
    hi_x := hx;
    lo_y := ly;
    hi_y := hy;
    a_x := hi_x - lo_x;
    a_y := hi_y - lo_y;
    cw := Form1.ClientWidth;
    ch := Form1.ClientHeight;

    frame := min(cw, ch);
end;

function scaleX(x: real): integer; inline;
var wx: real;
begin
    if equal_scale
    then wx := cw/2 - frame/2 + frame*(x - lo_x)/a_x
    else wx := cw*(x - lo_x)/a_x;
    result := Round(wx)+dp.x;
end;

function scaleY(y: real): integer; inline;
var wy: real;
begin
    if equal_scale
    then wy := ch/2 + frame/2 - frame*(y - lo_y)/a_y
    else wy := ch - ch*(y - lo_y)/a_y;
    result := Round(wy)-dp.y;
end;

function scale(x, y: real): TPoint;  overload;
var wx,wy: real;
begin
    if equal_scale
    then begin
        wx := cw/2 - frame/2 + frame*(x - lo_x)/a_x;
        wy := ch/2 + frame/2 - frame*(y - lo_y)/a_y;
    end
    else begin
        wx := cw*(x - lo_x)/a_x;
        wy := ch - ch*(y - lo_y)/a_y;
    end;
    result := classes.point(Round(wx)+dp.x, Round(wy)-dp.y);
end;

function scale(_p: TPointFloat): TPoint;  overload;
var wx,wy: real; p: TPointFloat;
begin
    p := mul(_p,tm);
    if equal_scale
    then begin
        wx := cw/2 - frame/2 + frame*(p.x - lo_x)/a_x;
        wy := ch/2 + frame/2 - frame*(p.y - lo_y)/a_y;
    end
    else begin
        wx := cw*(p.x - lo_x)/a_x;
        wy := ch - ch*(p.y - lo_y)/a_y;
    end;
    result := classes.point(Round(wx)+dp.x, Round(wy)-dp.y);
end;


function read_color(s: string): TColor;
begin
    if s='red'      then result := clRed else
    if s='green'    then result := clGreen else
    if s='blue'     then result := clBlue else
    if s='black'    then result := clBlack else
    if s='white'    then result := clWhite else
    if s='lime'     then result := clLime else
    if s='maroon'   then result := clMaroon else
    if s='gray'     then result := clGray else
    if s='silver'   then result := clSilver else
    if s='yellow'   then result := clYellow else
    if s='olive'    then result := clOlive else
    if s='navy'     then result := clNavy else
    if s='purple'   then result := clPurple else
    if s='teal'     then result := clTeal else
    if s='fuchsia'  then result := clFuchsia else
    if s='aqua'     then result := clAqua else
    if s='moneygreen' then result := clMoneyGreen else
    if s='skyblue'  then result := clSkyBlue else
    if s='cream'    then result := clCream else
    if s='medgray'  then result := clMedGray else
    result := TColor(StrToInt(s));
end;


{ TGLegend }

procedure TGLegend.paint;
var i, ix, iy, h,l: integer; //p: TPoint;
    procedure put;
    begin
        cnv.Font.Color:=lines[i].cl;
        cnv.TextOut(ix,iy+15*i,lines[i].s);
    end;
begin
    //p := scale(fp);
    ix := scaleX(x);
    iy := scaleY(y);
    case mode of
        up: begin
            iy := iy-15*Length(lines);
            for i := high(lines) downto 0 do put;
        end;
        down: begin
            for i := 0 to high(lines) do put;
        end;
        bottom: begin
            iy := Form1.ClientHeight-round(y)-15*Length(lines);;
            ix := round(x);
            for i := high(lines) downto 0 do put;
        end;
        mTop: begin
            iy := round(y);
            ix := round(x);
            for i := 0 to high(lines) do put;
        end;
    end;
end;

procedure TGLegend.add_line(sym: char; cl: TColor; s: unicodestring);
var l,i: integer;
begin
    if (limit>=0) and (Length(lines)>=limit) then begin
        for i := 1 to high(lines) do lines[i-1] := lines[i];
        l := high(lines);
    end
    else begin
        l := Length(lines);
        SetLength(lines,l+1);
    end;
    lines[l].sym:=sym;
    lines[l].cl:=cl;
    lines[l].s:=s;
end;



{ TGGrid }

procedure TGGrid.paint;
var i, px, lx, hx, py, ly, hy: integer;
begin
    ly := scaleY(lo_y);
    hy := scaleY(hi_y);
    for i := round(lo_x/stepX)+1 to round(hi_x/stepX)-1 do begin
        px := scaleX(i*stepX);
        cnv.Line(px,ly,px,hy);
    end;
    lx := scaleX(lo_x);
    hx := scaleX(hi_x);
    for i := round(lo_y/stepY)+1 to round(hi_y/stepY)-1 do begin
        py := scaleY(i*stepY);
        cnv.Line(lx,py,hx,py);
    end;
end;

{ TGMark }

procedure TGMark.paint;
var p: TPoint;
begin
    p := scale(fp);
    cnv.Pen.Style:=psSolid;
    case symbol of
        'x': begin
            cnv.Line(p.x-4, p.y-4, p.x+5, p.y+4);
            cnv.Line(p.x-4, p.y+4, p.x+5, p.y-4);
        end;
        'o': cnv.EllipseC(p.x,p.y,4,4);
        'O': begin cnv.Pen.Width:=7; cnv.EllipseC(p.x,p.y,2,2); end;
        'i': cnv.Line(p.x,p.y-4,p.x,p.y+4);
        '-': cnv.Line(p.x-4,p.y,p.x+4,p.y);
        'v': begin cnv.Line(p.x-3,p.y-7,p.x,p.y); cnv.Line(p.x,p.y,p.x+3,p.y-7); end;
        '>': begin cnv.Line(p.x-7,p.y-3,p.x,p.y); cnv.Line(p.x-7,p.y+3,p.x,p.y); end;
        '<': begin cnv.Line(p.x+7,p.y-3,p.x,p.y); cnv.Line(p.x+7,p.y+3,p.x,p.y); end;
        '^': begin cnv.Line(p.x-3,p.y+7,p.x,p.y); cnv.Line(p.x,p.y,p.x+3,p.y+7); end;
    end;
    cnv.Font.Color:=fcolor;
    cnv.TextOut(p.x+6,p.y+5,text);
end;

{ TGAxisY }

procedure TGAxisY.paint;
var h_pos, sstep: real; p: TPoint; py, i: integer; txt: string; pf: TPointFloat;
begin
    cnv.Font.Color:=fcolor;
    h_pos := tm[3,1];
   // WriteLn(tm[3,1],tm[3,2]);
    if lo_x>0 then h_pos := lo_x;
    if hi_x<0 then h_pos := hi_x;

    p := scale(h_pos, hi_y);
    p.y := p.y+1;
    if manual_pos then p.x := p.x + pos;
    cnv.Line(Point(p.x,scaleY(lo_y)),p);
    cnv.Line(p.X,p.Y,p.X+3,p.Y+5);
    cnv.Line(p.X,p.Y,p.X-3,p.Y+5);
    cnv.TextOut(p.x - cnv.TextWidth(name)-1, p.y+cnv.TextHeight(name) div 4, name);

    sstep := step*tm[2,2];

    for i := round(lo_y/sstep)+1 to round(hi_y/sstep)-1 do begin
        py := scaleY(i*sstep);
        cnv.Line(p.X+3, py, p.X, py);
        txt := FloatToStr(i*step);
        cnv.TextOut(p.x-cnv.TextWidth(txt)-1,py+1,txt);
    end;

end;

{ TGAxisX }

procedure TGAxisX.paint;
var v_pos, sstep: real; p: TPoint; px, i: integer; txt: string;
begin
    cnv.Font.Color:=fcolor;
    v_pos := tm[3,2];
    if lo_y>0 then v_pos := lo_y;
    if hi_y<0 then v_pos := hi_y;

    p := scale(hi_x, v_pos);
    p.x := p.x-1;
    if manual_pos then p.y := p.y - pos;
    cnv.Line(Point(scaleX(lo_x),p.y),p);
    cnv.Line(p.X,p.Y,p.X-5,p.Y+3);
    cnv.Line(p.X,p.Y,p.X-5,p.Y-3);
    cnv.TextOut(p.x - cnv.TextWidth(name)-1, p.y+cnv.TextHeight(name) div 4, name);

    sstep := step*tm[1,1];

    for i := round(lo_x/sstep)+1 to round(hi_x/sstep)-1 do begin
        px := scaleX(i*sstep);
        cnv.Line(px,p.Y-3,px,p.Y);
        txt := FloatToStr(i*step);
        cnv.TextOut(px - cnv.TextWidth(txt)-1,p.y+1,txt);
    end;
end;



{ TGPolyline }

procedure TGPolyline.paint;
var i: integer;
begin
    if Length(points)=0 then Exit;
    cnv.MoveTo(scale(points[0]));
    for i := 1 to high(points) do cnv.LineTo(scale(points[i]));
end;

procedure TGPolyline.add_point(x, y: real);
var l: integer;
begin
    l := Length(points);
    SetLength(points, l+1);
    points[l].x:=x;
    points[l].y:=y;
    if l=0 then Exit;
    tm := self.ftransform;
    displacement := self.fdisplacement;
    cnv.Pen.Color:=fcolor;
    cnv.MoveTo(scale(points[l-1]));
    cnv.LineTo(scale(points[l]));
end;

{ TGPoint }

procedure TGPoint.paint;
var p: TPoint;
begin
    p := scale(x,y);
    cnv.Pixels[p.X, p.Y] := fcolor;
end;

{ TForm1 }

procedure TForm1.Timer1Timer(Sender: TObject);
{$IFDEF THREADREADER}{$ELSE}var b1, b2: byte; ch: unicodechar; crlf: array[0..3] of byte = (13, 0 ,10 ,0);{$ENDIF}
begin
    {$IFDEF THREADREADER}
    cmd := next_command;
    while cmd.S<>'' do begin
        on_command;
        cmd := next_command;
    end;
    {$ELSE}
    Timer1.Enabled:=false;
    while stdin.NumBytesAvailable>1 do begin
        b1 := stdin.ReadByte;
        b2 := stdin.ReadByte;
        ch := unicodechar(b1+256*b2);
        case ch of
            #10,#13: begin
                on_command;
                //stdout.WriteByte(13); stdout.WriteByte(0); stdout.WriteByte(10); stdout.WriteByte(0);
                //WriteLn('OK');
                //flush();
                //Write(#13,#0,#10,#0);
            end
            else cmd := cmd + ch;
        end;
    end;
    Timer1.Enabled:=true;
    {$ENDIF}
end;




procedure TForm1.on_command;
var cnt, i: integer;
begin try try
    cnt := objects.Count;
    if cmd.s='' then Exit;
    cmdl.DelimitedText:= cmd.s;
    if cmdl.Count=0 then Exit;

    //ShowMessage(cmdl[0]);

    if cmdl[0]='area' then cmd_area else                          // min_x max_x min_y max_y
    if cmdl[0]='axis' then cmd_axis else                          // axis_name step [name] [pos]
    if cmdl[0]='background' then cmd_background else              // color
    if cmdl[0]='caption' then caption := tail(1) else             // caption
    if cmdl[0]='clear' then objects.Clear else                    //
    if cmdl[0]='color' then cmd_color else                        // color
    if cmdl[0]='displacement' then cmd_displacement else          // x y
    if cmdl[0]='equal_scale' then cmd_equal_scale else            // on/off
    if cmdl[0]='grid' then cmd_grid else                          // stepx stepy
    if cmdl[0]='legend' then cmd_legend else                      // x y mode
    if cmdl[0]='legend_line' then cmd_legend_line else            // symbol text
    if cmdl[0]='mark' then cmd_mark else                          // symbol x y text
    if cmdl[0]='pen' then cmd_pen else                            // penstyle|penmode
    if cmdl[0]='plot' then cmd_plot else                          // x y1 y2 ...
    if cmdl[0]='new_plot' then cmd_new_plot else                  //
    if cmdl[0]='point' then cmd_point else                        // x y
    if cmdl[0]='polyline' then cmd_polyline else                  // p1x p1y p2x p2y ...
    if cmdl[0]='binary_polyline' then cmd_polyline_binary else  // N format -> p1x p1y p2x p2y ...
    if cmdl[0]='position' then cmd_position else                  // x y w h
    if cmdl[0]='reset_transform' then transform := identity else  //
    if cmdl[0]='rotate' then cmd_rotate else                      // angle
    if cmdl[0]='save_as' then cmd_save_as else                    // filename
    if cmdl[0]='scale' then cmd_scale else                        // sx sy
    if cmdl[0]='size' then cmd_size;                              // width height
    if cmdl[0]='terminate' then Application.Terminate else        //
    if cmdl[0]='translate' then cmd_translate;                    // dx dy

    for i := cnt to objects.Count-1 do begin
        (objects[i] as TGO).fcolor:=color;
        (objects[i] as TGO).ftransform:=transform;
        (objects[i] as TGO).fdisplacement:=displacement;
        (objects[i] as TGO).ps:=penstyle;
        (objects[i] as TGO).pm:=penmode;
        (objects[i] as TGO).pw:=penwidth;
    end;

finally
    cmd.s := '';
    if cmdl[0]<>'plot' then paint;

    Application.ProcessMessages;
end;
except
    on E:Exception do
        MessageDlg(E.className, E.Message+' in '+LineEnding+cmd.s, mtError, [mbOk], 0);
end;
end;

procedure TForm1.cmd_size;
var w,h: integer;
begin
    w := StrToIntDef(cmdl[1],-1);
    h := StrToIntDef(cmdl[2],-1);
    if (w>0) and (h>0) then begin
        Form1.ClientWidth := w;
        Form1.ClientHeight := h;
    end
    else begin
        Form1.ClientWidth := Round(screen.Width*StrToFloatDef(cmdl[1],0.2));
        Form1.ClientHeight := Round(screen.Height*StrToFloatDef(cmdl[2],0.2));
    end;
end;

procedure TForm1.cmd_area;
begin
    set_area(
        StrToFloat(cmdl[1]),
        StrToFloat(cmdl[2]),
        StrToFloat(cmdl[3]),
        StrToFloat(cmdl[4]));
end;

procedure TForm1.cmd_axis;
var axis: TGAxis;
begin
    if cmdl[1]='x' then axis := TGAxisX.Create else
    if cmdl[1]='y' then axis := TGAxisY.Create else
    error('invalid axis '+cmdl[1]);

    axis.step:=StrToFloat(cmdl[2]);
    if cmdl.count>3 then axis.name:=cmdl[3] else axis.name := '';
    axis.manual_pos := cmdl.Count>4;
    if axis.manual_pos then axis.pos := StrToInt(cmdl[4]);

    objects.Add(axis);
end;

procedure TForm1.cmd_background;
begin
    background_color := read_color(cmdl[1]);
end;

procedure TForm1.cmd_circle;
begin

end;

procedure TForm1.cmd_color;
begin
    color := read_color(cmdl[1]);
end;

procedure TForm1.cmd_displacement;
begin
    if cmdl.Count=3
    then begin
        displacement.x := StrToInt(cmdl[1]);
        displacement.y := StrToInt(cmdl[2]);
    end;

    if cmdl.count=1
    then begin
        displacement.x := 0;
        displacement.y := 0;
    end;
end;

procedure TForm1.cmd_equal_scale;
begin
    equal_scale := true;
    if (cmdl.count>1) and (cmdl[1]='off') then equal_scale := false;
end;

procedure TForm1.cmd_grid;
var g: TGGrid;
begin
    g := TGGrid.Create;
    g.stepX:=StrToFloat(cmdl[1]);
    g.stepY:=StrToFloat(cmdl[2]);
    objects.Add(g);
end;

procedure TForm1.cmd_legend;
var l: TGLegend;
begin
    l := TGLegend.Create;
    SetLength(l.lines,0);
    l.x:=StrToFloat(cmdl[1]);
    l.y:=StrToFloat(cmdl[2]);
    if cmdl[3]='up' then l.mode:=up
    else if cmdl[3]='down' then l.mode:=down
    else if cmdl[3]='bottom' then l.mode:=bottom
    else if cmdl[3]='top' then l.mode:=mTop;
    if cmdl.Count>4 then l.limit:=StrToInt(cmdl[4]) else l.limit:=-1;
    objects.add(l);
    legend := l;
end;

procedure TForm1.cmd_legend_line;
begin
    legend.add_line(cmdl[1][1],color,tail(2));
end;

procedure TForm1.cmd_mark;
var m: TGMark;
begin
    m := TGMark.Create;
    m.symbol:=cmdl[1][1];
    m.fp.x:= StrToFloat(cmdl[2]);
    m.fp.y:= StrToFloat(cmdl[3]);
    m.text:=tail(4);
    objects.Add(m);
end;

procedure TForm1.cmd_plot;
var  x: real; i,o: integer;
label next_plot;
begin
    x := StrToFloat(cmdl[1]);
    o := 0;
    for i := 2 to cmdl.Count-1 do begin
        while true do begin
            Inc(o);
            if o>objects.Count then Exit;
            if objects[o-1] is TGPlot then begin
                (objects[o-1] as TGPlot).add_point(x,StrToFloat(cmdl[i]));
                goto next_plot;
            end;
        end;
        next_plot:
    end;
end;

procedure TForm1.cmd_new_plot;
begin
    objects.Add(TGPlot.Create);
end;

procedure TForm1.cmd_pen;
var w: integer;
begin
    if cmdl.Count<2
    then
        begin
            penstyle := psSolid;
            penmode := pmCopy;
            penwidth := 1
		end
    else

    if cmdl[1]='solid'      then penstyle := psSolid else
    if cmdl[1]='dash'       then penstyle := psDash else
    if cmdl[1]='dot'        then penstyle := psDot else
    if cmdl[1]='dashdot'    then penstyle := psDashDot else
    if cmdl[1]='dashdotdot' then penstyle := psDashDotDot else
    if cmdl[1]='clear'      then penstyle := psClear else

    if cmdl[1]='copy'       then penmode := pmCopy else
    if cmdl[1]='notcopy'    then penmode := pmNotCopy else
    if cmdl[1]='not'        then penmode := pmNot else
    if cmdl[1]='xor'        then penmode := pmXor else
    if cmdl[1]='notxor'     then penmode := pmNotXor else
    if cmdl[1]='mask'       then penmode := pmMask else
    if cmdl[1]='notmask'    then penmode := pmNotMask else
    if cmdl[1]='masknotpen' then penmode := pmMaskNotPen else
    if cmdl[1]='merge'      then penmode := pmMerge else
    if cmdl[1]='notmerge'   then penmode := pmNotMerge else
    if cmdl[1]='mergepennot' then penmode := pmMergePenNot else
    if cmdl[1]='mergenotpen' then penmode := pmMergeNotPen else

    begin
        w := StrToIntDef(cmdl[1],-1);
        if w>0 then penwidth := w;
	end;
end;

procedure TForm1.cmd_point;
var p: TGPoint;
begin
    p := TGPoint.Create;
    p.x := StrToFloat(cmdl[1]);
    p.y := StrToFloat(cmdl[2]);
    p.fcolor:=color;
    objects.Add(p);
end;

procedure TForm1.cmd_polyline;
var i: integer; pl: TGPolyline;
begin
    pl := TGPolyline.Create;
    pl.ps := penstyle;
    pl.pm := penmode;
    SetLength(pl.points, (cmdl.Count-1) div 2);
    for i := 0 to high(pl.points) do begin
        pl.points[i].x:= StrToFloat(cmdl[1+i*2]);
        pl.points[i].y:= StrToFloat(cmdl[1+i*2+1]);
    end;
    objects.Add(pl);
end;

procedure TForm1.cmd_polyline_binary;
var i: integer; pl: TGPolyline; sa: TSingleArrayObject;
begin
    pl := TGPolyline.Create;
    pl.ps := penstyle;
    pl.pm := penmode;
    SetLength(pl.points, StrToIntDef(cmdl[1],0) div 2);
    sa := cmd.data as TSingleArrayObject;
    for i := 0 to high(pl.points) do begin
        pl.points[i].x := sa.s[i*2];
        pl.points[i].y := sa.s[i*2+1];
    end;
    objects.add(pl);
    FreeAndNil(sa);
end;

procedure TForm1.cmd_position;
var x,y,w,h: integer;
begin
    if cmdl[1]='fullscreen' then begin
        form1.BorderStyle:=bsNone;
        form1.Width:=screen.Width;
        form1.Height:=screen.Height;
        form1.Left:=0;
        form1.Top:=0;
        Exit;
    end
    else form1.BorderStyle:=bsSizeable;

    if (cmdl.Count>5) and (cmdl[5]='no_border') then Form1.BorderStyle:=bsNone;

    if cmdl.Count>4 then begin
        w := StrToIntDef(cmdl[3],-1);
        h := StrToIntDef(cmdl[4],-1);
        if (w>0) and (h>0) then begin
            Form1.ClientWidth := w;
            Form1.ClientHeight := h;
        end
        else begin
            Form1.ClientWidth := Round(screen.Width*StrToFloatDef(cmdl[3],0.2));
            Form1.ClientHeight := Round(screen.Height*StrToFloatDef(cmdl[4],0.2));
        end;
    end;
    x := StrToIntDef(cmdl[1],-1);
    y := StrToIntDef(cmdl[2],-1);
    if (x>=0) and (y>=0) then begin
        Form1.Left := x;
        Form1.Top := y;
    end
    else begin
        Form1.Left := Round((screen.Width-Form1.Width)*(0.5+StrToFloatDef(cmdl[1],0)));
        Form1.Top := Round((screen.Height-Form1.Height)*(0.5-StrToFloatDef(cmdl[2],0)));
    end;

end;

procedure TForm1.cmd_save_as;
begin
    SaveAs(tail(1));
end;

procedure TForm1.cmd_translate;
var tm: TMatrix;
begin
    tm := identity;
    tm[3,1]:=StrToFloat(cmdl[1]);
    tm[3,2]:=StrToFloat(cmdl[2]);
    transform := mul(transform, tm);
end;

procedure TForm1.cmd_rotate;
var a: real; tm: TMatrix;
begin
    tm := identity;
    a := math.degtorad(StrToFloat(cmdl[1]));
    tm[1,1] := cos(a); tm[1,2] := sin(a);
    tm[2,1] :=-sin(a); tm[2,2] := cos(a);
    transform := mul(transform, tm);
end;

procedure TForm1.cmd_scale;
var tm: TMatrix;
begin
    tm := identity;
    tm[1,1] := StrToFloat(cmdl[1]);
    tm[2,2] := StrToFloat(cmdl[2]);
    transform := mul(transform, tm);
end;

procedure TForm1.ExecuteFile(fn: unicodestring);
var f: TextFile; s: string;
begin
    AssignFile(f,fn);
    Reset(f);
    while not eof(f) do begin
        ReadLn(f, s);
        cmd.s := s;
        on_command;
    end;
    CloseFile(f);
end;

procedure TForm1.SaveAs(fn: unicodestring);
var png: TPortableNetworkGraphic;
begin
    paint;
    if UpperCase(extractFileExt(fn))='.PNG' then try
        png := TPortableNetworkGraphic.Create;
        png.Assign(backbuffer);
        png.SaveToFile(fn);
    finally
        png.Free;
    end
    else backbuffer.SaveToFile(fn);
end;

function TForm1.tail(n: integer): unicodestring;
var i: integer;
begin
    result := '';
    for i := n to cmdl.Count-1 do result := result + ' ' + cmdl[i];
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
    {$IFDEF THREADREDER}
    {$ELSE}
    stdin := TInputPipeStream.Create({$IFDEF WINDOWS}GetStdHandle(std_input_handle){$ELSE}0{$ENDIF});
    stdout := TOutputPipeStream.Create({$IFDEF WINDOWS}GetStdHandle(std_output_handle){$ELSE}0{$ENDIF});
    {$ENDIF}
    cmdl := TStringList.Create;
    cmdl.Delimiter:=' ';
    objects := TObjectList.Create(true);
    cw := form1.ClientWidth;
    ch := form1.ClientHeight;
    cnv := form1.Canvas;
    set_area(lo_x, hi_x, lo_y, hi_y);
    backbuffer := TBitmap.Create;
end;

procedure TForm1.Action_SaveAsExecute(Sender: TObject);
begin
    if SavePictureDialog1.Execute then SaveAs(SavePictureDialog1.FileName);
end;

procedure TForm1.Action_font_sizeExecute(Sender: TObject);
begin
    backbuffer.Canvas.Font.Size:=backbuffer.Canvas.Font.Size+1;
    Form1.Paint;
    Application.ProcessMessages;
end;


procedure TForm1.FormDestroy(Sender: TObject);
begin
    {$IFDEF THREADREADER}
    {$ELSE}
    stdin.Free;
    stdout.Free;
    {$ENDIF}
    cmdl.Free;
    objects.Free;
    backbuffer.Free;
end;

procedure TForm1.FormPaint(Sender: TObject);
var i: integer;
begin

    backbuffer.SetSize(Form1.ClientWidth, form1.ClientHeight);
    cnv := backbuffer.Canvas;

    Form1.BeginFormUpdate;
    cnv.Brush.Color:=background_color;
    cnv.Brush.Style:=bsSolid;
    cnv.FillRect(cnv.ClipRect);
    cnv.Pen.Color := clBlack;
    cnv.Pen.Style := psSolid;
    cnv.Pen.Mode := pmCopy;
    cnv.Pen.Width := 1;
    cnv.Brush.Style:=bsClear;
    for i := 0 to objects.Count-1 do
        with (objects[i] as TGO) do begin
            cnv.pen.Color := fcolor;
            cnv.Pen.Style := ps;
            cnv.Pen.Mode := pm;
            cnv.Pen.Width:=pw;
            tm := ftransform;
            dp := fdisplacement;
            paint;
        end;
    Form1.EndFormUpdate;
    cnv := Form1.Canvas;
    cnv.CopyRect(cnv.ClipRect,backbuffer.Canvas,cnv.ClipRect);
end;

procedure TForm1.FormResize(Sender: TObject);
begin
    set_area(lo_x, hi_x, lo_y, hi_y);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
    if paramCount=1 then ExecuteFile(paramStr(1));
end;

end.


