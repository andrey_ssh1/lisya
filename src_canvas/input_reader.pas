unit input_reader;

{$mode objfpc}{$H+}

interface

uses
    {$IFDEF LINUX}
    cwstring, ctypes,
    {$ENDIF}
    {$IFDEF WINDOWS}
    Windows,
    {$ENDIF}
    Classes, SysUtils, pipes;

type
    TSingles = array of single;

    { TSingleArrayObject }

    TSingleArrayObject = class
        s: TSingles;
        constructor Create(_s: TSingles);
    end;

    TCommand = record
        s: unicodestring;
        data: TObject;
    end;


function next_command(): TCommand;


implementation

type

    { TReader }

    TReader = class (TThread)
    protected
        procedure Execute; override;
    end;



var
    reader: TReader;
    cs: TRTLCriticalSection;
    cmd_queue: TStringList;
    stdin: TInputPipeStream;
    stdout: TOutputPipeStream;

function next_command: TCommand;
begin
    result.s := '';
    result.data := nil;
    EnterCriticalSection(cs);
    if cmd_queue.Count>0
    then begin
        result.s := cmd_queue[0];
        result.data := cmd_queue.Objects[0];
        cmd_queue.Delete(0);
    end;
    LeaveCriticalSection(cs);
end;


function is_binary_command(s: unicodestring): boolean;
begin
  result := (Pos('binary_', s) = 1);
end;

//var d: record case byte of 0: (s: single); 1: (b: array [1..SizeOf(single)] of Byte); end;
//begin
//    ReadBytes(d.b[1], SizeOf(single), true);
//    result := d.s;

function read_binary(cmd: unicodestring): TObject;
var cmdl: TStringList; n, i: integer; t: (f32); s: TSingles;
    d: record case byte of 0: (s: single); 1: (dw: DWORD); end;
begin

    cmdl := TStringList.Create;
    cmdl.DelimitedText := cmd;
    n := StrToInt(cmdl[1]);
    if cmdl[2]='single' then t := f32;
    case t of f32: SetLength(s, n); end;

    for i := 0 to n-1 do
        case t of
            f32: begin
                d.dw := stdin.ReadDWord;
                s[i] := d.s;
              end;
        end;

    case t of
        f32: result := TSingleArrayObject.Create(s);
    end;
end;

{ TSingleArrayObject }

constructor TSingleArrayObject.Create(_s: TSingles);
begin
    s := _s;
end;

{ TReader }

procedure TReader.Execute;
var b1, b2: byte; ch: unicodechar; crlf: array[0..3] of byte = (13, 0 ,10 ,0);
  s: unicodestring; O: TObject;
begin
    while true do begin
        if Terminated then break;
        b1 := stdin.ReadByte;
        b2 := stdin.ReadByte;
        ch := unicodechar(b1+256*b2);
        case ch of
            #13:;
            #10: begin
                if is_binary_command(s)
                then O := read_binary(s)
                else O := nil;
                EnterCriticalSection(cs);
                cmd_queue.AddObject(s, O);
                s := '';
                LeaveCriticalSection(cs);
            end
            else s := s + ch;
        end;
    end;
end;

initialization
    stdin := TInputPipeStream.Create({$IFDEF WINDOWS}GetStdHandle(std_input_handle){$ELSE}0{$ENDIF});
    stdout := TOutputPipeStream.Create({$IFDEF WINDOWS}GetStdHandle(std_output_handle){$ELSE}0{$ENDIF});
    InitCriticalSection(cs);
    cmd_queue := TStringList.Create;
    cmd_queue.Clear;
    cmd_queue.OwnsObjects := false;
    reader := TREader.Create(false);

finalization
    reader.Terminate;
    FreeAndNil(cmd_queue);
    DoneCriticalSection(cs);
    FreeAndNil(stdin);
    FreeAndNil(stdout);


end.

