unit lisya_packages;

{$mode delphi}{$H+}

interface

uses
    {$IFDEF LINUX}
    cwstring,
    {$ENDIF}
    Classes, SysUtils, dlisp_values, mar, lisya_exceptions
    , dlisp_read
    , lisya_predicates
    , lisia_charset
    , lisya_streams;

type

    { TPackage }

    TPackage = class
        name: unicodestring;
        uname: unicodestring;
        stack: TVSymbolStack;
        export_list: TVList;

        constructor Create;
        destructor Destroy; override;
    end;


procedure AddPackage(P: TPackage);
function FindPackage(name: unicodestring): TPackage;
procedure FreePackages;

procedure LoadPackage(fn: unicodestring; eval: TEvalProc);

function SearchPath(name: unicodestring): unicodestring;

procedure AddPath(dir: unicodestring);

//TODO: в пакете хранится его стэк целиком (хотя он не нужен)
//и перечень экспортируемых переменных отдельно
//при каждом подключении пакета происходит поиск в стеке всего списка экспорта
//нужно удалить стек, а в список экспорта сохранить указатели

implementation

var packages: array of TPackage;
var use_path: TStringList;

procedure AddPackage(P: TPackage);
var i: integer; emessage: unicodestring;
begin
    for i := 0 to high(packages) do
        if packages[i].uname = P.uname
        then begin
            emessage := 'package '+P.name+'redefinition';
            FreeAndNil(P);
            raise ELE.Create(emessage, 'syntax');
        end;
    SetLength(packages, Length(packages)+1);
    packages[high(packages)] := P;
end;

function FindPackage(name: unicodestring): TPackage;
var i: integer; uname: unicodestring;
begin
    uname := UnicodeUpperCase(name);
    for i := high(packages) downto 0 do begin
        if packages[i].uname = uname then begin
            result := packages[i];
            exit;
        end;
    end;
    result := nil;
end;

{ TPackage }

constructor TPackage.Create;
begin
    stack := nil;
    export_list := nil;
end;

destructor TPackage.Destroy;
begin
    stack.Free;
    export_list.Free;
    inherited Destroy;
end;

procedure FreePackages;
var i: integer;
begin
    for i := 0 to high(packages) do packages[i].Free;
    SetLength(packages, 0);
end;


procedure LoadPackage(fn: unicodestring; eval: TEvalProc);
var expr: TValue; f: TLFileStream; name: unicodestring;
begin try
    name := ChangeFileExt(ExtractFileName(fn), unicodestring(''));
    expr := nil;
    f := nil;
    f := TLFileStream.Create(fn, fmOpenRead, seBOM);
    expr := read(f);
    if vpExpression_PACKAGE(expr)
        and ((expr as TVList).uname[1]=UnicodeUpperCase(name))
    then eval(expr).Free
    else raise ELE.Create(fn+' is not package '+name,'package');
finally
    f.Free;
    expr.Free;
end end;


function SearchPath(name: unicodestring): unicodestring;
var oname,ename,rname: unicodestring; path: TStringList; i: integer;
    function td(dir: unicodestring; out fn: unicodestring): boolean;
    var dird: unicodestring;
    begin
        dird := IncludeTrailingPathDelimiter(dir);
        result := true;
        fn := dird+oname; if FileExists(fn) then Exit;
        fn := dird+rname; if FileExists(fn) then Exit;
        fn := dird+ename; if FileExists(fn) then Exit;
        fn := '';
        result := false;
    end;
begin
    oname := DirSep(name);
    ename := oname+'.lisya';
    rname := oname+'.лися';

    //поиск в папке стартового скрипта
    if (ParamCount>0) and td(ExtractFilePath(ExpandFileName(paramStr(1))), result) then Exit;
    if (ParamCount>0) and td(ExtractFilePath(ExpandFileName(paramStr(1)))+DirSep('/packages'), result) then Exit;

    //поиск по путям, заданным в программе через WITH/USE
    for i := 0 to use_path.Count-1 do if td(use_path[i], result) then Exit;

    // поиск в LISYA_PATH
    try
        path := TStringList.Create;
        path.Delimiter:=PathSeparator;
        path.DelimitedText:=GetEnvironmentVariable('LISYA_PATH');
        for i := 0 to path.Count-1 do if td(path[i],result) then Exit;
    finally
        path.Free;
    end;

    //поиск в папке с программой
    if td(ExtractFilePath(paramStr(0)),result) then Exit;
    if td(ExtractFilePath(paramStr(0))+'пакеты',result) then Exit;
    if td(ExtractFilePath(paramStr(0))+'packages',result) then Exit;

    result := '';
end;


procedure AddPath(dir: unicodestring);
begin
    use_path.Add(DirSep(dir));
end;


initialization
    use_path := TStringList.Create;


finalization
    FreePackages;
    use_path.Free;


end.

