unit lisya_tkz_solver_poc;

{$mode delphi}{$H+}

interface

uses
            Classes, SysUtils, ucomplex, mar, mar_math, mar_math_arrays;


type
    TGridArcType = (gaLine, gaGenerator, gaTransformer, gaBreaker);

    TFaultObject = (foNode, foLine);

    TArc = record
        n1, n2: integer;
        I: COMPLEX;
        Z: COMPLEX;
        case TGridArcType of
            gaGenerator: (E: COMPLEX);
            gaTransformer: (K: COMPLEX);
            gaLine: ();
            gaBreaker: (X: COMPLEX);
	end;
    TArcs = array of TArc;


    TArcDescription = record
        name: unicodestring;
        atype: TGridArcType;
        arc: integer;
        n1, n2: integer;
        Z: COMPLEX;
        case TGridArcType of
            gaGenerator: (E: COMPLEX);
            gaTransformer: (K: COMPLEX);
            gaLine: ();
            gaBreaker: (X: COMPLEX);
	end;
    TArcDescriptions = array of TArcDescription;


    TNode = record
        U: COMPLEX;
        I: COMPLEX;
        Y: COMPLEX;
	end;
    TNodes = array of TNode;


	{ TGridState }

    TGridState = record
        nodes: TNodes;
        arcs: TArcs;
        w_low, w_high: integer;
        g_low, g_high: integer;
        t_low, t_high: integer;
        q_low: integer;
        folds: array of record
                k: complex;
                saved, expelled: integer;
            end;
        procedure node_Y;
        procedure clear_fault();
        procedure node_fault(n: integer; R: real);
        procedure line_fault(n: integer; R, L: real);
        procedure cb_fold;
        procedure cb_unfold;
        procedure Solve;
	end;


    TMeasurements = array of record name: unicodestring; V1: COMPLEX; end;

    TMeasurementPoints = array of record mo: TFaultObject; n: integer; end;

    TRandomization = record arc: integer; low, high: real; end;

    TFaultPoint = record
        fo: TFaultObject;
        point: integer;
        distance: real;
        R_low, R_high: real;
        rnd_count: integer;
    end;


	{ TGrid }

    TGrid = class (TCountingObject)
    private
        node_names: array of unicodestring;
        arcs: array of TArcDescription;
        faults: array of record
                fo: TFaultObject;
                point: integer;
                pos: real;
                R: real;
            end;
        state: TGridState;
        randomizations: array of TRandomization;
        ready: boolean;
        measurement_points: TMeasurementPoints;
        function find_node(name: unicodestring): integer;
        function find_arc(name: unicodestring): integer;

        function new_state(mode: TStringArray): TGridState;
        procedure restore_state(var gs: TGridState);
        procedure randomize(var gs: TGridState);
        function measurements(): TMeasurements;
        function measurements_vector(): TComplexes;
    public
        function description: unicodestring; override;

        constructor Create;
        destructor Destroy; override;

        procedure SetMode(mode: TStringArray);
        procedure SetMeasurements(mp: TStringArray);
        procedure Fault(point: unicodestring; R: real; L: real;
            out M: TMeasurements; out MV: TComplexes);
	end;


	{ TGridBuilder }

    TGridBuilder = class
    private
        raw_nodes: array of unicodestring;
        raw_arcs: array [TGridArcType] of TArcDescriptions;
        raw_randomizations: array of record name: unicodestring; high, low: real; end;
        function node_n(name: unicodestring): integer;
    public
        constructor Create;

        procedure AddArc(_atype: TGridArcType; _name, _n1, _n2: unicodestring; _Z, _X: COMPLEX);
        procedure AddRandomization(_name: unicodestring; _low, _high: real);

        function Completed(): TGrid;
	end;


implementation

const insignificant_R = 999999;


type
    ETKZ = class(Exception) end;


{ TGridBuilder } ///////////////////////////////////////////////////////////////

function TGridBuilder.node_n(name: unicodestring): integer;
begin
    for result := 0 to high(raw_nodes) do if raw_nodes[result]=name then Exit;
    SetLength(raw_nodes, Length(raw_nodes)+1);
    raw_nodes[high(raw_nodes)] := name;
    result := high(raw_nodes);
end;


constructor TGridBuilder.Create;
var ga: TGridArcType;
begin
    SetLength(raw_nodes, 2);
    raw_nodes[0] := '0';
    raw_nodes[1] := '';
    raw_randomizations := nil;
    for ga in TGridArcType do raw_arcs[ga] := nil;
end;


procedure TGridBuilder.AddArc(_atype: TGridArcType; _name, _n1, _n2: unicodestring; _Z, _X: COMPLEX);
var n: integer;
begin
    n := Length(raw_arcs[_atype]);
    SetLength(raw_arcs[_atype], n+1);
    with raw_arcs[_atype][n] do
        begin
            name := _name;
            arc := -1;
            atype := _atype;
            n1 := node_n(_n1);
            n2 := node_n(_n2);
            Z := _Z;
            X := _X;
		end;
end;


procedure TGridBuilder.AddRandomization(_name: unicodestring; _low, _high: real);
begin
    SetLength(raw_randomizations, Length(raw_randomizations)+1);
    with raw_randomizations[High(raw_randomizations)] do
        begin
            name := _name;
            low := _low;
            high := _high;
		end;
end;


function TGridBuilder.Completed: TGrid;
var a_cnt, r: integer; ga: TGridArcType;

    procedure add_arcs(var raw: TArcDescriptions);
    var a: integer;
    begin
        for a := 0 to high(raw) do
            begin
                result.arcs[a_cnt] := raw[a];
                Inc(a_cnt);
			end;
	end;

begin
    result := TGrid.Create;

    with result do
        begin
            node_names := raw_nodes;

            SetLength(arcs, Length(raw_arcs[gaLine]) +
                            Length(raw_arcs[gaGenerator]) +
                            Length(raw_arcs[gaTransformer]) +
                            Length(raw_arcs[gaBreaker]));

            a_cnt := 0;
            for ga in TGridArcType do add_arcs(raw_arcs[ga]);

            SetLength(randomizations, Length(raw_randomizations));
            for r := 0 to high(raw_randomizations) do
                begin
                    randomizations[r].arc := find_arc(raw_randomizations[r].name);
                    randomizations[r].low := raw_randomizations[r].low;
                    randomizations[r].high := raw_randomizations[r].high;
        		end;

            state := new_state(nil);
	    end;

    Destroy;
end;



{ TGrid } //////////////////////////////////////////////////////////////////////

function TGrid.find_node(name: unicodestring): integer;
begin
    for result := 0 to high(node_names) do if node_names[result]=name then Exit;
    result := -1;
end;


function TGrid.find_arc(name: unicodestring): integer;
begin
    for result := 0 to high(arcs) do if arcs[result].name=name then Exit;
    raise ETKZ.Create('Arc not found ('+name+')');
end;


function TGrid.new_state(mode: TStringArray): TGridState;
var a, n, a_cnt: integer; cnt: array [TGridArcType] of integer; ga: TGridArcType;

    function off(name: unicodestring): boolean;
    var i: integer;
    begin
        for i := 0 to high(mode) do if mode[i] = name then Exit(true);
        result := false;
	end;

begin
    SetLength(result.nodes, Length(node_names));
    for n := 0 to high(result.nodes) do
        with result.nodes[n] do
            begin
                U := 0;
                I := 0;
			end;

    a_cnt := 2; //ветвь 0 - переходное сопротивление; ветвь 1 - для пром.КЗ
    cnt[gaLine]        := 1;
    cnt[gaGenerator]   := 0;
    cnt[gaTransformer] := 0;
    cnt[gaBreaker]     := 0;

    for a := 0 to high(arcs) do
        if off(arcs[a].name)
        then
            arcs[a].arc := -1
        else
            begin
                arcs[a].arc := a_cnt;
                Inc(a_cnt);
                Inc(cnt[arcs[a].atype]);
            end;

    with result do
        begin
            SetLength(arcs, a_cnt);
            for a := 0 to high(arcs) do arcs[a].I := 0;

            w_low :=  0      + 1;
            w_high := 0      + cnt[gaLine];
            g_low :=  w_high + 1;
            g_high := w_high + cnt[gaGenerator];
            t_low :=  g_high + 1;
            t_high := g_high + cnt[gaTransformer];
            q_low :=  t_high + 1;
		end;

    restore_state(result);
    ready := true;
end;


procedure TGrid.restore_state(var gs: TGridState);
var a: integer;
begin

    //ветви КЗ
    gs.arcs[0].n1 := 0;
    gs.arcs[0].n2 := 0;
    gs.arcs[0].Z := insignificant_R;
    gs.arcs[1].n1 := 0;
    gs.arcs[1].n2 := 0;
    gs.arcs[1].Z := insignificant_R;

    for a := 0 to high(arcs) do
        if arcs[a].arc>=0
        then
            begin
                gs.arcs[arcs[a].arc].n1 := arcs[a].n1;
                gs.arcs[arcs[a].arc].n2 := arcs[a].n2;
                gs.arcs[arcs[a].arc].Z := arcs[a].Z;
                gs.arcs[arcs[a].arc].X := arcs[a].X;
			end;

    gs.node_Y;
end;


procedure TGrid.randomize(var gs: TGridState);
var r: integer; Y: COMPLEX;
begin

    for r := 0 to high(randomizations) do
        with randomizations[r] do
            if arcs[arc].arc>=0
            then
                case arcs[arc].atype of
                    gaGenerator: gs.arcs[arcs[arc].arc].E :=
                        arcs[arc].E * (low + system.Random*(high - low));
                    gaLine: begin
                        Y := cinv(arcs[arc].Z);
                        Y := Y * (low + system.Random*(high - low));
                        gs.arcs[arcs[arc].arc].Z := cinv(Y);
					end;
			end;

    gs.node_Y;
end;


function TGrid.description: unicodestring;
begin
    result := 'GRID ' + IntToStr(Length(node_names));
end;


constructor TGrid.Create;
begin
    inherited Create;
    arcs := nil;
    randomizations := nil;
    measurement_points := nil;
    SetLength(node_names, 1);
    node_names[0] := '0';
    ready := false;
end;


destructor TGrid.Destroy;
begin
    inherited Destroy;
end;


procedure TGrid.SetMode(mode: TStringArray);
begin
    state := new_state(mode);
end;


procedure TGrid.SetMeasurements(mp: TStringArray);
var i, f: integer;
begin
    SetLength(measurement_points, Length(mp));
    for i := 0 to high(mp) do
        begin
            f := find_node(mp[i]);
            if f>=0
            then
                begin
                    measurement_points[i].mo := foNode;
                    measurement_points[i].n := f
				end
			else
                begin
                    measurement_points[i].mo := foLine;
                    measurement_points[i].n := find_arc(mp[i]);
                end
        end;
end;


procedure TGrid.Fault(point: unicodestring; R: real; L: real;
    out M: TMeasurements; out MV: TComplexes);
var f: integer;
begin
    if not ready then state := new_state(nil);

    SetLength(faults, Length(faults)+1);
    f := high(faults);
    faults[f].pos := L;
    faults[f].R := R;
    faults[f].point := find_node(point);
    faults[f].fo := foNode;
    if faults[f].point<0
    then
        begin
            faults[f].point := arcs[find_arc(point)].arc;
            faults[f].fo := foLine;
		end;

    if faults[f].point>=0
    then
        case faults[f].fo of
            foNode: state.node_fault(faults[f].point, R);
            foLine: state.line_fault(faults[f].point, R, L);
	    end
    else
        state.clear_fault();

    randomize(state);

    state.Solve;

    MV := measurements_vector();
    if measurement_points=nil then M := measurements else M := nil;

    restore_state(state);
end;


function TGrid.measurements: TMeasurements;
var n, a, p: integer;
    procedure push(name: unicodestring; V1: complex);
    begin
        result[p].name := name;
        result[p].V1 := V1;
        inc(p);
	end;

begin
    SetLength(result, Length(state.nodes)-2 + Length(state.arcs)-2);
    p := 0;

    for n := 2 to high(state.nodes) do push(node_names[n], state.nodes[n].U);

    for a := 0 to high(arcs) do
        with arcs[a] do
            if arc>=0 then push(name, state.arcs[arc].I);
end;


function TGrid.measurements_vector: TComplexes;
var mp: integer;
begin
    SetLength(result, Length(measurement_points));
    for mp := 0 to high(measurement_points) do
        with measurement_points[mp] do
            case mo of
                foNode: result[mp] := state.nodes[n].U;
                foLine: if arcs[n].arc>=0
                        then result[mp] := state.arcs[arcs[n].arc].I
                        else result[mp] := 0;
			end;
end;



{ TGridState }

procedure TGridState.node_Y;
var n, a: integer;
begin
    for n := 0 to high(nodes) do
        nodes[n].Y := 1/insignificant_R;

    for a := 0 to g_high do
        with arcs[a] do
            begin
                nodes[n1].Y := nodes[n1].Y + cinv(Z);
                nodes[n2].Y := nodes[n2].Y + cinv(Z);
	    	end;

    for a := t_low to t_high do
        with arcs[a] do
            begin
                nodes[n1].Y := nodes[n1].Y + cinv(Z);
                nodes[n2].Y := nodes[n2].Y + cinv(Z)*cmod(K*K);
                //TODO: проверить корректность комплексного коэффициента трансформации
	    	end;
end;


procedure TGridState.clear_fault();
begin
    arcs[0].n1 := 0;
    arcs[0].n2 := 0;
    arcs[0].Z := insignificant_R;
    arcs[1].n1 := 0;
    arcs[1].n2 := 0;
    arcs[1].Z := insignificant_R;
end;


procedure TGridState.node_fault(n: integer; R: real);
var a: integer;
begin
    clear_fault;

    arcs[0].n1 := 0;
    arcs[0].n2 := n;

    if R>0.001
    then arcs[0].Z := R
    else arcs[0].Z := 0.001;
end;


procedure TGridState.line_fault(n: integer; R, L: real);
var Zl: COMPLEX;
begin
    if (L<0.01) or (L>0.99) then raise ETKZ.Create('out of line '+FloatToStr(L));

    clear_fault;


    Zl := arcs[n].Z;
    arcs[0].n1 := 1;
    arcs[0].n2 := 0;
    arcs[1].n1 := arcs[n].n2;
    arcs[1].n2 := 1;
    arcs[1].Z := Zl * (1-L);
    arcs[n].Z := Zl * L;
    arcs[n].n2 := 1; //узел КЗ


    if R>0.001
    then arcs[0].Z := R
    else arcs[0].Z := 0.001;
end;


procedure TGridState.cb_fold;
var a, q, expelled, saved: integer;

    procedure add(s, e: integer; k: complex);
    var f: integer;
    begin
        f := Length(folds);
        SetLength(folds, f+1);
        folds[f].saved := s;    //сохраняемый узел/ветвь
        folds[f].expelled := e; //сворачиваемый узел/ветвь
        folds[f].k := k;        //коэффициент учёта тока (полярность и Кт)
	end;

    procedure swap_ends(var a: TArc);
    var tmp: integer;
    begin
        tmp := a.n1;
        a.n1 := a.n2;
        a.n2 := tmp;
    end;

begin
    for q := q_low to high(arcs) do
        begin
            if arcs[q].n2 = 0 then swap_ends(arcs[q]); //костыль избегает исключения нулевого узла из схемы
            expelled := arcs[q].n2;
            saved := arcs[q].n1;
            arcs[q].n2 := saved;
            add(saved, expelled, 0);
            for a := 0 to high(arcs) do
                begin
                    if arcs[a].n1 = expelled then
                        begin
                            arcs[a].n1 := saved;
                            add(a, q, 1);
						end;
                    if arcs[a].n2 = expelled then
                        begin
                            arcs[a].n2 := saved;
                            if (a>=t_low) and (a<=t_high)
                            then add(a, q, -1 * cong(arcs[a].K))
                            else add(a, q, -1)
						end;
				end;
		end;
end;


procedure TGridState.cb_unfold;
var f, q: integer;
begin
    for q := q_low to high(arcs) do arcs[q].I := 0;

    for f := high(folds) downto 0 do
        with folds[f] do
            if k=0
            then nodes[expelled].U := nodes[saved].U
            else arcs[expelled].I := arcs[expelled].I + arcs[saved].I * k;

    folds := nil;
end;


procedure TGridState.Solve;
var a, n: integer; imbalance: real;
begin

    cb_fold;

    node_Y;

    repeat

        for a := 0 to 0 do //КЗ
            with arcs[a] do
                begin
                    //I := (nodes[n1].U - nodes[n2].U) / Z;
                    //nodes[n1].I := nodes[n1].I - I;
                    //nodes[n2].I := nodes[n2].I + I;
		    	end;

        for a := w_low to w_high do
            with arcs[a] do
                begin
                    I := (nodes[n1].U - nodes[n2].U) / Z;
                    nodes[n1].I := nodes[n1].I - I;
                    nodes[n2].I := nodes[n2].I + I;
    	    	end;

        for a := g_low to g_high do
            with arcs[a] do
                begin
                    I := (nodes[n1].U - nodes[n2].U + E) / Z;
                    nodes[n1].I := nodes[n1].I - I;
                    nodes[n2].I := nodes[n2].I + I;
		    	end;

        for a := t_low to t_high do
            with arcs[a] do
                begin
                    I := (nodes[n1].U - nodes[n2].U * K) / Z;
                    nodes[n1].I := nodes[n1].I - I;
                    nodes[n2].I := nodes[n2].I + I*cong(K);
		    	end;


        with arcs[0] do
            begin
                I := (nodes[n1].U - nodes[n2].U) / Z;
                nodes[n1].I := nodes[n1].I - I;
                nodes[n2].I := nodes[n2].I + I;
			end;

        imbalance := 0;

        for n := 1 to high(nodes) do
            with nodes[n] do
                begin
                    I := I - U / insignificant_R;
                    imbalance := imbalance + cmod(I);
                    U := U + I/Y;
                    I := 0;
    		    end;

    //WriteLn('Uf=', round(cmod(Uf)), '  ', 'dU=', round(dU), '  ', 'If=', round(cmod(arcs[0].I)));

    until imbalance<1;

    cb_unfold;

end;


end.

