unit lisya_tkz_solver_opencl;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils, cl, math, tkz_types, mar_opencl, ucomplex, mar
    {$IFDEF LINUX}
        , linux, unixtype
    {$ENDIF}
    ;



type

	{ TSolver }

    TSolver = class
    protected
        grid: TGridDescription;
        fictitious_nodes_map: array of integer;
        measurement_points: TMeasurementPoints;
        fault_index: TFaultPoints;
        measurements: complexes2;
        function determine_fictitious_nodes(): TIntegers;
        procedure create_fault_points();
        procedure store_measurements();
        function extract_measurements(): complexes; virtual; abstract;
    public
        constructor Create(const _grid: TGridDescription);
        destructor Destroy; override;

        procedure CalcTask(); virtual; abstract;
        procedure WaitResult(var I: complexes; var U: complexes); virtual; abstract; overload;
        procedure WaitResult(); virtual; abstract; overload;
        function GetMeasurements(): complexes2;
    end;


	{ TSNArc }

    TSNArc = record
        Yl: complex;
        Yc: complex;
        E: complex;
        K: complex;
        K2: real;
        I: array[0..1] of complex;
        node1, node2: integer;
        kind: TArcKind;
        arc_id: integer;
        class operator Implicit(a: TArc): TSNArc;
	end;
    TSNArcs = array of TSNArc;

	{ TSNNode }

    TSNNode = record
        U: complex;
        I: complex;
        cI: complex;
        Y: complex;
        cY: complex;
        procedure Init();
        procedure Clear();
        class operator Implicit(n: TNode): TSNNode;
	end;
    TSNNodes = array of TSNNode;


	{ TSolverNative }

    TSolverNative = class (TSolver)
    protected
        nodes: TSNNodes;
        arcs: TSNArcs;
        breakers: TSNArcs;
        U: complexes;
        fictitious_nodes_map: TIntegers;
        fault_node, calc_fault_node: integer;
        arc_index: TArcIndex;
        procedure arcs_currents();
        procedure breakers_currents();
        function node_voltages(): boolean;
        procedure fictitious_nodes_constant_conductivities();
        procedure nodes_constant_conductivities_and_currents();
        procedure nodes_clear();
        procedure restore_voltages();
        procedure print_nodes();
        function extract_measurements(): complexes; override;
    public
        constructor Create(const _grid: TGridDescription);
        destructor Destroy; override;

        procedure CalcTask; override;
        procedure Fault(_n: integer);
        procedure AllFaults();
	end;


    TOCLProperties = record
        build_log: string;
        work_group_info: TKernelWorkGroupInfo;
        compute_units: integer;
        local_mem_size: cardinal;
    end;


    { TOCLNode }

    TOCLNode = packed record
        U: scomplex;
        I: scomplex;
        Y: scomplex;
        procedure Init();
    end;
    TOCLNodes = array of TOCLNode;

	{ TOCLArc }

    TOCLArc = packed record
        Yl: scomplex;
        Yc: scomplex;
        K: scomplex;
        E: scomplex;
        I: scomplex;
        I_: array[0..1] of scomplex;
        node1, node2: cl_int;
        class operator Implicit(a: TArc): TOCLArc;
    end;
    TOCLArcs = array of TOCLArc;




    { TSolverOpenCL }

    TSolverOpenCL = class (TSolver)
    private
        compute_properties: TOCLProperties;
        context: cl_context;
        device: cl_device_id;
        queue: cl_command_queue;
        program_: cl_program;
        kernel: cl_kernel;
        kernel_event: cl_event;
        grid_buffer: cl_mem;
        U_buffer: cl_mem;
        I_buffer: cl_mem;
        nodes_buffer: cl_mem;
        arcs_buffer: cl_mem;
        arcs: TOCLArcs;
        nodes: TOCLNodes;
        procedure build;
    public
        constructor Create(const _grid: TGridDescription; pu: string);
        destructor Destroy; override;
        procedure ReleaseProgram();


        procedure PrintKernelInfo();
        procedure Run();
        procedure WaitResult(var I: TArcs; var U: scomplexes);
        procedure WaitResult();
        function GetKernelTime(): cardinal;
    end;

implementation

const KERNEL_SOURCE = 'src/tkz3.cl';

const BREAKER_Y = 1;


function timer: QWord;
{$IFDEF LINUX}
var tp: timespec;
begin
  clock_gettime(CLOCK_MONOTONIC, @tp);
  Result := (Int64(tp.tv_sec) * 1000000) + (tp.tv_nsec div 1000);
end;
{$ELSE}
begin
    result := GetTIckCount64*1000;
end;
{$ENDIF}



{ TOCLArc }

class operator TOCLArc.Implicit(a: TArc): TOCLArc;
begin
    result.Yl := a.Yl;
    result.Yc := a.Yc;
    result.K := a.K;
    result.E := a.E;
    result.I := 0;
    result.I_[0] := a.I[0];
    result.I_[1] := a.I[1];
    result.node1 := a.node1;
    result.node2 := a.node2;
end;


{ TOCLNode }

procedure TOCLNode.Init;
begin
    U := 0;
    I := 0;
    Y := 0;
end;





{ TSNNode }

procedure TSNNode.Init;
begin
    U := 0;
    I := 0;
    cI := 0;
    Y := 0;
    cY := 0;
end;

procedure TSNNode.Clear;
begin
    I := 0;
    cI := 0;
    Y := 0;
    cY := 0;
end;


class operator TSNNode.Implicit(n: TNode): TSNNode;
begin
    result.Clear();
    result.U := n.U;
end;


{ TSNArc }

class operator TSNArc.Implicit(a: TArc): TSNArc;
begin
    result.Yl := a.Yl;
    result.Yc := a.Yc;
    result.E := a.E;
    result.K := a.K;
    result.K2 := power(cmod(a.K), 2);
    result.node1 := a.node1;
    result.node2 := a.node2;
    result.I[0] := 0;
    result.I[1] := 0;
    result.kind := a.kind;
end;


{ TSolverNative }  /////////////////////////////////////////////////////////////

constructor TSolverNative.Create(const _grid: TGridDescription);
var n, a, e, b, mp: integer;
begin
    inherited Create(_grid);

    SetLength(nodes, Length(grid.nodes));

    for n := 0 to high(nodes) do
        nodes[n] := grid.nodes[n];

    measurement_points := copy(grid.measurement_points);

    SetLength(arc_index, Length(grid.arcs));

	SetLength(arcs, Length(grid.arcs));
    SetLength(breakers, Length(grid.arcs));

    e := 0;
    b := 0;
    for a := 0 to high(grid.arcs) do
        case grid.arcs[a].kind of
            akBreaker: begin
                breakers[b] := grid.arcs[a];
                breakers[b].arc_id := a;
                arc_index[a].target := akBreaker;
                arc_index[a].id := b;
                Inc(b);
		    end;
    		else begin
                arcs[e] := grid.arcs[a];
                arcs[e].arc_id := a;
                arc_index[a].target := akUniversal;
                arc_index[a].id := e;
                Inc(e);
            end;
    	end;

	SetLength(arcs, e);
    SetLength(breakers, b);

    fault_node := 0;
end;


destructor TSolverNative.Destroy;
begin
    SetLength(nodes, 0);
    SetLength(arcs, 0);
    SetLength(breakers, 0);
    SetLength(U, 0);
    SetLength(arc_index, 0);
    inherited Destroy;
end;


procedure TSolverNative.arcs_currents;
var a, n1, n2: integer; u1, u2, u2_, du, _I: complex;
begin
    {$RANGECHECKS OFF}
    for a := 0 to high(arcs) do
        with arcs[a] do
            begin
                n1 := fictitious_nodes_map[node1];
                n2 := fictitious_nodes_map[node2];
                u1 := nodes[n1].U;
                u2 := nodes[n2].U;
                u2_ := u2 * K - E;
                du := u1 - u2_;
                _I := du * Yl;
                I[0] := _I + u1*0.5*Yc;
                I[1] := K*(- _I + u2*0.5*Yc);

                nodes[n1].I := nodes[n1].I - I[0];
                nodes[n2].I := nodes[n2].I - I[1];
              end;
    {$RANGECHECKS ON}
end;


procedure TSolverNative.breakers_currents;
var a: integer; _I: complex;
begin
    {$RANGECHECKS OFF}
    for a := 0 to high(breakers) do
        with breakers[a] do
            begin
                _I := (nodes[node1].U - nodes[node2].U) * BREAKER_Y;
                I[0] :=   _I;
                I[1] := - _I;

                nodes[node1].I := nodes[node1].I - I[0];
                nodes[node2].I := nodes[node2].I - I[1];
              end;
    {$RANGECHECKS ON}
end;


function TSolverNative.node_voltages(): boolean;
var n: integer;
begin
    {$RANGECHECKS OFF}
    result := true;
    nodes[calc_fault_node].U := 0;
    nodes[calc_fault_node].I := 0;
    for n := 1 to high(nodes) do
        with nodes[n] do
            begin
                if cY <> 0 then U := U + cdiv_s(I+cI, Y+cY);
                result := result and (abs(I.re+cI.re)<1) and (abs(I.im+cI.im)<1);
                I := 0;
                Y := 0;
            end;

    {$RANGECHECKS ON}
end;


procedure TSolverNative.fictitious_nodes_constant_conductivities;
var a, n1, n2: integer; Y: complex;
begin
    nodes_clear;
    for a := 0 to high(arcs) do
        with arcs[a] do
            begin
                Y := Yl + 0.5*Yc;
                n1 := fictitious_nodes_map[node1];
                n2 := fictitious_nodes_map[node2];

                nodes[n1].cY := nodes[n1].cY + Y;
                nodes[n2].cY := nodes[n2].cY + K2*Y;
            end;
end;


procedure TSolverNative.nodes_constant_conductivities_and_currents;
var a : integer; Y: complex;
begin
    nodes_clear;
    for a := 0 to high(arcs) do
        with arcs[a] do
            begin
                Y := Yl + 0.5*Yc;

                nodes[node1].cY := nodes[node1].cY + Y;
                nodes[node1].cI := nodes[node1].cI - I[0];
                nodes[node2].cY := nodes[node2].cY + K2*Y;
                nodes[node2].cI := nodes[node2].cI - I[1];
            end;
    for a := 0 to high(breakers) do
        with breakers[a] do
            begin
                nodes[node1].cY := nodes[node1].cY + BREAKER_Y;
                nodes[node2].cY := nodes[node2].cY + BREAKER_Y;
            end;
    nodes[fault_node].cI := 0;
end;


procedure TSolverNative.nodes_clear;
var n: integer;
begin
    for n := 0 to high(nodes) do nodes[n].Clear;
end;


procedure TSolverNative.restore_voltages;
var n: integer;
begin
    SetLength(U, Length(nodes));
    for n := 0 to high(nodes) do
        begin
            nodes[n].U := nodes[fictitious_nodes_map[n]].U;
            U[n] := nodes[n].U;
		end;
end;


procedure TSolverNative.CalcTask;
var i: integer;
begin
    fictitious_nodes_map := determine_fictitious_nodes();

    fictitious_nodes_constant_conductivities();
    calc_fault_node := fictitious_nodes_map[fault_node];

    for i := 0 to 1000000 do
        begin
            arcs_currents();
            if node_voltages()
            then
                begin
                    WriteLn('Циклов ', i);
                    break;
                end;
        end;
    restore_voltages();

    nodes_constant_conductivities_and_currents();
    calc_fault_node := fault_node;

    for i := 0 to 1000000 do
        begin
            breakers_currents();
            if node_voltages()
            then
                begin
                    WriteLn('Циклов ', i);
                    break;
				end;

		end;
end;


procedure TSolverNative.Fault(_n: integer);
var t1, t2: qword;
begin
    t1 := timer();
    fault_node := _n;
    CalcTask();
    store_measurements();
    t2 := timer();
    WriteLn('Повреждение ', t2-t1, ' мкс');
end;


procedure TSolverNative.AllFaults;
var t1, t2: qword; fp: integer;
begin
    t1 := timer();
    for fp := 0 to high(fault_index) do
        begin
            fault_node := fault_index[fp].id;
            CalcTask();
            store_measurements();
		end;
    t2 := timer();
    WriteLn('Все повреждения ', t2-t1, ' мкс');
end;


procedure TSolverNative.print_nodes;
var n: integer;
begin
    WriteLn('--- nodes ------------------------------------------');
    WriteLn('n    U    I    cI    Y    cY    fict');
    for n := 0 to high(nodes) do
        with nodes[n] do
            WriteLn(format('%2d    %10.0f    %6f    %6f    %6f    %6f',
                [n, cmod(U), cmod(I), cmod(cI), cmod(Y), cmod(cY)]));
end;


function TSolverNative.extract_measurements: complexes;
var mp: integer;
begin
    SetLength(result, Length(measurement_points));
    for mp := 0 to high(measurement_points) do
        with measurement_points[mp] do
            case source of
                Node: result[mp] := U[index];
                Arc: with arc_index[index] do
                    case target of
                        akUniversal: result[mp] := arcs[id].I[0];
                        akBreaker: result[mp] := breakers[id].I[0];
				end;
			end;

end;



{ TSolver } ////////////////////////////////////////////////////////////////////

function TSolver.determine_fictitious_nodes(): TIntegers;
    procedure link(a, b: integer);
    var n: integer;
    begin
        for n := 0 to high(result) do
            if result[n] = a then result[n] := b;
	end;
var a, n: integer;
begin
    SetLength(result, Length(grid.nodes));
    for n := 0 to high(result) do result[n] := n;

    for a := 0 to high(grid.arcs) do
        with grid.arcs[a] do
            if kind = akBreaker
            then
                link(result[node2], result[node1]);
end;


procedure TSolver.create_fault_points;
var fp: integer;
begin
    SetLength(fault_index, Length(grid.nodes));
    for fp := 0 to high(fault_index) do
        with fault_index[fp] do
            begin
                zone := -1;
                R := 0;
                target := Node;
                id := fp;
			end;
end;


procedure TSolver.store_measurements;
begin
    SetLength(measurements, Length(measurements)+1);
    measurements[high(measurements)] := extract_measurements();
end;


constructor TSolver.Create(const _grid: TGridDescription);
begin
    grid := _grid;
    create_fault_points;
    measurements := nil;
end;


destructor TSolver.Destroy;
begin
    SetLength(grid.arcs, 0);
    SetLength(grid.nodes, 0);
    SetLength(grid.measurement_points, 0);
    SetLength(fault_index, 0);
    inherited Destroy;
end;


function TSolver.GetMeasurements: complexes2;
begin
    result := measurements;
end;


{ TSolverOpenCL } //////////////////////////////////////////////////////////////

constructor TSolverOpenCL.Create(const _grid: TGridDescription; pu: string);
var i: integer;
begin
    inherited Create(_grid);

    SetLength(nodes, Length(grid.nodes));
    for i := 0 to high(nodes) do
        nodes[i].Init;
    SetLength(arcs, Length(grid.arcs));
    for i := 0 to high(arcs) do
        arcs[i] := grid.arcs[i];

    device := mclDevice(pu);
    context := mclCreateContext(device);
    queue := mclCreateCommandQueue(context, device);
    program_ := nil;
    kernel := nil;
    kernel_event := nil;
    build();

    with compute_properties do
    begin
        compute_units := mclGetDeviceInfo_UINT(device, CL_DEVICE_MAX_COMPUTE_UNITS);
        local_mem_size := mclGetDeviceInfo_ULONG(device, CL_DEVICE_LOCAL_MEM_SIZE);
    end;

    nodes_buffer := mclCreateBuffer(context,
                                    CL_MEM_READ_WRITE or CL_MEM_USE_HOST_PTR,
                                    SizeOf(TOCLNode)*Length(nodes),
                                    @nodes[0]);
    arcs_buffer := mclCreateBuffer(context,
                                   CL_MEM_READ_WRITE or CL_MEM_USE_HOST_PTR,
                                   SizeOf(TOCLArc)*Length(arcs),
                                   @arcs[0]);
end;


destructor TSolverOpenCL.Destroy;
begin
    mclReleaseMemObject(arcs_buffer);
    mclReleaseMemObject(nodes_buffer);
    ReleaseProgram();
    mclReleaseCommandQueue(queue);
    mclReleaseContext(context);
    inherited Destroy;
end;


procedure TSolverOpenCL.ReleaseProgram;
begin
    if kernel <> nil then mclReleaseKernel(kernel);
    if program_ <> nil then mclReleaseProgram(program_);
end;


procedure TSolverOpenCL.build;
begin
    ReleaseProgram();
    program_ := mclCreateProgramWithSource(context,
                    file_text(ExtractFilePath(paramstr(0)) + KERNEL_SOURCE));
    mclBuildProgram(program_, device,
        format('-DMAX_NODES=%d  -DMAX_ARCS=%d -DWORK_GROUP_SIZE=%d',
                 [Length(nodes),  Length(arcs),  1]));
    kernel := mclCreateKernel(program_, 'tkz');
    with compute_properties do
    begin
        build_log := mclGetProgramBuildLog(program_, device);
        work_group_info := mclGetKernelWorkGroupInfo(kernel, device);
    end;
end;


procedure TSolverOpenCL.PrintKernelInfo;
begin
    with compute_properties.work_group_info do
    begin
        WriteLn('Kernel ', mclGetKernelInfo_STRING(kernel, CL_KERNEL_FUNCTION_NAME));
        WriteLn('    WORK_GROUP_SIZE = ', WORK_GROUP_SIZE);
        WriteLn('    PREFERRED_WORK_GROUP_SIZE_MULTIPLE = ', PREFERRED_WORK_GROUP_SIZE_MULTIPLE);
        WriteLn('    COMPILE_WORK_GROUP_SIZE = ', COMPILE_WORK_GROUP_SIZE[0], ' : ', COMPILE_WORK_GROUP_SIZE[1], ' : ', COMPILE_WORK_GROUP_SIZE[2]);
        WriteLn('    LOCAL_MEM_SIZE = ', LOCAL_MEM_SIZE);
        WriteLn('    PRIVATE_MEM_SIZE = ', PRIVATE_MEM_SIZE);
    end;
end;


procedure TSolverOpenCL.Run();
var nodesNumber, arcsNumber, tasksNumber, taskWidth: cl_int;
begin
    nodesNumber := Length(nodes);
    arcsNumber := Length(arcs);
    taskWidth := max(nodesNumber, arcsNumber);
    tasksNumber := 1;

    mclSetKernelArg(kernel, 0, SizeOf(nodesNumber), @nodesNumber);
    mclSetKernelArg(kernel, 1, SizeOf( arcsNumber), @arcsNumber);
    mclSetKernelArg(kernel, 2, SizeOf(tasksNumber), @tasksNumber);
    mclSetKernelArg(kernel, 3, SizeOf(nodes_buffer), @nodes_buffer);
    mclSetKernelArg(kernel, 4, SizeOf( arcs_buffer), @arcs_buffer);

    mclEnqueueWriteBuffer(queue, nodes_buffer, SizeOf(TOCLNode)*nodesNumber, @nodes[0]);
    mclEnqueueWriteBuffer(queue,  arcs_buffer, SizeOf(TOCLArc )* arcsNumber, @arcs[0]);

    mclEnqueueKernel(queue, kernel, 2, [1, taskWidth], [1, taskWidth], @kernel_event);

    mclEnqueueReadBuffer(queue, nodes_buffer, SizeOf(TOCLNode)*nodesNumber, @nodes[0]);
    mclEnqueueReadBuffer(queue,  arcs_buffer, SizeOf(TOCLArc )* arcsNumber, @arcs[0]);
end;


procedure TSolverOpenCL.WaitResult(var I: TArcs; var U: scomplexes);
begin
    mclFinish(queue);
end;


procedure TSolverOpenCL.WaitResult;
begin
    mclFinish(queue);
end;



function TSolverOpenCL.GetKernelTime: cardinal;
var t: TEventProfilingInfo;
begin
    if kernel_event <> nil
    then
        begin
            mclWaitForEvent(kernel_event);
            t := mclGetEventProfilingInfo(kernel_event);
            result := (t._END - t._START) div 1000000;
        end
    else
        result := 0;
end;


end.

