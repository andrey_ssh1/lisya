﻿unit TKZ;

{$mode DELPHI}

interface

uses
    Classes, SysUtils, ucomplex, TKZ_types, cl, mar_opencl,
    lisya_tkz_solver_opencl;


type

    TArcDescription = record
        name, node1, node2: unicodestring;
        Z,Y,K,E: complex;
    end;


    { TGrid }

    TGrid = class
    private
        nodes: TNodes;
        arcs: TArcs;
        measurement_points: TMeasurementPoints;
        protections: TProtections;
        faults: TFaultPoints;
        function FindNode(name: unicodestring): integer;
        function NodeByName(name: unicodestring): integer;
        function FindArc(name: unicodestring): integer;
        procedure AssertNewName(name: unicodestring);
        procedure append_measurement_points(_source: TNodeOrArc; _index: integer);
        function grid_description(): TGridDescription;
        function protection_index(_name: unicodestring): integer;
        procedure create_faults_list();
    public
        measurements: complexes2;

        constructor Create;
        destructor Destroy; override;

        function AddNode(_name: unicodestring): integer;
        function AddArc(_name, _node1, _node2: unicodestring; a: TArcDescription): integer;
        function AddBreaker(_name, _node1, _node2: unicodestring): integer;
        function AddMeasurementPoint(_name: unicodestring): integer;
        function AddProtection(_name, _U_mp, _I_mp: unicodestring): integer;
        function GetProtectionMeasurements(_name: unicodestring): TMeasurements;
        procedure Calc(iter: integer = -1);
        procedure Fault(_name: unicodestring);
        procedure AllFaults();

        procedure print();
        procedure printGraph();
    end;


    TArrayOfArrayOfString = array of array of unicodestring;

    function opencl_platforms(): TArrayOfArrayOfString;

    function set_opencl_processor(p: unicodestring): unicodestring;

var net: TGrid;

implementation


//// OpenCL ////////////////////////////////////////////////////////////////////

var opencl_processor: unicodestring = '';

const WORK_GROUP_SIZE = 2;

const TASKS_NUMBER = 32;


function opencl_platforms: TArrayOfArrayOfString;
var pl: cl_platform_id_array; dev: cl_device_id_array; i,j: integer;
begin
    pl := mclPlatforms();
    SetLength(result, Length(pl));
    for i := 0 to high(pl) do begin
        dev := mclDevices(pl[i]);
        SetLength(result[i], 1 + Length(dev));
        result[i][0] := mclGetPlatformInfo_STRING(pl[i], CL_PLATFORM_NAME);
        for j := 0 to high(dev) do
            result[i][j+1] := mclGetDeviceInfo_STRING(dev[j], CL_DEVICE_NAME)
    end;
end;


function set_opencl_processor(p: unicodestring): unicodestring;
var i, j: integer; pl: TArrayOfArrayOfString;
begin
    opencl_processor := '';
    result := '';
    pl := opencl_platforms();
    for i := 0 to high(pl) do
        for j := 1 to high(pl[i]) do
            if Pos(p, pl[i][j])>0 then begin
                result := pl[i][j];
                opencl_processor := result;
            end;


end;


//// complex ///////////////////////////////////////////////////////////////////

function complexToExpStr(c: complex): unicodestring;
begin
    result := FloatToStrF(cmod(c), ffFixed, 0, 0) + '∠' + IntToStr(round(180*carg(c)/pi)) + '°';
end;


function ComplexToStr(C: COMPLEX): unicodestring;
begin
    result := FloatToStr(C.re);
    if C.im<0      then result := result+'-i'+FloatToStr(-C.im)
    else if C.im>0 then result := result+'+i'+FloatToStr(+C.im);
end;



{ TGrid }

constructor TGrid.Create;
begin
    SetLength(nodes, 1);
    nodes[0].Init('земля');
    arcs := nil;
    measurement_points := nil;
    measurements := nil;
end;


destructor TGrid.Destroy;
var i: integer;
begin
    inherited Destroy;
    nodes := nil;
    arcs := nil;
    measurement_points := nil;
    measurements := nil;
end;


function TGrid.FindNode(name: unicodestring): integer;
var i: integer;
begin
    result := -1;
    for i := 0 to high(nodes) do
        if name=nodes[i].name then
            Exit(i);
end;


function TGrid.NodeByName(name: unicodestring): integer;
begin
    result := FindNode(name);
    if result<0 then raise ETKZ.Create('Узел ' + name + ' отсутствует');
end;


function TGrid.FindArc(name: unicodestring): integer;
var i: integer;
begin
    result := -1;
    for i := 0 to high(arcs) do
        if name=arcs[i].name then
            Exit(i);
end;


procedure TGrid.AssertNewName(name: unicodestring);
var n, a: integer;
begin
    n := FindNode(name);
    if (n>=0) then raise ETKZ.Create('Имя '+name+' занято узлом '+IntToStr(n));
    a := FindArc(name);
    if (a>=0) then raise ETKZ.Create('Имя '+name+' занято ветвью '+IntToStr(a));
end;


procedure TGrid.append_measurement_points(_source: TNodeOrArc; _index: integer);
var mp: integer;
begin
    SetLength(measurement_points, Length(measurement_points)+1);
    mp := high(measurement_points);
    measurement_points[mp].source := _source;
    measurement_points[mp].index := _index;
end;


function TGrid.grid_description: TGridDescription;
begin
    result.arcs := arcs;
    result.nodes := nodes;
    result.measurement_points := measurement_points;
end;

function TGrid.protection_index(_name: unicodestring): integer;
var i: integer;
begin
    for i := 0 to high(protections) do
        if protections[i].name = _name then Exit(i);
    result := -1;
end;

procedure TGrid.create_faults_list;
var fp: integer;
begin
    //SetLength(faults, Length(nodes));
    //for fp := 0 to high(faults) do
    //    with faults

end;


function TGrid.AddNode(_name: unicodestring): integer;
begin
    AssertNewName(_name);
    SetLength(nodes, Length(nodes)+1);
    result := high(nodes);
    nodes[result].Init(_name);
end;


function TGrid.AddArc(_name, _node1, _node2: unicodestring; a: TArcDescription): integer;
var n1, n2: integer;
begin
    AssertNewName(_name);
    n1 := NodeByName(_node1);
    n2 := NodeByName(_node2);
    SetLength(arcs, Length(arcs)+1);
    result := high(arcs);
    with arcs[result] do
        begin
            name := _name;
            kind := akUniversal;
            node1 := n1;
            node2 := n2;

            Yl := 1/a.Z;
            Yc := a.Y;
            E := a.E;
            K := a.K;

            I[0] := _0;
            I[1] := _0;
        end;
end;


function TGrid.AddBreaker(_name, _node1, _node2: unicodestring): integer;
var a: TArcDescription;
begin
    a.Z := cinit(0.001, 0.01);
    a.Y := 0;
    a.E := 0;
    a.K := 1;
    result := AddArc(_name, _node1, _node2, a);
    arcs[result].kind := akBreaker;
end;


function TGrid.AddMeasurementPoint(_name: unicodestring): integer;
var n, a: integer;
begin
    n := FindNode(_name);
    a := FindArc(_name);
    if n>=0
    then
        append_measurement_points(Node, n)
    else if a>=0
    then
        append_measurement_points(Arc, a)
    else
        raise ETKZ.Create('Не найден элемент '+_name+' для измерения');

    result := high(measurement_points);
end;


function TGrid.AddProtection(_name, _U_mp, _I_mp: unicodestring): integer;
begin
    if protection_index(_name )>= 0
    then raise ETKZ.Create('Повтор имени защиты '+_name);

    SetLength(protections, Length(protections)+1);
    result := high(protections);
    with protections[result] do
        begin
            name := _name;
            U_mp_id := AddMeasurementPoint(_U_mp);
            I_mp_id := AddMeasurementPoint(_I_mp);
		end;
end;


function TGrid.GetProtectionMeasurements(_name: unicodestring): TMeasurements;
var p, fp: integer; protection: TProtection;
begin
    p := protection_index(_name);
    if p<0 then raise ETKZ.Create('Защита '+_name+' не найдена');

    protection := protections[p];
    SetLength(result, Length(measurements));
    for fp := 0 to high(measurements) do
        begin
            result[fp].U := measurements[fp][protection.U_mp_id];
            result[fp].I := measurements[fp][protection.I_mp_id];
		end;
end;






procedure TGrid.Calc(iter: integer = -1);
var s: TSolverOpenCL; sn: TSolverNative; t1, t2, t: cardinal;  n: integer;

begin

    if opencl_processor = ''
    then
        begin
            sn := TSolverNative.Create(grid_description);
            t1 := GetTickCount64();
            sn.CalcTask;
            t2 := GetTickCount64();
            measurements := sn.GetMeasurements();
            sn.Free;
        end
    else
        begin
            n := TASKS_NUMBER;
            s := TSolverOpenCL.Create(grid_description, opencl_processor);
            t1 := GetTickCount64();
            s.Run();
            s.WaitResult();
            measurements := s.GetMeasurements();
            t2 := GetTickCount64();
            t := s.GetKernelTime();
            s.Free;
        end;
    print;
    //printGraph();
    WriteLn('Выполнено за ', t, ' / ', t2-t1, ' мс');
end;



procedure TGrid.Fault(_name: unicodestring);
var s: TSolverNative; f: integer;
begin
    f := NodeByName(_name);
    s := TSolverNative.Create(grid_description);
    s.Fault(f);
    measurements := s.GetMeasurements();
    s.Free;
end;


procedure TGrid.AllFaults();
var s: TSolverNative;
begin
    s := TSolverNative.Create(grid_description);
    s.AllFaults();
    measurements := s.GetMeasurements();
    s.Free;
end;


procedure TGrid.print;
var n, a: integer;
begin

    WriteLn('Память: ',
        SizeOf(TNode)*length(nodes) + SizeOf(TArc)*length(arcs));


    WriteLn(format('%2s    %13s',
                ['№', 'U' ]));
    for n := 0 to high(nodes) do
        with nodes[n] do
            WriteLn(format('%2d -  %6.0f ∠%4.0f°  %20s ',
                [n, cmod(U), 180*carg(U)/pi, name ]));

    WriteLn(format('%12s  %11s  %10s  %13s  %13s',
                ['Z', 'Y [мкСм]', 'K', 'E', 'I']));
    for a := 0 to high(arcs) do
        with arcs[a] do
            WriteLn(format(
                '%5.1f ∠%4.0f°  %4.0f ∠%4.0f°  %4.1f ∠%4.0f°  %6.0f ∠%4.0f°  %6.0f ∠%4.0f°  %s',
                [cmod(1/Yl), 180*carg(1/Yl)/pi,
                 cmod(Yc*1000000), 180*carg(Yc)/pi,
                 cmod(K), 180*carg(K)/pi,
                 cmod(E), 180*carg(E)/pi,
                 cmod(I[0]), 180*carg(I[0])/pi,
                 name]))
end;


procedure TGrid.printGraph;
var a, n: integer;
begin
    for a := 0 to high(arcs) do
        with arcs[a] do
            WriteLn(format('%20s  %20s  %20s',
                [nodes[node1].name, name, nodes[node2].name]));
end;



initialization
    net := TGrid.Create;

finalization
    FreeAndNil(net);

end.


