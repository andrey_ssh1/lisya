﻿unit dlisp_read;

{$mode delphi}

interface

uses
    {$IFDEF LINUX}
    cwstring,
    {$ENDIF}
    Classes, SysUtils, ucomplex, dlisp_values, mar, lisia_charset, lisya_exceptions,
    lisya_streams, lisya_string_predicates, mar_math_arrays;



function read(s: TLStream): TValue; overload;
function read(sp: TVStream): TValue; overload;
function read_from_string(s: unicodestring): TValue;

procedure print(V:TValue; stream: TVStream; line_end: boolean = false);

function str_to_float(s: unicodestring): double;
function str_to_integer(s: unicodestring): Int64;

function str_is_str(s: unicodestring; out ss: unicodestring): boolean;

implementation

var FormatSettings: TFormatSettings;


const special_keywords: array[1..6] of unicodestring = (
    'ELSE', 'EXCEPTION', 'INSET', 'THEN', 'VALUE', 'TRUE');

procedure raise_malformed(t: TStringList; i: integer; msg: unicodestring='');
var j, first, last: integer; s: unicodestring;
begin
    //добавляет в сообщение об ошибке конец списка токенов
    first := i-10;
    if first<0 then first := 0;
    if i<t.Count then last:=i else last:=t.Count-1;
    s:='';
    for j:=first to last do s := s + ' ' + t[j];
    raise ELE.Create(msg+s, 'syntax');
end;


function str_is_str(s: unicodestring; out ss: unicodestring): boolean;
var ns, nt: integer;
begin
    ss := '';
    result := (Length(s)>=2) and (s[1]='"') and (s[Length(s)]='"');
    if result then
        begin
            ss := s;
            nt := 0;
            ns := 2;
            while ns<length(s) do
                begin
                    if s[ns]='\' then Inc(ns);
                    Inc(nt);
                    ss[nt] := s[ns];
                    Inc(ns);
				end;
            SetLength(ss, nt);
		end;
end;


function str_is_char(s: unicodestring; out ss: unicodestring): boolean;
begin
    result := sp_char(s);
    if result then ss := ifh_character(StrToInt64(s[2..Length(s)]));
end;


function split_time_string(s: unicodestring): TStringArray;
var i, j: integer;
begin
    SetLength(result,3);
    j := 0;
    for i := 1 to Length(s) do
        case s[i] of
            ':': Inc(j);
            '_': ;
            '.',',': result[j] := DecimalSeparator;
            else result[j] := result[j]+s[i];
		end;
end;


function str_is_duration(s: unicodestring; out dt: TDateTime): boolean;
    procedure error;
    begin raise ELE.Create(s, 'value/time/literal/out of range'); end;
var hours, sign, minutes: integer; ss: TStringArray; seconds: double;
begin
    dt := 0;
    result := sp_duration(s);
    if not result then exit;

    if s[1]='-' then sign := -1 else sign := +1;
    ss := split_time_string(s);
    hours := StrToInt(ss[0]);
    minutes := StrToInt(ss[1]);
    if ss[2]=''
    then seconds := 0
    else seconds := StrToFloat(ss[2]{%H-},FormatSettings);

    if (minutes>59) or (minutes<0) then error;
    if (seconds>=60) or (seconds<0) then error;

    dt := hours/24 + sign*minutes/(24*60) + sign*seconds/(24*60*60);
end;


function split_datetime_string(s: unicodestring): TStringArray;
var i, component: integer;
begin
    SetLength(result, 4);
    component := 0;
    for i := 1 to length(s) do
        case s[i] of
            '-','.': Inc(component);
            '_','T':begin
                    result[3] := copy(s, i+1, Length(s)-i);
                    break;
				end;
			else result[component] := result[component] + s[i];
		end;
end;


function str_is_datetime(s: unicodestring; out dt: TDateTime): boolean;
var year, month, day: word;
    d: TDateTime; ss: TStringArray;
begin
    try
        result := sp_date_time(s);
        if not result then exit;

        ss := split_datetime_string(s);
        year := StrToInt(ss[0]);
        month := StrToInt(ss[1]);
        day := StrToInt(ss[2]);
        dt := EncodeDate(year, month, day);

        if str_is_duration(ss[3],d) then dt := dt+d;

        if (d<0) or (d>=1) then raise ELE.Create(s,'syntax/time/literal/out of range');
    except
        on E:ELE do raise;
    	on E:Exception do raise ELE.Create(s, 'syntax/time/literal/native/'+E.ClassName);
    end;
end;


function str_is_elt_call(s: unicodestring; out elt: TStringList): boolean;
var i: integer; acc: unicodestring;
    state: (sNormal, sString, sEscaped);
    procedure add; begin if acc<>'' then begin elt.Add(acc{%H-}); acc:=''; end; end;
begin
    result := (PosU('\', s)>1) or (PosU('/', s)>1);
    if not result then Exit;
try
    elt := TStringList.Create;
    elt.Add('(');
    elt.Add('ELT');
    acc := '';
    state := sNormal;

    for i := 1 to Length(s) do
        case state of
            sNormal: case s[i] of
                '"': begin acc:=acc+s[i]; state:=sString; end;
                '/': add;
                '\': begin add; acc:=' \'; add; end;
                else acc:=acc+s[i];
            end;
            sString: case s[i] of
                '\': begin acc:=acc+s[i]; state:=sEscaped; end;
                '"': begin acc:=acc+s[i]; state:=sNormal; end;
                else acc:=acc+s[i];
            end;
            sEscaped: begin acc:=acc+s[i]; state:=sString; end;
        end;

    if acc='' then raise ELE.Malformed('ELT '+s);
    add;
    elt.Add(')');
except
    FreeAndNil(elt);
    raise;
end;
end;


function str_to_integer(s: unicodestring): Int64;
var ss: unicodestring; j: integer; m: Int64;
begin
    ss := '';
    m := 1;
    for j:=1 to length(s) do case s[j] of
        '_':;
        'k','к': m := 1000;
        'M','М': m := 1000000;
        'G','Г': m := 1000000000;
        'T','Т': m := 1000000000000;
        else ss := ss+s[j];
    end;
    result := StrToInt64(ss{%H-})*m;
end;


function str_is_integer(s: unicodestring; out i: Int64): boolean;
begin
    result := sp_integer(s);
    if result then i := str_to_integer(s) else i := 0;
end;


function str_to_float(s: unicodestring): double;
const ml: array[1..20] of record n: unicodestring; v: real; end = (
    (n:'п';  v:1e-12),  (n:'p'; v:1e-12),
    (n:'н';  v:1e-9),   (n:'n'; v:1e-9),
    (n:'мк'; v:1e-6),   (n:'u'; v:1e-6),
    (n:'м';  v:1e-3),   (n:'m'; v:1e-3),
    (n:'к';  v:1e3),    (n:'k'; v:1e3),
    (n:'М';  v:1e6),    (n:'M'; v:1e6),
    (n:'Г';  v:1e9),    (n:'G'; v:1e9),
    (n:'Т';  v:1e12),   (n:'T'; v:1e12),
    (n:'гр'; v:pi/180),  (n:'deg'; v:pi/180), (n:'°'; v:pi/180),
    (n:'pi'; v:pi));
var i: integer; num, suffix: unicodestring;
begin
    num := '';
    for i:=1 to length(s) do case s[i] of
        '_':;
        ',','.': num := num+'.';
        '0','1','2','3','4','5','6','7','8','9','E','e','-','+': num := num+s[i];
        else break;
    end;

    result := StrToFloat(num{%H-},FormatSettings);

    suffix := copy(s, Length(num)+1, Length(s)-Length(num));

    if suffix='' then Exit;

    for i := low(ml) to high(ml) do if suffix=ml[i].n then Exit(result*ml[i].v);
end;


function str_is_float(s: unicodestring; out f: double): boolean;
begin
    result := sp_float(s);

    if result then f := str_to_float(s) else f := 0;
end;


function str_is_complex_exp(s: unicodestring; out re, im: double): boolean;
var a_pos: integer;
    m_s, a_s: unicodestring;
    m_f, a_f: double;
    c: complex;
begin
    result := sp_complex_exp(s);
    if not result then Exit;

    a_pos := PosU('a', s);
    if a_pos=0 then a_pos := PosU('у', s);
    if a_pos=0 then a_pos := PosU('∠', s);

    m_s := s[1..a_pos-1];
    a_s := s[a_pos+1..Length(s)];

    result := (str_is_float(m_s, m_f) and str_is_float(a_s, a_f));

    c := m_f*cexp(cinit(0,a_f));
    re := c.re;
    im := c.im;
end;


function str_is_complex_alg(s: unicodestring; out re, im: double): boolean;
var i_pos: integer;
    re_s, im_s: unicodestring;
    sign: unicodechar;
begin
    result := sp_complex_alg(s);
    if not result then Exit;

    i_pos := PosU('i', s);
    if i_pos=0 then i_pos := PosU('м', s);

    if i_pos>2 then re_s := s[1..i_pos-2] else re_s :='0';
    im_s := s[i_pos+1..Length(s)];

    if i_pos>1 then sign := s[i_pos-1] else sign := '+';

    result := (str_is_float(re_s, re) and str_is_float(im_s, im));

    if sign='-' then im := - im;
end;


function str_is_complex(s: unicodestring; out re, im: double): boolean;
begin
    result := str_is_complex_alg(s,re,im) or str_is_complex_exp(s,re,im);
end;


function str_is_range_integer(s: unicodestring; out l, h: Int64): boolean;
var p: integer;
begin
    result := sp_range_integer(s);
    if not result then Exit;
    p := PosU('..', s);
    l := str_to_integer(s[1..p-1]);
    h := str_to_integer(s[p+2..Length(s)]);
end;


function str_is_range_float(s: unicodestring; out l, h: double): boolean;
var p: integer;
begin
    result := sp_range_float(s);
    if not result then Exit;
    p := PosU('..', s);
    l := str_to_float(s[1..p-1]);
    h := str_to_float(s[p+2..Length(s)]);
end;


function str_is_keyword(s: unicodestring): boolean;
var i: integer; us: unicodestring;
begin
    result := true;
    if s[1]=':' then exit;

    us := UnicodeUpperCase(s);
    for i := 1 to high(special_keywords) do
        if us = special_keywords[i] then Exit;

    result := false;
end;


function str_is_operator(s: unicodestring; out oe: TOperatorEnum): boolean;
var us: unicodestring;
begin
    us := UnicodeUpperCase(s);
    for oe in TOperatorEnum do begin
        result := us = op_names[oe];
        if result then Exit;
    end;
end;

procedure add_bytes(var bb:TBytes; s: unicodestring);
var i, n, l: integer;
begin
    if ((Length(s) mod 2) <> 0) or (Length(s)=0) then raise ELE.Malformed('bytes '+s);
    n := Length(s) div 2;
    l := Length(bb);
    SetLength(bb, l+n);
    for i := 1 to n do begin
        bb[l-1+i] := StrToInt('$'+s[i*2-1]+s[i*2]{%H-});
    end;
end;

const screen_width = 50;
var ind: integer = 0;

procedure print(V:TValue; stream: TVStream; line_end: boolean);
var  i: integer; sn: unicodestring;
    procedure wr(s: unicodestring);
    begin
        if stream<>nil then stream.target.write_string(s) else System.Write(s);
    end;
    procedure indent;
    var i: integer;
    begin for i:=1 to ind do wr(' '); end;
begin try

    //TODO: нужен специальный код для печати списков ключ-значение, чтобы пара помещалась на одной строке
    if (V is TVList) then begin
        if (V as TVList).count=0
        then wr('NIL')
        else begin
            if (Length(V.AsString)+ind*3)<=screen_width
            then wr(V.AsString)
            else begin
                Inc(ind,2);
                wr('( ');
                print((V as TVList)[0], stream);
                for i := 1 to (V as TVList).high do begin
                    wr(LineEnding);
                    indent;
                    print((V as TVList)[i], stream);
                end;
                wr(')');
                Dec(ind,2);
            end;
        end;
        exit;
    end;

    if (V is TVRecord) then begin
        if ((Length(V.AsString)+ind*3)<=screen_width)
            or ((V as TVRecord).count=0)
        then wr(V.AsString)
        else begin
            Inc(ind,3);
            wr('#R(');
            sn := (V as TVRecord).name_n(0)+' ';
            wr(sn);
            Inc(ind, Length(sn));
            print((V as TVRecord)[0], stream);
            Dec(ind, length(sn));
            for i := 1 to (V as TVRecord).count-1 do begin
                wr(LineEnding);
                indent;
                sn := (V as TVRecord).name_n(i)+' ';
                wr(sn);
                inc(ind, Length(sn));
                print((V as TVRecord)[i], stream);
                dec(ind, Length(sn));
            end;
            wr(')');
            Dec(ind,3);
        end;
        exit;
    end;

    wr(V.asString);
    exit;

    if not (V is TVList)
    then wr(V.AsString())
    else
        if (V as TVList).count=0
        then wr('NIL')
        else
            if (Length((V as TVList).AsString())+ind*2)<=screen_width
            then wr((V as TVList).AsString())
            else begin
                Inc(ind);
                wr('(  ');
                print((V as TVList)[0], stream);
                for i:=1 to (V as TVList).count-1 do begin
                    wr(LineEnding);
                    indent;
                    print((V as TVList)[i], stream);
                end;
                wr(')');
                Dec(ind);
            end;

finally
    if line_end then wr(LineEnding);
end;

end;


function read_tokens(s: TLStream): TStringList;
var ch: unicodestring; depth: integer; state: (sStart, sToken, sString, sQuoted, sComment);
    acc: unicodestring;
    procedure add;
    begin
        if acc='' then Exit;
        while (Length(acc)>1) and (acc[1] in ['/','\','@']) do begin
            result.Add(' '+acc[1]{%H-});
            acc := acc[2..Length(acc)];
        end;
        result.Add(acc{%H-});
        acc := '';
    end;
begin try
    result := TStringList.Create;
    acc := '';
    depth := 0;
    state := sStart;
    while s.read_character(ch) do begin
        case state of
            sStart: case ch[1] of
                ' ',#13,#10,#$FEFF,#9:;
                '#': acc := acc+ch;
                '!': if acc = '#' then begin acc := ''; state := sComment; end;
                '(','{': begin acc:=acc+ch; add; inc(depth); state := sToken end;
                ')','}': begin add; acc:=acc+ch; add; dec(depth); state := sToken end;
                '"': begin acc:=acc+ch; state:= sString; end;
                '''': begin add; acc:=acc+'"'; state:=sQuoted; end;
                ';': begin add; state:=sComment; end;
                else begin acc:=acc+ch; state := sToken; end;
			end;
            sToken: case ch[1] of
                ' ',#13,#10,#$FEFF,#9: begin add; end;
                '(','{': begin acc:=acc+ch; add; inc(depth); end;
                ')','}': begin add; acc:=acc+ch; add; dec(depth);end;
                '"': begin {add;} acc:=acc+ch; state:= sString; end;
                '''': begin add; acc:=acc+'"'; state:=sQuoted; end;
                ';': begin add; state:=sComment; end;
                else acc:=acc+ch;
            end;
            sString: case ch[1] of
                '"': begin acc:=acc+ch; {add;} state:=sToken; end;
                '\': begin acc:=acc+'\'+s.read_character; end;
                else acc:=acc+ch;
            end;
            sQuoted: case ch[1] of
                '''': begin acc:=acc+'"'; add; state:=sToken; end;
                '\': begin acc:=acc+s.read_character; end;
                else acc:=acc+ch;
            end;
            sComment: case ch[1] of
                #13,#10: state:=sToken;
            end;
        end;
        if (state=sToken) and (depth=0) and (result.Count>0) then break;
    end;
    add;
    if state=sString then raise_malformed(result, result.Count, 'unmatched quotes');
    if state=sQuoted then raise_malformed(result, result.Count, 'unmatched quotes');
    if depth<>0 then raise_malformed(result, result.Count, 'unmatched parenthesis');
except
    FreeAndNil(result);
    raise;
end;
end;


function closing_parenthesis(t: TStringList; i: integer): boolean;
begin
    if i>=t.Count
    then raise_malformed(t,i, 'unmatched parenthesis')
    else result := t[i]=')';
end;


function closing_braces(t: TStringList; i: integer): boolean;
begin
    if i>=t.Count
    then raise_malformed(t,i, 'unmatched braces')
    else result := t[i]='}';
end;



function s_expr_tensor(t: TStringList; var i: integer): TVTensor;
var re, im: double; size: TIntegers; d: integer; c: TComplexes;
    is_complex: boolean;
begin
    c := nil;
    size := TIntegers.Create(0, 0);
    d := 0;
    is_complex := false;

    while not closing_braces(t, i) do
        begin
            if str_is_complex(t[i], re, im)
            then
                begin
                    is_complex := true;
                    d := 0;
                    SetLength(c, Length(c)+1);
                    c[high(c)] := cinit(re, im);
                    Inc(size[d]);
 				end
            else

            if str_is_float(t[i], re)
            then
                begin
                    d := 0;
                    SetLength(c, Length(c)+1);
                    c[high(c)] := re;
                    Inc(size[d]);
 				end
            else

            if t[i]='|'
            then
                begin
                    Inc(d);
                    if d>high(size) then SetLength(size, Length(size)+1);
                    Inc(size[d]);
				end
            else
                raise ELE.Create(t[i], 'read/tensor/nonconformant element');
            Inc(i);
 	    end;

    for d := 0 to high(size)-1 do
        begin
            if (size[d] mod size[d+1])<>0 then raise ELE.Create(t[i], 'read/tensor/malformed');
            size[d] := size[d] div size[d+1];
        end;
    SetLength(size, Length(size)-1);

    if is_complex
    then result := TVTensor.Create(size, c)
    else result := TVTensor.CreateReal(size, c)
end;



function s_expr(t: TStringList; var i: integer): TValue;
var slot: TValue; dt: TDateTime; re, im: double; l,h: Int64; str: unicodestring;
    oe: TOperatorEnum;
    elt: TStringList; j: integer;
    e: TComplexes;
begin
    if i>=t.Count then raise_malformed(t,i,'malformed expression');

    if str_is_str(t[i], str)
    then result := TVString.Create(str)
    else

    if str_is_char(t[i], str)
    then result := TVString.Create(str)
    else

    if t[i]='('
    then begin
        result := TVList.Create;
        Inc(i);
        while not closing_parenthesis(t,i) do (result as TVList).Add(s_expr(t, i));
    end
    else

    if UnicodeUpperCase(t[i])='#R('
    then begin
        result := TVRecord.Create;
        Inc(i);
        slot := nil;
        while not closing_parenthesis(t,i) do try
            slot := s_expr(t, i);
            if not (slot is TVSymbol)
            then raise ELE.Create('invalid slot name '+slot.AsString,'syntax');
            if closing_parenthesis(t,i) then raise_malformed(t,i, 'malformed record');
            (result as TVRecord).AddSlot(slot as TVSymbol, s_expr(t,i));
        finally
            slot.Free;
        end;
    end
    else

    if UnicodeUpperCase(t[i])='#B('
    then begin
        Inc(i);
        result := TVBytes.Create;
        while not closing_parenthesis(t,i) do begin
            add_bytes((result as TVBytes).fBytes, t[i]);
            Inc(i);
        end;
    end
    else


    if t[i]='{'
    then
        begin
            Inc(i);
            Inc(i);
            case UnicodeUpperCase(t[i-1])[1] of
                'T': result := s_expr_tensor(t, i);
			end
		end
    else


    if t[i]=' \'
    then begin
        Inc(i);
        result := TVList.Create([TVOperator.Create(oeQUOTE), s_expr(t,i)]);
        Dec(i);
    end
    else

    if t[i]=' /'
    then begin
        Inc(i);
        result := TVList.Create([TVSymbol.Create('VALUE'), s_expr(t,i)]);
        Dec(i);
    end
    else

    if t[i]=' @'
    then begin
        Inc(i);
        result := TVList.Create([TVSymbol.Create('INSET'), s_expr(t,i)]);
        Dec(i);
    end
    else

    if str_is_datetime(t[i], dt)
    then result := TVDateTime.Create(dt)
    else

    if str_is_duration(t[i], dt)
    then result := TVDuration.Create(dt)
    else

    if UnicodeUpperCase(t[i])='NIL'
    then result := TVList.Create
    else

    if str_is_integer(t[i], l)
    then result := TVInteger.Create(l)
    else

    if str_is_float(t[i], re)
    then result := TVFloat.Create(re)
    else

    if str_is_complex(t[i], re, im)
    then result := TVComplex.Create(re, im)
    else

    if str_is_range_integer(t[i], l, h)
    then result := TVRangeInteger.Create(l,h)
    else

    if str_is_range_float(t[i], re, im)
    then result := TVRangeFloat.Create(re,im)
    else

    if str_is_operator(t[i], oe)
    then result := TVOperator.Create(t[i], oe)
    else

    if str_is_elt_call(t[i], elt)
    then try
        //for j:=0 to elt.Count-1 do WriteLn(elt[j]);
        j := 0;
        result := s_expr(elt, j);
    finally
        elt.Free;
    end
    else

    if str_is_keyword(t[i])
    then result := TVKeyword.Create(t[i])
    else

    if t[i]=')'
    then raise_malformed(t,i, 'unmatched parenthesis')
    else

    result := TVSymbol.Create(t[i]);

    Inc(i);
end;


function read(s: TLStream): TValue;
var tokens: TStringList; i: integer;
begin try
    tokens := nil;
    tokens := read_tokens(s);
    //for i:=0 to tokens.count-1 do WriteLn(tokens[i]);
    i := 0;
    if tokens.Count>0
    then result := s_expr(tokens, i)
    else result := TVEndOfStream.Create;
finally
    tokens.Free;
end;
end;


function read(sp: TVStream): TValue;
begin
    //result := read(sp.stream.fstream, sp.stream.encoding);
  result := read(sp.target);
end;

function read_from_string(s: unicodestring): TValue;
var stream: TLMemoryStream;
begin try
    result := nil;
    stream := TLMemoryStream.Create(s);
    result := read(stream);
finally
    stream.Free;
end;
end;

initialization
    FormatSettings := DefaultFormatSettings;
    FormatSettings.DecimalSeparator := '.';

end. //514
