﻿unit lisya_sqlite;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils, sqldb, sqlite3conn, FileUtil, lisya_sql;



type

    { TSQLite_module }

    TSQLite_module = class(TDataModule)
	    SQLite3Connection: TSQLite3Connection;
		Query: TSQLQuery;
		Transaction: TSQLTransaction;
    end;


	{ TLSQLite }

    TLSQLite = class (TLSQL)
        module: TSQLite_module;
        connector: TSQLite3Connection;
        constructor Connect(database: unicodestring);
        destructor Destroy; override;

        function description: unicodestring; override;
    end;


implementation

{$R *.lfm}

{ TLSQLite }


constructor TLSQLite.Connect(database: unicodestring);
begin
    inherited Create;
    module := TSQLite_module.Create(nil);
    connector := module.SQLite3Connection;
    query := module.Query;
    connector.DatabaseName := database;
    connector.Open;
end;


destructor TLSQLite.Destroy;
begin
    connector.Close();
    module.Free;
    inherited Destroy;
end;


function TLSQLite.description: unicodestring;
begin
    result := 'SQLite: '+connector.DatabaseName;
end;


end.

