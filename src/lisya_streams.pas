﻿unit lisya_streams;

{$mode delphi}{$H+}

interface

uses
    {$IFDEF LINUX}
    cwstring, unix, BaseUnix,
    {$ENDIF}
    {$IFDEF WINDOWS}
    LazUnicode,
    windows,
    {$ENDIF}
    {$IFDEF NETWORK}ssockets in './fpc_backport/ssockets.pp',{$ENDIF}
    zstream, serial, pipes, process,
    Classes, SysUtils, mar, lisia_charset, lisya_exceptions, lisya_zip;

type

    { TLStream }

    TLStream = class(TCountingObject)
    private
      fencoding: TStreamEncoding;
      stream: TStream;
      procedure CheckState;
      procedure SetEncoding(enc: TStreamEncoding);
      function GetPosition: Int64;
      procedure SetPosition(p: Int64);
      function GetSize: Int64;
      procedure SetSize(s: Int64);
      function ReadBytes(out Buffer; count: integer; EoE: boolean = false): integer; virtual;
      function WriteBytes(const Buffer; count: integer; EoE: boolean = false): integer; virtual;
      function ReadCharacter(out ch: unicodestring; EoE: boolean = false): boolean;
      procedure WriteCodePoint(cp: DWORD);
    public
      constructor Create(trg: TStream; enc: TStreamEncoding=seUTF8);

      destructor Destroy; override;

      property encoding: TStreamEncoding read fencoding write SetEncoding;
      function read_byte: byte;
      procedure read_bytes(out bb: TBytes; count: integer);
      function read_WORD: WORD;
      function read_DWORD: DWORD;
      function read_i8: Int64;
      function read_i16: Int64;
      function read_i32: Int64;
      function read_i64: Int64;
      function read_single: single;
      function read_double: double;


      function read_character: unicodestring; overload; virtual;
      function read_character(out ch: unicodestring): boolean; overload; virtual;
      function read_string(count: integer): unicodestring;
      function read_line(ls: unicodestring; count: integer=-1): unicodestring; overload;
      function read_line(out ln: unicodestring; ls: unicodestring; count: integer=-1): boolean; overload;

      procedure write_byte(b: byte);
      procedure write_bytes(bb: TBytes);
      procedure write_string(s: unicodestring);
      procedure write_WORD(w: WORD);
      procedure write_DWORD(dw: DWORD);
      procedure write_i8(i: Int64);
      procedure write_i16(i: Int64);
      procedure write_i32(i: Int64);
      procedure write_i64(i: Int64);
      procedure write_single(f: double);
      procedure write_double(f: double);

      property position: Int64 read GetPosition write SetPosition;
      property size: Int64 read GetSize write SetSize;

      procedure close_stream; virtual;
      function active: boolean;
    end;


    { TLStdIn }

    TLStdIn = class(TLStream)
        function description: unicodestring; override;
    end;

    { TLMemoryStream }

    TLMemoryStream = class(TLStream)
      constructor Create(enc: TStreamEncoding = seUTF8); overload;
      constructor Create(const b: TBytes; enc: TStreamEncoding = seUTF8); overload;
      constructor Create(const s: unicodestring); overload;
      function description: unicodestring; override;
    end;


    { TLFileStream }

    TLFileStream = class(TLStream)
    private
        function GetFileName: unicodestring;
    public
        constructor Create(fn: unicodestring; mode: WORD; enc: TStreamEncoding);
        function description: unicodestring; override;
        procedure Log(msg: unicodestring);
        property FileName: unicodestring read GetFileName;
    end;

    { TLProxyStream }

    TLProxyStream = class(TLStream)
        target: TLStream;
        procedure Lock; override;
        procedure Unlock; override;
    end;

    { TLDeflateStream }

    TLDeflateStream = class(TLProxyStream)
      constructor Create(trg: TLStream; head: boolean = false; enc: TStreamEncoding=seUTF8);
      destructor Destroy; override;
      function description: unicodestring; override;
    end;

    { TLInflateStream }

    TLInflateStream = class(TLProxyStream)
      constructor Create(trg: TLStream; head: boolean = false; enc: TStreamEncoding=seUTF8);
      destructor Destroy; override;
      function description: unicodestring; override;
    end;


    { TLZipFile }

    TLZipFile = class(TLStream)
        archive: TZipArchive;
        file_name: unicodestring;
        constructor Create(Z: TZipArchive; fn: unicodestring; mode: WORD; enc: TStreamEncoding);
        destructor Destroy; override;
        function description: unicodestring; override;

        procedure Lock; override;
        procedure Unlock; override;
    end;


    { TLProcess }

    TLProcess = class(TLStream)
    private
        function ReadBytes(out Buffer; count: integer; EoE: boolean = false): integer; override;
        function WriteBytes(const Buffer; count: integer; EoE: boolean = false): integer; override;
    public
        proc: TProcess;
        constructor Run(cmd: unicodestring; dir: unicodestring);
        destructor Destroy; override;
        function description: unicodestring; override;

        procedure close_stream; override;
    end;

    { TLSerialStream }

    TLSerialStream = class(TLStream)
    private
        port: {$IFDEF LINUX}TSerialHandle{$ELSE}THandle{$ENDIF};
        timeout: integer;
        name: unicodestring;
        function ReadBytes(out Buffer; count: integer; EoE: boolean = false): integer; override;
        function WriteBytes(const Buffer; count: integer; EoE: boolean = false): integer; override;
    public
        constructor Create(port_name: unicodestring; boud: integer;
            enc: TStreamEncoding=seUTF8;
            _timeout: integer=0);
        destructor Destroy; override;
        function description: unicodestring; override;
        procedure discard_input;
    end;

    {$IFDEF NETWORK}

    { TLSocket }

    TLSocket = class(TLStream)
    private
        socket: TInetSocket;
        procedure SetTimeout(t: integer);
    public
        property timeout: integer write SetTimeout;
        constructor Connect(addr: unicodestring; port: WORD=0; enc: TStreamEncoding=seUTF8);
        constructor Create(_socket: TSocketStream);
        function description: unicodestring; override;
        destructor Destroy; override;
    end;

    {$ENDIF}


function ifh_character(n: DWORD):unicodestring;
function bytes_to_string(const bytes: TBytes; encoding: TStreamEncoding): unicodestring;
function string_to_bytes(const s: unicodestring; encoding: TStreamEncoding): TBytes;

var stdin: TLStream = nil;

implementation

function bytes_to_string(const bytes: TBytes; encoding: TStreamEncoding
    ): unicodestring;
var stream: TLMemoryStream;
begin try
    stream := TLMemoryStream.Create(bytes, encoding);
    result := stream.read_string(-1);
finally
    stream.Release;
end;
end;

function string_to_bytes(const s: unicodestring; encoding: TStreamEncoding
    ): TBytes;
var stream: TLMemoryStream;
begin try
    stream := TLMemoryStream.Create(s);
    stream.encoding := encoding;
    stream.read_bytes(result, -1);
finally
    stream.Release;
end;
end;



{В UTF-16 символы кодируются двухбайтовыми словами с использованием всех возможных
диапазонов значений (от 0 до FFFF_16). При этом можно кодировать символы Unicode
в диапазонах 000016..D7FF_16 и E000_16..10FFFF_16. Исключенный отсюда диапазон
D800_16..DFFF_16 используется как раз для кодирования так называемых суррогатных
пар — символов, которые кодируются двумя 16-битными словами.

Символы Unicode до FFFF_16 включительно (исключая диапазон для суррогатов)
записываются как есть 16-битным словом.

Символы же в диапазоне 10000_16..10FFFF_16 (больше 16 бит) кодируются по следующей схеме:

    Из кода символа вычитается 10000_16. В результате получится значение от нуля
    до FFFFF_16, которое помещается в разрядную сетку 20 бит.

    Старшие 10 бит (число в диапазоне 000016..03FF16) суммируются с D80016, и
    результат идёт в ведущее (первое) слово, которое входит в диапазон D80016..DBFF16.

    Младшие 10 бит (тоже число в диапазоне 000016..03FF16) суммируются с DC0016,
    и результат идёт в последующее (второе) слово, которое входит в диапазон DC0016..DFFF16.}

function is_surrogate(n: DWORD): boolean; inline; overload;
begin
    result := ($D800<=n) and (n<=$DFFF);
end;


function is_surrogate(ch: unicodechar): boolean; inline; overload;
begin
    result := is_surrogate(ord(ch));
end;


function surrogate_low(n: DWORD): WORD;
begin
    result := (((n - $10000) shr 10) and 1023) + $D800;
end;


function surrogate_high(n: DWORD): WORD;
begin
    result := ((n - $10000) and 1023) + $DC00;
end;


function ifh_character(n: DWORD):unicodestring;
begin
    if is_surrogate(n) or ($FFFF<n)
    then result := unicodechar(surrogate_low(n)) + unicodechar(surrogate_high(n))
    else result := unicodechar(n);
end;


function surrogate_ord(ch_l,ch_h: unicodechar):DWORD;
var n_l, n_h: DWORD;
begin
    n_l := ord(ch_l);
    n_h := ord(ch_h);
    result := (((n_l - $D800) shl 10) or (n_h - $DC00)) + $10000;
end;




{$IFDEF NETWORK}

{ TLSocket }

procedure TLSocket.SetTimeout(t: integer);
begin
    socket.IOTimeout:=t;
end;

constructor TLSocket.Connect(addr: unicodestring; port: WORD = 0;
    enc: TStreamEncoding=seUTF8);
var p: integer; _addr: unicodestring; _port: word;
begin
    inherited Create(nil, enc);
    p := PosU(':',addr);
    if p>0 then begin
        _addr := Copy(addr,1,p-1);
        _port := StrToInt(Copy(addr,p+1,Length(addr)-p));
    end
    else begin
        _addr := addr;
        _port := port;
    end;
    stream := TInetSocket.Create(_addr,_port);
    socket := stream as TInetSocket;
end;

constructor TLSocket.Create(_socket: TSocketStream);
begin
    inherited Create(_socket);
    socket := nil;
end;

function TLSocket.description: unicodestring;
begin
    if socket<>nil
    then result := 'SOCKET '+socket.Host+':'+IntToStr(socket.Port)
    else result := 'SERVER SOCKET';
end;

destructor TLSocket.Destroy;
begin
    socket := nil; //не освобождать, поскольку это только псевдоним
    inherited Destroy;
end;


{$ENDIF}

{ TLStdIn }

function TLStdIn.description: unicodestring;
begin
    result := 'STDIN';
end;

{ TLProxyStream }

procedure TLProxyStream.Lock;
begin
    target.Lock;
end;

procedure TLProxyStream.Unlock;
begin
    target.Unlock;
end;


{ TLSerialStream }

function TLSerialStream.ReadBytes(out Buffer; count: integer; EoE: boolean
    ): integer;
var b: array of byte;
begin
    if timeout=0
    then result := SerRead(port, Buffer{%H-}, Count)
    else begin
        SetLength(b, count);
        result := SerReadTimeout(port, b, count, timeout);
        Move(b[0],buffer,result);
        SetLength(b,0);
    end;
    if EoE and (result<count) then raise ELEmptyStream.Create(description, 'stream/empty/');
end;

function TLSerialStream.WriteBytes(const Buffer; count: integer; EoE: boolean
    ): integer;
{$IFDEF WINDOWS}var BytesWritten: DWORD;{$ENDIF}
begin
    {$IFDEF WINDOWS}
    BytesWritten := 0;
    if not WriteFile(port, Buffer, Count, BytesWritten, nil)
    then result := 0
    else result := BytesWritten;
    {$ENDIF}
    {$IFDEF LINUX}
    result := fpWrite(port, Buffer, Count);
    {$ENDIF}
    if EoE and (result<>count) then raise ELEmptyStream.Create(description, 'serial/full/');
end;

constructor TLSerialStream.Create(port_name: unicodestring; boud: integer;
    enc: TStreamEncoding; _timeout: integer);
begin
    inherited Create(nil, enc);
    timeout := _timeout;
    port := SerOpen(port_name{%H-});
    name := port_name;
    if port=0 then raise ELE.Create(port_name,'serial/not found/');
    SerSetParams(port, boud, 8, NoneParity, 1, []);
end;



destructor TLSerialStream.Destroy;
begin
    SerClose(port);
    port := 0;
    inherited Destroy;
end;

function TLSerialStream.description: unicodestring;
begin
    result := 'serial '+name;
end;

procedure TLSerialStream.discard_input;
begin
    SerFlushInput(port);
end;



{ TLProcess }

function TLProcess.ReadBytes(out Buffer; count: integer; EoE: boolean
    ): integer;
begin
   if proc=nil then raise ELEmptyStream.Create(description, 'stream/closed/');
   result := proc.Output.Read(Buffer{%H-}, count);
   if EoE and (result<count) then raise ELEmptyStream.Create(description, 'stream/empty/');
end;

function TLProcess.WriteBytes(const Buffer; count: integer; EoE: boolean
    ): integer;
begin
    if proc=nil then raise ELEmptyStream.Create(description, 'stream/closed/');
    result := proc.Input.Write(Buffer, count);
    if EoE and (result<>count) then raise ELEmptyStream.Create(description, 'stream/full/');
end;


constructor TLProcess.Run(cmd: unicodestring; dir: unicodestring);
begin
    inherited Create(nil, seUTF8);
    proc := TProcess.Create(nil);
    {$IFDEF WINDOWS}
    proc.CommandLine:=UnicodeToWinCP(cmd);
    proc.CurrentDirectory:=UnicodeToWinCP(DirSep(dir));
    {$ELSE}
    proc.{%H-}CommandLine:=cmd{%H-};
    proc.CurrentDirectory:={%H-}DirSep(dir);
    {$ENDIF};
    proc.InheritHandles:=false;
    proc.Options:=[poStderrToOutPut, poUsePipes];
    proc.Execute;
end;

destructor TLProcess.Destroy;
begin
    if proc<>nil then proc.Terminate(0);
    proc.Free;
    inherited Destroy;
end;

function TLProcess.description: unicodestring;
begin
    if proc=nil
    then result := 'nil'
    else result := {$IFDEF WINDOWS} WinCPtoUnicode(proc.CommandLine){$ELSE} proc.{%H-}CommandLine{$ENDIF};
end;

procedure TLProcess.close_stream;
begin
    proc.CloseInput;
end;


{ TLZipFile }

constructor TLZipFile.Create(Z: TZipArchive; fn: unicodestring; mode: WORD;
    enc: TStreamEncoding);
begin
    archive := Z;
    file_name := fn;
    inherited Create(archive.GetFileStream(fn, mode), enc);
end;

destructor TLZipFile.Destroy;
begin
    archive.Release;
    stream := nil; //при освобождении файла в архиве его поток не должен
                    //освобождаься, поскольку им управляет TLZipFile
    inherited Destroy;
end;

function TLZipFile.description: unicodestring;
begin
    result := '#<FILE '+archive.description+': '+file_name+'>';
end;

procedure TLZipFile.Lock;
begin
    archive.Lock;
end;

procedure TLZipFile.Unlock;
begin
    archive.Unlock;
end;

{ TLInflateStream }

constructor TLInflateStream.Create(trg: TLStream; head: boolean;
    enc: TStreamEncoding);
begin
    target := trg;
    inherited Create(TDecompressionStream.create(target.stream,  not head), enc);
end;

destructor TLInflateStream.Destroy;
begin
    target.Release;
    inherited Destroy;
end;

function TLInflateStream.description: unicodestring;
begin
    if stream<>nil
    then result := 'INFLATE STREAM -> '+target.description
    else result := 'INFLATE STREAM';
end;

{ TLDeflateStream }

constructor TLDeflateStream.Create(trg: TLStream; head: boolean;
    enc: TStreamEncoding);
begin
    target := trg;
    inherited Create(TCompressionStream.create(clDefault, target.stream , not head), enc);
end;

destructor TLDeflateStream.Destroy;
begin
    // сначала должен быть освобождён свой CompressionStream, а потом поток
    // в который он записывает, в противном случае происходит обращение к
    // не существующему объекту
    inherited Destroy;
    target.Release;
end;

function TLDeflateStream.description: unicodestring;
begin
    if stream<>nil
    then result := 'DEFLATE STREAM ('+IntToStr(refs)+') -> '+target.description
    else result := 'DEFLATE STREAM';
end;

{ TLFileStream }

function TLFileStream.GetFileName: unicodestring;
begin
    result := (stream as TFileStream).FileName;
end;


constructor TLFileStream.Create(fn: unicodestring; mode: WORD;
    enc: TStreamEncoding);
{$IFDEF LINUX}
var  fh: THandle;
const  FD_CLOEXEC = 1;
{$ENDIF}
begin
    case mode of
        fmOpenRead: if not FileExists(fn)
                        then raise ELE.Create(fn, 'stream/file not found/')
                        else stream := TFileStream.Create(fn{%H-},
                                    fmOpenRead or fmShareDenyWrite);
        fmCreate: stream := TFileStream.Create(fn{%H-}, fmCreate);
        fmOpenReadWrite: begin
            if FileExists(fn)
            then stream := TFileStream.Create(fn{%H-}, fmOpenReadWrite)
            else stream := TFileStream.Create(fn{%H-}, fmCreate);
            stream.Seek(stream.Size,0);
        end;
    end;

    //  Дескрипторы файлов помеченные флагом FD_CLOEXEC закрываются при выполнении
    //закрываются при выполнении execve, которые используется для запуска
    //дочерних процессов классом TProcess.
    //  Флаг O_CLOEXEC в режиме открытия файла устанавливает тот же флаг, но
    //его нельзя передать без переделки TFileStream. Это может выйти боком,
    //если при создании файла другой поток запустит дочерний процесс.
    {$IFDEF LINUX}
    fh := (stream as TFileStream).Handle;
    fpfcntl(fh, F_SETFD, fpfcntl(fh, F_GETFD) or FD_CLOEXEC);
    {$ENDIF}

    inherited Create(stream, enc);
end;


function TLFileStream.description: unicodestring;
begin
    if stream<>nil
    then result := 'FILE ' + (stream as TFileStream).FileName
    else result := 'FILE';
end;

procedure TLFileStream.Log(msg: unicodestring);
begin
    self.write_string(msg);
    {$IFDEF WINDOWS}
    FlushFileBuffers((self.stream as TFileStream).Handle);
    {$ELSE}
    fpfsync((self.stream as TFileStream).Handle);
    {$ENDIF}
end;

{ TLMemoryStream }

constructor TLMemoryStream.Create(enc: TStreamEncoding);
begin
    inherited Create(TMemoryStream.Create, enc);
end;

constructor TLMemoryStream.Create(const b: TBytes; enc: TStreamEncoding);
var i: integer;
begin
    inherited Create(TMemoryStream.Create, seUTF8);
    for i := 0 to high(b) do stream.WriteByte(b[i]);
    stream.Position:=0;
    encoding := enc;
end;

constructor TLMemoryStream.Create(const s: unicodestring);
begin
    inherited Create(TMemoryStream.Create, seUTF8);
    write_string(s);
    stream.Position:=0;
end;

function TLMemoryStream.description: unicodestring;
begin
    if stream<>nil
    then result := 'MEMORY STREAM ('+IntToStr(stream.Size)+' bytes)'
    else result := 'MEMORY STREAM (NIL)';
end;



{ TLStream }

procedure TLStream.CheckState;
begin
    if stream=nil then raise ELE.Create('','stream/closed/');
end;


constructor TLStream.Create(trg: TStream; enc: TStreamEncoding);
begin
    inherited Create;
    stream := trg;
    encoding := enc;
end;


destructor TLStream.Destroy;
begin
    FreeAndNil(stream);
    inherited Destroy;
end;


procedure TLStream.SetEncoding(enc: TStreamEncoding);
    function b: byte; inline; begin ReadBytes(result, 1, true); end;
var p: integer;
begin
    if enc <> seBOM then begin fencoding := enc; Exit; end;
    CheckState;

    p := stream.Position;
    //UTF-8
    if (b=$EF) and (b=$BB) and (b=$BF) then begin
        fencoding := seUTF8;
        exit;
    end else stream.Seek(p,0);
    //UTF16BE
    if (b=$FE) and (b=$FF) then begin
        fencoding := seUTF16BE;
        exit;
    end else stream.Seek(p,0);
    //UTF32BE
    if (b=$00) and (b=$00) and (b=$FE) and (b=$FF) then begin
        fencoding := seUTF32BE;
        exit;
    end else stream.Seek(p,0);
    //UTF32LE
    if (b=$FF) and (b=$FE) and (b=$00) and (b=$00) then begin
        fencoding := seUTF32LE;
        exit;
    end else stream.Seek(p,0);
    //UTF16LE
    if (b=$FF) and (b=$FE) then begin
        fencoding := seUTF16LE;
        exit;
    end else stream.Seek(p,0);

    fencoding := default_encoding;
end;

function TLStream.GetPosition: Int64;
begin
    CheckState;
    result := stream.Position;
end;

procedure TLStream.SetPosition(p: Int64);
begin
    CheckState;
    stream.Position:=p;
end;

function TLStream.GetSize: Int64;
begin
    CheckState;
    //if stream is TInputPipeStream
    //then result := (stream as TInputPipeStream).NumBytesAvailable
    //else
    result:=stream.Size;
end;

procedure TLStream.SetSize(s: Int64);
begin
    CheckState;
    stream.Size:=s;
end;

function TLStream.ReadBytes(out Buffer; count: integer; EoE: boolean): integer;
begin
    if stream=nil then raise ELE.Create('', 'stream/closed/read');
    result := stream.Read(Buffer{%H-}, count);
    if EoE and (result<count) then raise ELEmptyStream.Create(description, 'stream/empty/');
end;


function TLStream.ReadCharacter(out ch: unicodestring; EoE: boolean): boolean;
var b: array[1..4] of byte; w: word;
begin
    result := false;
    if ReadBytes(b[1], 1, EoE)<>1 then Exit;
    case encoding of
        seUTF8: begin
            case b[1] of
                {0xxxxxxx}$00..$7F: begin
                    ch := ifh_character(b[1]);
                end;
                {110xxxxx}$C0..$DF: begin
                    ReadBytes(b[2], 1, true);
                    ch := ifh_character(64*(b[1] and 31)
                                         + (b[2] and 63));
                end;
                {1110xxxx}$E0..$EF: begin
                    ReadBytes(b[2], 2, true);
                    ch := ifh_character(64*64*(b[1] and 15)
                                         + 64*(b[2] and 63)
                                            + (b[3] and 63));
                end;
                {11110xxx}$F0..$F7: begin
                    ReadBytes(b[2], 3, true);
                    ch := ifh_character(64*64*64*(b[1] and 7)
                                         + 64*64*(b[2] and 63)
                                            + 64*(b[3] and 63)
                                               + (b[4] and 63));
                end;
                else ch := '?';
            end;
        end;
        seUTF16LE: begin
            ReadBytes(b[2],1,true);
            w := b[1]+b[2]*256;
            ch := unicodechar(w);
            if is_surrogate(w) then begin
                ReadBytes(b[3],2,true);
                ch := ch + unicodechar(b[3]+b[4]*256);
            end;
        end;
        seUTF16BE: begin
            ReadBytes(b[2],1,true);
            w := 256*b[1]+b[2];
            ch := unicodechar(w);
            if is_surrogate(w) then begin
                ReadBytes(b[3],2,true);
                ch := ch + unicodechar(256*b[3]+b[4]);
            end;
        end;
        seUTF32LE: begin
            ReadBytes(b[2],3,true);
            ch := ifh_character(b[1]+b[2]*256+b[3]*256*256+b[4]*256*256*256);
        end;
        seUTF32BE: begin
            ReadBytes(b[2],3,true);
            ch := ifh_character(256*256*256*b[1]+256*256*b[2]+256*b[3]+b[4]);
        end;
        seCP1251: ch := cp1251_cp[b[1]];
        seCP1252: ch := cp1252_cp[b[1]];
        seCP866:  ch := cp866_cp[b[1]];
        seKOI8R:  ch := KOI8_R_cp[b[1]];
        seLatin1: ch := Latin1_cp[b[1]];
        else raise ECharsetError.Create('');
    end;
    result := true;
end;


function TLStream.read_byte: byte;
begin
    ReadBytes(result, 1, true)
end;


procedure TLStream.read_bytes(out bb: TBytes; count: integer);
const bs = 4096;
var bc: integer;
begin

    if count>=0
    then begin
        SetLength(bb, count);
        ReadBytes(bb[0], count, true);
    end
    else begin
        SetLength(bb, 0);
        repeat
            SetLength(bb, Length(bb)+bs);
            bc := ReadBytes(bb[Length(bb)-bs], bs);
        until bc<bs;
        SetLength(bb, Length(bb)-bs+bc);
    end;
end;

function TLStream.read_WORD: WORD;
begin
    ReadBytes(result, SizeOf(WORD), true);
end;

function TLStream.read_DWORD: DWORD;
begin
    ReadBytes(result, SizeOf(DWORD), true);
end;

function TLStream.read_i8: Int64;
var b: byte;
begin
    ReadBytes(b, SizeOf(byte), true);
    if b>$7F
    then result := b - $FF - 1
    else result := b;
end;

function TLStream.read_i16: Int64;
var w: WORD;
begin
    ReadBytes(w,SizeOf(WORD),true);
    if w>$7FFF
    then result := w - $FFFF - 1
    else result := w;
end;

function TLStream.read_i32: Int64;
var dw: DWORD;
begin
    ReadBytes(dw, SizeOf(DWORD), true);
    if dw>$7FFFFFFF
    then result := dw - $FFFFFFFF - 1
    else result := dw;
end;

function TLStream.read_i64: Int64;
begin
    ReadBytes(result,SizeOf(Int64), true);
end;

function TLStream.read_single: single;
var d: record case byte of 0: (s: single); 1: (b: array [1..SizeOf(single)] of Byte); end;
begin
    ReadBytes(d.b[1], SizeOf(single), true);
    result := d.s;
end;

function TLStream.read_double: double;
var d: record case byte of 0: (d: double); 1: (b: array [1..SizeOf(double)] of Byte); end;
begin
    ReadBytes(d.b[1], SizeOf(double), true);
    result := d.d;
end;


function TLStream.read_character: unicodestring;
begin
    ReadCharacter(result, true);
end;

function TLStream.read_character(out ch: unicodestring): boolean;
begin
    result := ReadCharacter(ch, false);
end;

function TLStream.read_string(count: integer): unicodestring;
var i: integer; ch: unicodestring;
begin
    result := '';
    if count>=0
    then for i := 1 to count do begin ReadCharacter(ch, true); result := result + ch end
    else while ReadCharacter(ch, false) do result := result + ch;
end;


function TLStream.read_line(ls: unicodestring; count: integer): unicodestring;
var ch: unicodestring; ln: unicodestring; i: integer;
begin
    ln := '';
    i := 0;
    while ReadCharacter(ch, false) do begin
        ln := ln + ch;
        Inc(i);
        if end_of_string_is(ln,ls)
        then begin
            result := ln[1..Length(ln)-Length(ls)];
            Exit;
        end;
        if (count>0) and (i>=count)
        then raise ELE.Create(description+' > '+IntToStr(count), 'stream/too long line/');
    end;
    result := ln;
end;

function TLStream.read_line(out ln: unicodestring; ls: unicodestring;
    count: integer): boolean;
var ch: unicodestring; i: integer;
begin
    result := true;
    ln := '';
    i := 0;
    while ReadCharacter(ch, false)  do begin
        ln := ln + ch;
        inc(i);
        if end_of_string_is(ln,ls)
        then begin
            ln := ln[1..Length(ln)-Length(ls)];
            Exit;
        end;
        if (count>0) and (i>=count)
        then raise ELE.Create(description+' > '+IntToStr(count), 'stream/too long line/');
    end;
    if ln='' then result := false;
end;


function TLStream.WriteBytes(const Buffer; count: integer; EoE: boolean
    ): integer;
begin
    if stream=nil then raise ELE.Create('', 'stream/closed/write/');
    result := stream.Write(Buffer, count);
    if EoE and (result<>count) then raise ELEmptyStream.Create(description, 'stream/full/');
end;

procedure TLStream.write_byte(b: byte);
begin
    WriteBytes(b, 1, true);
end;

procedure TLStream.write_bytes(bb: TBytes);
begin
    WriteBytes(bb[0], Length(bb), true);
end;


procedure TLStream.WriteCodePoint(cp: DWORD);
    procedure write(b: byte); inline; begin WriteBytes(b, 1, true); end;
    procedure writeLE(w: WORD); inline; begin write(w and $FF); write(w shr 8); end;
    procedure writeBE(w: WORD); inline; begin write(w shr 8); write(w and $FF); end;
    procedure write8bit(const codepage: TCodePage);
    var i: byte; ch: unicodechar;
    begin
        ch := unicodechar(cp);
        for i := 0 to 255 do
            if codepage[i]=ch then begin
                write(i);
                exit;
            end;
        write(ord('?'));
    end;
begin
    case encoding of
        seUTF8: case cp of
            0..127: begin
                write(cp);
            end;
            128..2047: begin
                write((cp shr 6) or 192);
                write((cp and 63) or 128);
            end;
            2048..65535: begin
                write((cp shr 12) or 224);
                write(((cp shr 6) and 63) or 128);
                write((cp and 63) or 128);
            end;
            65536..2097152: begin
                write((cp shr 18) or 240);
                write(((cp shr 12) and 63) or 128);
                write(((cp shr 6) and 63) or 128);
                write((cp and 63) or 128);
            end
            else raise ECharsetError.Create('Символ вне диапазона');
        end;
        seUTF16LE:
            if is_surrogate(cp) or (cp>$FFFF) then begin
                writeLE(surrogate_low(cp));
                writeLE(surrogate_high(cp));
            end
            else writeLE(cp);
        seUTF16BE:
            if is_surrogate(cp) or (cp>$FFFF) then begin
                writeBE(surrogate_low(cp));
                writeBE(surrogate_high(cp));
            end
            else writeBE(cp);
        seUTF32LE: begin
            write(cp and $FF);
            write((cp shr 8) and $FF);
            write((cp shr 16) and $FF);
            write((cp shr 24) and $FF);
        end;
        seUTF32BE: begin
            write((cp shr 24) and $FF);
            write((cp shr 16) and $FF);
            write((cp shr 8) and $FF);
            write(cp and $FF);
        end;
        seCP1251: write8bit(cp1251_cp);
        seCP1252: write8bit(cp1252_cp);
        seCP866:  write8bit(cp866_cp);
        seKOI8R:  write8bit(KOI8_R_cp);
        seLatin1: write8bit(Latin1_cp);
        else raise ECharsetError.Create('неизвестная кодировка');
    end;
end;


procedure TLStream.write_string(s: unicodestring);
var i: integer;
begin
    {$IFDEF ENDIAN_LITTLE}
    if encoding=seUTF16LE then begin WriteBytes(s[1],length(s)*2,true); Exit; end;
    {$ENDIF}
    i := 1;
    while i<=length(s) do
        if is_surrogate(s[i]) then begin
            WriteCodePoint(surrogate_ord(s[i],s[i+1]));
            Inc(i,2);
        end
        else begin
            WriteCodePoint(ord(s[i]));
            Inc(i);
        end;
end;

procedure TLStream.write_WORD(w: WORD);
begin
    WriteBytes(w, 2, true);
end;

procedure TLStream.write_DWORD(dw: DWORD);
begin
    WriteBytes(dw, 4, true);
end;

procedure TLStream.write_i8(i: Int64);
begin
    WriteBytes(i, 1, true);
end;

procedure TLStream.write_i16(i: Int64);
begin
    WriteBytes(i, 2, true);
end;

procedure TLStream.write_i32(i: Int64);
begin
    WriteBytes(i, 4, true);
end;

procedure TLStream.write_i64(i: Int64);
begin
    WriteBytes(i, 8, true);
end;


procedure TLStream.write_single(f: double);
var d: record case byte of 0: (f: single); 1: (b: array [1..SizeOf(single)] of Byte); end;
begin
    d.f := f;
    WriteBytes(d.b[1], SizeOf(single), true);
end;

procedure TLStream.write_double(f: double);
var d: record case byte of 0: (f: double); 1: (b: array [1..SizeOf(double)] of Byte); end;
begin
    d.f:=f;
    WriteBytes(d.b[1], SizeOf(double), true);
end;

procedure TLStream.close_stream;
begin
    FreeAndNil(stream);
end;

function TLStream.active: boolean;
begin
    result := stream <> nil;
end;

initialization
    stdin := TLStdIn.Create(TInputPipeStream.Create({$IFDEF WINDOWS}GetStdHandle(std_input_handle){$ELSE}0{$ENDIF}));

finalization
    stdin.Free;

end.   //426 659

