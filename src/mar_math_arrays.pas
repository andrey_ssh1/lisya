﻿unit mar_math_arrays;

{$mode delphi}{$H+}

interface

uses
    sysutils, math, ucomplex;


type
    TDoubles = array of double;
    TDoublesA = array[0..0] of double;
    PDoubles = ^TDoublesA;

    TComplexes = array of COMPLEX;
    TComplexesA = array[0..0] of COMPLEX;
    PComplexes = ^TComplexesA;


procedure doubles_append(var a: TDoubles; b: Double); overload;
procedure doubles_append(var a: TDoubles; b: TDoubles); overload;

function doubles_round_to(a: TDoubles; digits: TRoundToRange): TDoubles;
function doubles_abs(a: TDoubles): TDoubles;
function doubles_arg(a: TDoubles): TDoubles;

function doubles_mul(a, b: TDoubles): TDoubles; overload;
function doubles_mul(a: TDoubles; b: real): TDoubles; overload;
function doubles_mul(a: TDoubles; b: COMPLEX): TComplexes; overload;
function doubles_div(a, b: TDoubles): TDoubles; overload;
function doubles_div(a: TDoubles; b: real): TDoubles; overload;
function doubles_div(a: TDoubles; b: COMPLEX): TComplexes; overload;
function doubles_add(a, b: TDoubles): TDoubles; overload;
function doubles_add(a: TDoubles; b: real): TDoubles; overload;
function doubles_sub(a, b: TDoubles): TDoubles; overload;
function doubles_equal(a, b: TDoubles): boolean;
function doubles_rms(a: TDoubles): real;

function doubles_to_complexes(a: TDoubles): TComplexes;

function doubles_index_of_max(a: TDoubles): integer;
function doubles_index_of_min(a: TDoubles): integer;


procedure complexes_append(var c1: TComplexes; c2: TComplexes);

function complexes_abs(a: TComplexes): TDoubles;
function complexes_arg(a: TComplexes): TDoubles;
function complexes_re(a: TComplexes): TDoubles;
function complexes_im(a: TComplexes): TDoubles;

function complexes_mul(a, b: TComplexes): TComplexes; overload;
function complexes_mul(a: TComplexes; b: COMPLEX): TComplexes; overload;
function complexes_div(a, b: TComplexes): TComplexes; overload;
function complexes_div(a: TComplexes; b: COMPLEX): TComplexes; overload;
function complexes_add(a, b: TComplexes): TComplexes; overload;
function complexes_add(a: TComplexes; b: COMPLEX): TComplexes; overload;
function complexes_sub(a, b: TComplexes): TComplexes; overload;
function complexes_equal(a, b: TComplexes): boolean;
function complexes_sum(a: TComplexes): complex; overload;
function complexes_sum(p: PComplexes; cnt: integer): complex; overload;
function complexes_mean(a: TComplexes): complex; overload;
function complexes_mean(p: PComplexes; cnt: integer): complex; overload;


implementation


////////////////////////////////////////////////////////////////////////////////
//// TDoubles //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

procedure doubles_append(var a: TDoubles; b: Double);
begin
    SetLength(a, Length(a)+1);
    a[high(a)] := b;
end;


procedure doubles_append(var a: TDoubles; b: TDoubles);
var i, start: integer;
begin
    start := Length(a);
    SetLength(a, Length(a)+Length(b));
    for i:=0 to high(b) do a[start+i] := b[i];
end;


function doubles_round_to(a: TDoubles; digits: TRoundToRange): TDoubles;
var RV , f: Double; i: integer;
begin
    RV := IntPower(10, Digits);

    SetLength(result, Length(a));
    for i := 0 to high(result) do
        begin
            f := a[i]/RV;
            if (f <= 9223372036854775807) and (f >= -9223372036854775808)
            then result[i] := Round(f)*RV
            else result[i] := a[i];
    	end;
end;


function doubles_abs(a: TDoubles): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := abs(a[i]);
end;


function doubles_arg(a: TDoubles): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do
        if a[i]<0 then result[i] := pi else result[i] := 0;
end;


function doubles_mul(a, b: TDoubles): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to min(high(a), high(b)) do result[i] := a[i] * b[i];
end;


function doubles_mul(a: TDoubles; b: real): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := a[i] * b;
end;


function doubles_mul(a: TDoubles; b: COMPLEX): TComplexes;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := a[i] * b;
end;

function doubles_div(a, b: TDoubles): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to min(high(a), high(b)) do result[i] := a[i] / b[i];
end;


function doubles_div(a: TDoubles; b: real): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := a[i] / b;
end;


function doubles_div(a: TDoubles; b: COMPLEX): TComplexes;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := a[i] / b;
end;


function doubles_add(a, b: TDoubles): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to min(high(a), high(b)) do result[i] := a[i] + b[i];
end;


function doubles_add(a: TDoubles; b: real): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := a[i] + b;
end;


function doubles_sub(a, b: TDoubles): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to min(high(a), high(b)) do result[i] := a[i] - b[i];
end;


function doubles_equal(a, b: TDoubles): boolean;
var i: integer;
begin
    if a=b then Exit(true);
    if Length(a)<>Length(b) then Exit(false);

    for i := 0 to high(a) do if a[i]<>b[i] then Exit(false);
    result := true;
end;


function doubles_rms(a: TDoubles): real;
begin
    result := sqrt(sumofsquares(a)/Length(a));
end;


function doubles_to_complexes(a: TDoubles): TComplexes;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := a[i];
end;


function doubles_index_of_max(a: TDoubles): integer;
var f_max: double; i: integer;
begin
    if a=nil then Exit(-1);
    f_max := a[0];
    result := 0;
    for i := 1 to high(a) do
        if a[i]>f_max
        then
            begin
                result := i;
                f_max := a[i];
			end;
end;


function doubles_index_of_min(a: TDoubles): integer;
var f_min: double; i: integer;
begin
    if a=nil then Exit(-1);
    f_min := a[0];
    result := 0;
    for i := 1 to high(a) do
        if a[i]<f_min
        then
            begin
                result := i;
                f_min := a[i];
			end;
end;


////////////////////////////////////////////////////////////////////////////////
//// TComplexes ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

procedure complexes_append(var c1: TComplexes; c2: TComplexes);
var i, b: integer;
begin
    b := Length(c1);
    SetLength(c1, Length(c1)+Length(c2));
    for i:=0 to high(c2) do c1[b+i] := c2[i];
end;


function complexes_abs(a: TComplexes): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := cmod(a[i]);
end;


function complexes_arg(a: TComplexes): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := carg(a[i]);
end;


function complexes_re(a: TComplexes): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
        for i := 0 to high(a) do result[i] := a[i].re;
end;


function complexes_im(a: TComplexes): TDoubles;
var i: integer;
begin
    SetLength(result, Length(a));
        for i := 0 to high(a) do result[i] := a[i].im;
end;


function complexes_mul(a, b: TComplexes): TComplexes;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to min(high(a), high(b)) do result[i] := a[i] * b[i];
end;


function complexes_mul(a: TComplexes; b: COMPLEX): TComplexes;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := a[i] * b;
end;


function complexes_div(a, b: TComplexes): TComplexes;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := a[i] / b[i];
end;


function complexes_div(a: TComplexes; b: COMPLEX): TComplexes;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := a[i] / b;
end;


function complexes_add(a, b: TComplexes): TComplexes;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to min(high(a), high(b)) do result[i] := a[i] + b[i];
end;


function complexes_add(a: TComplexes; b: COMPLEX): TComplexes;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to high(a) do result[i] := a[i] + b;
end;


function complexes_sub(a, b: TComplexes): TComplexes;
var i: integer;
begin
    SetLength(result, Length(a));
    for i := 0 to min(high(a), high(b)) do result[i] := a[i] - b[i];
end;


function complexes_equal(a, b: TComplexes): boolean;
var i: integer;
begin
    if a=b then Exit(true);
    if Length(a)<>Length(b) then Exit(false);

    for i := 0 to high(a) do if not csamevalue(a[i], b[i]) then Exit(false);
    result := true;
end;


function complexes_sum(a: TComplexes): complex;
var i: integer;
begin
    result := 0;
    for i:= 0 to high(a) do result := result + a[i];
end;


function complexes_sum(p: PComplexes; cnt: integer): complex;
var i: integer;
begin
    {$R-}
    result := 0;
    for i:= 0 to cnt-1 do result := result + p^[i];
    {$R+}
end;


function complexes_mean(a: TComplexes): complex;
begin
    result := complexes_sum(a)/Length(a);
end;


function complexes_mean(p: PComplexes; cnt: integer): complex;
begin
    result := complexes_sum(p, cnt)/cnt;
end;


end.

