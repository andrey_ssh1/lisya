﻿unit lisya_thread;

{$mode delphi}

interface

uses
    {$IFDEF LINUX}
    cwstring, ctypes,
    {$ENDIF}
    Classes, SysUtils, dlisp_eval, dlisp_values, lisya_ifh, lisya_gc
    , lisya_exceptions
    , lisya_predicates
    {$IFDEF NETWORK} , ssockets {$ENDIF}
    , mar;

type

    TAction = procedure of object;

    { TEvaluationThread }

    TEvaluationThread = class (TThread)
    private
        fflow: dlisp_eval.TEvaluationFlow;

        fProc: TVSubprogram;
        fParams: TVList;
        fN: integer;

        fComplitedEvent: pRTLEvent;
        eclass, emessage, estack: unicodestring;
        fError: boolean;
        fAction: TAction;
        procedure fMap;
        procedure fReject;
        procedure fFilter;
    protected
        procedure Execute; override;
    public
        fResult: TValue;
        constructor Create;
        destructor Destroy; override;
        procedure RunMap;
        procedure RunReject;
        procedure RunFilter;
        procedure WaitEnd;
        function WaitResult: TValue;
    end;

    { TLThreadBody }

    TLThreadBody = class (TThread)
    private
        fflow: TEvaluationFlow;
        fexpr: TVList;
        fresult: TValue;
        eclass, emessage, estack: unicodestring;
        fError: boolean;
    protected
        procedure Execute; override;
    public
        constructor Create(expr: TVList);
        destructor Destroy; override;
        function WaitResult: TValue;
    end;

    TLThread = class (TCountingObject)
    private
        thread: TLThreadBody;
        fname: unicodestring;
    public
        constructor Create(expr: TVList);
        destructor Destroy; override;
        function description: unicodestring; override;
        function WaitResult: TValue;
    end;

    TVThread = TVPointer<TLThread>;

    {$IFDEF NETWORK}

    { TServerThread }

    TServerThread = class (TThread)
    private
        cs: TRTLCriticalSection;
        fAcceptedEvent: pRTLEvent;
        server: TInetServer;
        connections: array of TSocketStream;
        procedure OnConnect(sender: TObject; data: TSocketStream);
    protected
        procedure Execute; override;
    public
        constructor Create(port: word; address: unicodestring = '');
        destructor Destroy; override;
        function WaitConnection(): TSocketStream;
        function PopConnection(): TSocketStream;
    end;

    { TLServer }

    TLServer = class (TCountingObject)
        server: TServerThread;
        constructor Create(port: word; address: unicodestring = '');
        destructor Destroy; override;
        function Description():unicodestring; override;
    end;

    TVServer = TVPointer<TLServer>;

    {$ENDIF}



function ifh_map_th(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;
function ifh_reject_th(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;
function ifh_filter_th(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;

var threads_pool: array of TEvaluationThread;


var th: boolean = false;


procedure set_threads_count(n: integer);
function CPU_Count: integer;

function tpThread(V: TValue): boolean;
function tpServer(V: TValue): boolean;

implementation


var task_cs: TRTLCriticalSection;
    complited_event: pRTLEvent;
    task_queue: array of TValue;
    task_i: integer = 0;

{$IFDEF LINUX}
function sysconf(i:cint):clong;cdecl;external name 'sysconf';
{$ENDIF}

function CPU_Count: integer;
begin
    {$IFDEF LINUX}
        result := sysconf(83);  // вероятно это константа _SC_NPROCESSORS_CONF или _SC_NPROCESSORS_ONLN
    {$ELSE}
        result := GetCPUCount;
    {$ENDIF}
    if result <1 then result := 1;
end;

function tpThread(V: TValue): boolean;
begin
    result := V is TVThread;
end;

function tpServer(V: TValue): boolean;
begin
    {$IFDEF NETWORK}
    result := V is TVServer;
    {$ELSE} result := false;{$ENDIF}
end;


procedure set_threads_count(n: integer);
var i, n_old: integer;
begin
    if n<Length(threads_pool) then begin
        for i := n to high(threads_pool) do threads_pool[i].Free;
        SetLength(threads_pool, n);
    end;

    if n>Length(threads_pool) then begin
        n_old := Length(threads_pool);
        SetLength(threads_pool, n);
        for i := n_old to high(threads_pool) do
            threads_pool[i] := TEvaluationThread.Create;
    end;
end;


procedure pool_set_proc(P: TVSubprogram);
var i: integer;
begin
    if P=nil
    then
        for i := 0 to high(threads_pool) do FreeAndNil(threads_pool[i].fProc)
    else
        for i := 0 to high(threads_pool) do begin
            threads_pool[i].fProc.Free;
            threads_pool[i].fProc:=separate(P) as TVSubprogram;
        end;
end;

procedure pool_wait_end;
var i: integer;
begin
    for i := 0 to high(threads_pool) do threads_pool[i].WaitEnd;
end;

function ifh_map_th(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;
var i,j: integer;
begin
    //TODO: возможна утечка содержимого очереди заданий при возникновении исключения
    if (not th) and (length(threads_pool)>1) then try
        th := true;
        result := nil;
        pool_set_proc(P);
        SetLength(task_queue, PL.L[0].Count);
        task_i := 0;
        for i := 0 to high(task_queue) do begin
            task_queue[i] := TVList.Create();
            for j := 0 to PL.high do
                (task_queue[i] as TVList).Add(separate(PL.L[j][i]));
        end;
        for i := 0 to high(threads_pool) do threads_pool[i].RunMap;
        pool_wait_end;
        result := TVList.Create;
        for i := 0 to high(task_queue) do result.Add(task_queue[i]);
        SetLength(task_queue, 0);
        pool_set_proc(nil);
    finally
        th := false;
    end
    else result := ifh_map(call, P, PL);
end;


function ifh_reject_th(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;
var i: integer;
begin
    //TODO: возможна утечка содержимого очереди заданий при возникновении исключения
    if (not th) and (length(threads_pool)>1) then try
        th := true;
        result := nil;
        pool_set_proc(P);
        SetLength(task_queue, PL.Count);
        task_i := 0;
        for i := 0 to high(task_queue) do task_queue[i] := separate(PL[i]);
        for i := 0 to high(threads_pool) do threads_pool[i].RunReject;
        pool_wait_end;
        result := TVList.Create;
        for i := 0 to high(task_queue) do
            if task_queue[i]<>nil then result.Add(task_queue[i]);
        SetLength(task_queue, 0);
        pool_set_proc(nil);
    finally
        th := false;
    end
    else result := ifh_filter(PL, call, tpNIL) as TVList;
end;

function ifh_filter_th(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;
var i: integer;
begin
    //TODO: возможна утечка содержимого очереди заданий при возникновении исключения
    if (not th) and (length(threads_pool)>1) then try
        th := true;
        result := nil;
        pool_set_proc(P);
        SetLength(task_queue, PL.Count);
        task_i := 0;
        for i := 0 to high(task_queue) do task_queue[i] := separate(PL[i]);
        for i := 0 to high(threads_pool) do threads_pool[i].RunFilter;
        pool_wait_end;
        result := TVList.Create;
        for i := 0 to high(task_queue) do
            if task_queue[i]<>nil then result.Add(task_queue[i]);
        SetLength(task_queue, 0);
        pool_set_proc(nil);
    finally
        th := false;
    end
    else result := ifh_filter(PL, call, tpTrue) as TVList;
end;

function  NextTask(out tn: integer): boolean;
begin
    EnterCriticalSection(task_cs);
    result := task_i<Length(task_queue);
    tn := task_i;
    Inc(task_i);
    LeaveCriticalSection(task_cs);
end;



{ TLThread }

constructor TLThread.Create(expr: TVList);
begin
    inherited Create;
    fname := expr.AsString;
    thread := TLThreadBody.Create(expr);
end;


destructor TLThread.Destroy;
begin
    thread.FreeOnTerminate:=true;
//    thread.Destroy;
    inherited Destroy;
end;


function TLThread.description;
begin
    if thread.Finished
    then result := 'THREAD FINISHED '+fname
    else result := 'THREAD '+fname
end;


function TLThread.WaitResult: TValue;
begin
    result := thread.WaitResult;
end;

{ TLThreadBody }

procedure TLThreadBody.Execute;
begin
    try
        //WriteLn('TLThreadBody.Execure: '+fexpr.AsString);
        fresult := fflow.call(fexpr.ValueList);
        //WriteLn('TLThreadBody.Execure: '+fresult.AsString);
    except
        on E:ELE do begin
            fError := true;
            eclass := E.EClass;
            emessage := E.Message;
            estack := E.EStack;
        end;
        on E:Exception do begin
            fError := true;
            eclass := '!'+E.ClassName+'!';
            emessage := E.Message;
            estack := 'thread';
        end;
    end;
end;


constructor TLThreadBody.Create(expr: TVList);
begin
    fexpr := separate(expr) as TVList;
    //WriteLn('TLThreadBody: '+fexpr.AsString);
    fflow := TEvaluationFlow.CreatePure();
    fResult := nil;
    fError := false;
    FreeOnTerminate := false;
    inherited Create(false);
end;


destructor TLThreadBody.Destroy;
begin
    WaitFor;
    fflow.Free;
    fResult.Free;
    fExpr.Free;
    inherited;
end;


function TLThreadBody.WaitResult: TValue;
begin
    result := nil;
    self.WaitFor;
    if fError then raise ELE.Create(emessage, eclass, estack);
    result := fResult.Copy;
end;



{ TEvaluationThread }

procedure TEvaluationThread.fMap;
var PL: TVList; expr: TValues;
begin
    PL := task_queue[fN] as TVList;

    SetLength(expr, PL.Count+1);
    expr[0] := fProc;
    move(PL[0], expr[1], SizeOf(TValue)*PL.Count);

    task_queue[fN] := fFlow.call(expr);
    PL.Free;
end;


procedure TEvaluationThread.fReject;
var tmp: TValue; expr: TValues;
begin
    try
        expr := TValues.Create(fProc, task_queue[fN]);
        tmp := fFlow.call(expr);
        if tpTrue(tmp) then FreeAndNil(task_queue[fN]);
    finally
        tmp.Free;
    end;
end;


procedure TEvaluationThread.fFilter;
var tmp: TValue; expr: TValues;
begin
    try
        expr := TValues.Create(fProc, task_queue[fN]);
        tmp := fFlow.call(expr);
        if tpNIL(tmp) then FreeAndNil(task_queue[fN]);
    finally
        tmp.Free;
    end;
end;


procedure TEvaluationThread.Execute;
begin
    while not self.Terminated do begin
        try
            fError := false;

            while NextTask(fN) do fAction;
        except
            on E:ELE do begin
                fError := true;
                eclass := E.EClass;
                emessage := E.Message;
                estack := E.EStack;
            end;
            on E:Exception do begin
                fError := true;
                eclass := '!'+E.ClassName+'!';
                emessage := E.Message;
                estack := 'thread';
            end;
        end;
        RtlEventSetEvent(fComplitedEvent);
        self.Suspended := true;
    end;
end;


constructor TEvaluationThread.Create;
begin
    fProc := nil;
    fParams := nil;
    self.fflow := TEvaluationFlow.CreatePure();
    self.fResult := nil;
    self.FreeOnTerminate := false;
    fComplitedEvent := RTLEventCreate;
    inherited Create(true);
end;


destructor TEvaluationThread.Destroy;
begin
    fflow.Free;
    fResult.Free;
    fProc.Free;
    fParams.Free;
    RTLeventdestroy(fComplitedEvent);
    self.Terminate;
    self.Suspended := false;
    inherited;
end;


procedure TEvaluationThread.RunMap;
begin
    RTLEventResetEvent(fComplitedEvent);
    fAction := fMap;
    Start;
end;


procedure TEvaluationThread.RunReject;
begin
    RTLEventResetEvent(fComplitedEvent);
    fAction := fReject;
    Start;
end;


procedure TEvaluationThread.RunFilter;
begin
    RTLEventResetEvent(fComplitedEvent);
    fAction := fFilter;
    Start;
end;


procedure TEvaluationThread.WaitEnd;
begin
    RtlEventWaitFor(fComplitedEvent);
    if fError then raise ELE.Create(emessage, eclass, estack);
end;


function TEvaluationThread.WaitResult: TValue;
begin
    RtlEventWaitFor(fComplitedEvent);

    result := fResult;
    fResult := nil;
    if fError then raise ELE.Create(emessage, eclass, estack);
end;


{$IFDEF NETWORK}

{ TServerThread }

procedure TServerThread.OnConnect(sender: TObject; data: TSocketStream);
begin
    try
        EnterCriticalSection(cs);
        SetLength(connections,length(connections)+1);
        connections[high(connections)] := data;
    finally
        LeaveCriticalSection(cs);
        RtlEventSetEvent(fAcceptedEvent);
    end;
end;

procedure TServerThread.Execute;
begin
  server.StartAccepting;
end;

constructor TServerThread.Create(port: word; address: unicodestring);
begin
    InitCriticalSection(cs);
    if address<>''
    then server := TInetServer.Create(address,port)
    else server := TInetServer.Create(port);
    server.OnConnect := OnConnect;
    fAcceptedEvent := RTLEventCreate;
    RTLEventResetEvent(fAcceptedEvent);
    inherited Create(false);
end;

destructor TServerThread.Destroy;
begin
    server.StopAccepting(true);
    server.SetNonBlocking;
    server.Free;
    RTLEventDestroy(fAcceptedEvent);
    DoneCriticalSection(cs);
    self.Terminate;
    self.Suspended:=false;
    inherited Destroy;
end;

function TServerThread.WaitConnection: TSocketStream;
begin
    RtlEventWaitFor(fAcceptedEvent);
    result := PopConnection;
end;

function TServerThread.PopConnection: TSocketStream;
var i: integer;
begin
    result := nil;
    try
        EnterCriticalSection(cs);
        if length(connections)=0 then Exit;
        result := connections[0];
        for i := 1 to high(connections) do connections[i-1] := connections[i];
        SetLength(connections,length(connections)-1);
        if length(connections)=0 then RTLEventResetEvent(fAcceptedEvent);
    finally
        LeaveCriticalSection(cs);
    end;
end;

{ TLServer }

constructor TLServer.Create(port: word; address: unicodestring);
begin
    inherited Create;
    server := TServerThread.Create(port,address);
end;

destructor TLServer.Destroy;
begin
    server.Destroy;
    inherited Destroy;
end;


function TLServer.Description: unicodestring;
begin
    result := 'SERVER '+server.server.Host+':'+IntToStr(server.server.Port);
end;

{$ENDIF}

initialization
    set_threads_count(CPU_count);
    InitCriticalSection(task_cs);
    complited_event := RTLEventCreate;

finalization
    set_threads_count(0);
    DoneCriticalSection(task_cs);
    RTLeventdestroy(complited_event);
end.

