﻿unit lisya_ifh_subprograms;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils
    ,dlisp_values
    ,lisya_predicates
    ,lisya_sign
    ,lisya_symbols
    ,lisya_exceptions
    ,lisya_gc;


function ifh_apply(P: TVSubprogram; PL: TVList; call: TCallProc): TValue;

function ifh_composition(head: TVSubprogram; PL: TVList): TVRoutine;

function ifh_complement(f: TVSubprogram): TVFunction;

procedure pure_value_pf1(V: TValue);


function ifh_curry(subprogram: TVSubprogram; PL: TVList): TVRoutine;


implementation

uses dlisp_eval;


function ifh_apply(P: TVSubprogram; PL: TVList; call: TCallProc): TValue;
var expr, rest: TValues; i: integer;
begin
    result := nil;
    rest := PL.L[PL.high].ValueList;

    SetLength(expr, 1 + PL.Count-1 + Length(rest));
    expr[0] := P;
    for i := 0 to PL.high-1 do expr[i+1] := PL[i];
    for i := 0 to high(rest) do expr[i+1+PL.Count-1] := rest[i];

    result := call(expr);
end;


function extract_signature(P: TVSubprogram): TSignature;
begin
    if P is TVSubprogramComplex
    then
        result := (P as TVSubprogramComplex).sign

    else if tpPredicate(P) or tpInternalUnaryRoutine(P)
    then
        result := unary_sign

    else
        raise ELE.Create('invalid subprogram type','internal/extract_signature');
end;


procedure one_parameter_subprogram_pf(P: TVSubprogram);
var sign: TSignature; res: boolean;
begin
    sign := extract_signature(P);
    case sign[0].mode of
        spmReq: res := sign[0].req_count=1;
        spmOpt: res := (sign[0].req_count<=1) and (Length(sign)>1);
        spmKey: res := sign[0].req_count=1;
        spmRest: res := sign[0].req_count<=1;
        else raise ELE.Create('','internal/one_parameter_function');
    end;
    if not res
    then raise ELE.Create(P.AsString+' is not one parameter function','subprogram/parameters');
end;


function ifh_composition(head: TVSubprogram; PL: TVList): TVRoutine;
var i: integer; f_head: TVRoutine; expr: TVList;
begin
    //объединяет несколько подпрограмм в одну
    //результирующая подпрограмма наследует сигнатуру от головной
    //при этом сигнатура головной подпрограммы заменяется на плоскую для упрощения
    //генерации, вызывающего её выражения
    //если головная подпрограмма имеет остаточный параметр, полученный в результате
    //каррирования, то он переносится в остаточный параметр композиции

    result := nil;
    for i := 0 to PL.high do one_parameter_subprogram_pf(PL[i] as TVSubprogram);

    if tpSubprogramPure1(head) and tpListOfSubprogramsPure(PL)
    then result := TVFunction.Create
    else result := TVProcedure.Create;

    result.nN:=-1;
    result.stack := TVSymbolStack.Create;
    result.sign := extract_signature(head);
    result.rest := nil;

    expr := TVList.Create([head.Copy]);
    for i := 1 to high(result.sign) do
        expr.Add(TVSymbol.Create(result.sign[i].name));

    if (head is TVRoutine)
    then
        begin
            f_head := expr[0] as TVRoutine;
            if f_head.rest<>nil
            then
                begin
                    result.rest := f_head.rest;
                    f_head.rest := nil;
                end;
            f_head.sign := system.copy(f_head.sign);
            f_head.sign[0].req_count := Length(f_head.sign)-1;
            f_head.sign[0].mode := spmReq;
        end

    else if (head is TVInternalRoutine) or (head is TVInternalArrayRoutine)
    then
        (expr[0] as TVSubprogramComplex).sign := plain_sign(result.sign);

    for i := 0 to PL.high do expr := TVList.Create([PL[i].Copy, expr]);
    result.body := TVList.Create([expr]);
end;



////////////////////////////////////////////////////////////////////////////////
///// CURRY ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

procedure ifh_curry_routine(routine: TVRoutine; PL: TVList);
var _PL, h_PL, params, rest: TValues; i: integer;
begin
    _PL := PL.ValueList;
    SetLength(h_PL, Length(_PL)+1);
    h_PL[0] := nil;
    {$R-}move(_PL[0], h_PL[1], SizeOf(TValue)*Length(_PL));{$R+}

    ifh_bind_params(routine.sign, h_PL, params, rest);

    routine.sign := system.Copy(routine.sign);

    if rest<>nil
    then
        begin
            if routine.rest=nil then routine.rest := TVList.Create();
            for i := 0 to high(rest) do routine.rest.Add(rest[i].Copy);
		end;

    for i := high(params) downto 0 do
        if not ((params[i]=nil) or vpSymbol__(params[i]))
        then
            begin
                routine.stack.new_var(routine.sign[i+1].name, params[i].Copy, true);
                delete_from_sign(routine.sign, i+1);
            end;
end;


function wrap_internal_in_routine(internal: TVSubprogramComplex): TVRoutine;
var expr: TVList; i: integer;
begin
    // эта функция создаёт обёртку типа TVRoutine вокруг TVInternalRoutine,
    //что позволяет в дальнейшем писать код только для обработки TVRoutine
    //например при каррировании
    if tpInternalPureComplexRoutine(internal)
    then result := TVFunction.Create
    else result := TVProcedure.Create;

    result.nN := internal.nN;
    result.sign := internal.sign;
    result.stack := TVSymbolStack.Create;
    result.rest := nil;

    internal.sign := plain_sign(internal.sign);

    expr := TVList.Create([internal]);
    for i := 1 to high(result.sign) do
        expr.Add(TVSymbol.Create(result.sign[i].name));

    //функция с rest параметром помечается, чтобы при вызове можно было
    //преобразовать остаток в TVListRest
    if (internal is TVInternalArrayRoutine) and (result.sign[0].mode = spmRest)
    then
        internal.sign[0].mode := spmList;

    result.body := TVList.Create([expr]);
end;


function wrap_unary_in_routine(unary: TVSubprogram): TVRoutine;
begin
    // эта функция создаёт обёртку типа TVRoutine вокруг TVInternalUnaryRoutine
    //или TVPredicate

    if (unary is TVInternalUnaryFunction) or (unary is TVPredicate)
    then result := TVFunction.Create
    else result := TVProcedure.Create;

    result.nN := unary.nN;
    result.sign := unary_sign;
    result.stack := TVSymbolStack.Create;
    result.rest := nil;

    result.body := TVList.Create([TVList.Create([unary, TVSymbol.Create('X')])]);
end;


function ifh_curry(subprogram: TVSubprogram; PL: TVList): TVRoutine;
var p: TVSubprogram;
begin
    p := subprogram.Copy() as TVSubprogram;

    if tpRoutine(p)
    then
        result := p as TVRoutine

    else if tpInternalSubprogramComplex(p)
    then
        result := wrap_internal_in_routine(p as TVSubprogramComplex)

    else if (p is TVInternalUnaryRoutine) or (p is TVPredicate)
    then
        result := wrap_unary_in_routine(p as TVSubprogram)

    else
        begin
            p.Free;
            raise ELE.Create(subprogram.AsString(), 'invalid parameters/curry');
        end;

    ifh_curry_routine(result, PL);
end;



function ifh_complement(f: TVSubprogram): TVFunction;
var sp: TVSubprogram; body: TValues;
begin
    //создаёт из функции, возвращающей булево значение функцию, возвращающую
    //обратное значение

    sp := f.Copy as TVSubprogram;

    if tpRoutine(sp)
    then
        result := sp as TVFunction

    else if tpInternalSubprogramComplex(sp)
    then
        result := wrap_internal_in_routine(sp as TVSubprogramComplex) as TVFunction

    else if (sp is TVInternalUnaryRoutine) or ((sp is TVPredicate) and not (sp as TVPredicate).fAssert)
    then
        result := wrap_unary_in_routine(sp as TVSubprogram) as TVFunction

    else
        begin
            sp.Free;
            raise ELE.Create(f.AsString(), 'invalid parameters/complement');
        end;

    result.body.CopyOnWrite();
    body := result.body.ValueList;
    //TODO: импортируется тело if_not из dlisp_eval и создаётся функция NOT, лапшекод, однако
    body[high(body)] := TVList.Create([
        TVInternalUnaryFunction.Create(ifu_not, symbol_n('NOT')),
        body[high(body)]]);
end;


////////////////////////////////////////////////////////////////////////////////
///// PURE /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

procedure raise_impure(msg: unicodestring);
begin
    raise ELE.Create(msg, 'impurity');
end;


procedure pure_block_pf(V: TValue);
var proc: TVRoutine; ls: TVList; rec: TVRecord; ht: TVHashTable; ret: TVReturn;
    ss: TVSymbolStack;
    i: integer;
begin
    if (V is TVInternalProcedure)
        or (V is TVPointerBase)
        or (V is TVZipArchivePointer)
        or (V is TVProcedureForwardDeclaration)
        or (V is TVInternalUnaryProcedure)
        or (V is TVInternalArrayProcedure)
    then raise_impure(V.AsString);

    if V is TVRoutine then begin
        proc := V as TVRoutine;
        pure_block_pf(proc.stack);
        pure_block_pf(proc.rest);
        pure_block_pf(proc.body);
    end;

    if V is TVSymbolStack then begin
        ss := V as TVSymbolStack;
        for i := 0 to high(ss.stack) do
            if not ss.stack[i].V.constant
            then raise_impure('Variable '+symbol_uname(ss.stack[i].N));
    end

    else if V is TVList then begin
        ls := V as TVList;
        for i := 0 to ls.high do pure_block_pf(ls[i]);
    end

    else if V is TVRecord then begin
        rec := V as TVRecord;
        for i := 0 to rec.count-1 do pure_block_pf(rec[i]);
    end

    else if V is TVHashTable then begin
        ht := V as TVHashTable;
        for i := 0 to ht.Count-1 do begin
            pure_block_pf(ht[i]);
            pure_block_pf(ht.look_key[i]);
        end;
    end

    else if V is TVReturn then begin
        ret := V as TVReturn;
        pure_block_pf(ret.value);
    end;
end;


procedure pure_value_pf1(V: TValue);
var P: PVariable; pp: PVariables; i: integer;
begin try
    P := NewVariable(V, true);
    pp := extract_variables1(P);
    for i := 0 to high(pp) do pure_block_pf(pp[i].V);
finally
    Dispose(P);
end; end;


end.

