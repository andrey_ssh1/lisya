﻿unit dlisp_eval;

{$mode delphi}

{$ASSERTIONS ON}


interface

uses
    {$IFDEF WINDOWS}
    Windows, ShellApi, LazUnicode,
    {$ENDIF}
    {$IFDEF LINUX}
    cwstring,
    {$ENDIF}
    {$IFDEF GUI}
    Interfaces, forms,
    lisya_form,
    {$ENDIF}
    process,
    Classes, SysUtils, math, ucomplex, fileutil, LazFileUtils,
    zstream,

    {$IFDEF NETWORK}fphttpclient in './fpc_backport/fphttpclient.pp',{$ENDIF}
    regexpr in './fpc_backport/regexpr.pas'
    ,dlisp_values, dlisp_read, lisya_xml, mar, mar_math
    ,lisya_type_converters
    ,lisya_predicates
    ,lisya_ifh
    ,lisya_ifh_subprograms
    ,lisya_directory
    ,lisya_sign
    ,lisya_zip
    ,lisia_charset
    ,lisya_exceptions
    ,lisya_streams
    ,lisya_symbols
    ,lisya_gc
    ,lisya_heuristic
    ,lisya_protected_objects
    ,lisya_regexp
    ,lisya_sql
    {$IFDEF ODBC}  , odbc_module {$ENDIF}
    {$IFDEF ORACLE}, oracle_module{$ENDIF}
    {$IFDEF SQLITE}, lisya_sqlite {$ENDIF}
    ,lisya_packages  //должен находиться после odbc_module, в противном случае
                     //финализация odbcconn выполняет отключение раньше чем
                     //освобождаются переменные соединения (финализация lisya_packages)
                     //возникает ошибка "Could not free ODBC Environment handle"
    ,db
    ,variants
    ,TKZ, TKZ_types
    ,lisya_tkz_solver_poc
    ,mar_math_sp, mar_math_arrays, mar_tensor, mar_math_graph
    , mar_zemq
    ;


type

    { TEvaluationFlow }

    TEvaluationFlow = class
        stack: TVSymbolStack;
        main_stack: TVSymbolStack;
        pure_mode: boolean;

        constructor Create(parent_stack: TVSymbolStack);
        constructor CreatePure;
        destructor Destroy; override;


        function oph_loop_body(PL: TValues; start: integer): TValue;
        procedure oph_bind(S, V: TValue; constant: boolean);
        function oph_execute_file(fn: unicodestring): boolean;
        procedure oph_bind_package(name: unicodestring; import: boolean = false);
        function oph_eval_index(C: TVCompound; id: TValue): integer;
        procedure oph_eval_selector(C: TVCompound; var id: TValue);
        function oph_debug(PL: TVList): TValue;
        function oph_exception_class_match(exception_classes,
                                        handler_classes: unicodestring): boolean;


        function oph_for_iterator(PL: TValues; iterator: TVSubprogram): TValue;
        function oph_for_range(PL: TValues; first, last: integer): TValue;

        function opl_elt(PL: TValues): TVChainPointer;
        function opl_key(PL: TValues): TVChainPointer;

        function op_and(PL: TValues): TValue;
    {m} function op_append(PL: TValues): TValue;
        function op_assemble(PL: TValues): TValue;
        function op_block(PL: TValues; start: integer = 1): TValue;
        function op_case(PL: TValues): TValue;
        function op_cond(PL: TValues): TValue;
        function op_const(PL: TValues): TValue;
        function op_debug(PL: TValues): TValue;
    {m} function op_default(PL: TValues): TValue;
        function op_elt(PL: TValues): TValue;
        function op_error(PL: TValues): TValue;
        function op_execute_file(PL: TValues): TValue;
    {m} function op_extract(PL: TValues): TValue;
        function op_for(PL: TValues): TValue;
        function op_function(PL: TValues): TValue;
        function op_goto(PL: TValues): TValue;
        function op_if(PL: TValues): TValue;
    {m} function op_insert(PL: TValues): TValue;
        function op_key(PL: TValues): TValue;
        function op_let(PL: TValues): TValue;
        function op_macro_symbol(PL: TValues): TValue;
        function op_or(PL: TValues): TValue;
        function op_package(PL: TValues): TValue;
        function op_procedure(PL: TValues): TValue;
    {m} function op_push(PL: TValues): TValue;
    {m} function op_set(PL: TValues): TValue;
        function op_try(PL: TValues): TValue;
        function op_return(PL: TValues): TValue;
        function op_var(PL: TValues): TValue;
        function op_when(PL: TValues): TValue;
        function op_while(PL: TValues): TValue;
        function op_with(PL: TValues; export_symbols: boolean): TValue;

        procedure fill_function_stack(sp: TVFunction; symbols: TIntegers);
        procedure fill_procedure_stack(sp: TVRoutine; symbols: TIntegers);

        function call(expr: TValues): TValue;
        function callv(PL: TValue): TValue; inline;
        function call_routine(PL: TValues): TValue;
        function call_internal(PL: TValues): TValue;
        function call_internal_array(PL: TValues): TValue;
        function call_predicate(PL: TValues): TValue;
        function call_internal_unary(PL: TValues): TValue;
        function call_operator(PL: TValues): TValue;

        function eval_routine(PL: TValues): TValue;
        function eval_parameters(call: TCallProc; PL: TValues): TValue;

        function eval_link(P: TValue): TVChainPointer;

        procedure debugger(E: ELE; PL: TValue);

        function eval(V: TValue): TValue;
        function eval_and_free(V: TValue): TValue;
        function eval_without_var_definitions(V: TValue): TValue; inline;
    end;


function ifu_not                  (const A: TValue): TValue;


var max_error_message_length: integer = 1000;


implementation


uses lisya_thread, lisya_optimizer;


var
    log_file: TLFileStream = nil;
    log_cs, console_cs: TRTLCriticalSection;
    debug_in_place: boolean =false;


function value_by_key(L: TVList; key: unicodestring): TValue;
var i: integer;
begin
    i := L.High - 1;
    while i>=0 do begin
        if (tpSymbol(L[i]) and (key=L.uname[i]))
            or (tpString(L[i]) and (key=L.S[i]))
        then
            Exit(L[i+1].Copy);

        Dec(i, 2);
    end;

    result := TVList.Create;
end;


function key_pos(L: TVList; key: unicodestring; start: integer;
    out p: integer): boolean; overload;
begin
    p := L.High - 1;
    while p>=start do
        if (tpKeyword(L[p]) and (key=L.uname[p]))
        then Exit(true)
        else Dec(p, 2);

    result := false;
end;


function key_pos(L: TVList; key: TValue; out p: integer): boolean; overload;
begin
    p := L.High - 1;
    while p>=0 do
        if equal(L[p], key)
        then Exit(true)
        else Dec(p, 2);

    result := false;
end;


function flag_exists(L :TVList; key: unicodestring; start: integer): boolean;
var i: integer;
begin
    for i := start to L.high do
        if tpKeyword(L[i]) and (key=L.uname[i])
        then Exit(true);
    result := false;
end;


function min_list_length(L: TVList; from: integer = 0): integer;
var i: integer;
begin
    result := MaxInt;
    if tpNIL(L)
    then result := 0
    else
        for i := from to L.High do
            if L.L[i].Count < result then result := L.L[i].Count;
end;


procedure replace_value(var variable: TValue; value: TValue); inline;
begin
    variable.Free;
    variable := value;
end;


function oph_command_line(): TVList;
var i: integer;
begin
    result := TVList.Create();
    for i := 1 to ParamCount do result.Add(TVString.Create(paramStr(i)));
end;


//------------------------------------------------------------------------------
function params_is(PL: TVList;
                    out E: TValue;
                    const tp: array of TTypePredicate): integer; overload;
    function pr_confirm(case_n: integer): boolean; inline;
    var i: integer;
    begin
        for i := (case_n-1)*PL.Count to (case_n)*PL.Count-1 do
            if not tp[i](PL[i mod PL.Count]) then Exit(false);
        result := true;
    end;
var case_count, case_n: integer;
begin
    //  Эта функция проверяет переданный список параметров на предмет
    //соответствия параметров шаблону.
    //  Производится поиск подходящего шаблона
    //номер первого подошедшего возвращается как результат. Если ни один не
    //подошёл - возбуждается исключение
    E := nil;
    result := -1;

    case_count := Length(tp) div PL.Count;
    for case_n := 1 to case_count+1 do
        begin
            if case_n>case_count  then raise ELE.InvalidParameters(PL.AsString);
            if pr_confirm(case_n) then Exit(case_n);
        end;
end;


function lst_params(PL: TValues): unicodestring;
var i: integer;
begin
    if PL=nil then Exit('()');

    result := '(' + value_description(PL[0]);
    for i:=1 to high(PL) do result := result + ', ' + value_description(PL[i]);
    result := result + ')';
end;


function params_is(PL: TValues;
                    out E: TValue;
                    const tp: array of TTypePredicate): integer; overload;
    function pr_confirm(case_n: integer): boolean; inline;
    var i: integer;
    begin
        for i := (case_n-1)*Length(PL) to (case_n)*Length(PL)-1 do
            if not tp[i](PL[i mod Length(PL)]) then Exit(false);
        result := true;
    end;
var case_count, case_n: integer;
begin
    //  Эта функция проверяет переданный список параметров на предмет
    //соответствия параметров шаблону.
    //  Производится поиск подходящего шаблона
    //номер первого подошедшего возвращается как результат. Если ни один не
    //подошёл - возбуждается исключение
    E := nil;
    result := -1;

    case_count := Length(tp) div Length(PL);
    for case_n := 1 to case_count+1 do
        begin
            if case_n>case_count then
                raise ELE.InvalidParameters(lst_params(PL));

            if pr_confirm(case_n) then Exit(case_n);
        end;
end;


function param_is(P: TValue;
                  out E: TValue;
                  const tp: array of TTypePredicate): integer; overload;
begin
    E := nil;

    for result := 1 to Length(tp) do if tp[result-1](P) then Exit;

    raise ELE.InvalidParameters(P.AsString);
end;


function param_is(P: TValue;
                  const tp: array of TTypePredicate): integer; overload;
begin
    for result := 1 to Length(tp) do if tp[result-1](P) then Exit;

    raise ELE.InvalidParameters(P.AsString);
end;


procedure no_params(PL: TVList); overload;
begin
    if PL.Count>0 then raise ELE.InvalidParameters('No params needed');
end;


procedure no_params(PL: TValues); overload;
begin
    if Length(PL)>0 then raise ELE.InvalidParameters('No params needed');
end;


function key_is(V: TValue; K: unicodestring): boolean;
begin
    result := (V is TVKeyword) and ((V as TVKeyword).uname = K);
end;

procedure bool_to_TV(b: boolean; out E: TValue);
begin
    // Используется операторами сравнения, чтобы преобразовать
    // boolean в NIL или TRUE

    if b then E := CreateTRUE else E := TVList.Create;
end;


//------------------------------------------------------------------------------

//////////////////////////////////////////////
//// Internal Functions //////////////////////
//////////////////////////////////////////////

type TInternalFunctionRec = record
        n: unicodestring;
        s: unicodestring;
        case byte of
            0: (f: TInternalFunctionBody);
            1: (a: TInternalArrayFunctionBody);
    end;

type TInternalUnaryFunctionRec = record
        n: unicodestring;
        f: TInternalUnaryFunctionBody;
    end;


function ifh_str(V: TValue): unicodestring; inline;
begin
    if tpString(V) then result := (V as TVString).S else result := V.AsString;
end;


function ifh_format(const L: TVList): unicodestring;
var i: integer;
begin
    result := '';
    for i := 0 to L.High do result := result + ifh_str(L[i]);
end;


function ifh_keyword_to_encoding(const V: TValue): TStreamEncoding;
begin
    if tpNIL(V)             then result := seUTF8 else
    if vpKeyword_UTF8(V)    then result := seUTF8 else
    if vpKeyword_UTF8(V)    then result := seUTF8 else
    {$IFDEF ENDIAN_LITTLE}
    if vpKeyword_UTF16(V)   then result := seUTF16LE else
    if vpKeyword_UTF32(V)   then result := seUTF32LE else
    {$ENDIF}
    if vpKeyword_UTF16BE(V) then result := seUTF16BE else
    if vpKeyword_UTF16LE(V) then result := seUTF16LE else
    if vpKeyword_UTF32BE(V) then result := seUTF32BE else
    if vpKeyword_UTF32LE(V) then result := seUTF32LE else
    if vpKeyword_CP1251(V)  then result := seCP1251 else
    if vpKeyword_CP1252(V)  then result := seCP1252 else
    if vpKeyword_CP866(V)   then result := seCP866 else
    if vpKeyword_KOI8R(V)   then result := seKOI8R else
    if vpKeyword_LATIN1(V)  then result := seLatin1 else

    if vpKeyword_BOM(V)     then result := seBOM else
    raise ELE.InvalidParameters('invalid encoding '+V.AsString);
end;


function ifh_keyword_to_file_mode(const V: TValue): WORD;
begin
    if tpNIL(V)             then result := fmOpenRead else
    if vpKeyword_READ(V)    then result := fmOpenRead else
    if vpKeyword_WRITE(V)   then result := fmCreate else
    if vpKeyword_APPEND(V)  then result := fmOpenReadWrite else
    raise ELE.InvalidParameters('invalid file mode '+V.AsString);
end;


function ifh_keyword_to_file_visibility(const V: TValue): TFileVisibility;
begin
    if tpNIL(V)             then result := fvAny else
    if vpKeyword_VISIBLE(V) then result := fvVisible else
    if vpKeyword_HIDDEN(V)  then result := fvHidden else
    raise ELE.InvalidParameters('invalid file visibility '+V.AsString);
end;


function ifh_keywords_to_file_attributes(const L: TVList): DWORD;
var i: integer; kw: unicodestring;
begin
    result := 0;
    for i := 0 to L.high do begin
        kw := (L[i] as TVKeyword).uname;
        if kw=':READ-ONLY'  then result := result or sysutils.faReadOnly else
        if kw=':HIDDEN'     then result := result or faHidden else
        if kw=':SYS-FILE'   then result := result or faSysFile else
        if kw=':DIRECTORY'  then result := result or faDirectory else
        if kw=':SYM-LINK'   then result := result or faSymLink
        {$IFDEF WINDOWS}
        else
        if kw=':VOLUME-ID'  then result := result or faVolumeID else
        if kw=':ARCHIVE'    then result := result or faArchive
        {$ENDIF}
        ;
    end;
end;


function ifh_keyword_to_grid_arc(const V: TValue): TGridArcType;
var kw: unicodestring;
begin
    kw := (V as TVKeyword).uname;
    if kw=':G'  then result := gaGenerator else
    if kw=':W'  then result := gaLine else
    if kw=':T'  then result := gaTransformer else
    if kw=':Q'  then result := gaBreaker;
end;


function ifh_keyword_to_tensor_folding_function(const V: TValue): TTensorFoldFunctionR;
var kw: unicodestring;
begin
    kw := (V as TVKeyword).uname;
    if kw=':MAX'  then result := tffMax else
    if kw=':MIN'  then result := tffMin else
    if kw=':ADD'  then result := tffAdd else
    if kw=':MUL'  then result := tffMul;
end;


function ifh_file_attributes_to_keywords(fa: DWORD): TVList;
begin
    result := TVList.Create;
    if (fa and sysutils.faReadOnly)>0 then result.Add(TVKeyword.Create(':READ-ONLY'));
    if (fa and faHidden)>0 then result.Add(TVKeyword.Create(':HIDDEN'));
    if (fa and faSysFile)>0 then result.Add(TVKeyword.Create(':SYS-FILE'));
    if (fa and faDirectory)>0 then result.Add(TVKeyword.Create(':DIRECTORY'));
    if (fa and faSymLink)>0 then result.Add(TVKeyword.Create(':SYM-LINK'));
    {$IFDEF WINDOWS}
    if (fa and faVolumeID)>0 then result.Add(TVKeyword.Create(':VOLUME-ID'));
    if (fa and faArchive)>0 then result.Add(TVKeyword.Create(':ARCHIVE'));
    {$ENDIF}
end;


function default(const V: TValue; default: unicodestring):unicodestring; overload;
begin
    if V is TVString then result := (V as TVString).S else result := default;
end;

function default(const V: TValue; default: Int64): Int64; overload;
begin
    if V is TVInteger then result := (V as TVInteger).fI else result := default;
end;

function default(const V: TValue; default: complex): complex; overload;
begin
    if V is TVNumber then result := (V as TVNumber).C else result := default;
end;

function default(const V: TValue; default: double): double; overload;
begin
    if V is TVReal then result := (V as TVReal).F else result := default;
end;


function asString (V: TValue): unicodestring; inline; begin result := (V as TVString).S;     end;
function asInteger(V: TValue): Int64;         inline; begin result := (V as TVInteger).fI;   end;
function asFloat  (V: TValue): double;        inline; begin result := (V as TVReal).F;       end;
function asComplex(V: TValue): complex;       inline; begin result := (V as TVNumber).C;     end;
function asStream (V: TValue): TLStream;      inline; begin result := (V as TVStream).target;end;
function asTime   (V: TValue): TDateTIme;     inline; begin result := (V as TVTime).fDT;     end;




procedure ifh_lock_and_write_string(stream: TVStream; s: unicodestring);
begin
    if stream<>nil then
    try
        stream.target.Lock;
        stream.target.write_string(s);
    finally
        stream.target.Unlock;
    end
    else
    try
        EnterCriticalSection(console_cs);
        Write(s);
    finally
        LeaveCriticalSection(console_cs);
    end;
end;


function ifh_input(msg, prompt: unicodestring): unicodestring;
begin
    EnterCriticalSection(console_cs);
    if msg<>'' then WriteLn(msg);
    System.Write(prompt);
    System.ReadLn(result);
    LeaveCriticalSection(console_cs);
end;


function ifa_record_p          (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpRecord, tpNIL,
        tpRecord, tpRecord,
        tpAny,       tpAny]) of
        1: result := CreateTRUE;
        2: if (PL[0] as TVRecord).is_class(PL[1] as TVRecord)
            then result := CreateTRUE
            else result := TVList.Create;
        3: result := TVList.Create;
    end;
end;


function ifa_maybe                  (const PL: TValues; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpAny, tpNIL,
        tpSubprogram, tpANY]) of
        1: result := CreateTRUE;
        2: result := call(TValues.Create(PL[0], PL[1]));
    end;
end;


function ifa_add               (const PL: TValues; {%H-}call: TCallProc): TValue;
var i: integer; fres: double; ires: Int64; cres: COMPLEX; rest: TValues;
begin
    rest := (PL[0] as TVListRest).V;
    case params_is(PL, result, [
        rpIntegers,
        rpReals,
        rpNumbers,
        rpLists,
        rpTimes]) of
        1: begin
            ires := 0;
            for i := 0 to high(rest) do ires := ires + I_(rest[i]);
            result := TVInteger.Create(ires);
        end;
        2: begin
            fres := 0;
            for i := 0 to high(rest) do fres := fres + F_(rest[i]);
            result := TVFloat.Create(fres);
        end;
        3: begin
            cres := _0;
            for i := 0 to high(rest) do cres := cres + C_(rest[i]);
            result := TVComplex.Create(cres);
        end;
        4: result := ifh_union(rest);
        5: result := ifh_times_sum(rest);

    end;
end;


function ifa_add_b             (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
    {1} tpInteger,   tpInteger,
    {2} tpReal,      tpReal,
    {3} tpNumber,    tpNumber,
    {4} tpTime,      tpTime,

    {5} tpTensor,    tpTensor,
    {6} tpTensor,    tpNumber]) of
        1: result := TVInteger.Create(I_(PL[0]) + I_(PL[1]));
        2: result := TVFloat.Create(F_(PL[0]) + F_(PL[1]));
        3: result := TVComplex.Create(C_(PL[0]) + C_(PL[1]));
        4: result := ifh_times_sum(TValues.Create(PL[1], PL[2]));
        5: result := TVTensor.Create(tensor_add(T_(PL[0]), T_(PL[1])));
        6: result := TVTensor.Create(tensor_add(T_(PL[0]), C_(PL[1])));
    end;
end;


function if_sub                  (const PL: TVList; {%H-}call: TCallProc): TValue;
var b: TVList; ires: Int64; fres: double; cres: complex; dres: TDateTime; i: integer;
begin
    case params_is(PL, result, [
    {1} tpInteger,  tpNIL,
    {2} tpReal,     tpNIL,
    {3} tpNumber,   tpNIL,
    {4} tpInteger,  tpListOfIntegers,
    {5} tpReal,     tpListOfReals,
    {6} tpNumber,   tpListOfNumbers,
    {7} tpList,     tpListOfLists,
    {8} tpDuration, tpNIL,
    {9} tpDateTime, tpSingleDatetime,
    {10}tpDateTime, tpListOfDurations,
    {11}tpDuration, tpListOfDurations]) of
        1: result := TVInteger.Create(-PL.I[0]);
        2: result := TVFloat.Create(-PL.F[0]);
        3: result := TVComplex.Create(-PL.C[0]);
        4: begin
            ires := PL.I[0];
            b := PL.L[1];
            for i := 0 to b.high do ires := ires - b.I[i];
            result := TVInteger.Create(ires);
        end;
        5: begin
            fres := PL.F[0];
            b := PL.L[1];
            for i := 0 to b.high do fres := fres - b.F[i];
            result := TVFloat.Create(fres);
        end;
        6:begin
            cres := PL.C[0];
            b := PL.L[1];
            for i := 0 to b.high do cres := cres - b.C[i];
            result := TVComplex.Create(cres);
        end;
        7: try
            b := ifh_union(PL.L[1]);
            result := ifh_difference(PL.L[0], b);
        finally
            b.Free;
        end;
        8: result := TVDuration.Create(-(PL[0] as TVDuration).fDT);
        9: result := TVDuration.Create(
            (PL[0] as TVTime).fDT - (PL.L[1][0] as TVTime).fDT);
        10: begin
            dres := (PL[0] as TVTime).fDT;
            b := PL.L[1];
            for i := 0 to b.high do dres := dres - (b[i] as TVTime).fDT;
            result := TVDateTime.Create(dres);
        end;
        11: begin
            dres := (PL[0] as TVTime).fDT;
            b := PL.L[1];
            for i := 0 to b.high do dres := dres - (b[i] as TVTime).fDT;
            result := TVDuration.Create(dres);
        end;
    end;
end;


function ifa_sub               (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
    {1} tpInteger,    tpInteger,
    {2} tpReal,       tpReal,
    {3} tpNumber,     tpNumber,

    {4} tpDateTime,   tpDateTime,
    {5} tpDuration,   tpDuration,
    {6} tpDateTime,   tpDuration,

    {7} tpTensor,     tpTensor,
    {8} tpTensor,     tpNumber]) of

        1: result := TVInteger.Create(I_(PL[0]) + I_(PL[1]));
        2: result := TVFloat.Create(F_(PL[0]) + F_(PL[1]));
        3: result := TVComplex.Create(C_(PL[0]) + C_(PL[1]));

        4,5: result := TVDuration.Create((PL[0] as TVTime).fDT + (PL[1] as TVTIme).fDT);
        6: result := TVDateTime.Create((PL[0] as TVTime).fDT + (PL[1] as TVTIme).fDT);

        7: result := TVTensor.Create(tensor_sub(T_(PL[0]),   T_(PL[1])));
        8: result := TVTensor.Create(tensor_add(T_(PL[0]), - C_(PL[1])));
    end;
end;


function ifa_asterisk          (const PL: TValues; {%H-}call: TCallProc): TValue;
var i: integer; fres: double; ires: Int64; A: TValues; cres : COMPLEX; L: TVList;
begin
    WriteLn(PL[0].AsString);
    A := REST_(PL[0]);
    case params_is(PL, result, [
    {1} rpIntegers,
    {2} rpReals,
    {3} rpNumbers,
    {4} rpLists]) of
        1: begin
            ires := 1;
            for i := 0 to high(A) do ires := ires * I_(A[i]);
            result := TVInteger.Create(ires);
        end;
        2: begin
            fres := 1.0;
            for i := 0 to high(A) do fres := fres * F_(A[i]);
            result := TVFloat.Create(fres);
        end;
        3: begin
            cres := cinit(1, 0);
            for i := 0 to high(A) do cres := cres * C_(A[i]);
            result := TVComplex.Create(cres);
        end;
        4: try //TODO: нужна ifh_intersection, принимающая TValues;
            L := TVList.CreateCopy(A);
            result := ifh_intersection(L);
		finally
            L.Free;
		end;
    end;
end;


function if_mul          (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; fres: double; ires: Int64; A: TVList; cres : COMPLEX;
begin
    ires := 1;
    fres := 1.0;
    A := PL.L[0];
    case params_is(PL, result, [
    {1} tpListOfIntegers,
    {2} tpListOfReals,
    {3} tpListOfNumbers,
    {4} tpListOfLists,
    {5} vpListOfDurationsForMul,
    {6} vpPairListNatural,
    {7} vpPairNaturalList]) of
        1: begin
            for i := 0 to A.high do ires := ires * A.I[i];
            result := TVInteger.Create(ires);
        end;
        2: begin
            for i := 0 to A.high do fres := fres * A.F[i];
            result := TVFloat.Create(fres);
        end;
        3: begin
            cres.im := 0;
            cres.re := 1;
            for i := 0 to A.high do cres := cres * A.C[i];
            result := TVComplex.Create(cres);
        end;
        4: result := ifh_intersection(A);
        5: begin
            for i := 0 to A.high do
                if A[i] is TVDuration
                then fres := fres * (A[i] as TVDuration).fDT
                else fres := fres * A.F[i];
            result := TVDuration.Create(fres);
        end;
        6: begin
            result := TVList.Create;
            for i := 0 to PL.L[0].I[1]-1 do (result as TVList).Append(PL.L[0][0].Copy as TVList);
        end;
        7: begin
            result := TVList.Create;
            for i := 0 to PL.L[0].I[0]-1 do (result as TVList).Append(PL.L[0][1].Copy as TVList);
        end;
    end;
end;


function ifa_mul               (const PL: TValues; {%H-}call: TCallProc): TValue;
var k: integer;
begin
    case params_is(PL, result, [
    {1} tpInteger,   tpInteger,
    {2} tpReal,      tpReal,
    {3} tpNumber,    tpNumber,

    {4} tpDuration,  tpReal,
    {5} tpReal,      tpDuration,

    {6} tpTensor,    tpTensor,
    {7} tpTensor,    tpNumber,
    {8} tpNumber,    tpTensor,

    {9} tpList,      vpNatural]) of
        1: result := TVInteger.Create(I_(PL[0]) * I_(PL[1]));
        2: result := TVFloat.Create(F_(PL[0]) * F_(PL[1]));
        3: result := TVComplex.Create(C_(PL[0]) * C_(PL[1]));

        4: result := TVDuration.Create(asTime(PL[0]) * F_(PL[1]));
        5: result := TVDuration.Create(F_(PL[0]) * asTime(PL[1]));

        6: result := TVTensor.Create(tensor_mul(T_(PL[0]), T_(PL[1])));
        7: result := TVTensor.Create(tensor_mul(T_(PL[0]), C_(PL[1])));
        8: result := TVTensor.Create(tensor_mul(T_(PL[1]), C_(PL[0])));

        9: begin
            result := TVList.Create;
            with result as TVList do
                for k := 1 to I_(PL[1]) do Append(PL[0].Copy as TVList);
		end;
	end;
end;


function ifa_cross             (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
    {1} tpTensor2d, tpTensor2d]) of
        1: result := TVTensor.Create(matrix_cross(T_(PL[0]), T_(PL[1])));
	end;
end;


//декартово произведение множеств
function if_cartesian_product   (const PL: TVList; {%H-}call: TCallProc): TValue;
var i, j, k: integer; A, B, C, R: TVList;
begin
    case params_is(PL, result, [
        tpListOfLists]) of
        1: begin
            A := nil;
            B := nil;
            C := nil;
            R := TVList.Create;
            if PL.L[0].Count>0 then begin
                A := PL.L[0].L[0];
                for i := 0 to A.high do R.Add(TVList.Create([A[i].Copy]));
            end;
            for j := 1 to PL.L[0].high do begin
                A := PL.L[0].L[j];
                B.Free;
                B := R.Copy as TVList;
                R.Clear;
                for i := 0 to A.high do begin
                    C := B.Copy as TVList;
                    C.copy_on_write;
                    for k := 0 to C.high do C.L[k].Add(A[i].Copy);
                    R.Append(C);
                end;
            end;
            B.Free;
            result := R;
        end;
    end;
end;


function ifa_div               (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
    {1} tpInteger,          vpIntegerNotZero,
    {2} tpReal,             vpRealNotZero,
    {3} tpNumber,           vpNumberNotZero,
    {4} tpDuration,         vpRealNotZero]) of
        1: if ((I_(PL[0]) mod I_(PL[1])) = 0)
            then result := TVInteger.Create(I_(PL[0]) div I_(PL[1]))
            else result := TVFloat.Create(I_(PL[0]) / I_(PL[1]));
        2: result := TVFloat.Create(F_(PL[0]) / F_(PL[1]));
        3: result := TVComplex.Create(C_(PL[0]) / C_(PL[1]));
        4: result := TVDuration.Create((PL[0] as TVDuration).fDT / F_(PL[1]));
    end;
end;


function ifa_div_int           (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpInteger, vpIntegerNotZero,
        tpTensor,  tpTensor,
        tpTensor,  tpNumber]) of
        1: result := TVInteger.Create(I_(PL[0]) div I_(PL[1]));
        2: result := TVTensor.Create(tensor_div(T_(PL[0]), T_(PL[1])));
        3: result := TVTensor.Create(tensor_div(T_(PL[0]), C_(PL[1])));
    end;
end;


function ifa_mod               (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpInteger, vpIntegerNotZero]) of
        1: result := TVInteger.Create(I_(PL[0]) mod I_(PL[1]));
    end;
end;


function ifu_dec                (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpInteger,
        tpReal]) of
        1: result := TVInteger.Create(asInteger(A) - 1);
        2: result := TVFloat.Create(asFloat(A) - 1);
    end;
end;

function ifu_inc                (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpInteger,
        tpReal]) of
        1: result := TVInteger.Create(asInteger(A) + 1);
        2: result := TVFloat.Create(asFloat(A) + 1);
    end;
end;

function ifu_abs                (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpInteger,
        tpReal,
        tpComplex,
        tpTensor,
        tpDuration]) of
        1: result := TVInteger.Create(abs(asInteger(A)));
        2: result := TVFloat.Create(abs(asFloat(A)));
        3: result := TVFloat.Create(cmod(asComplex(A)));
        4: result := TVTensor.Create(tensor_abs((A as TVTensor).t));
        5: result := TVDuration.Create(abs(asTime(A)));
    end;
end;


function ifa_power              (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpInteger,            vpNatural,
        vpRealNotNegative,    tpReal,
        tpReal,               vpRealAbsOneOrMore,
        tpNumber,             tpNumber]) of
        1: result := TVInteger.Create(I_(PL[0]) ** I_(PL[1]));
        2,3: result := TVFloat.Create(F_(PL[0]) ** F_(PL[1]));
        4: result := TVComplex.Create(C_(PL[0]) ** C_(PL[1]));
    end;
end;


function ifu_factorial          (const P: TValue): TValue;
var i: integer; a,ires:integer; fres: real;
begin
    case param_is(P, result, [vpNatural]) of
        1: begin
          a := asInteger(P);
          if a<=20 then begin //факториал 20-и помещается в Int64, а 21-го - нет
            ires := 1;
            for i := a downto 1 do ires := ires*i;
            result := TVInteger.Create(ires);
          end
          else begin
            fres := 1.0;
            for i := a downto 1 do fres := fres*i;
            result := TVFloat.Create(fres);
          end; //факториал 171 не помещается в double
        end;
    end;
end;


function ifa_max               (const PL: TValues; {%H-}call: TCallProc): TValue;
var i: integer; max_i: integer; max_r: double; V: TValues;
begin
    V := (PL[0] as TVListRest).V;

    case params_is(PL, result, [
    {1} rpNIL,
    {2} rpIntegers,
    {3} rpReals,
    {4} rpDurations,
    {5} rpDateTimes]) of
        1: result := TVList.Create;
        2: begin
            max_i := I_(V[0]);
            for i:=1 to high(V) do max_i:=max(max_i, I_(V[i]));
            result := TVInteger.Create(max_i);
        end;
        3: begin
            max_r := F_(V[0]);
            for i:=0 to high(V) do max_r:=max(max_r, F_(V[i]));
            result := TVFloat.Create(max_r);
        end;
        4: begin
            max_r := asTime(V[0]);
            for i := 0 to high(V) do max_r := max(max_r, asTime(V[i]));
            result := TVDuration.Create(max_r);
        end;
        5: begin
            max_r := asTime(V[0]);
            for i := 0 to high(V) do max_r := max(max_r, asTime(V[i]));
            result := TVDateTime.Create(max_r);
        end;
    end;
end;


function ifa_min               (const PL: TValues; {%H-}call: TCallProc): TValue;
var i: integer; min_i: integer; min_r: double; V: TValues;
begin
    V := (PL[0] as TVListRest).V;

    case params_is(PL, result, [
    {1} rpNIL,
    {2} rpIntegers,
    {3} rpReals,
    {4} rpDurations,
    {5} rpDateTimes]) of
        1: result := TVList.Create;
        2: begin
            min_i := I_(V[0]);
            for i:=1 to high(V) do min_i := min(min_i, I_(V[i]));
            result := TVInteger.Create(min_i);
        end;
        3: begin
            min_r := F_(V[0]);
            for i:=0 to high(V) do min_r := min(min_r, F_(V[i]));
            result := TVFloat.Create(min_r);
        end;
        4: begin
            min_r := asTime(V[0]);
            for i := 0 to high(V) do min_r := min(min_r, asTime(V[i]));
            result := TVDuration.Create(min_r);
        end;
        5: begin
            min_r := asTime(V[0]);
            for i := 0 to high(V) do min_r := min(min_r, asTime(V[i]));
            result := TVDateTime.Create(min_r);
        end;
    end;
end;



function ifu_sqrt               (const A: TValue): TValue;
begin
    case param_is(A, result, [
        vpRealNotNegative,
        tpNumber]) of
        1: result := TVFloat.Create(asFloat(A) ** 0.5);
        2: result := TVComplex.Create(csqrt(asComplex(A)));
    end;
end;


function ifu_sign               (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1:  if asFloat(A)<0 then result := TVInteger.Create(-1) else
            if asFloat(A)>0 then result := TVInteger.Create(+1) else
            result := TVInteger.Create(0);
    end;
end;


function ifu_sin                (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVFloat.Create(sin(asFloat(A)));
    end;
end;


function ifu_arcsin             (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVFloat.Create(arcsin(asFloat(A)));
    end;
end;


function ifu_cos                (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVFloat.Create(cos(asFloat(A)));
    end;
end;


function ifu_arccos             (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVFloat.Create(arccos(asFloat(A)));
    end;
end;


function ifu_tan                (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVFloat.Create(tan(asFloat(A)));
    end;
end;


function ifu_arctan             (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVFloat.Create(arctan(asFloat(A)));
    end;
end;


function ifu_log_10             (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVFloat.Create(log10(asFloat(A)));
    end;
end;


function ifu_log_e              (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVFloat.Create(ln(asFloat(A)));
    end;
end;


function ifu_log_2              (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVFloat.Create(log2(asFloat(A)));
    end;
end;


function if_log_n               (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpReal, tpReal]) of
        1: result := TVFloat.Create(logn(PL.F[0],PL.F[1]));
    end;
end;


function ifh_round_range(R: TVRange; digits: TRoundToRange): TVRange;
begin
    result := TVRangeFloat.Create(
        mar_roundto(R.f_low,  digits),
        mar_roundto(R.f_high, digits));
end;


function ifa_round             (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
    {1} tpReal,     tpNIL,
    {2} tpReal,     vpIntegerRoundToRange,
    {3} tpNumber,   vpIntegerRoundToRangeOrNIL,
    {4} tpTensor,   vpIntegerRoundToRangeOrNIL,
    {5} tpRange,    tpNil,
    {6} tpRange,    vpIntegerRoundToRange]) of
        1: result := TVInteger.Create(round(F_(PL[0])));
        2: result := TVFloat.Create(mar_roundto(F_(PL[0]), default(PL[1], 0)));
        3: result := TVComplex.Create(mar_roundto(C_(PL[0]).re, default(PL[1], 0)), mar_roundto(C_(PL[0]).im, default(PL[1],0)));
        4: result := TVTensor.Create(tensor_round_to(T_(PL[0]), default(PL[1], 0)));
        5: result := TVRangeInteger.Create( round((PL[0] as TVRange).f_low),
                                            round((PL[0] as TVRange).f_high));
        6: result := ifh_round_range(PL[0] as TVRange, I_(PL[1]));
    end;
end;


function ifu_round_0            (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal,
        tpNumber,
        tpTensor,
        tpRange]) of
        1: result := TVFloat.Create(mar_roundto(AsFloat(A),0));
        2: result := TVComplex.Create(mar_roundto(AsComplex(A).re,0),mar_roundto(AsComplex(A).im,-3));
        3: result := TVTensor.Create(tensor_round_to(T_(A), 0));
        4: result := ifh_round_range(A as TVRange, 0);
    end;
end;


function ifu_round_3            (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal,
        tpNumber,
        tpTensor,
        tpRange]) of
        1: result := TVFloat.Create(mar_roundto(AsFloat(A),-3));
        2: result := TVComplex.Create(mar_roundto(AsComplex(A).re,-3),mar_roundto(AsComplex(A).im,-3));
        3: result := TVTensor.Create(tensor_round_to(T_(A), -3));
        4: result := ifh_round_range(A as TVRange, -3);
    end;
end;


function ifu_round_6            (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal,
        tpNumber,
        tpTensor,
        tpRange]) of
        1: result := TVFloat.Create(mar_roundto(AsFloat(A),-6));
        2: result := TVComplex.Create(mar_roundto(AsComplex(A).re,-6),mar_roundto(AsComplex(A).im,-6));
        3: result := TVTensor.Create(tensor_round_to(T_(A), -6));
        4: result := ifh_round_range(A as TVRange, -6);
    end;
end;


function ifu_floor              (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVInteger.Create(floor(asFloat(A)));
    end;
end;


function ifu_ceil               (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVInteger.Create(ceil(asFloat(A)));
    end;
end;


function ifu_fractional         (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVFloat.Create(frac(asFloat(A)));
    end;
end;


function ifu_integer            (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal,
        tpString]) of
        1: result := TVInteger.Create(round(int((A as TVReal).F)));
        2: result := TVInteger.Create(str_to_integer((A as TVString).S));
    end;
end;


function ifu_inverse            (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal,
        tpNumber,
        tpTensor]) of
        1: result := TVFloat.Create(1/asFloat(A));
        2: result := TVComplex.Create(cinv(asComplex(A)));
        3: result := TVTensor.Create(matrix_inverse(T_(A)));
    end;
end;


function ifu_float              (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal,
        tpString]) of
        1: result := TVFloat.Create((A as TVReal).F);
        2: result := TVFloat.Create(str_to_float((A as TVString).S));
    end;
end;


function ifa_range              (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpInteger,     tpInteger,
        tpSequence,    tpNILnil,
        vpNatural,     tpNILnil,
        vpNatural,     tpSequence,
        tpReal,        tpReal]) of
        1: result := TVRangeInteger.Create(I_(PL[0]), I_(PL[1]));
        2: result := TVRangeInteger.Create(0, (PL[0] as TVSequence).Count);
        3: result := TVRangeInteger.Create(0, I_(PL[0]));
        4: result := TVRangeInteger.Create(I_(PL[0]), (PL[1] as TVSequence).Count);
        5: result := TVRangeFloat.Create(F_(PL[0]), F_(PL[1]));
    end;
end;


function ifu_symbol             (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpString]) of
        1: begin
            result := read_from_string(S_(A));
            if not tpSymbol(result) then begin
                FreeAndNil(result);
                raise ELE.Create(S_(A), 'syntax/invalid symbol name');
            end;
        end;
    end;
end;


function ifa_gensym            (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    no_params(PL);
    result := TVSymbol.Gensym;
end;


function ifa_random            (const PL: TValues; {%H-}call: TCallProc): TValue;
var rf: TVRangeFloat;  c: complex; a, arg: double;
begin
    case params_is(PL, result, [
    {1} vpNatural,
    {2} tpRange,
    {3} tpReal,
    {4} tpNumber,
    {5} tpListNotEmpty,
    {6} vpKeyword_TRUE,
    {7} tpNativNIL]) of
        1: result := TVInteger.Create(system.Random(I_(PL[0])));
        2: begin
            rf := PL[0] as TVRangeFloat;
            result := TVFloat.Create(rf.low + system.Random*(rf.high - rf.low));
        end;
        3: result := TVFloat.Create(randg(1, F_(PL[0])));
        4: begin
            c := C_(PL[0]);
            a := cmod(c);
            arg := carg(c);
            result := TVComplex.Create(       (1-a + system.Random*2*a)
                            *cexp(cinit(0,     system.Random*2*arg - arg)));
		end;
		5: result := L_(PL[0])[system.Random(L_(PL[0]).Count)].Copy;
        6: if system.Random(2)=1 then result := CreateTRUE else result := TVList.Create;
        7: result := TVFloat.Create(system.Random);
    end;
end;


function ifa_noisy             (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
    //  V           mul         add
        tpReal,     tpReal,     tpRealOrNIL,
        tpNumber,   tpReal,     tpRealOrNIL,
        tpTensor,   tpReal,     tpRealOrNIL]) of
        1: result := TVFloat.Create(noisy(F_(PL[0]), F_(PL[1]), default(PL[2], 0.0)));
        2: result := TVComplex.Create(noisy(C_(PL[0]), F_(PL[1]), default(PL[2], 0.0)));
        3: result := TVTensor.Create(tensor_noisy(T_(PL[0]), F_(PL[1]), default(PL[2], 0.0)));
	end;
end;


function ifa_shuffled          (const PL: TValues; {%H-}call: TCallProc): TValue;
var src, res: TValues; n, t: integer;
begin
    case params_is(PL, result, [
    {1} tpList]) of
        1: begin
            src := system.Copy((PL[0] as TVList).ValueList);
            SetLength(res, Length(src));
            n := 0;
            while Length(src)>0 do
                begin
                    t := system.Random(Length(src));
                    res[n] := src[t].Copy;
                    Inc(n);
                    src[t] := src[high(src)];
                    SetLength(src, Length(src)-1);
				end;
            result := TVList.Create(res);
		end;
	end;
end;


function ifu_re                 (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal,
        tpComplex,
        tpTensor]) of
        1: result := A.Copy;
        2: result := TVFloat.Create((A as TVComplex).C.re);
        3: result := TVTensor.Create(tensor_re(T_(A)));
    end;
end;


function ifu_im                 (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal,
        tpComplex,
        tpTensor]) of
        1: result := TVFloat.Create(0);
        2: result := TVFloat.Create((A as TVComplex).C.im);
        3: result := TVTensor.Create(tensor_im(T_(A)));
    end;
end;


function ifu_arg                (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal,
        tpComplex,
        tpTensor]) of
        1: if F_(A)>=0 then result := TVFloat.Create(0) else result := TVFloat.Create(pi);
        2: result := TVFloat.Create(carg((A as TVComplex).C));
        3: result := TVTensor.Create(tensor_arg(T_(A)));
    end;
end;


function ifu_as_seconds         (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpTime]) of
        1: result := TVFloat.Create(asTime(A)*24*60*60);
    end;
end;


function ifu_conjuge            (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal,
        tpComplex]) of
        1: result := A.Copy;
        2: result := TVComplex.Create(cong((A as TVComplex).C));
    end;
end;


function ifu_rad                (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal]) of
        1: result := TVFloat.Create(math.degtorad((A as TVReal).F));
    end;
end;


function ifu_deg                (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpReal,
        tpTensorReal]) of
        1: result := TVFloat.Create(math.radtodeg((A as TVReal).F));
        2: result := TVTensor.Create(tensor_radtodeg(T_(A)));
    end;
end;


function ifa_complex           (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpReal, tpReal]) of
        1: result := TVComplex.Create(F_(PL[0]), F_(PL[1]));
    end;
end;


function ifa_complex_exp       (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpReal, tpReal]) of
        1: result := TVComplex.Create(F_(PL[0])*cexp(cinit(0, F_(PL[1]))));
    end;
end;


function if_duration            (const PL: TVList; {%H-}call: TCallProc): TValue;
var d: double;
begin
    case params_is(PL, result, [
        tpRealOrNIL, tpRealOrNIL, tpRealOrNIL]) of
        1: begin
            d := 0;
            if tpReal(PL[0]) then d := d + PL.F[0]/24;
            if tpReal(PL[1]) then d := d + PL.F[1]/(24*60);
            if tpReal(PL[2]) then d := d + PL.F[2]/(24*60*60);
        end;
    end;
    result := TVDuration.Create(d);
end;


function ifa_calc              (const PL: TValues; {%H-}call: TCallProc): TValue;
var expr, subexpr, tmp: TValues; i: integer;

    function newV (V: TValue): TValue;
    begin
        SetLength(tmp, Length(tmp)+1);
        tmp[high(tmp)] := V;
        result := V;
	end;

    procedure print_expr();
    var i: integer;
    begin
        WriteLn('------');
        for i := 0 to high(expr) do begin
            WriteLn(expr[i].AsString,' ', expr[i].ClassName);

		end;
		WriteLn('------');
	end;

    procedure callu (n: integer);
    var i: integer;
    begin
        subexpr[0] := expr[n];
        subexpr[1] := expr[n+1];
        expr[n] := newV(call(subexpr));
        for i := n+1 to high(expr)-1 do expr[i]:=expr[i+1];
        SetLength(expr, Length(expr)-1);
        //print_expr();
	end;

    procedure callb (n: integer);
    var i: integer;
    begin
        subexpr[0] := expr[n];
        subexpr[1] := expr[n-1];
        subexpr[2] := expr[n+1];
        expr[n-1] := newV(call(subexpr));
        for i := n to high(expr)-2 do expr[i] := expr[i+2];
        SetLength(expr, Length(expr)-2);
        //print_expr();
	end;

    function fpPriority1(V: TValue): boolean;
    begin
        result := (V is TVInternalArrayFunction)
            and (@((V as TVInternalArrayFunction).body) = @ifa_power);  // **
	end;

    function fpPriority2(V: TValue): boolean;
    begin
        result :=
            ((V is TVInternalArrayFunction)
                and ((@((V as TVInternalArrayFunction).body) = @ifa_div)       // /
                    or (@((V as TVInternalArrayFunction).body) = @ifa_div_int) // div
                    or (@((V as TVInternalArrayFunction).body) = @ifa_mod)     // mod
                    or (@((V as TVInternalArrayFunction).body) = @ifa_mul)     // mul
                    or (@((V as TVInternalArrayFunction).body) = @ifa_asterisk)// *
                    or (@((V as TVInternalArrayFunction).body) = @ifa_cross))) // cross
            or
            ((V is TVInternalFunction)
                and (@((V as TVInternalFunction).body) = @if_mul))             // *
	end;

begin
    expr := Copy((PL[0] as TVListRest).V);
    result := nil;
    tmp := nil;

    //print_expr();

    try
        //одиночный аргумент
        if length(expr)=1 then expr[0] := newV(expr[0].Copy);

        // унарные операторы
        SetLength(subexpr, 2);

        for i := high(expr)-1 downto 0 do
            if ((i=0) or (not tpData(expr[i-1]))) and (not tpData(expr[i])) and tpData(expr[i+1])
            then
                callu(i);

        // бинарные операторы
        SetLength(subexpr, 3);

        // возведение в степень
        i := 1;
        while i<high(expr) do
            if tpData(expr[i-1]) and fpPriority1(expr[i]) and tpData(expr[i+1])
            then
                callb(i)
            else
                Inc(i);

        // умножение и деление
        i := 1;
        while i<high(expr) do
            if tpData(expr[i-1]) and fpPriority2(expr[i]) and tpData(expr[i+1])
            then
                callb(i)
            else
                Inc(i);

        // прочие
        i := 1;
        while i<high(expr) do
            if tpData(expr[i-1]) and (not tpData(expr[i])) and tpData(expr[i+1])
            then
                callb(i)
            else
                Inc(i);

        if length(expr)>1 then raise ELE.Create('', 'malformed/calc');

        if length(expr)=0 then result := TVList.Create else result := expr[0];

	finally
        for i := 0 to high(tmp)-1 do tmp[i].Free;
	end;
end;



function if_datetime            (const PL: TVList; {%H-}call: TCallProc): TValue;
var year,month,day,h,m: WORD; s: double;
begin
    case params_is(PL, result, [
        vpIntegerWORD, vpIntegerWORD, vpIntegerWORD,
        vpIntegerWORDorNIL, vpIntegerWORDorNIL, vpRealNotNegativeOrNIL]) of
        1: begin
            year := PL.I[0];
            month := PL.I[1];
            day := PL.I[2];
            if tpInteger(PL[3]) then h := PL.I[3] else h := 0;
            if tpInteger(PL[4]) then m := PL.I[4] else m := 0;
            if tpReal(PL[5]) then s := PL.F[5] else s := 0;
            result := TVDatetime.Create(ComposeDateTime(
                EncodeDate(year,month,day), EncodeTime(h,m,floor(s),0))
                +frac(s)/(24*60*60));
        end;
    end;
end;


function if_matrix              (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpIntegerPositive, tpListOfReals,
        vpIntegerPositive, vpListOfNumbers]) of
        1: result := TVTensor.Create(tensor_new(ListOfRealsToDoubles(L_(PL[1])), I_(PL[0])));
        2: result := TVTensor.Create(tensor_new(ListOfNumbersToComplexes(L_(PL[1])), I_(PL[0])));
	end;
end;


function ifa_vector            (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        rpReals,
        rpNumbers]) of
        1: result := TVTensor.Create(tensor_new(ValuesToDoubles(REST_(PL[0]))));
        2: result := TVTensor.Create(tensor_new(ValuesToComplexes(REST_(PL[0]))));
	end;
end;


procedure ifh_tree_to_tensor_r(L :TVList; var d: integer; var dim: TIntegers; var e: TDoubles);
var i : integer;
begin
    //WriteLn('--- ', L.AsString);
    if tpListOfReals(L)
    then
        begin
            doubles_append(e, ListOfRealsToDoubles(L));
            d := 0;
            if Length(dim)=0
            then
                begin
                    SetLength(dim, 1);
                    dim[0] := L.Count;
				end
            else
			    if dim[0]<>L.Count then raise ELE.InvalidParameters('Unrectangular tensor');
		end
	else
        begin
            Dec(d);
            for i := 0 to L.high do ifh_tree_to_tensor_r(L.L[i], d, dim, e);
            Inc(d);
            if high(dim)<d
            then
                begin
                    SetLength(dim, d+1);
                    dim[d] := L.Count;
				end
            else
                if dim[d]<>L.Count then raise ELE.InvalidParameters('Unrectangular tensor');
		end;
end;


procedure ifh_tree_to_tensor_c(L :TVList; var d: integer; var dim: TIntegers; var e: TComplexes);
var i : integer;
begin
    //WriteLn('--- ', L.AsString);
    if tpListOfNumbers(L)
    then
        begin
            complexes_append(e, ListOfNumbersToComplexes(L));
            d := 0;
            if Length(dim)=0
            then
                begin
                    SetLength(dim, 1);
                    dim[0] := L.Count;
				end
            else
			    if dim[0]<>L.Count then raise ELE.InvalidParameters('Unrectangular tensor');
		end
	else
        begin
            Dec(d);
            for i := 0 to L.high do ifh_tree_to_tensor_c(L.L[i], d, dim, e);
            Inc(d);
            if high(dim)<d
            then
                begin
                    SetLength(dim, d+1);
                    dim[d] := L.Count;
				end
            else
                if dim[d]<>L.Count then raise ELE.InvalidParameters('Unrectangular tensor');
		end;
end;


function ifa_tensor            (const PL: TValues; {%H-}call: TCallProc): TValue;
var e: TDoubles; c: TComplexes; dim: TIntegers; d: integer; t: TTensors; L: TValues;
begin
    case params_is(PL, result, [
        tpTreeOfReal,
        tpTreeOfNumbers,
        tpListOfTensors,
        tpTensor]) of
        1: begin
            d := 0;
            e := nil;
            dim := nil;
            ifh_tree_to_tensor_r(L_(PL[0]), d, dim, e);
            result := TVTensor.Create(dim, e);
		end;
        2: begin
            d := 0;
            e := nil;
            dim := nil;
            ifh_tree_to_tensor_c(L_(PL[0]), d, dim, c);
            result := TVTensor.Create(dim, c);
		end;
        3: begin
            L := LV_(PL[0]);
            SetLength(t, Length(L));
            for d := 0 to high(L) do t[d] := (L[d] as TVTensor).t;
            result := TVTensor.Create(tensor_stack(t));
		end;
        4: result := TVTensor.Create(tensor_add_dim(T_(PL[0])));
	end;
end;


function ifa_split             (const PL: TValues; {%H-}call: TCallProc): TValue;
var tensors: TTensors; i: integer; L: TVList;
begin
    case params_is(PL, result, [
        tpTensor, vpNatural]) of
        1: begin
            tensors := tensor_split(T_(PL[0]), I_(PL[1]));
            L := TVList.Create;
            result := L;
            for i := 0 to high(tensors) do L.Add(TVTensor.Create(tensors[i]));
		end;
	end;
end;


function ifa_flat              (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpTensor, vpNatural]) of
        1: result := TVTensor.Create(tensor_flat(T_(PL[0])))
	end;
end;


function ifa_diff              (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpTensorReal1d]) of
        1: result := TVTensor.Create(sp_diff(T_(PL[0]).e));
    end;
end;


function ifa_rms               (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        //vector        //window
        tpTensorReal1d, vpIntegerPositive,
        tpTensorReal,   tpNIL]) of
        1: result := TVTensor.Create(sp_rms(T_(PL[0]).e, I_(PL[1])));
        2: result := TVFloat.Create(doubles_rms(T_(PL[0]).e));
    end;
end;


function ifa_sample            (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        //signal        //index    //order
        tpTensorReal1d, tpReal,    vpNaturalOrNIL,
        tpTensor1d,     tpReal,    vpNaturalOrNIL]) of
        1: result := TVFloat.Create(sp_sample(T_(PL[0]).e, F_(PL[1]), default(PL[2], 1)));
        2: with T_(PL[0]) do
            result := TVComplex.Create(
                sp_sample(PComplexes(@e[0]), Length(e) div 2, F_(PL[1]), default(PL[2],1)));
    end;
end;


function ifa_stretch           (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpTensorReal1d, vpRealNotNegative, vpNatural]) of
        1: result := TVTensor.Create(sp_stretch(T_(PL[0]).e , F_(PL[1]), default(PL[2], 1)));
    end;
end;


function ifa_convolution       (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpTensorReal1d, tpTensorReal1d, tpAny]) of
        1: result := TVTensor.Create(sp_convolution(T_(PL[0]).e, T_(PL[1]).e, tpTrue(PL[2])));
    end;
end;


function ifa_trigger_over      (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpTensorReal1d, tpReal, tpRealOrNIL,
        tpListOfReals,  tpReal, tpRealOrNIL]) of
        1: result := IntegersToListOfRanges(
                trigger_over(T_(PL[0]).e, F_(PL[1]), default(PL[2], F_(PL[1]))));
        2: result := IntegersToListOfRanges(
                trigger_over(ListOfRealsToDoubles(L_(PL[0])), F_(PL[1]), default(PL[2], F_(PL[1]))));
    end;
end;


function ifa_trigger_under     (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpTensorReal1d, tpReal, tpRealOrNIL,
        tpListOfReals,  tpReal, tpRealOrNIL]) of
        1: result := IntegersToListOfRanges(
                trigger_under(T_(PL[0]).e, F_(PL[1]), default(PL[2], F_(PL[1]))));
        2: result := IntegersToListOfRanges(
                trigger_under(ListOfRealsToDoubles(L_(PL[0])), F_(PL[1]), default(PL[2], F_(PL[1]))));
    end;
end;


function ifa_trigger_out     (const PL: TValues; {%H-}call: TCallProc): TValue;
var t_h, t_l, r_h, r_l: real;
begin
    case params_is(PL, result, [
        tpTensorReal1d, tpRange, tpRangeOrNIL]) of
        1: begin
            RNG_(PL[1], t_l, t_h);
            if tpTrue(PL[2])
            then RNG_(PL[2], r_l, r_h)
            else RNG_(PL[1], r_l, r_h);

            result := IntegersToListOfRanges(trigger_out(T_(PL[0]).e, t_l, t_h, r_l, r_h));
		end;
    end;
end;


function ifa_correlation       (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpTensorReal,   tpTensorReal]) of
        1: result := TVFloat.Create(correlation(T_(PL[0]).e, T_(PL[1]).e));
	end;
end;


function ifa_limited           (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        // value        top             bottom
        tpTensorReal,   tpRealOrNIL,    tpRealOrNIL]) of
        1: result := TVTensor.Create(T_(PL[0]).size(),
                                        limited(T_(PL[0]).e,
                                                tpTrue(PL[1]), default(PL[1], 0),
                                                tpTrue(PL[2]), default(PL[2], 0)));
	end;
end;


function ifa_matrix_identity   (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpIntegerPositive]) of
        1: result := TVTensor.Create(matrix_identity(I_(PL[0])));
    end;
end;


function ifa_matrix_diagonal    (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        rpReals,
        rpNumbers]) of
        1: result := TVTensor.Create(matrix_diagonal(ValuesToDoubles  (REST_(PL[0]))));
        2: result := TVTensor.Create(matrix_diagonal(ValuesToComplexes(REST_(PL[0]))));
    end;
end;


function ifa_matrix_minor       (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpTensor, vpNatural, vpNatural]) of
        1: result := TVTensor.Create(matrix_minor(T_(PL[0]), I_(PL[1]), I_(PL[2])));
    end;
end;


function ifu_matrix_determinant                (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpTensorReal2d,
        tpTensor2d]) of
        1: with T_(A) do result := TVFloat.Create(matrix_determinant(e, size(1)));
        2: with T_(A) do result := TVComplex.Create(matrix_determinant_c(e, size(1)));
	end;
end;


function ifa_fourier           (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        //SV           :HARMONIC                :WINDOW             :PERIOD
        tpTensorReal1d, vpNaturalOrNIL,         vpIntegerPositive,  tpNILnil,
        tpTensorReal1d, vpIntegerPositiveOrNIL, vpIntegerPositive,  vpRealPositive]) of
        1: result := TVTensor.Create(DFT(T_(PL[0]).e, I_(PL[2]), default(PL[1], 1)));
        2: result := TVTensor.Create(harmonic(T_(PL[0]).e, I_(PL[2]), default(PL[1], 1) / F_(PL[3])));
    end;
end;


function ifa_thinned           (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        //SV           :N                  :MODE
        tpTensorReal1d, vpIntegerPositive, vpKeyword_MEAN,
        tpTensorReal1d, vpIntegerPositive, tpNIL]) of
        1: result := TVTensor.Create(thinned_mean(T_(PL[0]).e, I_(PL[1])));
        2: result := TVTensor.Create(thinned(T_(PL[0]).e, I_(PL[1])));
    end;
end;


function ifu_hash               (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpAny]) of
        1: result := TVInteger.Create(A.hash);
    end;
end;


function if_split_string        (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, tpStringOrNIL]) of
        1: result := StringArrayToListOfStrings(
                SplitString(PL.S[0], default(PL[1], ' ')));
    end;
end;


function if_trim                (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString]) of
        1: result := TVString.Create(trim(PL.S[0]));
    end;
end;


function if_trim_quotes         (const PL: TVList; {%H-}call: TCallProc): TValue;
var s: unicodestring;
begin
    case params_is(PL, result, [
        tpString]) of
        1: begin
            s := trim(PL.S[0]);
            if (Length(s)>0) and (s[1] in ['''', '"']) then delete(s,1,1);
            if (Length(s)>0) and (s[Length(s)] in ['''', '"']) then delete(s,Length(s),1);
            result := TVString.Create(s);
        end;
    end;
end;


function ifa_mean                (const PL: TValues; {%H-}call: TCallProc): TValue;
var ff: TDoubles;
begin
    case params_is(PL, result, [
        tpNIL,
        tpListOfReals,
        tpTensorReal,
        tpTensor]) of
        1: result := TVList.Create;
        2: begin
            ff := ListOfRealsToDoubles(L_(PL[0]));
            result := TVFloat.Create(mean(ff));
        end;
        3: result := TVFloat.Create(mean(T_(PL[0]).e));
        4: with T_(PL[0]) do
            result := TVComplex.Create(complexes_mean(PComplexes(@e[0]), Length(e) div 2));
    end;
end;


function if_standard_deviation  (const PL: TVList; {%H-}call: TCallProc): TValue;
var ff: TDoubles;
begin
    case params_is(PL, result, [
        tpListOfReals]) of
        1: begin
            if PL.L[0].Count<2 then raise ELE.InvalidParameters(PL.L[0].AsString);
            ff := ListOfRealsToDoubles(PL.L[0]);
            result := TVFloat.Create(stddev(ff));
        end;
    end;
end;


function if_equal               (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [tpAny, tpAny]) of
        1: if equal(PL[0], PL[1])
            then result := CreateTRUE
            else result := TVList.Create;
    end;
end;


function if_match               (const PL: TVList; {%H-}call: TCallProc): TValue;

    function p (p, a: TValue): boolean;
    var res: TValue;
    begin
        try
            res := nil;
            res := call(TValues.Create(p, a));
            result := tpTrue(res);
        finally
            res.Free;
        end;
    end;

    function m(a, b: TValue): boolean;
    var i: integer; al,bl: TVList;
    begin
        result := vpSymbol__(a) or equal(a,b);
        if result then Exit;
        result := tpSubprogram(a) and p(a,b);
        if result then Exit;
        result := tpList(a) and tpList(b);
        if not result then Exit;
        al := a as TVList;
        bl := b as TVList;
        result := al.Count=bl.Count;
        if not result then Exit;
        for i := 0 to al.high do begin
            result := m(al[i],bl[i]);
            if not result then Exit;
        end;
    end;

begin
    case params_is(PL, result, [tpAny, tpAny]) of
        1: bool_to_TV(m(PL[0], PL[1]), result);
    end;
end;


function if_more                (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpInteger, tpInteger,
        tpReal,    tpReal,
        tpString,  tpString,
        tpDateTime, tpDateTime,
        tpDuration, tpDuration,
        tpList, tpList]) of
        1: bool_to_TV( PL.I[0]>PL.I[1] , result);
        2: bool_to_TV( PL.F[0]>PL.F[1] , result);
        3: bool_to_TV( PL.S[0]>PL.S[1] , result);
        4,5: bool_to_TV( (PL[0] as TVTime).fDT>(PL[1] as TVTime).fDT, result);
        6: bool_to_TV(ifh_set_include(PL.L[0], PL.L[1]) and not ifh_equal_sets(PL.L[0],PL.L[1]), result);
    end;
end;


function if_less                (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpInteger, tpInteger,
        tpReal,    tpReal,
        tpString,  tpString,
        tpDateTime, tpDateTime,
        tpDuration, tpDuration,
        tpList, tpList]) of
        1: bool_to_TV( PL.I[0]<PL.I[1] , result);
        2: bool_to_TV( PL.F[0]<PL.F[1] , result);
        3: bool_to_TV( PL.S[0]<PL.S[1] , result);
        4,5: bool_to_TV( (PL[0] as TVTime).fDT<(PL[1] as TVTime).fDT, result);
        6: bool_to_TV(ifh_set_include(PL.L[1], PL.L[0]) and not ifh_equal_sets(PL.L[0],PL.L[1]), result);
    end;
end;


function if_more_or_equal       (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpInteger, tpInteger,
        tpReal,    tpReal,
        tpString,  tpString,
        tpList,    tpList,
        tpDateTime, tpDateTime,
        tpDuration, tpDuration]) of
        1: bool_to_TV( PL.I[0]>=PL.I[1] , result);
        2: bool_to_TV( PL.F[0]>=PL.F[1] , result);
        3: bool_to_TV( PL.S[0]>=PL.S[1] , result);
        4: bool_to_TV(ifh_set_include(PL.L[0], PL.L[1]), result);
        5,6: bool_to_TV( (PL[0] as TVTime).fDT>=(PL[1] as TVTime).fDT, result);
    end;
end;


function if_less_or_equal       (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpInteger, tpInteger,
        tpReal,    tpReal,
        tpString,  tpString,
        tpList,    tpList,
        tpDateTime, tpDateTime,
        tpDuration, tpDuration]) of
        1: bool_to_TV( PL.I[0]<=PL.I[1] , result);
        2: bool_to_TV( PL.F[0]<=PL.F[1] , result);
        3: bool_to_TV( PL.S[0]<=PL.S[1] , result);
        4: bool_to_TV(ifh_set_include(PL.L[1], PL.L[0]), result);
        5,6: bool_to_TV( (PL[0] as TVTime).fDT<=(PL[1] as TVTime).fDT, result);
    end;
end;


function if_not_equal           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpInteger, tpInteger,
        tpReal,    tpReal,
        tpString,  tpString,
        tpAny,   tpAny]) of
        1: bool_to_TV( PL.I[0]<>PL.I[1] , result);
        2: bool_to_TV( PL.F[0]<>PL.F[1] , result);
        3: bool_to_TV( PL.S[0]<>PL.S[1] , result);
        4: bool_to_TV(not equal(PL[0], PL[1]), result);
    end;
end;


function if_and                 (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer;
begin
    case params_is(PL, result, [
        tpNIL,
        tpList]) of
        1: result := CreateTRUE;
        2: begin
            for i := 0 to PL.L[0].high do
                if tpNIL(PL.L[0][i]) then
                    Exit(TVList.Create);
            result := PL.L[0][PL.L[0].high].Copy;
        end;
    end;
end;


function if_or                  (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer;
begin
    case params_is(PL, result, [
        tpNIL,
        tpList]) of
        1: result := TVList.Create;
        2: begin
            for i := 0 to PL.L[0].high do
                if tpTrue(PL.L[0][i]) then
                    Exit(PL.L[0][i].Copy);

            result := TVList.Create;
        end;
    end;
end;


function if_xor                 (const PL: TVList; {%H-}call: TCallProc): TValue;
var res, a: boolean; i, n: integer;
begin
    case params_is(PL, result, [
        tpNIL,
        tpList]) of
        1: result := TVList.Create;
        2: begin
            res := tpTrue(PL.L[0][0]);
            n := 0;
            for i:=1 to PL.L[0].high do begin
                a := tpTrue(PL.L[0][i]);
                res := res xor a;
                if a then n := i; //положение последнего значащего аргумента
            end;
            if res
            then result := PL.L[0][n].Copy
            else result := TVList.Create;
        end;
    end;
end;


function ifu_not                (const A: TValue): TValue;
begin
    if tpNIL(A)
    then result := CreateTRUE
    else result := TVList.Create;
end;


function if_equal_case_insensitive(const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, tpString]) of
        1: if UnicodeUpperCase(PL.S[0])=UnicodeUpperCase(PL.S[1])
            then result := CreateTRUE
            else result := TVList.Create;
    end;
end;


function if_equal_sets            (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpNIL, tpNIL,
        tpList, tpList]) of
        1: result := CreateTRUE;
        2: bool_to_TV(ifh_equal_sets(PL.L[0], PL.L[1]), result);
    end;
end;


function if_length_more           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpList, tpList,
        tpString, tpString]) of
        1: bool_to_TV(PL.L[0].Count > PL.L[1].Count, result);
        2: bool_to_TV(Length(PL.S[0]) > Length(PL.S[1]), result);
    end;
end;


function if_length_less           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpList, tpList,
        tpString, tpString]) of
        1: bool_to_TV(PL.L[0].Count < PL.L[1].Count, result);
        2: bool_to_TV(Length(PL.S[0]) < Length(PL.S[1]), result);
    end;
end;


function if_equal_lengths         (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSequence, tpSequence]) of
        1: bool_to_TV((PL[0] as TVSequence).Count=(PL[1] as TVSequence).Count, result);
    end;
end;


function ifu_identity             (const A: TValue): TValue;
begin
    result := A.Copy;
end;


function if_test_dyn            (const {%H-}PL: TVList; {%H-}call: TCallProc): TValue;
{$IFDEF GUI} var i: integer; {$ENDIF}
begin
{$IFDEF GUI}
    Application.Initialize;
    Application.CreateForm(TCanvasForm, CanvasForm);
    for i:= 0 to PL.L[0].high do
    CanvasForm.Post(PL.L[0].L[i]);
    Application.Run;
{$ENDIF}
    result := CreateTRUE;
end;


function if_likeness            (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, tpString]) of
        1: result := TVInteger.Create(ifh_like(PL.S[0],PL.S[1]));
    end;
end;


function if_levenshtein         (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, tpString]) of
        1: result := TVInteger.Create(levenshtein(PL.S[0],PL.S[1]));
    end;
end;


function if_extract_file_ext    (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString]) of
        1: result := TVString.Create(ExtractFileExt(PL.S[0]));
    end;
end;


function if_change_file_ext     (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, tpString]) of
        1:  result := TVString.Create(ChangeFileExt(PL.S[0], PL.S[1]));
    end;
end;


function if_extract_file_name   (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString]) of
        1: result := TVString.Create(ExtractFileName(PL.S[0]));
    end;
end;


function if_extract_file_path   (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString]) of
        1: result := TVString.Create(DirSep(ExtractFilePath(PL.S[0])));
    end;
end;


function if_relative_path       (const PL: TVList; {%H-}call: TCallProc): TValue;
var rel_path: unicodestring;
begin
    case params_is(PL, result, [
        tpString, tpStringOrNIL]) of
        1: begin
            rel_path := CreateRelativePath(
                ExpandFileName(PL.S[0]),
                ExpandFileName(default(PL[1], GetCurrentDir)));
            if end_of_string_is(PL.S[0], DirectorySeparator)
            then rel_path := IncludeTrailingPathDelimiter(rel_path);
            result := TVString.Create(rel_path);
        end;
    end;
end;


function if_expand_file_name    (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString]) of
        1: result := TVString.Create(ExpandFileName(DirSep(PL.S[0])));
    end;
end;


function if_file_exists         (const PL: TVList; {%H-}call: TCallProc): TValue;
var fn: unicodestring;
begin
    case params_is (PL, result, [
        tpString]) of
        1: begin
            fn := ExpandFileName(DirSep(PL.S[0]));
            if FileExists(fn)
            then begin
                if (FileGetAttr(fn) and faDirectory)<>0
                then result := TVString.Create(IncludeTrailingPathDelimiter(fn))
                else result := TVString.Create(fn);
            end
            else result := TVList.create;
        end;
    end;
end;


function if_directory_exists    (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is (PL, result, [
        tpString]) of
        1: if DirectoryExists(DirSep(PL.S[0]))
            then result := TVString.Create(IncludeTrailingPathDelimiter(ExpandFileName(DirSep(PL.S[0]))))
            else result := TVList.create;
    end;
end;


function if_environment_variable(const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is (PL, result, [
        tpString]) of
        1: result := TVString.Create(GetEnvironmentVariable(PL.S[0]));
    end;
end;


function if_change_directory    (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is (PL, result, [
        tpString]) of
        1: if SetCurrentDir(DirSep(PL.S[0]))
            then result := CreateTRUE
            else raise ELE.Create('change-directory error: '+PL.S[0]);
    end;
end;


function if_delete_file         (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is (PL, result, [
        tpString]) of
        1: if DeleteFile(DirSep(PL.S[0]))
            then result := CreateTRUE
            else raise ELE.Create('delete-file error: '+PL.S[0]);
    end;
end;


function if_copy_file           (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer;
begin
    case params_is (PL, result, [
        tpString, tpString,
        tpListOfStrings, tpString]) of
        1: ifh_copy_file(DirSep(PL.S[0]), DirSep(PL.S[1]));
        2: for i := 0 to PL.L[0].high do ifh_copy_file(DirSep(PL.L[0].S[i]), DirSep(PL.S[1]));
    end;
    result := CreateTRUE;
end;


function if_file_size           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is (PL, result, [
        tpString,
        tpStream]) of
        1: result := TVInteger.Create(FileSize(DirSep(PL.S[0])));
        2:  result := TVInteger.Create(
            (PL[0] as TVStream).target.size);
    end;
end;


function if_file_age            (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is (PL, result, [
        tpString, tpNIL,
        tpString, tpDateTime]) of
        1: result := TVDateTime.Create(FileDateToDateTime(FileAge(DirSep(PL.S[0]))));
        2: if 0=FileSetDate(DirSep(PL.S[0]),DateTimeToFileDate((PL[1] as TVDatetime).fDT))
           then result := CreateTRUE
           else raise ELE.Create('FileSetDate on '+PL.S[0],'file');
    end;
end;


function if_file_attributes     (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is (PL, result, [
        tpString, tpNIL,
        tpString, vpListOfFileAttributes]) of
        1: result := ifh_file_attributes_to_keywords(FileGetAttr(DirSep(PL.S[0])));
        2: if 0=FileSetAttr(DirSep(PL.S[0]),ifh_keywords_to_file_attributes(PL.L[1]))
            then result := CreateTRUE
            else raise ELE.Create('FileSetAttr on '+PL.S[0],'file');
    end;
end;


function if_rename_file         (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is (PL, result, [
        tpString, tpString]) of
        1: if RenameFile(DirSep(PL.S[0]), DirSep(PL.S[1]))
            then result := CreateTRUE
            else raise ELE.Create('rename-file error: '+PL.S[0]);
    end;
end;


function if_create_directory    (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is (PL, result, [
        tpString]) of
        1: if CreateDir(DirSep(PL.S[0]))
            then result := CreateTRUE
            else raise ELE.Create('create-directory error: '+PL.S[0]);
    end;
end;


function if_remove_directory    (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is (PL, result, [
        tpString]) of
        1: if RemoveDir(DirSep(PL.S[0]))
            then result := CreateTRUE
            else raise ELE.Create('remove-directory error: '+PL.S[0]);
    end;
end;


function if_guid                (const PL: TVList; {%H-}call: TCallProc): TValue;
var g: TGUID;
begin
    no_params(PL);
    if CreateGUID(g)=0
    then result := TVSymbol.Create(GUIDToString(g))
    else raise ELE.Create('GUID error');
end;


function ifa_every                  (const PL: TValues; call: TCallProc): TValue;  //HIGH
var i: integer; tmp: TValue; expr, L: TValues;
begin
    case params_is(PL, result, [
        tpSubprogram, tpList]) of
        1: begin
            L := LV_(PL[1]);
            expr := TValues.Create(PL[0], nil);
            for i := 0 to high(L) do
                try
                    expr[1] := L[i];
                    tmp := call(expr);
                    if tpNil(tmp) then EXIT(TVList.Create(nil));
				finally
                    tmp.Free;
				end;
            result := CreateTRUE;
		end;
    end;
end;



function ifa_some                   (const PL: TValues; call: TCallProc): TValue;  //HIGH
var i: integer; expr, L: TValues;
begin
    case params_is(PL, result, [
        tpSubprogram, tpList]) of
        1: begin
            expr := TValues.Create(PL[0], nil);
            L := LV_(PL[1]);
            for i := 0 to high(L) do
                begin
                    expr[1] := L[i];
                    result := call(expr);
                    if tpTrue(result) then Exit;
				end;
            result := TVList.Create(nil);
        end;
    end;
end;


function ifu_head               (const A: TValue): TValue;
begin
    case param_is(A, result, [
    {1} tpNIL,
    {2} tpList,
    {3} tpAny]) of
        1: result := TVList.Create;
        2: result := (A as TVList)[0].Copy;
        3: result := A.Copy;
    end;
end;


function ifu_tail               (const A: TValue): TValue;
begin
    case param_is(A, result, [
    {1} tpNIL,
    {2} tpList]) of
        1: result := TVList.Create;
        2: result := (A as TVList).tail(1);
    end;
end;


function ifu_low                (const A: TValue): TValue;
begin
    case param_is(A, result, [
    {1} tpRangeInteger,
    {2} tpRange]) of
        1: result := TVInteger.Create((A as TVRangeInteger).low);
        2: result := TVFloat.Create((A as TVRange).f_low);
    end;
end;


function ifu_high               (const A: TValue): TValue;
begin
    case param_is(A, result, [
    {1} tpRangeInteger,
    {2} tpRange,
    {3} tpSequence,
    {4} tpTensor1d]) of
        1: result := TVInteger.Create((A as TVRangeInteger).high);
        2: result := TVFloat.Create((A as TVRange).f_high);
        3: result := TVInteger.Create((A as TVSequence).high);
        4: with T_(A) do result := TVInteger.Create(Length(e) div dim[0] -1);
    end;
end;


function ifu_second             (const A: TValue): TValue;
begin
    case param_is(A, result, [
    {1} tpList]) of
        1: if (A as TVList).Count>1
            then result := (A as TVList)[1].Copy
            else result := TVList.Create;
    end;
end;


function ifu_last               (const A: TValue): TValue;
begin
    case param_is(A, result, [
    {1} vpSequenceNotEmpty]) of
        1: result := (A as TVSequence).Get((A as TVSequence).high);
    end;
end;


function if_prepend             (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpListWithLastList]) of
        1: begin
            result := PL.L[0].subseq(0,-1);
            (result as TVList).Append(PL.L[0][PL.L[0].high].Copy as TVList);
        end;
    end;
end;


function ifa_subseq            (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSequence, tpInteger, tpNIL,
        tpSequence, tpInteger, tpInteger,
        tpTensor1d, tpInteger, tpInteger]) of
        1: result := (PL[0] as TVSequence).tail(I_(PL[1]));
        2: result := (PL[0] as TVSequence).subseq(I_(PL[1]), I_(PL[2]));
        3: result := TVTensor.Create(tensor_subseq(T_(PL[0]), I_(PL[1]), I_(PL[2])));
    end;
end;


function ifa_subsequences      (const PL: TValues; {%H-}call: TCallProc): TValue;
var i, ln, step: integer; seq: TVSequence; res: TVList;
begin
    case params_is(PL, result, [
        tpSequence, vpIntegerPositive, vpIntegerPositiveOrNil]) of
        1: begin
            res := TVList.Create;
            result := res;
            seq := PL[0] as TVSequence;
            ln := I_(PL[1]);
            if tpTrue(PL[2]) then step := I_(PL[2]) else step := ln;
            i := 0;
            while i<=seq.Count-ln do
                begin
                    res.Add(seq.subseq(i, i+ln));
                    inc(i, step);
            	end;
            if i-step+ln<seq.Count then res.Add(seq.subseq(i, seq.Count));
        end;
    end;
end;


function if_subrec              (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; r: TVRecord; res: TVRecord; f: TVList;
begin
    case params_is(PL, result, [
        tpRecord, tpListOfSymbols]) of
        1: begin
            result := TVRecord.Create;
            res := result as TVRecord;
            r := PL[0] as TVRecord;
            f := PL.L[1];
            for i := 0 to f.high do res.AddSlot(f.SYM[i], r.GetSlot(f.SYM[i].N));
        end;
    end;
end;


function if_sort                 (const PL: TVList; call: TCallProc): TValue; //HIGH
var list: array of TValue; expr: TValues; if_compare: TInternalFunctionBody;

    procedure to_list;
    var i: integer;
    begin
        SetLength(list, PL.L[0].Count);
        for i := 0 to PL.L[0].high do list[i] := PL.L[0][i];
    end;

    procedure to_result;
    var i: integer;
    begin
        result := TVList.Create;
        for i := 0 to high(list) do (result as TVList).Add(list[i].Copy);
    end;

    procedure swap(a, b: integer); inline;
    var tmp: TValue;
    begin
        tmp := list[a];
        list[a] := list[b];
        list[b] := tmp;
    end;

    function compare(e: integer): boolean;
    var tmp: TValue;
    begin
        expr[1] := list[e];
        try
            tmp := nil;
            tmp := call(expr);
            result := tpTrue(tmp);
        finally
            tmp.Free;
        end;
    end;

    procedure sort(lo,hi: integer);
    var a, b: integer;
    begin
        if lo>=hi then exit;

        expr[2] := list[hi];

        a := lo;
        b := hi;
        for b := lo to hi-1 do
            if compare(b) then begin
                swap(a,b);
                Inc(a);
            end;
        swap(a,hi);

        sort(lo,a-1);
        sort(a+1,hi);
    end;

    function compare_by_internal(e: integer): boolean;
    var tmp: TValue; lexpr: TVList;
    begin
        expr[0] := list[e];
        try
            tmp := nil;
            //TODO: излишнее копирование при сортировке
            lexpr := TVList.CreateCopy(expr);
            tmp := if_compare(lexpr, nil);
            result := tpTrue(tmp);
        finally
            tmp.Free;
            lexpr.Free;
        end;
    end;

    procedure sort_by_internal(lo,hi: integer);
    var a, b: integer;
    begin
        if lo>=hi then exit;

        expr[1] := list[hi];

        a := lo;
        b := hi;
        for b := lo to hi-1 do
            if compare_by_internal(b) then begin
                swap(a,b);
                Inc(a);
            end;
        swap(a,hi);

        sort_by_internal(lo,a-1);
        sort_by_internal(a+1,hi);
    end;

begin

    if_compare := nil;
    case params_is(PL, result, [
        tpList, tpInternalRoutine, //TODO: здесь должна быть функция?
        tpList, tpSubprogram,
        tpList, tpNIL]) of
        1: begin
            to_list;
            if_compare := (PL[1] as TVInternalRoutine).body;
            expr := TValues.Create(nil, nil);
            sort_by_internal(0, high(list));
            to_result;
        end;
        2: begin
            to_list;
            expr := TValues.Create(PL[1], nil, nil);
            sort(0, high(list));
            to_result;
        end;
        3: begin
            to_list;
            if_compare := if_less;
            expr := TValues.Create(nil, nil);
            sort_by_internal(0, high(list));
            to_result;
        end;
    end;
end;


function if_slots               (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer;
begin
    case params_is(PL, result, [
        tpRecord,
        tpHashTable,
        tpList,
        tpRangeInteger]) of
        1: begin
            result := TVList.Create;
            for i := 0 to (PL[0] as TVRecord).count-1 do
                (result as TVList).Add(
                    TVSymbol.Create((PL[0] as TVRecord).name_n(i)));
        end;
        2: result := (PL[0] as TVHashTable).GetKeys;
        3: begin
            result := TVList.Create;
            for i := 0 to PL.L[0].high do (result as TVList).Add(TVInteger.Create(i));
        end;
        4: begin
            result := TVList.Create;
            for i := (PL[0] as TVRangeInteger).low to (PL[0] as TVRangeInteger).high-1 do
                (result as TVList).Add(TVInteger.Create(i));
        end;
    end;
end;


function ifu_elements                                  (const A: TValue): TValue;
var i: integer; dt: TDateTime; year, month, day, hour, minute, second, ms: WORD;
    ril, rih: integer; rrl, rrh: real;
begin
    case param_is(A, result, [
    {1} tpTensorReal,
    {2} tpTensor,
    {3} tpRecord,
    {4} tpDateTime,
    {5} tpRangeInteger,
    {6} tpRange]) of
        1: result := DoublesToListOfFloats(T_(A).e);
        2: result := ComplexesToListOfComplexes(T_(A).Complexes);
        3: begin
            result := TVList.Create;
            for i := 0 to (A as TVRecord).count-1 do
                (result as TVList).Add((A as TVRecord)[i].Copy);
        end;
        4: begin
            dt := (A as TVDateTime).fDT;
            DecodeDate(dt, year, month, day);
            DecodeTime(dt, hour, minute, second, ms);
            result := TVList.Create([
                TVInteger.Create(year), TVInteger.Create(month),  TVInteger.Create(day),
                TVInteger.Create(hour), TVInteger.Create(minute),
                TVFloat.Create(roundto(frac(dt)*86400 - hour*60*60 - minute*60, -6))]);
        end;
        5: begin
            RNG_(A, ril, rih);
            result := TVList.Create([TVInteger.Create(ril), TVInteger.Create(rih)]);
		end;
        6: begin
            RNG_(A, rrl, rrh);
            result := TVList.Create([TVFloat.Create(rrl), TVFloat.Create(rrh)]);
		end;
	end;
end;



function ifa_index_key         (const PL: TValues; {%H-}call: TCallProc): TValue;
var p: integer;
begin
    case params_is(PL, result, [
        tpList,      tpAny]) of
        1: if key_pos(PL[0] as TVList, PL[1], p)
            then Exit(TVInteger.Create(p+1))
            else result := TVList.Create();
    end;
end;


function ifa_index_max         (const PL: TValues; {%H-}call: TCallProc): TValue;
var i: integer; max_i: integer; max_r: double; index: integer; V: TValues;
begin
    case params_is(PL, result, [
    {1} tpNIL,
    {2} tpListOfIntegers,
    {3} tpListOfReals,
    {4} tpListOfDurations,
    {5} tpListOfDateTImes,
    {6} tpTensorReal]) of
        1: Exit(TVList.Create);
        2: begin
            index := 0;
            V := L_(PL[0]).ValueList;
            max_i := I_(V[0]);
            for i:=1 to high(V) do
                if I_(V[i])>max_i then begin index:=i; max_i:=I_(V[i]) end;
        end;
        3: begin
            index := 0;
            V := L_(PL[0]).ValueList;
            max_r := F_(V[0]);
            for i:=1 to high(V) do
                if F_(V[i])>max_r then begin index:=i; max_r:=F_(V[i]) end;
        end;
        4,5: begin
            index := 0;
            V := L_(PL[0]).ValueList;
            max_r := asTime(V[0]);
            for i:=1 to high(V) do
                if asTime(V[i])>max_r then begin index:=i; max_r:=asTime(V[i]) end;
        end;
        6: index := doubles_index_of_max(T_(PL[0]).e);
    end;

    result := TVInteger.Create(index);
end;


function ifa_index_min         (const PL: TValues; {%H-}call: TCallProc): TValue;
var i: integer; min_i: integer; min_r: double; index: integer; V: TValues;
begin
  case params_is(PL, result, [
  {1} tpNIL,
  {2} tpListOfIntegers,
  {3} tpListOfReals,
  {4} tpListOfDurations,
  {5} tpListOfDateTImes,
  {6} tpTensorReal]) of
      1: Exit(TVList.Create);
      2: begin
          index := 0;
          V := L_(PL[0]).ValueList;
          min_i := I_(V[0]);
          for i:=1 to high(V) do
              if I_(V[i])<min_i then begin index:=i; min_i:=I_(V[i]) end;
      end;
      3: begin
          index := 0;
          V := L_(PL[0]).ValueList;
          min_r := F_(V[0]);
          for i:=1 to high(V) do
              if F_(V[i])<min_r then begin index:=i; min_r:=F_(V[i]) end;
      end;
      4,5: begin
          index := 0;
          V := L_(PL[0]).ValueList;
          min_r := asTime(V[0]);
          for i:=1 to high(V) do
              if asTime(V[i])<min_r then begin index:=i; min_r:=asTime(V[i]) end;
      end;
      6: index := doubles_index_of_min(T_(PL[0]).e);
  end;

  result := TVInteger.Create(index);
end;




function if_curry               (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram, tpList]) of
        1: result := ifh_curry(PL[0] as TVSubprogram, L_(PL[1]));
    end;
end;


function if_composition         (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram,  tpListOfSubprograms]) of
        1: result := ifh_composition(PL[0] as TVSubprogram, PL.L[1]);
    end;
end;


function ifu_complement         (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpSubprogramPure1]) of
        1: result := ifh_complement(A as TVSubprogram);
    end;
end;


function if_filter              (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram, tpList]) of
        1: result := ifh_filter(PL, call, tpTrue);
    end;
end;


function if_filter_th           (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram, tpList]) of
        1: result := ifh_filter_th(call, PL[0] as TVSubprogram, PL.L[1]);
    end;
end;


function if_reject              (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram, tpList]) of
        1: result := ifh_filter(PL, call, tpNIL);
    end;
end;


function if_reject_th           (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram, tpList]) of
        1: result := ifh_reject_th(call, PL[0] as TVSubprogram, PL.L[1]);
    end;
end;


//function ifh_tensor_fold_function(V: TValue): TTensorFoldFunctionR;
//var b: TVInternalArrayFunction;
//begin
//    if (V is TVInternalArrayFunction) then b := @((V as TVInternalArrayFunction).body);
//    if b = @ifa_max then result := tffMax else
//    if b = @ifa_min then result := tffMin else
//    if b = @ifa_mul then result := tffProd else
//    if b = @ifa_add_b then result := tffSum else
//    raise ELE.Create(V.AsString, 'tensor/invalid fold');
//end;


function if_fold                     (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
    {1} tpSubprogram,          tpList,        tpNILnil,
    {2} vpKeywordFoldFunction, tpTensorReal,  vpNatural]) of
        1: case PL.L[1].Count of
            0: result := TVList.Create;
            1: result := PL.L[1][0].Copy;
            else result := ifh_fold(call, PL[0] as TVSubprogram, PL.L[1], 0, PL.L[1].high);
        end;
        2: result := TVTensor.Create(tensor_fold(T_(PL[1]), I_(PL[2]),
                ifh_keyword_to_tensor_folding_function(PL[0])));
	end;
end;


function if_count                    (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram, tpList]) of
        1: result := TVInteger.Create(ifh_count(call, PL[0] as TVSubprogram, PL.L[1]));
    end;
end;


function if_map                      (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram, tpNIL,
        tpSubprogram, vpListMatrix]) of
        1: result := TVList.Create;
        2: result := ifh_map(call, PL[0] as TVSubprogram, PL.L[1]);
    end;
end;


function if_map_tail                 (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram, tpNIL,
        tpSubprogram, vpListMatrix]) of
        1: result := TVList.Create;
        2: result := ifh_map_tail(call, PL[0] as TVSubprogram, PL.L[1]);
    end;
end;


function if_map_tree                 (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram, tpNIL,
        tpSubprogram, vpListMatrix]) of
        1: result := TVList.Create;
        2: result := ifh_map_tree(call, PL[0] as TVSubprogram, PL.L[1]);
    end;
end;

function if_map_th              (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram, tpNIL,
        tpSubprogram, vpListMatrix]) of
        1: result := TVList.Create;
        2: result := ifh_map_th(call,PL[0] as TVSubprogram, PL.L[1]);
    end;
end;


function if_map_concatenate     (const PL: TVList; call: TCallProc): TValue;
var L: TVList; i: integer; s: unicodestring;
begin try
    L := nil;
    result := nil;
    L := if_map(PL, call) as TVList;

    if tpListOfLists(L)
    then begin
        result := TVList.Create;
        for i := 0 to L.high do (result as TVList).Append(L[i].Copy as TVList);
    end                 //TODO: излишнее копирование элементов
    else

    if tpListOfStrings(L)
    then begin
        s := '';
        for i := 0 to L.high do s := s + L.S[i];
        result := TVString.Create(s);
    end
    else

    raise ELE.InvalidParameters(PL.AsString);
finally
    L.Free;
end;end;


function if_map_atom            (const PL: TVList; call: TCallProc): TValue;
var expr: TValues; i: integer;
begin
    case params_is(PL, result, [
        tpSubprogram, tpList,
        tpSubprogram, tpAtom]) of
        1: begin
            expr := TValues.Create(PL[0], nil);
            result := TVList.Create;
            for i := 0 to PL.L[1].high do
                begin
                    expr[1] := PL.L[1][i];
                    (result as TVList).Add(call(expr));
                end;
        end;
        2: result := call(PL.ValueList);
    end;
end;


function if_at_depth            (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpIntegerZero, tpSubprogram, tpList,
        vpNatural, tpSubprogram, tpList]) of
        1: result := call(PL.CdrValueList);
        2: begin
            result := PL[2].Copy;
            ifh_at_depth(PL.I[0], PL[1] as TVSubprogram, result as TVList, call);
        end;
    end;
end;


function if_apply               (const PL: TVList; call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpSubprogram, vpListWithLastList]) of
        1: result := ifh_apply(PL[0] as TVSubprogram, PL.L[1], call);
    end;
end;


function if_thread              (const PL: TVList; {%H-}call: TCallProc): TValue;
var expr: TVList;
begin
    case params_is(PL, result, [
        tpSubprogram, tpList]) of
        1: try
            expr := TVList.Create([PL[0].Copy]);
            expr.Append(PL.L[1].Copy as TVList);
            result := TVThread.Create(TLThread.Create(expr));
        finally
            expr.Free;
        end;
    end;
end;


function if_queue               (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    no_params(PL);
    result := TVQueue.Create(TQueue.Create);
end;


function if_wait                (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpThread,
        tpQueue,
        vpDurationNotNegative,
        vpRealNotNegative,
        tpProcess,
        tpServer]) of
        1: result := (PL[0] as TVThread).target.WaitResult;
        2: result := (PL[0] as TVQueue).target.wait as TValue;
        3: begin
            result := CreateTRUE;
            sleep(round((PL[0] as TVDuration).fDT*1000*60*60*24));
        end;
        4: begin
            result := CreateTRUE;
            sleep(round((PL[0] as TVReal).F*1000));
        end;
        5: begin
            ((PL[0] as TVStream).target as TLProcess).proc.WaitOnExit;
            result := TVString.Create((PL[0] as TVStream).target.read_string(-1));
        end;
        6: begin
            {$IFDEF NETWORK}
            result := TVStream.Create(TLSocket.Create(
            ((PL[0] as TVServer).target as TLServer).server.WaitConnection));
            {$ENDIF}
        end;
    end;
end;


function if_union               (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [tpListOfLists]) of
        1: result := ifh_union(PL.L[0]);
    end;
end;


function if_intersection        (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpListOfLists,
        tpListOfRangesInteger,
        tpListOfRanges]) of
        1: result := ifh_intersection(PL.L[0]);
        2: result := ifh_range_integer_intersection(L_(PL[0]));
        3: result := ifh_range_float_intersection(L_(PL[0]));
    end;
end;


function if_difference          (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [tpList, tpList]) of
        1: result := ifh_difference(PL.L[0], PL.L[1]);
    end;
end;


function ifu_reverse            (const A: TValue): TValue;
begin
    case param_is(A, result, [tpList]) of
        1: result := ifh_reverse(A as TVList)
    end;
end;


function ifu_transposed         (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpTensor2d,
        tpNIL,
        vpListMatrix]) of
        1: result := TVTensor.Create(matrix_transpose(T_(A)));
        2: result := TVList.Create();
		3: result := ifh_transpose(A as TVList);
	end;
end;


function ifa_position         (const PL: TValues; {%H-}call: TCallProc): TValue;
var p: integer; L: TVList;
begin
    case params_is(PL, result, [
        //sequence  element
    {1} tpList,     tpAny,
    {2} tpString,   tpString]) of
        1: begin
            L := PL[0] as TVList;
            for p := 0 to L.high do
                if equal(L[p], PL[1]) then Exit(TVInteger.Create(p));
            result := TVList.Create(nil);
		end;
		2: begin
            p := PosU(S_(PL[1]), S_(PL[0])) - 1;
            if p>=0
            then result := TVInteger.Create(p)
            else result := TVList.Create;
		end;
	end;
end;


function ifu_length             (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpSequence,
        tpRangeInteger,
        tpRange,
        tpTensor1d]) of
        1: result := TVInteger.Create((A as TVSequence).Count);
        2: result := TVInteger.Create((A as TVRangeInteger).Count());
        3: result := TVFloat.Create((A as TVRange).f_length());
        4: result := TVInteger.Create(T_(A).size(1));
    end;
end;


function ifu_size                                      (const A: TValue): TValue;
var size: TIntegers;
begin
    case param_is(A, result, [
        tpTensor]) of
        1: begin
            size := (A as TVTensor).t.dim;
            result := IntegersToListOfIntegers(Copy(size, Length(size) div 2, Length(size) div 2));
		end;
    end;
end;


function ifu_width              (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpTensor]) of
        1: result := TVInteger.Create(T_(A).size(1));
    end;
end;


function if_list                (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpList]) of
        1: result := PL.L[0].Copy;
    end;
end;


function ifa_of_length          (const PL: TValues; {%H-}call: TCallProc): TValue;
var n, i: integer; L: TVList; V, src: TValues;
begin
    case params_is(PL, result, [
        vpNatural,     tpListOrNIL,    tpAny]) of
		1: begin
            SetLength(V, I_(PL[0]));

            if PL[1]=nil
            then src := nil
            else src := LV_(PL[1]);

            for i := 0 to min(high(src), high(V)) do V[i] := src[i].Copy;

            if PL[2]=nil
            then for i := Length(src) to high(V) do V[i] := TVList.Create
            else for i := Length(src) to high(V) do V[i] := PL[2].Copy;

            result := TVList.Create(V);
        end;
    end;
end;


function ifa_plain_list        (const PL: TValues; {%H-}call: TCallProc): TValue;
var VL: TValues; i: integer; res: TVList;
begin
    res := TVList.Create;
    result := res;
    VL := (PL[0] as TVListRest).V;
    for i := 0 to high(VL) do
        if VL[i] is TVList
        then res.Append(L_(VL[i].Copy))
        else res.Add(VL[i].Copy);
end;


function if_record              (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer;
begin
    case params_is(PL, result, [
        vpListSymbolValue]) of
        1: begin
            result := TVRecord.Create;
            i := 0;
            while i<(PL.L[0].Count div 2) do begin
                (result as TVRecord).AddSlot(PL.L[0].SYM[i*2], PL.L[0][i*2+1].Copy);
                Inc(i);
            end;
        end;
    end;
end;


function if_record_as           (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; rec: TVRecord;
begin
    case params_is(PL, result, [
        tpRecord, vpListSymbolValue]) of
        1: begin
            result := PL[0].Copy;
            rec := result as TVRecord;
            i := 0;
            while i<(PL.L[1].Count div 2) do
                begin
                    rec.slot[PL.L[1].SYM[i*2].N] := PL.L[1][i*2+1].Copy;
                    Inc(i);
                end;
        end;
    end;
end;


function if_hash_table          (const PL: TVList; {%H-}call: TCallProc): TValue;
var ht: TVHashTable; i: integer;
begin
    case params_is(PL, result, [
        vpListEvenLength]) of
        1: begin
            ht := TVHashTable.Create;
            for i := 0 to PL.L[0].Count div 2 - 1 do
                    ht[ht.Index(PL.L[0][i*2])] := PL.L[0][i*2+1].Copy;
			result := ht;
		end;
	end;
end;


function if_concatenate         (const PL: TVList; {%H-}call: TCallProc): TValue;
var sres: unicodestring; i,j: integer;
begin
    case params_is(PL, result, [
        tpListOfLists,
        tpListOfStrings,
        tpListOfByteses,
        tpListOfTensors]) of
        1: begin
            result := TVList.Create;
            for i := 0 to PL.L[0].high do
                (result as TVList).Append(PL.L[0][i].Copy as TVList);
        end;
        2: begin
            sres := '';
            for i := 0 to PL.L[0].high do
                sres := sres + PL.L[0].S[i];
            result := TVString.Create(sres);
        end;
        3: begin
            result := TVBytes.Create;
            //TODO: неэффективная конкатенация TVByteVector
            for i := 0 to PL.L[0].high do begin
                for j := 0 to (PL.L[0][i] as TVBytes).High do
                    (result as TVBytes).Add(
                        (PL.L[0][i] as TVBytes).bytes[j]);
            end;
        end;
        4: result := TVTensor.Create(tensor_concatenate(ListOfTensorsToTensors(L_(PL[0]))));
	end;
end;



function if_interlaced          (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpListOfLists]) of
        1: result := ifh_interlaced(PL.L[0]);
    end;
end;


function if_group               (const PL: TVList; call: TCallProc): TValue;
begin
    //функция группировки. Разбивает переданный список на группы в соответствии
    //с переданными предикатами. Элементы, соответствующие более чем одному
    //предикату попадают в первую подходящую группу, не подошедшие никуда
    // попадают в последнюю
    case params_is(PL, result, [
        tpList, tpListOfSubprograms]) of
        1: result := ifh_group(PL.L[0], PL.L[1], call);
    end;
end;


function ifa_group_by               (const PL: TValues; call: TCallProc): TValue;
begin
    //Функция группировки. Разбивает переданный список на группы в соответствии
    //с критерием, возвращаемым переданной функцией или по значению поля, если
    // первый аргумент символ.
    //флаг :KEY-VALUE включает вывод в виде списка ключ-значение
    //целое число трактуется как количество групп (критерий должен быть номером группы)
    case params_is(PL, result, [
        //property    <list>           output-mode
        tpSubprogram, tpList,          tpAny,
        tpSymbol,     tpListOfRecords, tpAny]) of

        1: case param_is(PL[2], [vpNatural, tpNIL, vpKeyword_KEY_VALUE]) of
            1: result := ifh_group_by(L_(PL[1]), PL[0] as TVSubprogram, call, I_(PL[2]));
            2: result := ifh_group_by(L_(PL[1]), PL[0] as TVSubprogram, call, false);
            3: result := ifh_group_by(L_(PL[1]), PL[0] as TVSubprogram, call, true);
		end;

        2: case param_is(PL[2], [vpNatural, tpNIL, vpKeyword_KEY_VALUE]) of
            1: result := ifh_group_by(L_(PL[1]), PL[0] as TVSymbol, I_(PL[2]));
            2: result := ifh_group_by(L_(PL[1]), PL[0] as TVSymbol, false);
            3: result := ifh_group_by(L_(PL[1]), PL[0] as TVSymbol, true);
		end;

    end;
    //TODO: падает с nil reference если перепутать аргументы
end;


function if_strings_mismatch    (const PL: TVList; {%H-}call: TCallProc): TValue;
var mm: integer;
begin
    case params_is(PL, result, [
        tpString, tpString, tpNIL,
        tpString, tpString, vpKeyword_CASE_INSENSITIVE]) of
        1: mm := ifh_strings_mismatch(PL.S[0],PL.S[1]);
        2: mm := ifh_strings_mismatch(UnicodeUpperCase(PL.S[0]),UnicodeUpperCase(PL.S[1]));
    end;
    if mm<0
    then result := TVList.Create
    else result := TVInteger.Create(mm);
end;


function if_replace             (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; res: unicodestring;
begin
    case params_is(PL, result, [
        tpString, vpListOfStringsEvenLength]) of
        1: begin
            res := PL.S[0];
            for i := 0 to (Pl.L[1].Count div 2) - 1 do
                res := StringSubstitute(res, PL.L[1].S[i*2], PL.L[1].S[i*2+1]);
            result := TVString.Create(res);
        end;
    end;
end;


function if_substitute          (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpList, vpListEvenLength]) of
        1: result := ifh_list_substitute(PL.L[0],PL.L[1]);
    end;
end;


function if_update              (const PL: TVList; {%H-}call: TCallProc): TValue;
var n: integer;
begin
    case params_is(PL, result, [
        tpList,   tpInteger,          tpSubprogram,
        tpList,   tpInteger,          tpAny]) of
        1: begin
            n := PL.L[0].index(PL.I[1]);
            result := PL[0].Copy;
            (result as TVList)[n] := call(TValues.Create(PL[2], PL.L[0][n]));
        end;
        2: begin
            result := PL.L[0].Copy;
            (result as TVList)[PL.I[1]] := PL[2].Copy;
        end;
    end;
end;


function if_remove              (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer;
begin
    case params_is(PL, result, [
        tpList, tpAny,
        tpString, tpString]) of
        1: begin
            result := TVList.Create;
            for i := 0 to PL.L[0].high do
                if not equal(PL.L[0][i], PL[1])
                then (result as TVList).Add(PL.L[0][i].Copy);
        end;
        2: result := TVString.Create(StringSubstitute(PL.S[0], PL.S[1], ''));
    end;
end;


function ifu_remove_empty       (const A: TValue): TValue;
var i: integer; L, src: TVList;
begin
    case param_is(A, result, [
        tpList]) of
        1: begin
            L := TVList.Create;
            result := L;
            src := A as TVList;
            for i := 0 to src.high do
                if not vpEmpty(src[i]) then L.Add(src[i].Copy);
        end;
    end;
end;


function if_associations        (const PL: TVList; {%H-}call: TCallProc): TValue;
var res, L,ll: TVList; i, j: integer; by_head, lazy: boolean; k: TValue;
begin
    case params_is(PL, result, [
        tpListOfLists, tpAny, tpListOfKeywords]) of
        1,2: try
            L := PL.L[0];
            k := PL[1];
            by_head := ListContainKeyword(PL.L[2],':BY-HEAD');
            lazy    := ListContainKeyword(PL.L[2],':LAZY');
            res := TVList.Create;
            for i := L.high downto 0 do begin
                for j := 0 to L.L[i].high do begin
                    if equal(L.L[i][j], k) then begin
                        ll := L[i].Copy as TVList;
                        ll.extract(j).Free;
                        res.Append(ll);
                    end;
                    if by_head then break;
                end;
                if lazy and (res.Count>0) then break;
            end;
        finally
            result := res;
        end;
    end;
end;


function if_interval            (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer;
begin
    case params_is(PL, result, [
        tpListOfReals, tpReal]) of
        1: begin
            for i := 0 to PL.L[0].high do
                if PL.F[1]<=PL.L[0].F[i] then
                    Exit(TVInteger.Create(i));
            result := TVInteger.Create(PL.L[0].Count);
        end;
    end;
end;


function ifa_element           (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
    {1} tpTensorReal, vpListOfNaturals,
    {2} tpTensorReal, vpNatural,
    {3} tpTensor,     vpListOfNaturals,
    {4} tpTensor,     vpNatural,

    {5} tpCompound, tpAny]) of
        1: result := TVFloat.Create(T_(PL[0]).GetR(ListOfIntegersToIntegers(L_(PL[1]))));
        2: result := TVFloat.Create(T_(PL[0]).e[I_(PL[1])]);
        3: result := TVComplex.Create(T_(PL[0]).GetC(ListOfIntegersToIntegers(L_(PL[1]))));
        4: result := TVComplex.Create(T_(PL[0]).GetC(I_(PL[1])));

        5: result := (PL[0] as TVCompound).Get(PL[1]);
    end;
end;


function if_n_th                (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpList, vpNatural]) of
        1:
            if PL.L[0].Count>PL.I[1]
            then result := PL.L[0][PL.I[1]].Copy
            else result := TVList.Create;
    end;
end;


function ifh_graphs_original_representation(src:TVList; g: TIntegers2): TVList;
var i, j: integer; subgraph: TVList;
begin
    result := TVList.Create;
    for i := 0 to high(g) do
        begin
            subgraph := TVList.Create;
            for j := 0 to high(g[i]) do subgraph.Add(src[g[i][j]].Copy);
            result.Add(subgraph);
		end;
end;


function ifa_graph_connectivity(const PL: TValues; {%H-}call: TCallProc): TValue;
var src: TVList;
begin
    case params_is(PL, result, [
    {1} tpListOfCompounds, tpTrue, tpTrue]) of
        1: begin
            src := PL[0] as TVList;
            result := ifh_graphs_original_representation(src,
                mar_math_graph.Connectivity(ListOfCompoundsToGraph(src, PL[1], PL[2])));
		end;
	end;
end;


function ifa_graph_path        (const PL: TValues; {%H-}call: TCallProc): TValue;
var src: TVList;
begin
    case params_is(PL, result, [
        //graph            n1      n2      from   to
    {1} tpListOfCompounds, tpTrue, tpTrue, tpAny, tpAny]) of
        1: begin
            src := PL[0] as TVList;
            result := ifh_graphs_original_representation(src,
                mar_math_graph.Paths(ListOfCompoundsToGraph(src, PL[1], PL[2]),
                                     ifh_str(PL[3]),
                                     ifh_str(PL[4])));
		end;
	end;
end;



function if_bytes               (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer;
begin
    case params_is(PL, result, [
        vpListOfByte]) of
        1: begin
            result := TVBytes.Create;
            (result as TVBytes).SetCount(PL.L[0].Count);
            (result as TVBytes).SetCount(0);
            for i:= 0 to PL.L[0].High do
                (result as TVBytes).Add(PL.L[0].I[i]);
        end;
    end;
end;


function if_bit_or              (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; a, b: TVBytes;
begin
    //TODO: побитовые операторы имеют много общего кода, нужно разделить
    case params_is(PL, result, [
        tpBytes,   tpBytes,
        tpInteger, tpInteger]) of
        1: begin
            a := PL[0] as TVBytes;
            b := PL[1] as TVBytes;
            if a.Count<>b.Count then raise ELE.Create('inequal length', 'invalid parameters');
            result := TVBytes.Create;
            (result as TVBytes).SetCount(a.Count);
            for i := 0 to a.High do
                (result as TVBytes).fBytes[i] := a.fBytes[i] or b.fBytes[i];
        end;
        2: result := TVInteger.Create(PL.I[0] or PL.I[1]);
    end;
end;


function if_bit_not             (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; a: TVBytes;
begin
    case params_is(PL, result, [
        tpBytes,
        tpInteger]) of
        1: begin
            a := PL[0] as TVBytes;
            result := TVBytes.Create;
            (result as TVBytes).SetCount(a.Count);
            for i := 0 to a.High do
                (result as TVBytes).fBytes[i] := not a.fBytes[i];
        end;
        2: result := TVInteger.Create(not PL.I[0]);
    end;
end;


function if_bit_and             (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; a, b: TVBytes;
begin
    case params_is(PL, result, [
        tpBytes,   tpBytes,
        tpInteger, tpInteger]) of
        1: begin
            a := PL[0] as TVBytes;
            b := PL[1] as TVBytes;
            if a.Count<>b.Count then raise ELE.Create('inequal length', 'invalid parameters');
            result := TVBytes.Create;
            (result as TVBytes).SetCount(a.Count);
            for i := 0 to a.High do
                (result as TVBytes).fBytes[i] := a.fBytes[i] and b.fBytes[i];
        end;
        2: result := TVInteger.Create(PL.I[0] and PL.I[1]);
    end;
end;


function if_bit_xor             (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; a, b: TVBytes;
begin
    case params_is(PL, result, [
        tpBytes,   tpBytes,
        tpInteger, tpInteger]) of
        1: begin
            a := PL[0] as TVBytes;
            b := PL[1] as TVBytes;
            if a.Count<>b.Count then raise ELE.Create('inequal length', 'invalid parameters');
            result := TVBytes.Create;
            (result as TVBytes).SetCount(a.Count);
            for i := 0 to a.High do
                (result as TVBytes).fBytes[i] := a.fBytes[i] xor b.fBytes[i];
        end;
        2: result := TVInteger.Create(PL.I[0] xor PL.I[1]);
    end;
end;


function if_shift               (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpInteger, vpNatural,         tpNIL,
        tpInteger, vpIntegerNegative, tpNIL,
        tpInteger, vpNatural,         vpNatural,
        tpInteger, vpIntegerNegative, vpNatural]) of
        1: result := TVInteger.Create(PL.I[0] shl PL.I[1]);
        2: result := TVInteger.Create(PL.I[0] shr abs(PL.I[1]));
        3: result := TVInteger.Create((PL.I[0] shl PL.I[1]) and (2**PL.I[2]-1));
        4: result := TVInteger.Create((PL.I[0] shr abs(PL.I[1])) and (2**PL.I[2]-1));
    end;
end;


function if_crc32               (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpBytes,
        tpString]) of
        1: result := TVInteger.Create((PL[0] as TVBytes).crc32);
        2: result := TVInteger.Create((PL[0] as TVString).crc32);
    end;
end;


function if_crc8                (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpIntegerByte, tpNIL,
        vpIntegerByte, vpIntegerByte,
        tpBytes, tpNIL]) of
        1: result := TVInteger.Create(crc8(byte(PL.I[0]), 0));
        2: result := TVInteger.Create(crc8(byte(PL.I[0]), byte(PL.I[1])));
        3: result := TVInteger.Create(crc8((PL[0] as TVBytes).fBytes, 0));
    end;
end;


function if_character           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpNatural]) of
        1: result := TVString.Create(ifh_character(PL.I[0]));
    end;
end;


function if_byte_vector_to_string(const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpBytes, vpKeywordEncodingOrNIL]) of
        1: result := TVString.Create(
                bytes_to_string(
                    (PL[0] as TVBytes).fBytes,
                    ifh_keyword_to_encoding(PL[1])));
    end;
end;


function if_string_to_bytes      (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, vpKeywordEncodingOrNIL]) of
        1: begin
            result := TVBytes.Create;
            (result as TVBytes).fBytes := string_to_bytes(
                                            PL.S[0],
                                            ifh_keyword_to_encoding(PL[1]));
        end;
    end;
end;


function if_base64               (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString,
        tpBytes]) of
        1: begin
            result := TVBytes.Create;
            (result as TVBytes).fBytes := decode_base64_2(PL.S[0]);
        end;
        2: begin
            result := TVString.Create(encode_base64((PL[0] as TVBytes).fBytes));
        end;
    end;
end;


function if_assertion           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpTrue, tpList,
        tpNIL,  tpList]) of
        1: result := CreateTRUE;
        2: raise ELE.Create(ifh_format(PL.L[1]), 'assertion');
    end;
end;


function if_documentation       (const PL: TVList; {%H-}call: TCallProc): TValue;
var proc: TVRoutine; int_fun: TVInternalRoutine;
begin
    case params_is(PL, result, [
        tpRoutine,
        tpInternalRoutine,
        tpAny]) of
        1: begin
            proc := PL[0] as TVRoutine;
            WriteLn(proc.AsString);
            WriteLn(usign_asstring(proc.sign));
            WriteLn(proc.ClassName);
            if tpString(proc.body[0])
            then WriteLn(ifh_single_spaces(proc.body[0].AsString));
            WriteLn(':REST ',proc.rest.AsString);
        end;
        2: begin
            int_fun := PL[0] as TVInternalRoutine;
            WriteLn(int_fun.AsString);
            WriteLn(usign_AsString(int_fun.sign));
        end;
        3: begin
            WriteLn(PL[0].AsString);
            WriteLn(PL[0].ClassName);
        end;
    end;
    result := CreateTRUE;
end;


function if_signature           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpRoutine,
        tpInternalRoutine,
        tpPredicate,
        tpInternalUnaryRoutine]) of
        1: result := sign_to_list((PL[0] as TVRoutine).sign);
        2: result := sign_to_list((PL[0] as TVInternalRoutine).sign);
        3,4: result := TVList.Create([TVSymbol.Create('X')]);
    end;
end;


function if_directory           (const PL: TVList; {%H-}call: TCallProc): TValue;
var mask: unicodestring;
begin
    case params_is(PL, result, [
        tpString, tpStringOrNIL, vpKeywordVisibilityOrNIL]) of
        1: begin
            mask := DirSep(default(PL[1],'*'));
            result := StringArrayToListOfStrings(ifh_directory(
                ifhh_path(PL.S[0]),
                ExcludeTrailingPathDelimiter(mask),
                end_of_string_is(mask,DirectorySeparator),
                ifh_keyword_to_file_visibility(PL[2])));
        end;
    end;
end;


function if_directory_tree      (const PL: TVList; {%H-}call: TCallProc): TValue;
var mask: unicodestring;
begin
    case params_is(PL, result, [
        //dir     mask           visibility
        tpString, tpStringOrNIL, vpKeywordVisibilityOrNIL]) of
        1: begin
            mask := DirSep(default(PL[1],'*'));
            result := StringArrayToListOfStrings(ifh_directory_tree(
                ifhh_path(PL.S[0]),
                ExcludeTrailingPathDelimiter(mask),
                end_of_string_is(mask, DirectorySeparator),
                ifh_keyword_to_file_visibility(PL[2])));
        end;
    end;
end;


function if_sleep               (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpNatural]) of
        1: begin
            Sleep(PL.I[0]);
            result := CreateTRUE;
        end;
    end;
end;


function if_platform            (const PL: TVList; {%H-}call: TCallProc): TValue;
const p: unicodestring = ''
    {$IFDEF WINDOWS}+' WINDOWS '{$ENDIF}
    {$IFDEF LINUX}+' LINUX '{$ENDIF}
    {$IFDEF ENDIAN_BIG}+' BIG-ENDIAN '{$ENDIF}
    {$IFDEF ENDIAN_LITTLE}+' LITTLE-ENDIAN '{$ENDIF}
    {$IFDEF CPUX86_64}+' X86_64 '{$ENDIF}
    {$IFDEF CPU32}+' CPU32 '{$ENDIF}
    {$IFDEF CPU64}+' CPU64 '{$ENDIF}
    ;
var i: integer; res: boolean;
begin
    case params_is(PL, result, [
        tpListOfStrings]) of
        1: begin
            res := true;
            for i := 0 to PL.L[0].high do
                res := res and (PosU(' '+UnicodeUpperCase(PL.L[0].S[i])+' ',p)>0);
            bool_to_TV(res, result);
        end;
    end;
end;


function if_run_command         (const PL: TVList; {%H-}call: TCallProc): TValue;
    {$IFDEF WINDOWS}
    procedure run(cmdln, dir: unicodestring);
    var cmd, params: unicodestring; p, i, h: integer; q: boolean;
    begin
        q := false;
        p := Length(cmdln)-1;
        for i := 1 to length(cmdln) do
            case cmdln[i] of
                '"': q := not q;
                ' ': if not q then begin
                    p := i;
                    Break;
                end;
            end;
        cmd := cmdln[1..p-1];
        params := cmdln[p+1..length(cmdln)];
        if dir<>''
        then h := ShellExecuteW(0, nil, PWideChar(cmd), PWideChar(params), PWideChar(dir), 4)
        else h := ShellExecuteW(0, nil, PWideChar(cmd), PWideChar(params), nil, 4);

        case h of
            0: raise ELE.Create('0', 'ShellExecute');
            ERROR_FILE_NOT_FOUND: raise ELE.Create('ERROR_FILE_NOT_FOUND', 'ShellExecute');
            ERROR_PATH_NOT_FOUND: raise ELE.Create('ERROR_PATH_NOT_FOUND', 'ShellExecute');
            ERROR_BAD_FORMAT: raise ELE.Create('ERROR_BAD_FORMAT', 'ShellExecute');
            SE_ERR_ACCESSDENIED : raise ELE.Create('SE_ERR_ACCESSDENIED', 'ShellExecute');
            SE_ERR_ASSOCINCOMPLETE: raise ELE.Create('SE_ERR_ASSOCINCOMPLETE', 'ShellExecute');
            SE_ERR_DDEBUSY: raise ELE.Create('SE_ERR_DDEBUSY', 'ShellExecute');
            SE_ERR_DDEFAIL: raise ELE.Create('SE_ERR_DDEFAIL', 'ShellExecute');
            SE_ERR_DDETIMEOUT: raise ELE.Create('SE_ERR_DDETIMEOUT', 'ShellExecute');
            SE_ERR_DLLNOTFOUND: raise ELE.Create('SE_ERR_DLLNOTFOUND', 'ShellExecute');
            SE_ERR_NOASSOC: raise ELE.Create('SE_ERR_NOASSOC', 'ShellExecute');
            SE_ERR_OOM: raise ELE.Create('SE_ERR_OOM', 'ShellExecute');
            SE_ERR_SHARE: raise ELE.Create('SE_ERR_SHARE', 'ShellExecute');
        end;
    end;
    {$ELSE}
var p: TProcess;
    {$ENDIF}
begin
    case params_is(PL, result, [
        tpString, tpStringOrNIL]) of
        1: begin
            {$IFDEF WINDOWS}
            run(DirSep(PL.S[0]), DirSep(default(PL[1], '')));
            {$ELSE}
            p := TProcess.Create(nil);
            p.{%H-}CommandLine:= DirSep(PL.S[0]){%H-};
            p.CurrentDirectory:={%H-}DirSep(default(PL[1], ''));
            p.Options:=[poNewConsole, poNewProcessGroup];
            p.Execute;
            p.Free;
            {$ENDIF}
        end;
    end;
    result := CreateTRUE;
end;


function if_command             (const PL: TVList; {%H-}call: TCallProc): TValue;
var p: TProcess; i: integer; cmd: unicodestring;
begin
    case params_is(PL, result, [
        tpAny, tpList]) of
        1: try
            p := TProcess.Create(nil);
            cmd := ifh_str(PL[0]);
            for i := 0 to PL.L[1].high do
                cmd := cmd + ' "'+ifh_str(PL.L[1][i])+'"';
            p.CommandLine:={$IFDEF WINDOWS}UnicodeToWinCP{$ENDIF}(cmd);
            p.Options:=[poWaitOnExit,poStderrToOutPut];
            p.Execute;
            result := TVInteger.Create(p.ExitCode);
        finally
            p.Free;
        end;
    end;
end;


function if_process             (const PL: TVList; {%H-}call: TCallProc): TValue;
var dir: unicodestring;
begin
    case params_is(PL, result, [
        tpString, tpStringOrNIL, vpKeywordEncodingOrNIL]) of
        1: begin
            if tpString(PL[1]) then dir := PL.S[1] else dir := '.';
            result := TVStream.Create(TLProcess.Run(PL.S[0], dir));

            (result as TVStream).target.Encoding :=
                                            ifh_keyword_to_encoding(PL[2]);
        end;
    end;
end;


function if_process_id          (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpNIL,
        tpProcess]) of
        1: result := TVInteger.Create(GetProcessID);
        2: result := TVInteger.Create(((PL[0] as TVStream).target as TLProcess).proc.ProcessID);
    end;
end;


function if_now                 (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    no_params(PL);
    result := TVDateTime.Create(now);
end;


function if_today               (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    no_params(PL);
    result := TVDateTime.Create(int(now));
end;


function if_tomorrow            (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    no_params(PL);
    result := TVDateTime.Create(int(now)+1);
end;


function if_yesterday           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    no_params(PL);
    result := TVDateTime.Create(int(now)-1);
end;

function if_time                (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpDateTime,
        tpNIL]) of
        1: result := TVDuration.Create(frac(asTime(PL[0])));
        2: result := TVDuration.Create(time);
        end;
end;


function if_open_file           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, vpKeywordFileModeOrNIL, vpKeywordEncodingOrNIL]) of
        1: result := TVStream.Create(
            TLFileStream.Create(
                        DirSep(PL.S[0]),
                        ifh_keyword_to_file_mode(PL[1]),
                        ifh_keyword_to_encoding(PL[2])));
    end;
end;


function if_zip_open            (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, vpKeywordFileModeOrNIL, vpKeywordEncodingOrNIL]) of
        1: result := TVZipArchivePointer.Create(
                TZipArchive.Open(DirSep(PL.S[0]),
                    ifh_keyword_to_file_mode(PL[1]),
                    ifh_keyword_to_encoding(PL[2])));
    end;
end;


function if_zip_filelist        (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; file_names: TStringArray;
begin
    case params_is(PL, result, [
        tpZIPArchivePointer]) of
        1: begin
            file_names := (PL[0] as TVZIPArchivePointer).Z.FileList;
            result := TVList.Create;
            for i := 0 to high(file_names) do
                (result as TVList).Add(TVString.Create(file_names[i]));
        end;
    end;
end;


function if_zip_file            (const PL: TVList; {%H-}call: TCallProc): TValue;
var zf: TLZIPFile;  enc: TStreamEncoding; mode: WORD;
begin
    case params_is(PL, result, [
        tpZIPArchivePointer, tpString, vpKeywordFileModeOrNIL, vpKeywordEncodingOrNIL]) of
        1: begin
            mode := ifh_keyword_to_file_mode(PL[2]);
            enc := ifh_keyword_to_encoding(PL[3]);

            zf := TLZipFile.Create(
                (PL[0] as TVZIPArchivePointer).Z.Ref as TZIPArchive,
                PL.S[1], mode, enc);
            if zf.active
            then result := TVStream.Create(zf)
            else begin
                zf.Free;
                result := TVList.Create;
            end;
        end;
    end;
end;


function if_zip_delete          (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpZIPArchivePointer, tpString]) of
        1: begin
            (PL[0] as TVZipArchivePointer).Z.Delete(PL.S[1]);
            result := CreateTRUE;
        end;
    end;
end;


function if_close_stream        (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpStream]) of
        1: begin
            (PL[0] as TVStream).target.close_stream;
            result := CreateTRUE;
        end;
    end;
end;


function if_text_from_file      (const PL: TVList; {%H-}call: TCallProc): TValue;
var f: TLFileStream;
begin
  case params_is(PL, result, [
      tpString, vpKeywordEncodingOrNIL]) of
      1: try
          f := nil;
          f := TLFileStream.Create(PL.S[0], fmOpenRead, ifh_keyword_to_encoding(PL[1]));
          result := TVString.Create(f.read_string(-1));
      finally
          f.Free;
      end;
  end;
end;


function if_text_to_file        (const PL: TVList; {%H-}call: TCallProc): TValue;
var f: TLFileStream;
begin
    case params_is(PL, result, [
        tpString, tpString, vpKeywordEncodingOrNIL]) of
        1: try
            f := nil;
            f := TLFileStream.Create(PL.S[0], fmCreate, ifh_keyword_to_encoding(PL[2]));
            result := CreateTRUE;
            f.write_string(PL.S[1]);
        finally
            f.Free;
        end;
    end;
end;


function if_bytes_from_file     (const PL: TVList; {%H-}call: TCallProc): TValue;
var f: TLFileStream;
begin
  case params_is(PL, result, [
      tpString]) of
      1: try
          f := nil;
          f := TLFileStream.Create(PL.S[0], fmOpenRead, seUTF8);
          result := TVBytes.Create;
          f.read_bytes((result as TVBytes).fBytes,-1);
      finally
          f.Free;
      end;
  end;
end;


function if_bytes_to_file       (const PL: TVList; {%H-}call: TCallProc): TValue;
var f: TLFileStream;
begin
    case params_is(PL, result, [
        tpString, tpBytes]) of
        1: try
            f := nil;
            f := TLFileStream.Create(PL.S[0], fmCreate, seUTF8);
            result := CreateTRUE;
            f.write_bytes((PL[1] as TVBytes).fBytes);
        finally
            f.Free;
        end;
    end;
end;


function if_deflate             (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; bv: TVBytes; cs: TCompressionStream; ms: TMemoryStream;
begin
    case params_is(PL, result, [
        vpStream, tpKeywordOrNIL, tpAny,
        tpBytes,  tpKeywordOrNil, tpAny]) of
        1:  begin
            result := TVStream.Create(
                    TLDeflateStream.Create(
                        (PL[0] as TVStream).target.Ref as TLStream,
                        tpTrue(PL[2]),
                        ifh_keyword_to_encoding(PL[1])));
        end;
        2: begin
            bv := PL[0] as TVBytes;
            ms := TMemoryStream.Create;
            ms.Position :=0 ;
            cs := TCompressionStream.create(clDefault, ms, not tpTrue(PL[2]));
            for i := 0 to high(bv.fBytes) do cs.WriteByte(bv.fBytes[i]);
            cs.Free;
            ms.Position := 0;
            result := TVBytes.Create;
            bv := result as TVBytes;
            bv.SetCount(ms.Size);
            for i := 0 to high(bv.fBytes) do bv.fBytes[i] := ms.ReadByte;
            ms.Free;
        end;
    end;
end;


function if_inflate             (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; bv: TVBytes; ds: TDecompressionStream; ms: TMemoryStream;
begin
    case params_is(PL, result, [
        vpStream, tpKeywordOrNIL, tpAny,
        tpBytes,  tpKeywordOrNil, tpAny]) of
        1:  begin
            result := TVStream.Create(
                    TLInflateStream.Create(
                        (PL[0] as TVStream).target.Ref as TLStream,
                        tpTrue(PL[2]),
                        ifh_keyword_to_encoding(PL[1])));
        end;
        2: begin
            bv := PL[0] as TVBytes;
            ms := TMemoryStream.Create;
            ms.SetSize(Length(bv.fBytes));
            ms.Position :=0 ;
            for i := 0 to high(bv.fBytes) do ms.WriteByte(bv.fBytes[i]);
            try
                ms.Position := 0;
                ds := TDecompressionStream.create(ms, not tpTrue(PL[2]));
                result := TVBytes.Create;
                bv := result as TVBytes;
                try
                    while true do bv.Add(ds.ReadByte);
                except
                    //on E:EDecompressionError do;
                end;
            finally
                ms.Free;
                ds.Free;
            end;
        end;
    end;
end;


function if_memory_stream       (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpBytes,
        tpString,
        tpNIL]) of
        1: result := TVStream.Create(
                    TLMemoryStream.Create(
                        (PL[0] as TVBytes).fBytes,
                        seUTF8));
        2: result := TVStream.Create(
                    TLMemoryStream.Create(
                        (PL[0] as TVString).S));
        3: result := TVStream.Create(TLMemoryStream.Create(seUTF8));
    end;
end;


function if_serial_port         (const PL: TVList; {%H-}call: TCallProc): TValue;
    function timeout(V: TValue): integer;
    begin if tpNIL(V) then result := 0 else result := (V as TVInteger).fI; end;
begin
    case params_is(PL, result, [
        tpString, vpIntegerPositive, vpKeywordEncodingOrNIL, vpNaturalOrNIL]) of
        1: result := TVStream.Create(
                    TLSerialStream.Create(
                        PL.S[0],
                        PL.I[1],
                        ifh_keyword_to_encoding(PL[2]),
                        timeout(PL[3])));
    end;
end;


function if_serial_discard_input(const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpStreamSerial]) of
        1: ((PL[0] as TVStream).target as TLSerialStream).discard_input;
    end;
    result := CreateTRUE;
end;


function if_stream_position     (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, vpNatural,
        vpStream, tpNIL]) of
        1: begin
            (PL[0] as TVStream).target.Position := PL.I[1];
            result := CreateTRUE;
        end;
        2: result := TVInteger.Create(
            (PL[0] as TVStream).target.Position);
    end;
end;


function if_stream_length       (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, tpNIL,
        vpStream, vpNatural]) of
        1: result := TVInteger.Create(
            (PL[0] as TVStream).target.Size);
        2: begin
            (PL[0] as TVStream).target.Size := PL.I[1];
            result := CreateTRUE;
        end;
    end;
end;


function if_stream_encoding     (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
    {1} vpStream, tpNIL,
    {2} vpStream, tpKeyword]) of
        1: ;
        2: (PL[0] as TVStream).target.Encoding :=
                                            ifh_keyword_to_encoding(PL[1]);
    end;
    result := TVSymbol.Create(':'+EncodingNames[
                                (PL[0] as TVStream).target.Encoding]);
end;


function ifu_read_byte          (const A: TValue): TValue;
begin
    case param_is(A, result, [
        vpStream]) of
        1: result := TVInteger.Create((A as TVStream).target.read_byte);
    end;
end;


function ifu_read_float_single  (const A: TValue): TValue;
begin
    case param_is(A, result, [
        vpStream]) of
        1: result := TVFloat.Create((A as TVStream).target.read_single);
    end;
end;


function ifu_read_float_double  (const A: TValue): TValue;
begin
    case param_is(A, result, [
        vpStream]) of
        1: result := TVFloat.Create((A as TVStream).target.read_double);
    end;
end;


function ifu_read_integer_8     (const A: TValue): TValue;
begin
    case param_is(A, result, [
        vpStream]) of
        1: result := TVInteger.Create((A as TVStream).target.read_i8);
    end;
end;


function ifu_read_integer_16    (const A: TValue): TValue;
begin
    case param_is(A, result, [
        vpStream]) of
        1: result := TVInteger.Create((A as TVStream).target.read_i16);
    end;
end;


function ifu_read_integer_32    (const A: TValue): TValue;
begin
    case param_is(A, result, [
        vpStream]) of
        1: result := TVInteger.Create((A as TVStream).target.read_i32);
    end;
end;


function ifu_read_integer_64    (const A: TValue): TValue;
begin
    case param_is(A, result, [
        vpStream]) of
        1: result := TVInteger.Create((A as TVStream).target.read_i64);
    end;
end;


function ifu_read_unsigned_16   (const A: TValue): TValue;
begin
    case param_is(A, result, [
        vpStream]) of
        1: result := TVInteger.Create((A as TVStream).target.read_WORD);
    end;
end;


function ifu_read_unsigned_32   (const A: TValue): TValue;
begin
    case param_is(A, result, [
        vpStream]) of
        1: result := TVInteger.Create((A as TVStream).target.read_DWORD);
    end;
end;


function if_read_bytes          (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, vpNatural,
        vpStream, vpKeyword_ALL]) of
        1: begin
            result := TVBytes.Create;
            (PL[0] as TVStream).target.read_bytes(
                (result as TVBytes).fBytes, PL.I[1]);
        end;
        2: begin
            result := TVBytes.Create;
            (PL[0] as TVStream).target.read_bytes(
                (result as TVBytes).fBytes, -1);
        end;
    end;
end;


function if_write_byte          (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, vpIntegerByte,
        vpStream, tpBytes]) of
        1: (PL[0] as TVStream).target.write_byte(PL.I[1]);
        2: (PL[0] as TVStream).target.write_bytes(
                (PL[1] as TVBytes).fBytes);
    end;
    result := CreateTRUE;
end;


function if_write_word          (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, vpIntegerWord]) of
        1: asStream(PL[0]).write_word(PL.I[1]);
    end;
    result := CreateTRUE;
end;


function if_write_dword         (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, vpIntegerDword]) of
        1: asStream(PL[0]).write_dword(PL.I[1]);
    end;
    result := CreateTRUE;
end;


function if_write_integer_8     (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, vpInteger8]) of
        1: asStream(PL[0]).write_i8(PL.I[1]);
    end;
    result := CreateTRUE;
end;


function if_write_integer_16    (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, vpInteger16]) of
        1: asStream(PL[0]).write_i16(PL.I[1]);
    end;
    result := CreateTRUE;
end;


function if_write_integer_32    (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, vpInteger32]) of
        1: asStream(PL[0]).write_i32(PL.I[1]);
    end;
    result := CreateTRUE;
end;


function if_write_integer_64    (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, tpInteger]) of
        1: asStream(PL[0]).write_i64(PL.I[1]);
    end;
    result := CreateTRUE;
end;


function if_write_float_single  (const PL: TVList; {%H-}call: TCallProc): TValue;
var s: TLStream; i: integer; L: TVList;
begin
    case params_is(PL, result, [
        vpStream, tpReal,
        vpStream, tpListOfReals]) of
        1: (PL[0] as TVStream).target.write_single(PL.F[1]);
        2: begin
            s := (PL[0] as TVStream).target;
            L := PL.L[1];
            for i := 0 to L.high do s.write_single(L.F[i]);
        end;
    end;
    result := CreateTRUE;
end;


function if_write_float_double  (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, tpReal]) of
        1: (PL[0] as TVStream).target.write_double(PL.F[1]);
    end;
    result := CreateTRUE;
end;


function if_read_character      (const PL: TVList; {%H-}call: TCallProc): TValue;
var ch: unicodechar;
begin
    case params_is(PL, result, [
        vpStream, tpNIL,
        vpStream, vpNatural,
        vpStream, vpKeyword_ALL,
        tpNIL, tpAny]) of
        1: result := TVString.Create(
                (PL[0] as TVStream).target.read_string(1));
        2: result := TVString.Create(
                (PL[0] as TVStream).target.read_string(PL.I[1]));
        3: result := TVString.Create(
                (PL[0] as TVStream).target.read_string(-1));
        4: begin
            System.Read(ch);
            result := TVString.Create(ch);
        end;
    end;
end;


function if_write_string        (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, tpString,
        tpNil,    tpString]) of
        1: (PL[0] as TVStream).target.write_string(PL.S[1]);
        2: System.Write(PL.S[1]);
    end;
    result := CreateTRUE;
end;


function if_read_line           (const PL: TVList; {%H-}call: TCallProc): TValue;
var s: unicodestring; sp: TVStream;
begin
    case params_is(PL, result, [
        vpStream, tpStringOrNIL, vpNaturalOrNIL,
        tpNIL,    tpNIL,         tpNIL]) of
        1: try
            sp := PL[0] as TVStream;
            sp.target.Lock;
            if sp.target.read_line(s,
                default(PL[1], LineEnding),
                default(PL[2],-1))
            then result := TVString.Create(s)
            else result := TVList.Create();
        finally sp.target.Unlock; end;
        2: try
            EnterCriticalSection(console_cs);
            System.ReadLn(s);
            result := TVString.Create(s);
        finally LeaveCriticalSection(console_cs); end;
    end;
end;


function if_write_line          (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, tpString, tpStringOrNIL,
        tpNIL,    tpString, tpNIL]) of
        1: ifh_lock_and_write_string(PL[0] as TVStream,
                            PL.S[1]+default(PL[2], LineEnding));
        2: ifh_lock_and_write_string(nil, PL.S[1]);
    end;
    result := CreateTRUE
end;



function ifa_read             (const PL: TValues; {%H-}call: TCallProc): TValue;
var s: unicodestring;
begin
    case params_is(PL, result, [
        vpStream,
        tpString,
        tpNILnil]) of
        1: try
            (PL[0] as TVStream).target.Lock;
            result := dlisp_read.read(PL[0] as TVStream);

            if result is TVEndOfStream then begin
                FreeAndNil(result);
                result := TVList.Create;
            end;
        finally
            (PL[0] as TVStream).target.Unlock;
        end;
        2: result := dlisp_read.read_from_string(S_(PL[0]));
        3: try
            EnterCriticalSection(console_cs);
            ReadLn(s);
            result := dlisp_read.read_from_string(s);
        finally
            LeaveCriticalSection(console_cs);
        end;
    end;
end;


function ifu_read_from_string   (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpString]) of
        1: result := dlisp_read.read_from_string(asString(A));
    end;
end;


function if_write               (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream, tpAny,
        tpNIL,    tpAny]) of
        1: ifh_lock_and_write_string(PL[0] as TVStream,
                PL[1].AsString+LineEnding);
            //TODO: не возвращается ошибка при записи в файл
        2: ifh_lock_and_write_string(nil, PL[1].AsString+LineEnding);
    end;
    result := CreateTRUE;
end;


function if_print               (const PL: TVList; {%H-}call: TCallProc): TValue;
var sp: TVStream;
begin
        case params_is(PL, result, [
            vpStream,         tpAny,
            tpNIL,            tpAny,
            vpKeyword_RESULT, tpAny,
            vpKeyword_TRUE,   tpAny]) of
            1: try
                sp := PL[0] as TVStream;
                sp.target.Lock;
                print(PL[1], sp, true);
                result := CreateTRUE
            finally
                sp.target.Unlock;
            end;
            2: try
                EnterCriticalSection(console_cs);
                print(PL[1], nil, true);
                result := CreateTRUE
            finally
                LeaveCriticalSection(console_cs);
            end;
            3,4: result := TVString.Create(PL[1].AsString);
        end;
end;


function if_input               (const PL: TVList; {%H-}call: TCallProc): TValue;
var msg: unicodestring;
begin
    case params_is(PL, result, [
        tpString, tpList]) of
        1: begin
            if tpTrue(PL[1]) then msg := ifh_format(PL.L[1]) else msg := '';
            result := TVString.Create(ifh_input(msg, PL.S[0]));
        end;
    end;
end;


function if_fmt                 (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream,         tpList,
        tpNIL,            tpList,
        vpKeyword_RESULT, tpList,
        vpKeyword_TRUE,   tpList]) of
        1: begin
            ifh_lock_and_write_string(PL[0] as TVStream, ifh_format(PL.L[1]));
            result := CreateTRUE
        end;
        2: try
            EnterCriticalSection(console_cs);
            System.Write(ifh_format(PL.L[1]));
            result := CreateTRUE
        finally LeaveCriticalSection(console_cs); end;
        3,4: result := TVString.Create(ifh_format(PL.L[1]));
    end;
end;


function if_str                 (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [tpList]) of
        1: result := TVString.Create(ifh_format(PL.L[0]));
    end;
end;


function if_log                 (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpList]) of
        1: try
            EnterCriticalSection(log_cs);
            if log_file=nil
            then try
                EnterCriticalSection(console_cs);
                System.WriteLn(ifh_format(PL.L[0]));
            finally LeaveCriticalSection(console_cs); end
            else log_file.Log(ifh_format(PL.L[0])+LineEnding);
        finally LeaveCriticalSection(log_cs); end;
    end;
    result := CreateTRUE
end;


function if_log_file            (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpNIL, tpNIL,
        tpString, vpKeywordEncodingOrNIL]) of
        1: try EnterCriticalSection(log_cs);
            FreeAndNil(log_file);
        finally LeaveCriticalSection(log_cs) end;
        2: try EnterCriticalSection(log_cs);
          log_file := TLFileStream.Create(PL.S[0], fmOpenReadWrite,
            ifh_keyword_to_encoding(PL[1]));
        finally LeaveCriticalSection(log_cs) end;
    end;
    result := CreateTRUE
end;


function if_hex                 (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpNatural,  tpNIL,
        vpNatural,  vpNatural]) of
        1: result := TVString.Create(IntToHex(PL.I[0], 0));
        2: result := TVString.Create(IntToHex(PL.I[0], PL.I[1]));
    end;
end;


function if_fixed               (const PL: TVList; {%H-}call: TCallProc): TValue;
var sint, frac: unicodestring; d: integer;
begin
    case params_is(PL, result, [
        tpInteger,  vpIntegerZero,  vpNatural,
        tpReal,     vpNatural,      tpNIL,
        tpReal,     vpNatural,      vpNatural]) of
        1: result := TVString.Create(format('%.'+IntToStr(PL.I[2])+'d', [PL.I[0]]));
        2: result := TVString.Create(FloatToStrF(PL.F[0], ffFixed, 0, PL.I[1]));
        3: begin
            d := PL.I[1];
            sint := format('%.'+IntToStr(PL.I[2])+'d', [round(int(PL.F[0]))]);
            frac := FloatToStrF(PL.F[0], ffFixed, 0, d);
            result := TVString.Create(sint+frac[Length(frac)-d..Length(frac)]);
        end;
    end;

end;


function ifa_cer               (const PL: TValues; {%H-}call: TCallProc): TValue;
var c: COMPLEX; dm, da, argln: integer; smod, sarg: unicodestring; arg: real;
begin
    case params_is(PL, result, [
        tpNumber,   vpNaturalOrNIL,  vpNaturalOrNIL]) of
        1: begin
            c := C_(PL[0]);
            dm := default(PL[1], 0);
            da := default(PL[2], 0);
            arg := carg(c)*180/pi;
            smod := FloatToStrF(cmod(c), ffFixed, 0, dm);
            sarg := FloatToStrF(arg, ffFixed, 0, da)+'°';
            if roundto(arg, -da)>0 then sarg := '+'+sarg;
            if da>0 then argln:=5+1+da else argln := 5;
            while Length(sarg)<argln do sarg := sarg+' ';
            result := TVString.Create(smod + '∠' + sarg);
        end;
    end;
end;


function if_fdt                 (const PL: TVList; {%H-}call: TCallProc): TValue;
var fmtstr: unicodestring;
begin
    case params_is(PL, result, [
        tpDateTime, tpStringOrNIL,
        tpDuration, tpStringOrNIL]) of
        1: begin
            fmtstr := default(PL[1], 'yyyy.mm.dd hh:mm:ss');
            result := TVString.Create(FormatDateTime(fmtstr{%H-}, (PL[0] as TVDatetime).fDT));
        end;
        2: begin
            fmtstr := default(PL[1], '[h]:mm:ss.zzz');
            result := TVString.Create(FormatDateTime(fmtstr{%H-}, (PL[0] as TVDuration).fDT, [fdoInterval]));
        end;
    end;

end;


function if_col                 (const PL: TVList; {%H-}call: TCallProc): TValue;
var s: unicodestring; w,i: integer; lr: boolean;
begin
    case params_is(PL, result, [
        vpNatural, tpAny, vpKeywordAlignOrNIL]) of
        1: begin
            if tpString(PL[1])
            then s := PL.S[1]
            else s := PL[1].AsString;
            w := PL.I[0];
            lr := false;
            if Length(s)<w
            then
                for i := 1 to W-Length(s) do
                    if tpNIL(PL[2]) or vpKeyword_LEFT(PL[2])
                    then s := s + ' '
                    else
                        if vpKeyword_RIGHT(PL[2])
                        then s := ' ' + s
                        else
                            if vpKeyword_CENTER(PL[2])
                            then begin
                                if lr then s := s + ' ' else s := ' ' + s;
                                lr := not lr;
                            end;
            result := TVString.Create(s);
        end;
    end;
end;


function if_fmt_list            (const PL: TVList; {%H-}call: TCallProc): TValue;
var i: integer; res, s : unicodestring; expr: TValues;
    function el(V: TValue): unicodestring;
    var tmp: TValue;
    begin try
        tmp := nil;
        expr[1] := V;
        tmp := call(expr);
        result := ifh_str(tmp);
    finally
        tmp.Free;
    end; end;
begin
    case params_is(PL, result, [
        tpList, tpStringOrNIL, tpNIL,
        tpList, tpStringOrNIL, tpSubprogram]) of
        1: begin
            s := default(PL[1],' ');
            res := '';
            if PL.L[0].Count>=1 then res := res + ifh_str(PL.L[0][0]);
            for i := 1 to PL.L[0].High do res := res + s + ifh_str(PL.L[0][i]);
        end;
        2: begin
            expr := TValues.Create(PL[2], nil);
            s := default(PL[1],' ');
            res := '';
            if PL.L[0].Count>=1 then res := res + el(PL.L[0][0]);
            for i := 1 to PL.L[0].High do res := res + s + el(PL.L[0][i]);
        end;
    end;
    Result := TVString.Create(res);
end;


function ifa_fmt_list          (const PL: TValues; {%H-}call: TCallProc): TValue;
var i: integer; res, s : unicodestring; expr, L: TValues;
    function el(V: TValue): unicodestring;
    var tmp: TValue;
    begin try
        tmp := nil;
        expr[1] := V;
        tmp := call(expr);
        result := ifh_str(tmp);
    finally
        tmp.Free;
    end; end;
begin
    case params_is(PL, result, [
        tpList, tpStringOrNIL, tpNIL,
        tpList, tpStringOrNIL, tpSubprogram]) of
        1: begin
            L := LV_(PL[0]);
            s := default(PL[1],' ');
            res := '';
            if Length(L)>=1 then res := res + ifh_str(L[0]);
            for i := 1 to High(L) do res := res + s + ifh_str(L[i]);
        end;
        2: begin
            expr := TValues.Create(PL[2], nil);
            s := default(PL[1],' ');
            res := '';
            if L_(PL[0]).Count>=1 then res := res + el(L_(PL[0])[0]);
            for i := 1 to L_(PL[0]).High do res := res + s + el(L_(PL[0])[i]);
        end;
    end;
    Result := TVString.Create(res);
end;


function ifu_upper_case         (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpString]) of
        1: result := TVString.Create(UnicodeUpperCase(asString(A)));
    end;
end;


function ifu_lower_case         (const A: TValue): TValue;
begin
    case param_is(A, result, [
        tpString]) of
        1: result := TVString.Create(UnicodeLowerCase(AsString(A)));
    end;
end;


function if_fmt_table           (const PL: TVList; {%H-}call: TCallProc): TValue;
var data: array of array of unicodestring; cols: array of integer; i,j,k,w,h: integer;
    table: TVList; stdout: boolean;
    hs: unicodestring;
    mode: (pseudo, csv, html);
    procedure put(s: unicodestring);
    begin
        if stdout then Write(s)
        else (PL[0] as TVStream).target.write_string(s);
    end;

begin
    case params_is(PL, result, [
        tpNIL,    vpListMatrix, tpStringOrNIL, vpKeywordTableModeOrNIL,
        vpStream, vpListMatrix, tpStringOrNIL, vpKeywordTableModeOrNIL]) of
        1,2: begin
            result := CreateTRUE;
            stdout := tpNIL(PL[0]);
            table := PL.L[1];
            if tpNIL(PL[2]) then hs:='' else hs:=PL.S[2];
            if tpNIL(PL[3]) then mode:=pseudo else
            if vpKeyword_HTML(PL[3]) then mode := html else mode:=csv;
            if (mode=csv) and (hs='') then hs:=';';
            h := table.Count;
            if h=0 then Exit;
            w := table.L[0].Count;
            SetLength(data, h);
            for i := 0 to h-1 do SetLength(data[i], w);
            SetLength(cols, w);
            for j := 0 to w-1 do cols[j]:=0;
            for i := 0 to h-1 do
                for j := 0 to w-1 do begin
                    if tpList(table.L[i][j])
                    then data[i,j] := ifh_format(table.L[i].L[j])
                    else
                        if tpString(table.L[i][j])
                        then data[i,j] := table.L[i].S[j]
                        else data[i,j] := table.L[i][j].AsString;
                    if Length(data[i,j])>cols[j] then cols[j]:=Length(data[i,j]);
                end;

            case mode of
                pseudo: for i := 0 to h-1 do begin
                    put(hs);
                    for j := 0 to w-1 do begin
                        put(data[i,j]);
                        for k := Length(data[i,j]) to cols[j] do Put(' ');
                        put(hs);
                    end;
                    put(LineEnding);
                end;
                csv: for i := 0 to h-1 do begin
                    for j := 0 to w-1 do begin
                        put(data[i,j]);
                        put(hs);
                    end;
                    put(#13#10);
                end;
                html: begin
                    //put('<table>'); put(#13#10);
                    for i := 0 to h-1 do begin
                        put('<tr>');
                        for j := 0 to w-1 do begin
                            if tpNumber(table.L[i][j])
                            then put('<td align="right">')
                            else put('<td>');
                            put(data[i,j]);
                            put('</td>');
                        end;
                        put('</tr>'); put(#13#10);
                    end;
                    //put('</table>'); put(#13#10);
                end;
            end;
        end;
    end;
end;


function if_single_spaces       (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString]) of
        1: result := TVString.Create(ifh_single_spaces(PL.S[0]));
    end;
end;


function if_xml_read            (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream,
        tpString]) of
        1: result := xml_read((PL[0] as TVStream).target);
        2: result := xml_from_string(PL.S[0]);
    end;
end;


function if_xml_write           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpStream,         tpList,
        vpKeyword_RESULT, tpList,
        vpKeyword_TRUE,   tpList]) of
        1: begin
            xml_write((PL[0] as TVStream).target, PL.L[1]);
            result := CreateTRUE
        end;
        2,3: result := TVString.Create(xml_to_string(PL.L[1]));
    end;
end;


function if_xml_encode          (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString]) of
        1: result := TVString.Create(lisya_xml.encode(PL.S[0]));
    end;
end;


function if_xml_decode          (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString]) of
        1: result := TVString.Create(lisya_xml.decode(PL.S[0]));
    end;
end;


function if_xml_children        (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpListXMLnode, tpString]) of
        1: result := xml_children(PL.L[0],PL.S[1]);
    end;
end;


function ifa_xml_children     (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpListXMLnode, tpString]) of
        1: result := xml_children(L_(PL[0]), S_(PL[1]));
    end;
end;


function if_xml_child           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpListXMLnode, tpListOfStrings]) of
        1: result := xml_child(PL.L[0],PL.L[1],0);
    end;
end;


function if_xml_attribute       (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpListXMLnode, tpString]) of
        1: result := xml_attribute(PL.L[0],PL.S[1]);
    end;
end;


function if_sql_ODBC_connection (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString]) of
        1: begin
            {$IFDEF ODBC}
            result := TVSQL.Create(TLSQLodbc.Connect(PL.S[0]));
            {$ELSE}
            raise ELE.Create('ODBC not supported','not implemented');
            {$ENDIF}
        end;
    end;
end;


function if_sql_oracle_connection(const PL: TVList; {%H-}call: TCallProc): TValue;
var database, username, host, password: unicodestring;
begin
    case params_is(PL, result, [
        tpString, tpStringOrNIL, tpStringOrNIL, tpStringOrNIL]) of
        1: begin
            database := PL.S[0];
            host :=     default(PL[1],'localhost');
            username := default(PL[2],'');
            password := default(PL[3],'');
            {$IFDEF ORACLE}
            result := TVSQL.Create(TLSQLoracle.Connect(database, host, username,password));
            {$ELSE}
            raise ELE.Create('ORACLE not supported','not implemented');
            {$ENDIF}
        end;
    end;
end;


function ifa_sql_sqlite_connection(const PL: TValues; {%H-}call: TCallProc): TValue;
var database: unicodestring;
begin
    case params_is(PL, result, [
        tpString]) of
        1: begin
            database := S_(PL[0]);
            {$IFDEF SQLITE}
            result := TVSQL.Create(TLSQLite.Connect(database));
			{$ELSE}
            raise ELE.Create('SQLITE not supported','not implemented');
            {$ENDIF}
        end;
    end;
end;


function ifh_SQL_datatype(F: TField): TValue;
begin
    if VarIsNull(F.Value)
    then result := TVList.Create
    else case F.DataType of
        ftUnknown, ftString, ftWideString,ftFmtMemo, ftMemo, ftFixedWideChar,
        ftWideMemo,ftFixedChar:
            result := TVString.Create(F.AsString);

        ftSmallint, ftInteger, ftWord, ftLargeInt:
            result := TVInteger.Create(F.AsInteger);

        ftBoolean:
            bool_to_TV(F.AsBoolean, result);

        ftFloat:
            result := TVFloat.Create(F.AsFloat);

        ftDateTime, ftDate, ftTimeStamp:
            result := TVDateTime.Create(F.AsDateTime);

        ftTime: result := TVDuration.Create(F.AsDateTime);

        else result := TVString.Create(F.AsString);

        //ftCurrency, ftBCD,
        //ftBytes, ftVarBytes, ftAutoInc, ftBlob, , ftGraphic, ,
        //ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor,
        //, ftADT, ftArray, ftReference,
        //ftDataSet, ftOraBlob, ftOraClob, ftVariant, ftInterface,
        //ftIDispatch, ftGuid, ftFMTBcd, );
    end;
end;


function ifh_SQL_query(db: TLSQL; query: unicodestring;
    out head: TStringArray; out body: TValues): integer;
var ucommand: unicodestring; i, j: integer;
begin
    result := 0;
    with db.query do try
        db.Lock;
        SQL.Text := query;
        ucommand := UnicodeUpperCase(query);
        if (Pos('SELECT', ucommand)=1)
        or (Pos('SHOW', ucommand)=1)
        or (Pos('DESCRIBE', ucommand)=1)
        then Active := true
        else begin
            SetLength(head, 0);
            SetLength(body, 0);
            ExecSQL;
            Exit;
        end;
        Last;

        Setlength(head, FieldCount);
        for j := 0 to FieldCount-1 do head[j] := Fields[j].DisplayName;

        First;
        SetLength(body, FieldCount*RecordCount);
        for i := 0 to RecordCount-1 do begin
            for j := 0 to FieldCount-1 do
                body[i*FieldCount+j] := ifh_SQL_datatype(Fields[j]);
            Next;
        end;
        result := RecordCount;
    finally
        Active := false;
        db.Unlock;
    end;
end;


function if_sql_query           (const PL: TVList; {%H-}call: TCallProc): TValue;
var rec: TVRecord; i,j, w, c: integer; head: TStringArray; body: TValues;
    L: TVList;
begin
    case params_is(PL, result, [
        tpSQL, tpList]) of
        1: begin
            result := TVList.Create;
            c := ifh_sql_query((PL[0] as TVSQL).target, ifh_format(PL.L[1]), head, body);

            if c=0 then Exit;

            w := Length(head);
            L := result as TVList;

            try
                rec := TVRecord.Create;
                for j := 0 to w-1 do rec.AddSlot(head[j],CreateTrue);
                for i := 0 to c-1 do begin
                    for j := 0 to w-1 do rec[j] := body[i*w+j];
                    L.Add(rec.Copy);
                end;
            finally
                rec.Free;
            end;
        end;
    end;
end;


function if_sql_test            (const PL: TVList; {%H-}call: TCallProc): TValue;
    //type pword = ^word;
    //type abyte = array[0..3] of byte;
    //type pbyte = ^abyte;
var //p: pbyte;
    s: unicodestring;
begin
  {$IFDEF ODBC}
    case params_is(PL, result, [
        tpSQL, tpString]) of
        1: with ((PL[0] as TVSQL).target as TLSQLodbc).module.connector do try
            (PL[0] as TVSQL).target.Lock;
             //s := 'фигня';
            //p := pbyte(@(s[1]));
            //WriteLn(p[0],' ',p[1],' ',p[2],' ',p[3]);
            s := PL.S[1];
            //ExecuteDirect('SET charset utf8');
            ExecuteDirect(s);
        finally
            (PL[0] as TVSQL).target.Unlock;
        end;
    end;
    result := CreateTRUE;
    {$ENDIF}
end;


function if_sql_query_list      (const PL: TVList; {%H-}call: TCallProc): TValue;
var i,c: integer; head: TStringArray; body: TValues; L: TVList;
begin
    case params_is(PL, result, [
        tpSQL, tpList]) of
        1: begin
            result := TVList.Create;
            c := ifh_sql_query((PL[0] as TVSQL).target, ifh_format(PL.L[1]), head, body);
            L := result as TVList;
            for i := 0 to c-1 do L.Add(body[i]);
        end;
    end;
end;


function if_sql_query_table     (const PL: TVList; {%H-}call: TCallProc): TValue;
var rec, L: TVList; i,j,w,c: integer; head: TStringArray; body: TValues;
begin
    case params_is(PL, result, [
        tpSQL, tpList]) of
        1: begin
            result := TVList.Create;
            c := ifh_sql_query((PL[0] as TVSQL).target, ifh_format(PL.L[1]), head, body);
            L := result as TVList;
            w := Length(head);

            for i := 0 to c-1 do begin
                rec := TVList.Create;
                for j := 0 to w-1 do rec.Add(body[i*w+j]);
                L.Add(rec);
            end;
        end;
    end;
end;


function if_http_get            (const PL: TVList; {%H-}call: TCallProc): TValue;
{$IFDEF NETWORK}var http: TFPHTTPClient; scp: integer;{$ENDIF}
begin
    //текущая версия модуля openssl не поддерживает интерфейс версии 1.1
    //в Debian требуется установка пакета libssl1.0-dev
    case params_is(PL, result, [
        tpString, tpStringOrNIL, tpStringOrNIL, tpIntegerOrNIL]) of
        1: {$IFDEF NETWORK}try
            http := TFPHTTPClient.Create(nil);
            http.AllowRedirect:=true;
            if tpString(PL[1]) then begin
                scp:= PosU(':',PL.S[1]);
                if scp>0
                then begin
                    http.Proxy.host := PL.S[1][1..scp-1];
                    http.proxy.port := StrToIntDef(PL.S[1][scp+1..Length(PL.S[0])],0);
                end
                else http.Proxy.host := PL{%H-}.S[1];
            end;
            http.IOTimeout:=default(PL[3],0);
            http.AddHeader('User-Agent', default(PL[2], 'Mozilla/5.0 (compatible; fpweb)'));
            result := TVString.Create(http.Get(PL.S[0]{%H-}));
        finally
            http.Free;
        end;
        {$ELSE} raise ELE.NotSupported('HTTP disabled'); {$ENDIF}
    end;
end;


function if_socket              (const PL: TVList; {%H-}call: TCallProc): TValue;
{$IFDEF NETWORK}var stream: TLSocket;{$ENDIF}
begin
    case params_is(PL, result, [
        tpString, vpKeywordEncodingOrNIL, vpNaturalOrNil]) of
        1: {$IFDEF NETWORK}try
            stream := TLSocket.Connect(PL.S[0],0,ifh_keyword_to_encoding(PL[1]));
            if vpNatural(PL[2]) then stream.timeout:=PL.I[2];
            result := TVStream.Create(stream);
        finally
        end;
        {$ELSE} raise ELE.NotSupported('SOCKET disabled'); {$ENDIF}
    end;
end;


function if_server              (const PL: TVList; {%H-}call: TCallProc): TValue;
{$IFDEF NETWORK}{$ENDIF}
begin
    case params_is(PL, result, [
        vpNatural, tpStringOrNIL]) of
        1: {$IFDEF NETWORK}
            result := TVServer.Create(TLServer.Create(PL.I[0], default(PL[1],'')));
        {$ELSE} raise ELE.NotSupported('SERVER disabled'); {$ENDIF}
    end;
end;


function if_regexp_match        (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, tpString, vpNaturalOrNIL]) of
        1: result := StringArrayToListOfStrings(
            regexp_match(PL.S[0], PL.S[1], default(PL[2],0)+1));
    end;
end;


function ifa_regexp_is           (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, tpString]) of
        1: if regexp_is(S_(PL[0]), S_(PL[1]))
            then result := PL[1].Copy
            else result := TVList.Create;
    end;
end;


function if_regexp_replace      (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, tpString, tpString, vpNaturalOrNIL]) of
        1: result := TVString.Create(
            regexp_replace(PL.S[0], PL.S[1], PL.S[2], default(PL[3],0)+1));
    end;
end;


function if_regexp_remove       (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, tpString, vpNaturalOrNIL]) of
        1: result := TVString.Create(
            regexp_replace(PL.S[0], PL.S[1], '', default(PL[2],0)+1));
    end;
end;


function ifa_contains_words   (const PL: TValues; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        //words   source
        tpString, tpString]) of
        1: if contains_words(S_(PL[0]), S_(PL[1]))
            then result := CreateTRUE
            else result := TVList.Create(nil);
    end;
end;


function if_if                  (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString, tpAny, tpAny]) of
        1: if tpTrue(PL[0])
            then result := PL[1].Copy
            else result := PL[2].Copy;
    end;
end;

//// TKZ ///////////////////////////////////////////////////////////////////////

function ValueToTKZParameter(_name: unicodestring; P: TValue): TTKZParameter;
var _: TValue;
begin
    case param_is(P, _, [tpString, tpInteger, tpReal, tpComplex]) of
        1: result := NewParameter(_name, asString(P));
        2: result := NewParameter(_name, asInteger(P));
        3: result := NewParameter(_name, asFloat(P));
        4: result := NewParameter(_name, asComplex(P));
        else raise ELE.InvalidParameters('Некорректный тип параметра ТКЗ');
    end;
end;


function TKZParameterToValue(p: TTKZParameter): TValue;
begin
    case p.pt of
        tptString:  result := TVString.Create(p.s);
        tptInteger: result := TVInteger.Create(p.i);
        tptReal:    result := TVFloat.Create(p.r);
        tptComplex: result := TVComplex.Create(p.c);
        else ETKZParameter.Create('Неполная обработка типов параметров ТКЗ');
    end;
end;


function TVListToTKZParameters(L: TVList): TTKZParameters;
var i: integer;
begin
    result := nil;
    for i := 0 to (L.Count div 2) - 1 do
        PushParameter(result, ValueToTKZParameter(L.S[i*2], L[i*2+1]));
end;


function TKZParametersToTVList(const pp: TTKZParameters): TVList;
var i: integer;
begin
    result := TVList.Create;
    for i := 0 to high(pp) do
        begin
            result.Add(TVString.Create(pp[i].name));
            result.Add(TKZParameterToValue(pp[i]));
        end;
end;


function if_tkz_node            (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString]) of
        1: begin
            result := TVInteger.Create(TKZ.net.AddNode(PL.S[0]));
        end;
    end;
end;


function if_tkz_arc             (const PL: TVList; {%H-}call: TCallProc): TValue;
var a: TKZ.TArcDescription;
begin
    case params_is(PL, result, [
        //name    node1     node2     Z                Y              E              K
        tpString, tpString, tpString, vpNumberNotZero, tpNumberOrNIL, tpNumberOrNIL, tpNumberOrNIL]) of
        1: begin
            a.Z := PL.C[3];
            a.Y := default(PL[4], _0);
            a.E  := default(PL[5], _0);
            a.K  := default(PL[6], cinit(1,0));
            result := TVInteger.Create(TKZ.net.AddArc(PL.S[0], PL.S[1], PL.S[2], a));
        end;
    end;
end;


function if_tkz_breaker         (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        //name    node1     node2
        tpString, tpString, tpString]) of
        1: result := TVInteger.Create(TKZ.net.AddBreaker(PL.S[0], PL.S[1], PL.S[2]));
    end;
end;


function if_tkz_fault           (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        //name
        tpString,
        vpKeyword_ALL]) of
        1: TKZ.net.Fault(PL.S[0]);
        2: TKZ.net.AllFaults();
    end;
    result := CreateTRUE;
end;


function if_tkz_calc            (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        vpIntegerPositive,
        tpNIL]) of
        1: TKZ.net.Calc(PL.I[0]);
        2: TKZ.net.Calc(1000000);
    end;
    result :=  CreateTRUE;
end;


function if_tkz_measurement_point(const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        //name   //ending
        tpString, tpNIL]) of
        1: result := TVInteger.Create(TKZ.net.AddMeasurementPoint(PL.S[0]));
        end;
end;


function if_tkz_measurements    (const PL: TVList; {%H-}call: TCallProc): TValue;
var fp, mp: integer; M, F: TVList;
begin
    no_params(PL);
    F := TVList.Create;
    result := F;
    for fp := 0 to high(TKZ.net.measurements) do
        begin
            M := TVList.Create();
            for mp := 0 to high(TKZ.net.measurements[fp]) do
                M.Add(TVComplex.Create(TKZ.net.measurements[fp][mp]));
            F.Add(M);
		end;
end;


function if_tkz_protection_measurements(const {%H-}PL: TVList; {%H-}call: TCallProc): TValue;
//var fp, U_slot, I_slot: integer; M: TMeasurements; L: TVList; R: TVRecord;
begin
 //   case params_is(PL, result, [
 //       //name
 //       tpString]) of
 //       1: begin
 //           M := TKZ.net.GetProtectionMeasurements(S_(PL[0]));
 //           L := TVList.Create();
 //           result := L;
 //           R := TVRecord.Create(['U','I']);
 //           U_slot := symbol_n('U');
 //           I_slot := symbol_n('I');
 //           for fp := 0 to high(M) do
 //               begin
 //                   R.slot[U_slot] := TVComplex.Create(M[fp].U);
 //                   R.slot[I_slot] := TVComplex.Create(M[fp].I);
 //                   L.Add(R.Copy);
 //               end;
	//	end;
	//end;
    result := nil;
end;


function if_tkz_protection      (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        //name   U-point  I-point
        tpString, tpString, tpString]) of
        1: result := TVInteger.Create(
                    TKZ.net.AddProtection(S_(PL[0]), S_(PL[1]), S_(PL[2])));
	end;
end;


function ifa_tkz_grid          (const PL: TValues; {%H-}call: TCallProc): TValue;
var gb: TGridBuilder; i: integer; recs: TValues;

    procedure add_arc(arc: TValues);
    var Z, X: COMPLEX;
    begin
        if Length(arc)>4 then Z := C_(arc[4]) else Z := _0;
        if Length(arc)>5 then X := C_(arc[5]) else X := _0;
        gb.AddArc(ifh_keyword_to_grid_arc(arc[0]),        //arc type
                                 ifh_str(arc[1]),        //name
                                 ifh_str(arc[2]),        //n1
                                 ifh_str(arc[3]),        //n2
                                             Z,          //Z
                                             X);        //E|K
    end;

    procedure add_rnd(rnd: TValues);
    var h, l: real;
    begin
        RNG_(rnd[2], l, h);
        gb.AddRandomization(ifh_str(rnd[1]), l, h);
    end;

begin
    case params_is(PL, result, [
        rpAny]) of
        1: begin
            gb := TGridBuilder.Create;
            recs := REST_(PL[0]);
            for i := 0 to high(recs) do
                case param_is(recs[i], [
                    vpListGridArc,
                    vpListGridRandomization]) of
                    1: add_arc(LV_(recs[i]));
                    2: add_rnd(LV_(recs[i]));
				end;

    		result := TVGrid.Create(gb.Completed());
        end;
	end;
end;


function ifa_tkz_measurements  (const PL: TValues; {%H-}call: TCallProc): TValue;
var g: TGrid; i: integer; mp: TStringArray; L: TValues;
begin
    case params_is(PL, result, [
        tpGrid, rpAny]) of
        1: begin
            g := (PL[0] as TVGrid).target;
            L := REST_(PL[1]);
            SetLength(mp, Length(L));
            for i := 0 to high(L) do mp[i] := ifh_str(L[i]);
            g.SetMeasurements(mp);
            result := CreateTRUE;
        end;
	end;
end;


function ifa_tkz_fault         (const PL: TValues; {%H-}call: TCallProc): TValue;
var i: integer; measurements: TMeasurements; MV: TComplexes; L: TVList;
begin
    case params_is(PL, result, [
        //grid  point  //R          distance     count
        tpGrid, tpAny, tpRealOrNIL, tpRealOrNIL, tpNIL]) of
        1: begin
            (PL[0] as TVGrid).target.Fault(
                ifh_str(PL[1]),
                default(PL[2], 0.001),
                default(PL[3], 0.99),
                measurements, MV);

            if MV<>nil
            then
                result := TVTensor.Create(tensor_new(MV))
            else
                begin
                    L := TVList.Create;
                    result := L;
                    for i := 0 to high(measurements) do
                        begin
                            L.Add(TVString.Create(measurements[i].name));
                            L.Add(TVComplex.Create(measurements[i].V1));
				        end;
		        end;
	    end;
	end;
end;


function ifa_tkz_mode          (const PL: TValues; {%H-}call: TCallProc): TValue;
var g: TGrid; i: integer; L: TValues; off: TStringArray;
begin
    case params_is(PL, result, [
        tpGrid, rpAny]) of
        1: begin
            g := (PL[0] as TVGrid).target;
            L := (PL[1] as TVListRest).V;
            SetLength(off, Length(L));
            for i := 0 to high(L) do off[i] := ifh_str(L[i]);
            g.SetMode(off);
		end;
	end;
    result := CreateTRUE;
end;


////////////////////////////////////////////////////////////////////////////////
//// ZEMQ //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


function ifa_zemq              (const PL: TValues; {%H-}call: TCallProc): TValue;
var zemq: TZEMQ; A, rec: TValues; i: integer; meas: TZEMQMeasurements; L: TVList;
begin
    case params_is(PL, result, [
        tpListOfLists]) of
        1: try
            zemq := TZEMQ.Create;
            A := LV_(PL[0]);
            for i := 0 to high(A) do
                begin
                    rec := LV_(A[i]);
                    if key_is(rec[0], ':Z')
                    then
                        zemq.AddZ(ifh_str(rec[1]), ifh_str(rec[2]), C_(rec[3]))
                    else

                    if key_is(rec[0], ':G')
                    then
                        zemq.AddG(ifh_str(rec[1]), ifh_str(rec[2]), C_(rec[3]), C_(rec[4]))
                    else

                    if key_is(rec[0], ':M')
                    then
                        zemq.AddM(ListTOStrings(L_(rec[1])), ListToStrings(L_(rec[2])), T_(rec[3]).Complexes())
					else

                    if key_is(rec[0], ':Q')
                    then
                        zemq.AddQ(ifh_str(rec[1]), ifh_str(rec[2]), ifh_str(rec[3]))
                    else
                        raise ELE.Create('invalid element type'+ifh_str(rec[0]), 'ZEMQ/data');
				end;

            meas := zemq.Measurements();
            result := TVList.Create;
            L := result as TVList;

            for i := 0 to high(meas) do
                with meas[i] do
                    L.Add(TVList.Create([TVString.Create(name), TVComplex.Create(U), TVComplex.Create(I)]));
        finally
            zemq.Free;
		end;

	end;
end;


////////////////////////////////////////////////////////////////////////////////
//// OpenCL ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


function if_opencl_platforms    (const PL: TVList; {%H-}call: TCallProc): TValue;
var platforms: TArrayOfArrayOfString; L: TVList; i: integer;
begin
    result := nil;
    no_params(PL);
    platforms := opencl_platforms();
    L := TVList.Create;
    for i := 0 to high(platforms) do L.Add(StringArrayToListOfStrings(platforms[i]));
    result := L;
end;


function if_opencl_processor    (const PL: TVList; {%H-}call: TCallProc): TValue;
begin
    case params_is(PL, result, [
        tpString]) of
        1: result := TVString.Create(set_opencl_processor(PL.S[0]));
    end;
end;


function ifa_test         (const {%H-}PL: TValues; {%H-}call: TCallProc): TValue;
var res: TComplexes;
begin
    case params_is(PL, result, [
        tpTensor, tpTensor]) of
        1: begin
            SetLength(res, T_(PL[0]).size(1));
            matrix_cross(T_(PL[0]).Complexes(), T_(PL[1]).Complexes(), res);
            result := TVTensor.Create(res);
		end;

	end;
end;


function if_canvas              (const {%H-}PL: TVList; {%H-}call: TCallProc): TValue;
begin
{$IFDEF GUI}
    no_params(PL);
    if not AppInitialized in Application.Flags then Application.Initialize;
    Application.CreateForm(TFormCanvas, canvas);
{$ENDIF}
    result := nil;
end;


////////////////////////////////////////////////////////////////////////////////

const int_un_fun_count = 62;
const int_un_fun: array[1..int_un_fun_count] of TInternalUnaryFunctionRec = (
(n:'F _-1';                       f:ifu_dec),
(n:'F _+1';                       f:ifu_inc),
(n:'F ABS';                       f:ifu_abs),
(n:'F ARCCOS';                    f:ifu_arccos),
(n:'F ARCSIN';                    f:ifu_arcsin),
(n:'F ARCTAN';                    f:ifu_arctan),
(n:'F ARG';                       f:ifu_arg),
(n:'F AS-SECONDS';                f:ifu_as_seconds),
(n:'F CEIL';                      f:ifu_ceil),
(n:'F COMPLEMENT ДОПОЛНЕНИЕ';     f:ifu_complement),
(n:'F CONJUGE СОПРЯЖЁННОЕ';       f:ifu_conjuge),
(n:'F COS';                       f:ifu_cos),
(n:'F DEG';                       f:ifu_deg),
(n:'F DETERMINANT';               f:ifu_matrix_determinant),
(n:'F ELEMENTS';                  f:ifu_elements),
(n:'F FACTORIAL';                 f:ifu_factorial),
(n:'F FLOAT';                     f:ifu_float),
(n:'F FLOOR';                     f:ifu_floor),
(n:'F FRACTIONAL';                f:ifu_fractional),
(n:'F HASH ОКРОШКА ХЭШ';          f:ifu_hash),
(n:'F HEAD CAR ГОЛОВА';           f:ifu_head),
(n:'F HIGH';                      f:ifu_high),
(n:'F IDENTITY';                  f:ifu_identity),
(n:'F IM';                        f:ifu_im),
(n:'F INTEGER';                   f:ifu_integer),
(n:'F INVERSE INV';               f:ifu_inverse),
(n:'F LAST';                      f:ifu_last),
(n:'F LENGTH ДЛИНА';              f:ifu_length),
(n:'F LG LOG-10';                 f:ifu_log_10),
(n:'F LN LOG-E';                  f:ifu_log_e),
(n:'F LOG-2';                     f:ifu_log_2),
(n:'F LOW';                       f:ifu_low),
(n:'F LOWER-CASE';                f:ifu_lower_case),
(n:'F NOT НЕ';                    f:ifu_not),
(n:'F RAD';                       f:ifu_rad),
(n:'F RE';                        f:ifu_re),
(n:'P READ-BYTE';                 f:ifu_read_byte),
(n:'P READ-FLOAT-SINGLE';         f:ifu_read_float_single),
(n:'P READ-FLOAT-DOUBLE';         f:ifu_read_float_double),
(n:'F READ-FROM-STRING';          f:ifu_read_from_string),
(n:'P READ-INTEGER-8';            f:ifu_read_integer_8),
(n:'P READ-INTEGER-16';           f:ifu_read_integer_16),
(n:'P READ-INTEGER-32';           f:ifu_read_integer_32),
(n:'P READ-INTEGER-64';           f:ifu_read_integer_64),
(n:'P READ-UNSIGNED-16 READ-WORD';f:ifu_read_unsigned_16),
(n:'P READ-UNSIGNED-32 READ-DWORD';f:ifu_read_unsigned_32),
(n:'F REMOVE-EMPTY';              f:ifu_remove_empty),
(n:'F REVERSE REVERSED';          f:ifu_reverse),
(n:'F ROUND-0';                   f:ifu_round_0),
(n:'F ROUND-3';                   f:ifu_round_3),
(n:'F ROUND-6';                   f:ifu_round_6),
(n:'F SECOND ВТОРОЙ';             f:ifu_second),
(n:'F SIGN';                      f:ifu_sign),
(n:'F SIN';                       f:ifu_sin),
(n:'F SIZE';                      f:ifu_size),
(n:'F SQRT КОРЕНЬ';               f:ifu_sqrt),
(n:'F SYMBOL';                    f:ifu_symbol),
(n:'F TAIL CDR ХВОСТ';            f:ifu_tail),
(n:'F TAN';                       f:ifu_tan),
(n:'F TRANSPOSED';                f:ifu_transposed),
(n:'F UPPER-CASE';                f:ifu_upper_case),
(n:'F WIDTH';                     f:ifu_width)
);


const int_fun_count = 263;
var int_fun_persistent: array[1..int_fun_count] of array of TVSubprogram;
const int_fun: array[1..int_fun_count] of TInternalFunctionRec = (
(n:'f TEST';                    s:'m v';                      a:ifa_test                 ),
(n:'f RECORD?';                 s:'s :optional type';       a:ifa_record_p             ),
(n:'f MAYBE МОЖЕТ-БЫТЬ';        s:'p a';                    a:ifa_maybe                ),

(n:'f **';                      s:'a b';                    a:ifa_power                ), // 1
(n:'F *';                       s:':rest n';                f:if_mul                   ), // 2
//(n:'f *';                       s:':rest n';                a:ifa_asterisk             ), // 2
(n:'f MUL';                     s:'a b';                    a:ifa_mul                  ), // 2
(n:'f CROSS ⨯';                 s:'a b';                    a:ifa_cross                ), // 2
(n:'f /';                       s:'a b';                    a:ifa_div                  ), // 2
(n:'f DIV';                     s:'a b';                    a:ifa_div_int              ), // 2
(n:'f MOD';                     s:'a b';                    a:ifa_mod                  ), // 2
(n:'f +';                       s:':rest n';                a:ifa_add                  ),
(n:'f ADD';                     s:'a b';                    a:ifa_add_b                ),
(n:'F -';                       s:'a :rest b';              f:if_sub                   ),
(n:'f SUB';                     s:'a b';                    a:ifa_sub                  ),
(n:'f CALC';                    s:':rest expr';             a:ifa_calc                 ), //приоритет

(n:'F LOG-N';                   s:'n x';                    f:if_log_n                 ),

(n:'f MAX';                     s:':rest a';                a:ifa_max                  ),
(n:'f MIN';                     s:':rest a';                a:ifa_min                  ),
(n:'f ROUND';                   s:'a :optional n';          a:ifa_round                ),
(n:'f RANGE';                   s:'l :optional h';          a:ifa_range                ),
(n:'p GENSYM';                  s:'';                       a:ifa_gensym               ),
(n:'p RANDOM';                  s:':optional range';        a:ifa_random               ),
(n:'p NOISY';                   s:'V mul :optional add';    a:ifa_noisy                ),
(n:'p SHUFFLED';                s:'<list>';                 a:ifa_shuffled             ),
(n:'f COMPLEX';                 s:'re im';                  a:ifa_complex              ),
(n:'f COMPLEX-EXP';             s:'a arg';                  a:ifa_complex_exp          ),
(n:'F DURATION';                s:':optional h m s';        f:if_duration              ),
(n:'F DATE-TIME ДАТА-ВРЕМЯ';    s:'year month day :optional h m s'; f:if_datetime      ),
(n:'F SPLIT-STRING';            s:'s :optional separator';  f:if_split_string          ),
(n:'F TRIM';                    s:'s';                      f:if_trim                  ),
(n:'F TRIM-QUOTES';             s:'s';                      f:if_trim_quotes           ),

(n:'F MATRIX';                  s:'w :rest e';              f:if_matrix                ),
(n:'f TENSOR';                  s:'src';                    a:ifa_tensor               ),
(n:'f SPLIT';                   s:'<tensor> dim';           a:ifa_split                ),
(n:'f FLAT';                    s:'<tensor>';               a:ifa_flat                 ),
(n:'f MATRIX-IDENTITY';         s:'w';                      a:ifa_matrix_identity      ),
(n:'f MATRIX-DIAGONAL';         s:':rest d';                a:ifa_matrix_diagonal      ),
(n:'f MINOR';                   s:'m i j';                  a:ifa_matrix_minor         ),
(n:'f VECTOR';                  s:':rest e';                a:ifa_vector               ),
(n:'f DIFF';                    s:'vector';                 a:ifa_diff                 ),
(n:'f RMS';                     s:'vector :optional window';a:ifa_rms                  ),
(n:'f SAMPLE';                  s:'<signal> index :optional order'; a:ifa_sample       ),
(n:'f STRETCH';                 s:'<vector> scale order';           a:ifa_stretch      ),
(n:'f CONVOLUTION';             s:'<vector> core :optional normalization'; a:ifa_convolution),

(n:'f LIMITED';                 s:'a :key top bottom';      a:ifa_limited              ),
(n:'f TRIGGER-OVER';            s:'<sequence> treshold :optional return-treshold';a:ifa_trigger_over   ),
(n:'f TRIGGER-UNDER';           s:'<sequence> treshold :optional return-treshold';a:ifa_trigger_under  ),
(n:'f TRIGGER-OUT';             s:'<sequence> treshold :optional return-treshold';a:ifa_trigger_out    ),
(n:'f CORRELATION';             s:'a b';                    a:ifa_correlation          ),
(n:'f FOURIER';                 s:'sv :key harmonic window period';a:ifa_fourier       ),
(n:'f THINNED';                 s:'sv n :optional mode';    a:ifa_thinned              ),

(n:'f MEAN СРЕДНЕЕ';            s:'data';                   a:ifa_mean                 ),
(n:'F STANDARD-DEVIATION СТАНДАРТНОЕ-ОТКЛОНЕНИЕ'; s:':rest n'; f:if_standard_deviation ),

(n:'F =';                       s:'a b';                    f:if_equal                 ),
(n:'F MATCH СООТВЕТСТВУЕТ';     s:'a b';                    f:if_match                 ),
(n:'F >';                       s:'a b';                    f:if_more                  ),
(n:'F <';                       s:'a b';                    f:if_less                  ),
(n:'F >=';                      s:'a b';                    f:if_more_or_equal         ),
(n:'F <=';                      s:'a b';                    f:if_less_or_equal         ),
(n:'F <>';                      s:'a b';                    f:if_not_equal             ),
(n:'F F-AND И';                 s:':rest a';                f:if_and                   ),
(n:'F F-OR ИЛИ';                s:':rest a';                f:if_or                    ),
(n:'F XOR';                     s:':rest a';                f:if_xor                   ),
(n:'F EQUAL-CASE-INSENSITIVE';  s:'a b';                    f:if_equal_case_insensitive),
(n:'F EQUAL-SETS';              s:'a b';                    f:if_equal_sets            ),
(n:'F LENGTH-MORE';             s:'a b';                    f:if_length_more           ),
(n:'F LENGTH-LESS';             s:'a b';                    f:if_length_less           ),
(n:'F EQUAL-LENGTHS';           s:'a b';                    f:if_equal_lengths         ),

(n:'P TEST-DYN';                s:':rest msgs';             f:if_test_dyn              ),
(n:'F LIKENESS СХОДСТВО';       s:'s1 s2';                  f:if_likeness              ),
(n:'F LEVENSHTEIN';             s:'s1 s2';                  f:if_levenshtein           ),

(n:'F EXTRACT-FILE-EXT';        s:'s';                      f:if_extract_file_ext      ),
(n:'F CHANGE-FILE-EXT';         s:'filename extension';     f:if_change_file_ext       ),
(n:'F EXTRACT-FILE-NAME';       s:'s';                      f:if_extract_file_name     ),
(n:'F EXTRACT-FILE-PATH';       s:'s';                      f:if_extract_file_path     ),
(n:'P RELATIVE-PATH';           s:'filename :optional root';f:if_relative_path         ),
(n:'P EXPAND-FILE-NAME';        s:'filename';               f:if_expand_file_name      ),
(n:'P FILE-EXISTS';             s:'n';                      f:if_file_exists           ),
(n:'P DIRECTORY-EXISTS';        s:'d';                      f:if_directory_exists      ),
(n:'P ENVIRONMENT-VARIABLE';    s:'name';                   f:if_environment_variable  ),
(n:'P CHANGE-DIRECTORY CD';     s:'d';                      f:if_change_directory      ),
(n:'P DELETE-FILE УДАЛИТЬ-ФАЙЛ';s:'f';                      f:if_delete_file           ),
(n:'P COPY-FILE';               s:'source destination';     f:if_copy_file             ),
(n:'P FILE-SIZE';               s:'filename';               f:if_file_size             ),
(n:'P FILE-AGE';                s:'filename :optional age'; f:if_file_age              ),
(n:'P FILE-ATTRIBUTES';         s:'filename :rest attributes';f:if_file_attributes     ),
(n:'P RENAME-FILE ПЕРЕИМЕНОВАТЬ-ФАЙЛ';s:'o n';              f:if_rename_file           ),
(n:'P CREATE-DIRECTORY СОЗДАТЬ-ПАПКУ';s:'d';                f:if_create_directory      ),
(n:'P REMOVE-DIRECTORY УДАЛИТЬ-ПАПКУ';s:'d';                f:if_remove_directory      ),
(n:'P GUID';                    s:'';                       f:if_guid                  ),

(n:'f EVERY КАЖДЫЙ';            s:'p l';                    a:ifa_every                ),
(n:'f SOME ЛЮБОЙ';              s:'p l';                    a:ifa_some                 ),
(n:'F PREPEND';                 s:':rest args';             f:if_prepend               ),
(n:'f SUBSEQ SLICE';            s:'s b :optional e';        a:ifa_subseq               ),
(n:'f SUBSEQUENCES';            s:'s length :key step';     a:ifa_subsequences         ),
(n:'F SUBREC';                  s:'r :rest fields';         f:if_subrec                ),
(n:'F SORT';                    s:'s :optional p';          f:if_sort                  ),
(n:'F SLOTS';                   s:'r';                      f:if_slots                 ),
(n:'F CURRY ШФ';                s:'f :rest p';              f:if_curry                 ),
(n:'F COMPOSITION КОМПОЗИЦИЯ';  s:'head :rest subprograms'; f:if_composition           ),
(n:'F FILTER';                  s:'p l';                    f:if_filter                ),
(n:'F FILTER-TH';               s:'p l';                    f:if_filter_th             ),
(n:'F REJECT';                  s:'p l';                    f:if_reject                ),
(n:'F REJECT-TH';               s:'p l';                    f:if_reject_th             ),
(n:'F FOLD';                    s:'p l :optional d';        f:if_fold                  ),
(n:'F COUNT';                   s:'predicate <list>';       f:if_count                 ),
(n:'F MAP ОТОБРАЖЕНИЕ';         s:'p :rest l';              f:if_map                   ),
(n:'F MAP-TAIL';                s:'p :rest l';              f:if_map_tail              ),
(n:'F MAP-TREE';                s:'p :rest l';              f:if_map_tree              ),
(n:'F MAP-TH';                  s:'p :rest l';              f:if_map_th                ),
(n:'F MAP-CONCATENATE';         s:'p :rest l';              f:if_map_concatenate       ),
(n:'F MAP-ATOM';                s:'p a';                    f:if_map_atom              ),
(n:'F AT-DEPTH НА-ГЛУБИНЕ';     s:'d p l';                  f:if_at_depth              ),
(n:'F APPLY ПРИМЕНИТЬ';         s:'p :rest params';         f:if_apply                 ),
(n:'P THREAD НИТЬ';             s:'p :rest params';         f:if_thread                ),
(n:'P QUEUE ОЧЕРЕДЬ';           s:'';                       f:if_queue                 ),
(n:'P WAIT ОЖИДАНИЕ';           s:'thread';                 f:if_wait                  ),


(n:'F UNION';                   s:':rest a';                f:if_union                 ),
(n:'F INTERSECTION ПЕРЕСЕЧЕНИЕ';s:':rest a';                f:if_intersection          ),
(n:'F DIFFERENCE';              s:'a b';                    f:if_difference            ),
(n:'F CARTESIAN-PRODUCT ДЕКАРТОВО-ПРОИЗВЕДЕНИЕ';s:':rest n';f:if_cartesian_product     ),

(n:'f POSITION';                s:'sequence element';       a:ifa_position             ),
(n:'f KEY-INDEX INDEX-KEY';     s:'list key-v';             a:ifa_index_key            ),
(n:'f INDEX-MAX';               s:'list';                   a:ifa_index_max            ),
(n:'f INDEX-MIN';               s:'list';                   a:ifa_index_min            ),
(n:'F LIST СПИСОК';             s:':rest e';                f:if_list                  ),
(n:'f OF-LENGTH ДЛИНЫ';         s:'n :optional l e';        a:ifa_of_length            ),
(n:'f PLAIN-LIST';              s:':rest l';                a:ifa_plain_list           ),
(n:'F RECORD ЗАПИСЬ';           s:':rest slots';            f:if_record                ),
(n:'F RECORD-AS ЗАПИСЬ-КАК';    s:'template :rest slots';   f:if_record_as             ),
(n:'F HASH-TABLE DICTIONARY СЛОВАРЬ';
                                s:':rest key-val';          f:if_hash_table            ),
(n:'F CONCATENATE';             s:':rest a';                f:if_concatenate           ),
(n:'F INTERLACED';              s:':rest l';                f:if_interlaced            ),
(n:'F GROUP ГРУППИРОВКА';       s:'s :rest p';              f:if_group                 ),
(n:'f GROUP-BY ГРУППИРОВКА-ПО'; s:'property <list> :optional output-mode'; a:ifa_group_by),
(n:'F STRINGS-MISMATCH';        s:'a b';                    f:if_strings_mismatch      ),
(n:'F REPLACE ЗАМЕНА';          s:'s :rest pairs';          f:if_replace               ),
(n:'F SUBSTITUTE ПОДСТАНОВКА';  s:'l :rest pairs';          f:if_substitute            ),
(n:'F UPDATE';                  s:'structure index modification'; f:if_update          ),
(n:'F REMOVE ИСКЛЮЧИТЬ';        s:'src v';                  f:if_remove                ),
(n:'F ASSOCIATIONS';            s:'al k :rest keys';        f:if_associations          ),
(n:'F INTERVAL ИНТЕРВАЛ';       s:'r v';                    f:if_interval              ),
(n:'f ELEMENT ЭЛЕМЕНТ';         s:'c <index>';              a:ifa_element              ),
(n:'F N-TH №';                  s:'l n';                    f:if_n_th                  ),

(n:'f GRAPH:CONNECTIVITY';      s:'g n1 n2';                a:ifa_graph_connectivity   ),
(n:'f GRAPH:PATH';              s:'g n1 n2 from to';        a:ifa_graph_path           ),

(n:'F BYTES';                   s:':rest b';                f:if_bytes                 ),
(n:'F BIT-AND';                 s:'a b';                    f:if_bit_and               ),
(n:'F BIT-NOT';                 s:'a';                      f:if_bit_not               ),
(n:'F BIT-OR';                  s:'a b';                    f:if_bit_or                ),
(n:'F BIT-XOR';                 s:'a b';                    f:if_bit_xor               ),
(n:'F SHIFT';                   s:'a n :optional bits';     f:if_shift                 ),
(n:'F CRC32';                   s:'b';                      f:if_crc32                 ),
(n:'F CRC8';                    s:'b :optional seed';       f:if_crc8                  ),
(n:'F CHARACTER';               s:'n';                      f:if_character             ),
(n:'F BYTES-TO-STRING';         s:'bv :optional encoding';  f:if_byte_vector_to_string ),
(n:'F STRING-TO-BYTES';         s:'str :optional encoding'; f:if_string_to_bytes       ),
(n:'F BASE64';                  s:'src';                    f:if_base64                ),

(n:'F ASSERTION';               s:'c :rest m';              f:if_assertion             ),
(n:'P DOCUMENTATION DOC';       s:'a';                      f:if_documentation         ),
(n:'F SIGNATURE';               s:'subprogram';             f:if_signature             ),
(n:'P DIRECTORY';               s:'dir :optional mask invisible'; f:if_directory       ),
(n:'P DIRECTORY-TREE';          s:'dir :optional mask invisible'; f:if_directory_tree  ),
(n:'P SLEEP';                   s:'m';                      f:if_sleep                 ),

(n:'P PLATFORM ПЛАТФОРМА';      s:':rest a';                f:if_platform              ),
(n:'P RUN-COMMAND';             s:'c :optional d';          f:if_run_command           ),
(n:'P COMMAND';                 s:'command :rest parameters';f:if_command              ),
(n:'P PROCESS';                 s:'cmd :key directory encoding';f:if_process           ),
(n:'P PROCESS-ID';              s:':optional proc';         f:if_process_id            ),
//(n:'P PROCESS-TERMINATE';     f:if_process_pipe;          s:'(proc :optional encoding)'),
(n:'P NOW';                     s:'';                       f:if_now                   ),
(n:'P TODAY СЕГОДНЯ';           s:'';                       f:if_today                 ),
(n:'P TOMORROW ЗАВТРА';         s:'';                       f:if_tomorrow              ),
(n:'P YESTERDAY ВЧЕРА';         s:'';                       f:if_yesterday             ),
(n:'P TIME';                    s:':optional date-time';    f:if_time                  ),


(n:'P OPEN-FILE';               s:'n :key mode encoding';   f:if_open_file             ),
(n:'P FILE ФАЙЛ';               s:'n :optional mode encoding';f:if_open_file           ),
(n:'P CLOSE-FILE';              s:'s';                      f:if_close_stream          ),
(n:'P TEXT-FROM-FILE ТЕКСТ-ИЗ-ФАЙЛА';s:'fn :optional encoding'; f:if_text_from_file    ),
(n:'P TEXT-TO-FILE ТЕКСТ-В-ФАЙЛ';s:'fn text :optional encoding'; f:if_text_to_file     ),
(n:'P BYTES-FROM-FILE БАЙТЫ-ИЗ-ФАЙЛА';s:'fn';               f:if_bytes_from_file       ),
(n:'P BYTES-TO-FILE БАЙТЫ-В-ФАЙЛ'; s:'fn bb';               f:if_bytes_to_file         ),
(n:'P DEFLATE';                 s:'s :key encoding header'; f:if_deflate               ),
(n:'P INFLATE';                 s:'s :key encoding header'; f:if_inflate               ),
(n:'P MEMORY-STREAM';           s:':optional bv';           f:if_memory_stream         ),
(n:'P SERIAL:PORT';             s:'name boud :key encoding timeout'; f:if_serial_port  ),
(n:'P SERIAL:DISCARD-INPUT';    s:'port';                   f:if_serial_discard_input  ),

(n:'P ZIP:OPEN';                s:'n :key mode encoding';   f:if_zip_open              ),
(n:'P ZIP:FILELIST';            s:'z';                      f:if_zip_filelist          ),
(n:'P ZIP:FILE';                s:'z fn :optional mode encoding'; f:if_zip_file        ),
(n:'P ZIP:DELETE';              s:'z fn';                   f:if_zip_delete            ),

(n:'P STREAM-POSITION';         s:'s :optional p';          f:if_stream_position       ),
(n:'P STREAM-LENGTH';           s:'s :optional l';          f:if_stream_length         ),
(n:'P STREAM-ENCODING';         s:'s :optional e';          f:if_stream_encoding       ),
(n:'P READ-BYTES';              s:'s count';                f:if_read_bytes            ),
(n:'P READ-CHARACTER READ-STRING'; s:':optional s count';   f:if_read_character        ),
(n:'P READ-LINE';               s:':optional s sep max';    f:if_read_line             ),
(n:'P WRITE-STRING';            s:'stream s';               f:if_write_string          ),
(n:'P WRITE-LINE';              s:'s l :optional sep';      f:if_write_line            ),
(n:'P WRITE-BYTE';              s:'s i';                    f:if_write_byte            ),
(n:'P WRITE-WORD';              s:'s i';                    f:if_write_word            ),
(n:'P WRITE-DWORD';             s:'s i';                    f:if_write_dword           ),
(n:'P WRITE-INTEGER-8';         s:'s i';                    f:if_write_integer_8       ),
(n:'P WRITE-INTEGER-16';        s:'s i';                    f:if_write_integer_16      ),
(n:'P WRITE-INTEGER-32';        s:'s i';                    f:if_write_integer_32      ),
(n:'P WRITE-INTEGER-64';        s:'s i';                    f:if_write_integer_64      ),
(n:'P WRITE-FLOAT-SINGLE';      s:'s f';                    f:if_write_float_single    ),
(n:'P WRITE-FLOAT-DOUBLE';      s:'s f';                    f:if_write_float_double    ),

(n:'p READ';                    s:':optional s';            a:ifa_read                 ),
(n:'P WRITE';                   s:'s a';                    f:if_write                 ),
(n:'P PRINT';                   s:'s a';                    f:if_print                 ),
(n:'P INPUT ВВОД';              s:'prompt :rest query';     f:if_input                 ),
(n:'P LOG-FILE';                s:':optional fn encoding';  f:if_log_file              ),

(n:'P FMT';                     s:'stream :rest s';         f:if_fmt                   ),
(n:'F STR';                     s:':rest s';                f:if_str                   ),
(n:'F LOG';                     s:':rest s';                f:if_log                   ),
(n:'F HEX';                     s:'i :optional d';          f:if_hex                   ),
(n:'F FIXED';                   s:'f d :optional n';        f:if_fixed                 ),
(n:'f CER';                     s:'c :optional d n';        a:ifa_cer                  ),
(n:'F FDT';                     s:'dt :optional format';    f:if_fdt                   ),
(n:'F COL';                     s:'w v :optional a';        f:if_col                   ),
(n:'F LST';                     s:'l :optional s f';        f:if_fmt_list              ),
//(n:'f LST';                     s:'l :optional s f';        a:ifa_fmt_list             ),
(n:'F FMT-TABLE';               s:'stream data :key hs mode'; f:if_fmt_table           ),
(n:'F SINGLE-SPACES';           s:'s';                      f:if_single_spaces         ),


(n:'P XML:READ';                s:'stream';                 f:if_xml_read              ),
(n:'P XML:WRITE';               s:'stream xml';             f:if_xml_write             ),
(n:'F XML:ENCODE';              s:'s';                      f:if_xml_encode            ),
(n:'F XML:DECODE';              s:'s';                      f:if_xml_decode            ),
(n:'f XML:CHILDREN';            s:'node name';              a:ifa_xml_children         ),
(n:'F XML:CHILD';               s:'node :rest names';       f:if_xml_child             ),
(n:'F XML:ATTRIBUTE';           s:'node name';              f:if_xml_attribute         ),

(n:'P SQL:ODBC-CONNECTION';     s:'dsn';                    f:if_sql_ODBC_connection   ),
(n:'P SQL:ORACLE-CONNECTION';   s:'database :key host username password'; f:if_sql_oracle_connection),
(n:'p SQL:SQLITE';              s:'database';               a:ifa_sql_sqlite_connection),
(n:'P SQL:QUERY';               s:'db :rest q';             f:if_sql_query             ),
(n:'P SQL:QUERY-LIST';          s:'db :rest q';             f:if_sql_query_list        ),
(n:'P SQL:QUERY-TABLE';         s:'db :rest q';             f:if_sql_query_table       ),
(n:'P SQL:TEST';                s:'db query';               f:if_sql_test              ),

(n:'P HTTP:GET';                s:'url :key proxy user-agent timeout'; f:if_http_get   ),

(n:'P SOCKET';                  s:'url :key encoding timeout'; f:if_socket             ),
(n:'P SERVER';                  s:'port :key address';      f:if_server                ),

(n:'F REGEXP:MATCH';            s:'pattern s :key from';    f:if_regexp_match          ),
(n:'f REGEXP:IS';               s:'pattern s';              a:ifa_regexp_is            ),
(n:'F REGEXP:REPLACE';          s:'s pattern new :key from'; f:if_regexp_replace       ),
(n:'F REGEXP:REMOVE';           s:'s pattern :key from';    f:if_regexp_remove         ),
(n:'f CONTAINS-WORDS СОДЕРЖИТ-СЛОВА'; s:'words source';     a:ifa_contains_words       ),

(n:'F ?';                       s:'condition a :optional b'; f:if_if                   ),

(n:'P TKZ:NODE ТКЗ:УЗЕЛ';       s:'name';                   f:if_tkz_node              ),
(n:'P TKZ:ARC ТКЗ:ВЕТВЬ';       s:'name node1 node2 :key Z Y E K'; f:if_tkz_arc        ),
(n:'P TKZ:BREAKER ТКЗ:ВЫКЛЮЧАТЕЛЬ';s:'name node1 node2';    f:if_tkz_breaker           ),
(n:'P TKZ:FAULT ТКЗ:ПОВРЕЖДЕНИЕ';s:'name';                  f:if_tkz_fault             ),
(n:'P TKZ:CALCULATE ТКЗ:ВЫЧИСЛИТЬ';s:':optional iter';      f:if_tkz_calc              ),
(n:'P TKZ:MEASUREMENT-POINT ТКЗ:ТОЧКА-ИЗМЕРЕНИЯ';s:'name :optional ending'; f:if_tkz_measurement_point),
(n:'P TKZ:MEASUREMENTS ТКЗ:ИЗМЕРЕНИЯ'; s:'';                f:if_tkz_measurements      ),
(n:'P TKZ:PROTECTION ТКЗ:ЗАЩИТА'; s:'name U-point I-point'; f:if_tkz_protection        ),
(n:'P TKZ:PROTECTION-MEASUREMENTS ТКЗ:ИЗМЕРЕНИЯ-ЗАЩИТЫ'; s:'name';
                                        f:if_tkz_protection_measurements        ),

(n:'p TKZ:GRID';                s:':rest arcs';             a:ifa_tkz_grid             ),
(n:'p TKZ:MEASUREMENTS';        s:'grid :rest mp';          a:ifa_tkz_measurements     ),
(n:'p TKZ:FAULT';               s:'grid point :key R distance count';  a:ifa_tkz_fault ),
(n:'p TKZ:MODE';                s:'grid :rest off';         a:ifa_tkz_mode             ),

(n:'f ZEMQ';                    s:'cirquit';                a:ifa_zemq                 ),


(n:'P OPENCL:PLATFORMS';        s:'';                       f:if_opencl_platforms      ),
(n:'P OPENCL:PROCESSOR';        s:'proc';                   f:if_opencl_processor      ),

(n:'P GUI:CANVAS';              s:'';                       f:if_canvas)

);



const predicates: array[1..44] of record n: unicodestring; f: TTypePredicate; end = (
(n:'ANY';                  f:tpAny),
(n:'NIL';                  f:tpNIL),
(n:'TRUE';                 f:tpTRUE),
(n:'NUMBER';               f:tpNumber),
(n:'REAL';                 f:tpReal),
(n:'INTEGER';              f:tpInteger),
(n:'NATURAL';              f:vpNatural),
(n:'FLOAT';                f:tpFloat),
(n:'DURATION';             f:tpDuration),
(n:'DATE-TIME';            f:tpDateTime),
(n:'COMPLEX';              f:tpComplex),
(n:'MATRIX';               f:tpTensor2d),
(n:'MATRIX-REAL';          f:tpTensorReal2d),
(n:'NAME';                 f:tpName),
(n:'VECTOR';               f:tpTensor1d),
(n:'VECTOR-REAL';          f:tpTensorReal1d),
(n:'ATOM';                 f:tpAtom),
(n:'SUBPROGRAM';           f:tpSubprogram),
(n:'LIST';                 f:tpList),
(n:'EMPTY';                f:vpEmpty),
(n:'SYMBOL';               f:tpSymbol),
(n:'KEYWORD';              f:tpKeyword),
(n:'STREAM';               f:vpStream),
(n:'STRING';               f:tpString),
(n:'POSITIVE';             f:vpRealPositive),
(n:'NEGATIVE';             f:vpRealNegative),
(n:'BYTES';                f:tpBytes),
(n:'END-OF-STREAM';        f:vpStreamEnd),
(n:'END-OF-FILE';          f:vpStreamEnd),
(n:'EXPRESSION-CONST';     f:vpExpression_CONST),
(n:'RANGE';                f:tpRange),
(n:'RANGE-INTEGER';        f:tpRangeInteger),
(n:'STRING-INTEGER';       f:vpStringInteger),
(n:'STRING-NUMBER';        f:vpStringNumber),
(n:'STRING-REAL';          f:vpStringReal),
(n:'STRING-DURATION';      f:vpStringDuration),
(n:'STRING-DATE-TIME';     f:vpStringDateTime),
(n:'STRING-RANGE-INTEGER'; f:vpStringRangeInteger),
(n:'STRING-RANGE';         f:vpStringRange),
(n:'STRING-TEST-PREDICATE';f:vpStringTestPredicate),
(n:'TENSOR';               f:tpTensor),
(n:'TENSOR-REAL';          f:tpTensorReal),
(n:'OPERATOR';             f:tpOperator),
(n:'TABLE';                f:vpListMatrix)


);



procedure create_persistent_internal_functions;
var i, j: integer; fun_names: TStringArray; sign: TVList;
begin
    for i := low(int_fun) to high(int_fun) do
        begin
            fun_names := SplitString(int_fun[i].n);
            SetLength(int_fun_persistent[i], Length(fun_names)-1);
            sign := read_from_string('('+int_fun[i].s+')') as TVList;
            for j := 1 to high(fun_names) do
                case fun_names[0][1] of
                    'f': int_fun_persistent[i][j-1] := TVInternalArrayFunction.Create(
                                                        ifh_build_sign(sign),
                                                        int_fun[i].a,
                                                        symbol_n(fun_names[j]));
                    'p': int_fun_persistent[i][j-1] := TVInternalArrayProcedure.Create(
                                                        ifh_build_sign(sign),
                                                        int_fun[i].a,
                                                        symbol_n(fun_names[j]));
                    'F': int_fun_persistent[i][j-1] := TVInternalFunction.Create(
                                                        ifh_build_sign(sign),
                                                        int_fun[i].f,
                                                        symbol_n(fun_names[j]));
                    'P': int_fun_persistent[i][j-1] := TVInternalProcedure.Create(
                                                        ifh_build_sign(sign),
                                                        int_fun[i].f,
                                                        symbol_n(fun_names[j]));
                end;
            FreeAndNil(sign);
		end;
end;


procedure destroy_persistent_internal_functions;
var i, j: integer;
begin
    for i := low(int_fun_persistent) to high(int_fun_persistent) do
        for j := 0 to high(int_fun_persistent[i]) do
            int_fun_persistent[i][j].Destroy;
end;


function create_base_stack: TVSymbolStack;
var i, j: integer; fun_names: TStringArray;
begin
    result := TVSymbolStack.Create;


    //загрузка внутренних функций
    for i := low(int_fun_persistent) to high(int_fun_persistent) do
        for j := 0 to high(int_fun_persistent[i]) do
            result.new_const(
                int_fun_persistent[i][j].nN,
                int_fun_persistent[i][j].Copy);

    //загрузка унарных внутренних функций
    for i := low(int_un_fun) to high(int_un_fun) do begin
        fun_names := SplitString(int_un_fun[i].n);
        for j := 1 to high(fun_names) do
            case fun_names[0][1] of
                'F': result.new_const(
                            fun_names[j],
                            TVInternalUnaryFunction.Create(
                                    int_un_fun[i].f,
                                    symbol_n(fun_names[j])));
                'P': result.new_const(
                            fun_names[j],
                            TVInternalUnaryProcedure.Create(
                                    int_un_fun[i].f,
                                    symbol_n(fun_names[j])));
            end;
    end;

    //загрузка предикатов
    for i := low(predicates) to high(predicates) do begin
        result.new_const(
            predicates[i].n+'?',
            TVPredicate.Create(predicates[i].n, predicates[i].f, false));
        result.new_const(
            predicates[i].n+'!',
            TVPredicate.Create(predicates[i].n, predicates[i].f, true));
    end;

    //загрузка констант
    result.new_const('T',CreateTRUE);
    result.new_const('IF-NIL', TVOperator.Create(oeOR));
    result.new_const('DELETE', TVOperator.Create(oeEXTRACT));
    result.new_const('POP', TVOperator.Create(oeEXTRACT));
    result.new_const('ERROR-CLASS',TVString.Create(''));
    result.new_const('ERROR-MESSAGE',TVString.Create(''));
    result.new_const('ERROR-STACK',TVString.Create(''));
    result.new_const('EXECUTABLE-PATH', TVString.Create(ExtractFilePath(paramstr(0))));
    result.new_const('COMMAND-LINE', oph_command_line);
    result.new_const('NL', TVString.Create(LineEnding));
    result.new_const('CR', TVString.Create(#13));
    result.new_const('LF', TVString.Create(#10));
    result.new_const('CRLF', TVString.Create(#13#10));
    result.new_const('BACKSPACE', TVString.Create(#08));
    result.new_const('TAB', TVString.Create(#09));
    result.new_const('BOM', TVString.Create(BOM));
    result.new_const('_', TVSymbol.Create('_'));
    result.new_const('PI', TVFloat.Create(pi));
    result.new_const('Π', TVFloat.Create(pi));
    result.new_const('SQRT2', TVFloat.Create(sqrt(2)));
    result.new_const('SQRT3', TVFloat.Create(sqrt(3)));
end;


{ TEvaluationFlow }

constructor TEvaluationFlow.Create(parent_stack: TVSymbolStack);
begin
    if parent_stack<>nil
    then main_stack := parent_stack
    else main_stack := create_base_stack;
    stack := main_stack;
    pure_mode := false;
end;

constructor TEvaluationFlow.CreatePure;
begin
    main_stack := nil;
    stack := nil;
    pure_mode := true;
end;


destructor TEvaluationFlow.Destroy;
begin
    main_stack.Free;
    inherited Destroy;
end;


function TEvaluationFlow.oph_loop_body(PL: TValues; start: integer): TValue;
var frame_start: integer;
begin
    try
        frame_start := stack.Count;
        result := op_block(PL, start);
    finally
        stack.clear_frame(frame_start);
    end;
end;


procedure TEvaluationFlow.oph_bind(S, V: TValue; constant: boolean);

    procedure push_var(_n: integer; _v: TValue);
    begin
        if _.N = _n
        then _v.Free
        else stack.new_var(_n, _v, constant)
	end;

    procedure bind_to_nil(_name: TValue);
    var i: integer;
    begin
        if tpSymbol(_name)
        then
            push_var((_name as TVSymbol).N, TVList.Create())
        else
            if tpList(_name)
            then
                for i := 0 to (_name as TVList).high do
                    bind_to_nil((_name as TVList)[i])
            else
                raise ELE.Create('invalid signature', 'syntax');
    end;


    procedure bind(_s, _v: TValue);
    var i: integer; mode: (necessary, optional, key, rest);
        pl, sign: TVList;
        key_start: integer;

        procedure invalid_bind;
        begin
            raise ELE.Create('invalid binding '+_v.AsString+' to '+_s.AsString,
                'syntax');
        end;

        procedure invalid_sign;
        begin
            raise ELE.Create('invalid signature '+_s.AsString, 'syntax');
        end;

        procedure select_param_mode(V: TVKeyword);
        begin
            if vpKeyword_OPTIONAL(V) then mode := optional else
            if vpKeyword_KEY(V)      then mode := key      else
            if vpKeyword_REST(V)     then mode := rest     else
            raise ELE.Create(V.AsString+' - invalid parameter mode','syntax');
        end;

        procedure bind_key(S: TVSymbol);
        var i: integer; nN: integer;
        begin
            nN := symbol_n(':'+S.uname);
            i := pl.high-1;
            while i>=key_start do
                begin
                    if tpKeyword(pl[i]) and (nN=N_(pl[i]))
                    then
                        begin
                            push_var(S.N, pl.GetValue(i+1));
                            Exit;
                        end;
                    Dec(i, 2);
                end;
            bind_to_nil(S);
        end;

        procedure bind_rest(S: TVSymbol);
        var i: integer; L: TVList;
        begin
            L := TVList.Create;
            for i := key_start to pl.high do L.Add(pl.GetValue(i));
            push_var(S.N, L);
        end;

    begin

        if tpName(_s)
        then
            begin
                push_var((_s as TVSymbol).N, _v);
                Exit;
			end;

        if tpList(_s) and tpList(_v)
        then
            begin
                pl := _v as TVList;
                pl.CopyOnWrite();
                sign := _s as TVList;
            end
        else
            invalid_bind;

        mode := necessary;
        for i := 0 to sign.high do
            if tpKeyword(sign[i])
            then
                begin
                    if mode<>necessary then invalid_sign;
                    select_param_mode(sign[i] as TVKeyword);
                    key_start := i;
                end
            else
                case mode of
                    necessary: if i<=pl.high
                        then bind(sign[i], pl.GetValue(i))
                        else invalid_bind;
                    optional: if (i-1)<=pl.high
                        then bind(sign[i], pl.GetValue(i-1))
                        else bind_to_nil(sign[i]);
                    key: bind_key(sign[i] as TVSymbol);
                    rest: bind_rest(sign[i] as TVSymbol);
                end;
    end;

begin
    if tpName(S)
    then
        push_var((S as TVSymbol).N, V)
    else
        try
            bind(S, V);
		finally
            V.Free;
		end;
end;


function TEvaluationFlow.oph_execute_file(fn: unicodestring): boolean;
var prog_file: TVStream; expr, res: TValue;
begin
    result := true;
    try
        prog_file := nil;
        res := nil;

        prog_file := TVStream.Create(
                    TLFileStream.Create(DirSep(fn), fmOpenRead, seBOM));
        while true do begin
            expr := nil;
            expr := dlisp_read.read(prog_file);

            if ((expr is TVSymbol) and ((expr as TVSymbol).uname='EXIT'))
                or (expr is TVEndOfStream)
            then begin expr.Free; break; end;

            if ((expr is TVSymbol) and ((expr as TVSymbol).uname='REPL'))
            then begin expr.Free; result := false; break; end;

            res := eval_and_free(expr);
            if res is TVReturn then break;
            FreeAndNil(res);
        end;
    finally
        FreeAndNil(prog_file);

        FreeAndNil(res);
    end;
end;


procedure TEvaluationFlow.oph_bind_package(name: unicodestring; import: boolean);
var pack: TPackage;
    package_file: unicodestring;
    procedure bind_pack;
    var i: integer; prefix: unicodestring;
    begin
        pack := FindPackage(name);
        if pack=nil then raise ELE.Create('package '+name+' not found');
        prefix := UnicodeUpperCase(name)+':';
        for i := 0 to pack.export_list.high do
            stack.new_ref(
                prefix+pack.export_list.uname[i],
                pack.stack.get_ref(pack.export_list.SYM[i]));
        if import then for i := 0 to pack.export_list.high do
                stack.new_ref(
                    pack.export_list.uname[i],
                    pack.stack.get_ref(pack.export_list.SYM[i]));
    end;
begin
    pack := FindPackage(name);
    if pack<>nil
    then begin
        bind_pack;
        Exit;
    end;

    package_file := SearchPath(name);
    if package_file<>'' then begin LoadPackage(package_file,eval); bind_pack; Exit; end;

    raise ELE.Create('package '+name+' not found');
end;


function TEvaluationFlow.oph_eval_index(C: TVCompound; id: TValue): integer;
var ret_id: TValue;
begin
    if id = nil then Exit(-1);
    ret_id := nil;

    try
        if id is TVSubprogram
        then
            begin
                ret_id := call(TValues.Create(id, C));
                result := C.Index(ret_id);
			end
        else
            result := C.Index(id);
	finally
        ret_id.Free;
	end;
end;


procedure TEvaluationFlow.oph_eval_selector(C: TVCompound; var id: TValue);
var ret_id: TValue;
begin
    if id is TVSubprogram
    then
        begin
            ret_id := call(TValues.Create(id, C));
            id.Free;
            id := ret_id;
		end;
end;


function TEvaluationFlow.opl_elt(PL: TValues): TVChainPointer;
var selectors: TValues; s: integer;
begin
    if Length(PL)<2 then raise ELE.malformed('ELT');

    SetLength(selectors, Length(PL)-2);
    for s := 0 to high(selectors) do selectors[s] := nil;
    result := nil;

    try
        result := eval_link(PL[1]);
        for s := 0 to high(selectors) do selectors[s] := eval(PL[s+2]);
        result.EvalLink(selectors, call);
    finally
        for s := 0 to high(selectors) do FreeAndNil(selectors[s]);
    end;
end;


function TEvaluationFlow.opl_key(PL: TValues): TVChainPointer;
var key, target: TValue;
begin
    result := nil;
    key := nil;

    if Length(PL)<>3 then raise ELE.Malformed('KEY');

    try
        result := eval_link(PL[1]);
        key := eval(PL[2]);
        target := result.Target;

        if target is TVHashTable
        then
            result.add_index((target as TVHashTable).Index(key))

        else if target is TVList
        then
            result.add_index((target as TVList).KeyIndex(key))

        else
            raise ELE.Create('invalid KEY target')

    finally
        key.Free;
    end;
end;


function TEvaluationFlow.eval_link(P: TValue): TVChainPointer;
var i: integer; head: TValue;
begin try
    //эта функция должна попытаться вычислить указатель на место
    //если выражение не является корректным указанием на место,
    //то создать новую константу, содержащую значение выражения, и вернуть
    //указатель на неё
    result := nil;
    head := nil;

    if tpName(P) then begin
        i := stack.index_of((P as TVSymbol).N);
        if stack.stack[i].V.V is TVChainPointer
        then result := stack.stack[i].V.V.Copy as TVChainPointer
        else result := TVChainPointer.Create(RefVariable(stack.stack[i].V));
        exit;
    end;

    if tpSelfEvaluating(P) then begin
        result := TVChainPointer.Create(NewVariable(P.Copy, true));
        exit;
    end;

    try
        if tpListNotEmpty(P) then begin
            head := eval((P as TVList)[0]);
            if tpOperator(head) then begin
                case (head as TVOperator).op_enum of
                    oeELT: result := opl_elt((P as TVList).ValueList);
                    oeKEY: result := opl_key((P as TVList).ValueList);
                    else result := TVChainPointer.Create(NewVariable(eval(P), true));
                end;
                exit;
            end;
            result := TVChainPointer.Create(NewVariable(eval(P), true));
            exit;
        end;
    finally
        head.Free;
    end;

    raise ELE.Create('eval_link не обработанный случай');
except
    on E:ELE do begin
        E.EStack := 'eval link '+P.AsString+LineEnding+'=> '+E.EStack;
        raise ELE.Create(E.Message, E.EClass, E.EStack);
    end;
end;
end;


procedure TEvaluationFlow.debugger(E: ELE; PL: TValue);
var s, us, prompt, reply: unicodestring; res: TValue; expr: TVList;
begin
    debug_in_place := false;
    prompt := '!> ';
    reply := '>>> '+PL.AsString+LineEnding+E.Message+' ('+E.EClass+')';
    while true do try
        s := ifh_input(reply, prompt);
        reply := '';
        us := UnicodeUpperCase(s);
        if us='' then Break;
        if us='BREAK' then Exit;
        if us=':PL' then begin WriteLn(PL.AsString); Continue; end;
        try
            res := nil;
            if us=':EVAL' then begin res := eval(PL); Continue; end;
            if us[1]=':' then try
                expr := nil;
                expr := read_from_string('('+s+')') as TVList;
                res := oph_debug(expr);
                Continue;
            finally
                expr.Free;
            end;
            res := eval_and_free(read_from_string(s));
        finally
            if res<> nil then reply := ' = '+res.AsString else reply := '';
            res.Free;
        end;
    except
        on E:ELE do WriteLn(E.EStack, E.Message, ' (', E.EClass,')');
        on E:Exception do WriteLn(E.Message, ' (!', E.ClassName, '!)');
    end;
    debug_in_place := true;
end;


function TEvaluationFlow.op_and                            (PL: TValues): TValue;
var pc: integer;
begin
    result := CreateTRUE;
    for pc := 1 to high(PL) do begin
        FreeAndNil(result);
        result := eval(PL[pc]);
        if tpNIL(result) then exit;
    end;
end;


function TEvaluationFlow.op_block          (PL: TValues; start: integer): TValue;
var pc, i: integer; V: TValue;
begin
    pc := start;
    V := nil;
    result := nil;

    while pc<Length(PL) do
        begin
            FreeAndNil(V);
            V := eval(PL[pc]);
            Inc(pc);
            if V is TVGo
            then
                if tpBreak(V) or tpContinue(V) or tpReturn(V)
                then
                    BREAK

                else if V is TVGoTo
                then
                    begin
                        pc := -1;
                        for i := start to high(PL) do
                            if tpKeyword(PL[i]) and (N_(PL[i])=(V as TVGoto).N)
                            then
                                begin
                                    pc := i;
                                    BREAK;
                                end;
                        if pc<0 then BREAK;
                    end;
        end;

    if V=nil then result := TVList.Create(nil) else result := V;
end;


function TEvaluationFlow.op_case                    (PL: TValues): TValue;
var i: integer; expr: TValue;
begin
    if Length(PL)<2 then raise ELE.Malformed('CASE');

    for i := 2 to high(PL) do
        if tpNIL(PL[i]) then raise ELE.Malformed('CASE');
    //TODO: CASE не проверяет наличие ELSE в середине списка альтернатив
    try
        expr := nil;
        result := nil;
        expr := eval(PL[1]);

        for i := 2 to high(PL) do
            if equal(expr, L_(PL[i])[0])
                or (tpListNotEmpty(L_(PL[i])[0]) and
                        ifh_member(L_(L_(PL[i])[0]), expr))
                or vpSymbol_ELSE(L_(PL[i])[0])
            then
                EXIT(op_block(L_(PL[i]).ValueList, 1));

        raise ELE.Create('CASE without result', 'invalid parameters');
    finally
        expr.Free;
    end;
end;


function TEvaluationFlow.op_push                     (PL: TValues): TValue;
var CP :TVChainPointer; i: integer; elts: TValues;
    target: TValue; target_queue: TQueue;
begin
    result := nil;
    CP := nil;
    if Length(PL)<2 then raise ELE.Create('malformed '+ValsToStr(PL), 'syntax/');

    try
        CP := eval_link(PL[1]);
        if CP.P.constant then raise ELE.TargetNotVariable(PL[1].AsString);

        SetLength(elts, Length(PL) - 2);
        for i := 2 to high(PL) do elts[i-2] := eval(PL[i]);

        target := CP.TargetForModification();

        if target is TVList
        then
            (target as TVList).Append(elts)

        else if target is TVQueue
        then
            begin
                target_queue := (target as TVQueue).target;
        		for i := 0 to high(elts) do
                    target_queue.push(elts[i]);
		    end
        else
            raise ELE.Create('PUSH '+target.ClassName, 'invalid target/');

        result := CreateTRUE;
    finally
        CP.Free;
    end;
end;


function TEvaluationFlow.op_const                       (PL: TValues): TValue;
begin
    if (Length(PL)<>3) or not (tpName(PL[1]) or tpList(PL[1]))
    then raise ELE.malformed('CONST');

    oph_bind(PL[1], eval_without_var_definitions(PL[2]), true); //только CONST и VAR вызывают эту функцию

    result := CreateTRUE
end;


function TEvaluationFlow.oph_debug(PL: TVList): TValue;
var arg: TValue; expr, vv: TValues; i: integer; P: PVariable;

    function params(mode: unicodestring; tp: TTypePredicate=nil): boolean;
    begin
        result := mode = PL.uname[0];
        if @tp=nil
        then result := result // and (arg=nil)
        else result := result and tp(arg);
    end;

begin try
    result := nil;
    arg := nil;

    if params(':MACROEXPAND')
    then
        begin
            if tpListNotEmpty(PL[1])
            then
                try
                    expr := Copy(PL.L[1].ValueList);
                    arg := eval(expr[0]);
                    expr[0] := arg;
                    EXIT(eval_routine(expr));
                finally
                    arg.Free;
                end

            else if tpName(PL[1])
            then
                begin
                    P := stack.look_var(PL.SYM[1].N);
                    EXIT(eval_routine(TValues.Create(P.V)));
                end;
        end;

    if PL.Count>1 then arg := eval(PL[1]);
    ////////////////////////////////////////////
    if params(':RESET-STACK') then begin
        main_stack.Free;
        main_stack := create_base_stack;
        stack := main_stack;
    end;

    if params(':RESET-PACKAGES') then begin
        FreePackages;
    end;

    if params(':PRINT-STACK') then begin
        stack.Print;
    end;

    if params(':PRINT-STACK', tpRoutine) then begin
        WriteLn('STACK OF ', arg.AsString);
        (arg as TVRoutine).stack.Print;
    end;

    if params(':PRINT-STACK', vpNatural) then begin
        stack.Print((arg as TVInteger).fI);
    end;

    if params(':CLEAR-STACK-TOP') then begin
        stack.clear_top();
    end;

    if params(':PRINT-HASH-TABLE', tpHashTable) then begin
        (arg as TVHashTable).print;
    end;

    if params(':THREADS-COUNT') then begin
        result := TVInteger.Create(Length(threads_pool));
    end;

    if params(':THREADS-COUNT', vpNatural) then begin
        set_threads_count((arg as TVInteger).fI);
    end;

    if params(':PRINT-LINKS', tpAny) then begin
        print_links(arg);
    end;

    if params(':SEPARATE', tpAny) then begin
        result := separate(arg);
    end;

    if params(':CPU-COUNT') then begin
        result := TVInteger.Create(CPU_Count);
    end;

    if params(':TYPE', tpAny) then begin

        result := TVString.Create(arg.ClassName);
    end;

    if params(':ROUTINE-BODY', tpRoutine) then begin
        result := (arg as TVRoutine).body.Copy;
    end;

    if params(':ROUTINE-REST', tpRoutine) then begin
        result := (arg as TVRoutine).rest.Copy;
    end;

    if params(':PRINT-VALUES', tpAny) then begin
        SetLength(vv,0);
        extract_block_values(vv,arg);
        for i := 0 to high(vv) do WriteLn(vv[i].AsString);
    end;

    if params(':IN-PLACE', tpAny) then begin
        debug_in_place := tpTrue(arg);
    end;

    if params(':REF-COUNT', tpStream) then begin
        result := TVInteger.Create(((arg as TVStream).target as TCountingObject).refs-1);
    end;

    if params(':RRP') then begin
        result := TVInteger.Create(lisya_gc.rrp);
    end;

    if params(':RRP', vpNatural) then begin
        lisya_gc.rrp:= asInteger(arg);
    end;

    if params(':DUMP-HEAP', tpString) then begin
        //{$if declared(UseHeapTrace)}
        WriteLn('dumpheap');

        //{$ifend}
    end;

    if params(':MAX-ERROR-MESSAGE-LENGTH', vpNaturalOrNIL) then begin
        max_error_message_length := default(arg,-1);
    end;

finally
    arg.Free;
    if result=nil then result := CreateTRUE
end; end;


function TEvaluationFlow.oph_exception_class_match(exception_classes,
			handler_classes: unicodestring): boolean;
var e_classes, h_classes: TStringArray; h: integer;
begin
    if handler_classes='' then Exit(true);

    e_classes := SplitString(UnicodeUpperCase(exception_classes),'/');
    h_classes := SplitString(UnicodeUpperCase(handler_classes),'/');
    RemoveStrings(e_classes, '');
    RemoveStrings(h_classes, '');

    result := true;
    for h := 0 to high(h_classes) do
        result := result and (FindInStringArray(e_classes, h_classes[h])>=0);
end;


function TEvaluationFlow.op_debug                       (PL: TValues): TValue;
var PLtail: TVList; i: integer;
begin
    //TODO: перевести oph_debug на TValues
    try
        PLtail := TVList.Create();
        for i := 1 to high(PL) do PLtail.Add(PL[i].Copy);
        result := oph_debug(PLtail);
    finally
        PLtail.Free;
    end;
end;


function TEvaluationFlow.op_default                     (PL: TValues): TValue;
var CP: TVChainPointer; target: TValue;
begin
    CP := nil;
    result := nil;

    if Length(PL)<>3 then raise ELE.malformed('DEFAULT');

    CP := eval_link(PL[1]);
    target := CP.Target;

    try
        if tpNIL(target)
        then
            if CP.constant
            then
                begin
                    if tpName(PL[1])
                    then
                        stack.new_const(N_(PL[1]),
                                            eval_without_var_definitions(PL[2]))
                    else
                        raise ELE.TargetNotVariable(PL[1].AsString);
				end
            else
                CP.SetTarget(eval_without_var_definitions(PL[2]));

        result := CreateTRUE;
    finally
        CP.Free;
    end;
end;


function TEvaluationFlow.op_elt                         (PL: TValues): TValue;
var CP: TVChainPointer;
begin
    CP := opl_elt(PL);
    result := CP.GetTarget;
    CP.Free;
end;


function TEvaluationFlow.op_error                       (PL: TValues): TValue;
var eclass: TValue; smsg,sclass,sstack: unicodestring;
    P: PVariable;
    i: integer; msgL: TVList;
begin
    result:=nil;
    //если задан класс [и сообщение] то вызывается новое исключение
    //если параметры не заданы, то в текущем стеке ищется ранее возникшее исключение
    //стэк исключения всегда ищется в стеке и если не найден принимается пустым

    sclass := '';
    if Length(PL)>1 then
        try
            eclass := nil;
            eclass := eval(PL[1]);
            if tpString(eclass)
            then sclass := (eclass as TVString).S
            else raise ELE.InvalidParameters;
        finally
            eclass.Free;
        end;

    smsg := '';
    if Length(PL)>2 then
        try
            msgL := TVList.Create;
            for i := 2 to high(PL) do msgL.Add(eval(PL[i]));
            smsg := ifh_format(msgL);
        finally
            msgL.Free;
        end;

    sstack := '';
    try
        P := stack.look_var_or_nil(symbol_n('ERROR-STACK'));
        if (p<>nil) and tpString(p.V)
        then sstack := (P.V as TVString).S;
    finally
    end;

    if Length(PL)=1 then begin
        P := stack.look_var(symbol_n('ERROR-CLASS'));
        if tpString(p.V) then sclass := (P.V as TVString).S;
    end;

    if Length(PL)=1 then begin
        P := stack.look_var(symbol_n('ERROR-MESSAGE'));
        if tpString(p.V) then smsg := (P.V as TVString).S;
    end;

    raise ELE.Create(smsg, sclass, sstack);
end;


function TEvaluationFlow.op_execute_file                (PL: TValues): TValue;
var fn: TValue;
begin
    if Length(PL)<>2 then raise ELE.Malformed('EXECUTE-FILE');

    try
        result := nil;
        fn := nil;
        fn := eval(PL[1]);

        if not tpString(fn) then raise ELE.InvalidParameters;

        oph_execute_file((fn as TVString).S);
        result := CreateTRUE;
    finally
        fn.Free;
    end;
end;


function TEvaluationFlow.op_extract                     (PL: TValues): TValue;
var target: TValue; CP: TVChainPointer; index: TValue;
begin
    result := nil;
    index := nil;
    CP := nil;

    if (Length(PL)<2) or (Length(PL)>3)
    then
        raise ELE.Create('malformed '+ValsToStr(PL), 'syntax/');

    try
        CP := eval_link(PL[1]);
        if CP.P.constant then raise ELE.TargetNotVariable(PL[1].AsString);

        target := CP.TargetForModification();

        if target is TVList
        then
            try
                if Length(PL)=3 then index := eval(PL[2]);
                oph_eval_selector(target as TVList, index); //index := index(C) - если индекс программа

                if index=nil
                then
                    result := (target as TVList).extract(-1)

                else if index is TVInteger
                then
                    result := (target as TVList).extract((index as TVInteger).fI)

                else if index is TVRangeInteger
                then
                    result := (target as TVList).extract_subseq(
                        (index as TVRangeInteger).low, (index as TVRangeInteger).high)

                else
                    raise ELE.Create('EXTRACT '+index.AsString, 'invalid index/')
            finally
                index.Free;
			end

        else if target is TVQueue
        then
            result := (target as TVQueue).target.pop as TValue

        else
            raise ELE.Create('EXTRACT '+target.ClassName, 'invalid target/');

	finally
        CP.Free;
	end;
end;


function TEvaluationFlow.op_set                         (PL: TValues): TValue;
var CP :TVChainPointer;
begin
    result := nil;
    CP := nil;
    if (Length(PL)<3) or (Length(PL)>3) then raise ELE.InvalidParameters;

    try
        CP := eval_link(PL[1]);

        CP.SetTarget(eval(PL[2]));

        result := CreateTRUE;
    finally
        CP.Free;
    end;
end;


function TEvaluationFlow.op_try                         (PL: TValues): TValue;
var i: integer;
begin
    if Length(PL)<2 then raise ELE.Malformed('TRY');

    result := nil;
    try
        result := eval(PL[1]);
    except on E:ELE do //eval может выбросить только ELisyaError
        begin
            stack.new_const('EXCEPTION-CLASS', TVString.Create(E.EClass));
            stack.new_const('EXCEPTION-MESSAGE', TVString.Create(E.Message));
            stack.new_const('EXCEPTION-STACK', TVString.Create(E.EStack));
            stack.new_const('ERROR-CLASS', TVString.Create(E.EClass));
            stack.new_const('ERROR-MESSAGE', TVString.Create(E.Message));
            stack.new_const('ERROR-STACK', TVString.Create(E.EStack));

            for i := 2 to high(PL) do
                begin
                    if vpListHeaded_ELSE(PL[i])
                        or (vpListHeadedByString(PL[i])
                            and oph_exception_class_match(E.EClass, S_(L_(PL[i])[0])))
                    then
                        begin
                            result := op_block(L_(PL[i]).ValueList, 1);
                            Break;
	    				end;
			    end;

            if result=nil then raise ELE.Create(E.Message, E.EClass, E.EStack);
        end;
    end;
end; //33


function TEvaluationFlow.op_return                      (PL: TValues): TValue;
begin
    result := nil;
    case Length(PL) of
        2: result := TVReturn.Create(eval(PL[1]));
        1: result := TVReturn.Create(TVList.Create(nil));
        else raise ELE.Malformed('RETURN');
    end;
end;


function TEvaluationFlow.op_cond                        (PL: TValues): TValue;
var i: integer; tmp: TValue;
begin
    try
        tmp := TVList.Create;

        for i := 1 to high(PL) do
            if not tpListNotEmpty(PL[i]) then raise ELE.Malformed('COND');

        for i := 1 to high(PL) do
        begin
            FreeAndNil(tmp);
            tmp := eval(L_(PL[i])[0]);
            if not tpNIL(tmp)
            then
                begin
                    FreeAndNil(tmp);
                    tmp := op_block(L_(PL[i]).ValueList, 1);
                    break;
                end;
        end;

        result := tmp;
    except
        tmp.Free;
        raise;
    end;
end;


function TEvaluationFlow.oph_for_iterator(PL: TValues; iterator: TVSubprogram): TValue;
var expr: TValues; e: PVariable; V: TValue;
begin
    result := nil;
    expr := TValues.Create(iterator);
    V := nil;
    e := stack.new_var(N_(PL[1]), nil, true);

    while true do
        begin
            FreeAndNil(e^.V);
            e^.V := call(expr);
            if tpNIL(e^.V)
            then
                begin
                    result := e^.V;
                    e^.V := nil;
                    break;
                end;

            V := oph_loop_body(PL, 3);
            if V is TVGo
            then
                begin
                    if V is TVBreak
                    then
                        begin
                            result := e^.V;
                            e^.V := nil;
                            FreeAndNil(V);
                            break;
                        end
                    else if V is TVReturn or V is TVGoTo
                    then
                        Exit(V);
                end
            else
                FreeAndNil(V);
        end;
end;


function TEvaluationFlow.oph_for_range(PL: TValues; first, last: integer): TValue;
var e: PVariable; V: TValue; index: TVInteger; i: integer;
begin
    result := nil;
    V := nil;
    index := TVInteger.Create(first);
    e := stack.new_var(N_(PL[1]), index, true);

    for i := first to last do
    begin
        index.fI := i;
        V := oph_loop_body(PL, 3);
        if V is TVGo
        then
            begin
                if V is TVBreak
                then
                    begin
                        result := index;
                        e^.V := nil;
                        FreeAndNil(V);
                        break;
                    end
                else if V is TVReturn or V is TVGoto
                then
                    begin
                        e^.V := nil;
                        FreeAndNil(index);
                        result := V;
                        break;
                    end
            end
        else
            FreeAndNil(V);
    end;
end;


function TEvaluationFlow.op_for                     (PL: TValues): TValue;
var i, frame_start, high_i, low_i: integer;
    V, E, target: TValue;
    CP: TVChainPointer;
    mode : (sequence, range, iterator);

    function vpSubseq(V: TValue): boolean;
    begin
        result := (V is TVInternalArrayRoutine)
            and (@((V as TVInternalArrayRoutine).body) = @ifa_subseq);
    end;

    function subseq_expr(): boolean;
    var expr: TVList; c: integer;
    begin
        result := false;
        if not tpList(E) then Exit;
        expr := E as TVList;
        if not (expr.Count in [3..4]) then Exit;
        expr[0] := eval(expr[0]);
        if not vpSubseq(expr[0]) then Exit;
        CP := eval_link(expr[1]);
        expr[1] := CP.Copy;
        if not (CP.Target is TVSequence) then raise ELE.InvalidParameters;
        c := (CP.Target as TVSequence).Count;
        high_i := c;

        expr[2] := eval(expr[2]);
        if tpInteger(expr[2])
        then low_i := expr.I[2]
        else raise ELE.InvalidParameters;

        if expr.Count=4
        then
            begin
                expr[3] := eval(expr[3]);
                if tpInteger(expr[3])
                then high_i := expr.I[3]
                else if not tpNIL(expr[3]) then raise ELE.InvalidParameters;
            end;

        if low_i<0 then low_i := c+low_i;
        if high_i<0 then high_i := c+high_i;
        high_i := high_i-1;
        mode := sequence;
        result := true;
    end;

begin
    //оператор цикла возвращает NIL в случае завершения и последнее значение
    //итерируемой переменной в случае досрочного прерывания оператором (BREAK)
    //это упрощает написание функций поиска
    try
        E := nil;
        CP := nil;
        result := nil;
        frame_start := stack.Count;

        if (Length(PL)<3) or not tpName(PL[1]) then raise ELE.malformed('FOR');

        E := PL[2].Copy;
        if not subseq_expr
        then
            begin
                CP := eval_link(E);
                target := CP.Target;

                if tpSequence(target)
                then
                    begin
                        mode := sequence;
                        low_i := 0;
                        high_i := (target as TVSequence).high;
                    end

                else if tpRangeInteger(target)
                then
                    begin
                        mode := range;
                        low_i := (target as TVRangeInteger).low;
                        high_i := (target as TVRangeInteger).high-1;
                    end

                else if vpNatural(target)
                then
                    begin
                        mode := range;
                        low_i := 0;
                        high_i := (target as TVInteger).fI-1;
                    end

                else if tpSubprogram(target)
                then
                        mode := iterator
                else
                    raise ELE.InvalidParameters;
            end;


        case mode of
            sequence:
                try
                    V := nil;
                    CP.add_index(0);
                    stack.new_var(N_(PL[1]), CP, true);
                    for i := low_i to high_i do
                        begin
                            CP.set_last_index(i);
                            FreeAndNil(V);
                            V := oph_loop_body(PL, 3);

                            if tpBreak(V) then Exit(CP.GetTarget);

                            if tpReturn(V) then Exit(V.Copy);
                        end;
				finally
                    V.Free;
				end;

            range:
                begin
                    CP.Free;
                    result := oph_for_range(PL, low_i, high_i);
				end;

            iterator:
                try
                    result := oph_for_iterator(PL, target as TVSubprogram);
				finally
                    CP.Free;
				end;
        end;

    finally
        if result=nil then result:= TVList.Create(nil);
        stack.clear_frame(frame_start);
        E.Free;
    end;
end;


function TEvaluationFlow.op_goto                        (PL: TValues): TValue;
begin
    if (Length(PL)<>2) or not tpSymbol(PL[1]) then raise ELE.malformed('GOTO');

    result := TVGoto.Create(N_(PL[1]));
end;


function TEvaluationFlow.op_if                          (PL: TValues): TValue;
var condition: TValue;
begin
    if (Length(PL)<3) or (Length(PL)>4)
        //TODO: эти проверки синтаксиса IF нужны только для более ясного сообщения об ошибке
        //их отсутствие приведёт к падению с сообщением THEN/ELSE is not subprogram
        //or vpListHeaded_ELSE(PL[2])
        //or ((Length(PL)=4) and vpListHeaded_THEN(PL[3]))
    then raise ELE.malformed('IF');

    try
        condition := nil;
        condition := eval(PL[1]);

        if not tpNIL(condition)
        then
            begin
                if vpListHeaded_THEN(PL[2])
                then result := op_block(L_(PL[2]).ValueList, 1)
                else result := eval(PL[2]);
            end

        else if Length(PL)=4
        then
            begin
                if vpListHeaded_ELSE(PL[3])
                then result := op_block(L_(PL[3]).ValueList, 1)
                else result := eval(PL[3]);
            end
        else
            result := TVList.Create;
    finally
        condition.Free;
    end;
end;


function TEvaluationFlow.op_insert                      (PL: TValues): TValue;
var target, selector: TValue; CP: TVChainPointer;
begin
    if Length(PL)<>4 then raise ELE.Malformed('INSERT');
    CP := nil;
    selector := nil;

    try
        CP := eval_link(PL[1]);
        if CP.P.constant
        then raise ELE.Create('INSERT '+PL[1].AsString, 'target is not variable/');

        target := CP.TargetForModification();

        selector := eval(PL[2]);

        if target is TVList
        then
            (target as TVList).insert(
                oph_eval_index(target as TVCompound, selector),
                eval(PL[3]))
        else
            raise ELE.Create('INSERT '+target.ClassName, 'invalid target/');

        result := CreateTRUE;
    finally
        CP.Free;
        selector.Free;
    end;
end;


function TEvaluationFlow.op_key                         (PL: TValues): TValue;
var CP: TVChainPointer;
begin
    CP := opl_key(PL);
    result := CP.GetTarget;
    CP.Free;
end;


function TEvaluationFlow.op_let(PL: TValues): TValue;
var old_v, VPL: TVList; i, j, count: integer;
begin
    //временно изменяет значение переменной, по завершении возвращает исходное
    //значение
    if (Length(PL)<3) or not vpListOfSymbolValuePairs(PL[1])
    then raise ELE.malformed('LET');

    VPL := L_(PL[1]);
    try
        old_v := TVList.Create;
        for i := 0 to VPL.High do old_v.Add(eval(VPL.L[i][0]));

        try
            count := 0;
            for i := 0 to VPL.High do begin
                stack.set_var(VPL.L[i].SYM[0], eval(VPL.L[i][1]));
                Inc(count);
            end;

            result := op_block(PL, 2);
        finally
            for j := 0 to count-1 do
                stack.set_var(VPL.L[j].SYM[0], old_v[j].Copy);
        end;
    finally
        FreeAndNil(old_v);
    end;
end;


//TODO: перенести в более подходящее место
function values_tail (PL: TValues; start: integer): TVList;
var i: integer; VL: TValues;
begin
    SetLength(VL, Length(PL) - start);
    for i := start to high(PL) do VL[i-start] := PL[i].Copy;
    result := TVList.Create(VL);
end;


function TEvaluationFlow.op_macro_symbol                (PL: TValues): TValue;
var proc: TVRoutine;
begin
    result := nil;

    if Length(PL)<3 then raise ELE.Malformed('MACRO-SYMBOL');
    if not tpName(PL[1]) then raise ELE.InvalidParameters;

    proc := TVMacroSymbol.Create(N_(PL[1]), nil, values_tail(PL, 2));
    fill_procedure_stack(proc, extract_expression_symbols(PL));

    stack.new_var(N_(PL[1]), proc, true);
    result := CreateTRUE;
end;


function TEvaluationFlow.op_or                          (PL: TValues): TValue;
var pc: integer;
begin
    result := nil;
    for pc := 1 to high(PL) do begin
        FreeAndNil(result);
        result := eval(PL[pc]);
        if not tpNIL(result) then exit;
    end;
    if result=nil then result := TVList.Create(nil);
end;


function TEvaluationFlow.op_package                     (PL: TValues): TValue;
var external_stack, package_stack: TVSymbolStack; pack: TPackage;
begin
    if (Length(PL)<4)
        or (not tpName(PL[1]))
        or (not tpListOfNames(PL[2]))
    then raise ELE.Malformed('PACKAGE');

    if FindPackage(symbol_uname(N_(PL[1])))<>nil
    then raise ELE.Create('package redefinition: '+(PL[1] as TVSymbol).name, 'syntax');

    package_stack := create_base_stack;
    external_stack := stack;
    stack := package_stack;
    result := nil;

    try
        result := op_block(PL, 3);

        //  сохранение пакета
        pack := TPackage.Create;
        pack.name := (PL[1] as TVSymbol).name;
        pack.uname := symbol_uname(N_(PL[1]));
        pack.export_list := PL[2].Copy as TVList;
        pack.stack := package_stack.Copy as TVSymbolStack;
        AddPackage(pack);

        Result.Free;
        result := CreateTRUE;
    finally
        stack := external_stack;
        package_stack.Free;
    end;
end;


function TEvaluationFlow.op_append                     (PL: TValues): TValue;
var CP :TVChainPointer; i: integer; supplements: TValues;
    target: TValue;
begin
    result := nil;
    CP := nil;
    if Length(PL)<2 then raise ELE.Create('malformed '+ValsToStr(PL), 'syntax/');

    try
        CP := eval_link(PL[1]);
        if CP.P.constant then raise ELE.TargetNotVariable(PL[1].AsString);

        SetLength(supplements, Length(PL) - 2);
        for i := 2 to high(PL) do supplements[i-2] := eval(PL[i]);

        target := CP.TargetForModification();

        if target is TVString
        then
            for i := 0 to high(supplements) do
                (target as TVString).Append(supplements[i] as TVString)

        else if target is TVList
        then
            for i := 0 to high(supplements) do
                (target as TVList).Append(supplements[i] as TVList)

        else if target is TVBytes
        then
            for i := 0 to high(supplements) do
                (target as TVBytes).append(supplements[i] as TVBytes)

        else
            raise ELE.Create('APPEND '+target.ClassName, 'invalid target/');

        result := CreateTRUE;
    finally
        CP.Free;
    end;
end;


function TEvaluationFlow.op_assemble                    (PL: TValues): TValue;

    function asmbl(expr: TValues; from: integer = 0): TVList;
    var i, j: integer; tmp: TValue;
    begin
        result := TVList.Create;
        for i := from to high(expr) do
            begin
                if vpListHeaded_VALUE(expr[i])
                then
                    result.Add(eval(L_(expr[i])[1]))

                else if vpListHeaded_INSET(expr[i])
                then
                    try
                        tmp := nil;
                        tmp := eval(L_(expr[i])[1]);
                        if not tpList(tmp) then raise ELE.Malformed('INSET');
                        for j := 0 to (tmp as TVList).high do
                            result.Add((tmp as TVList)[j].Copy);
                    finally
                        tmp.Free;
                    end

                else if tpList(expr[i])
                then
                    result.Add(asmbl(L_(expr[i]).ValueList))

            else result.Add(expr[i].Copy);
        end
    end;

begin
    // этот оператор должен работать аналогично BACKQUOTE в Лиспе
    // использует вторичные операторы VALUE и INSET

    result := asmbl(PL, 1);
end;


type TRoutineDeclarationMode = (declaration, lambda, forward_declaration, invalid);


function routine_declaration_mode(PL: TVList): TRoutineDeclarationMode; overload;
begin
    if (PL.count>=3) and tpName(PL[1]) and tpList(PL[2])
    then result := declaration
    else
        if (PL.count>=2) and tpList(PL[1])
        then result := lambda
        else
            if (PL.Count=2) and tpName(PL[1])
            then result := forward_declaration
            else
                result := invalid;
end;


function routine_declaration_mode(PL: TValues): TRoutineDeclarationMode; overload;
begin
    if (Length(PL)>=3) and tpName(PL[1]) and tpList(PL[2])
    then result := declaration
    else
        if (Length(PL)>=2) and tpList(PL[1])
        then result := lambda
        else
            if (Length(PL)=2) and tpName(PL[1])
            then result := forward_declaration
            else
                result := invalid;
end;


function TEvaluationFlow.op_function                    (PL: TValues): TValue;
var fun: TVFunction; P: PVariable;
    procedure f;
    var es: TIntegers; //i: integer;
    begin
        es := extract_expression_symbols(PL);
        //for i := 0 to high(es) do Write('   ', symbol_uname(es[i]));
        fill_function_stack(fun, es);
        try
            pure_value_pf1(fun);
        except
            FreeAndNil(fun); raise;
        end;
    end;

begin
    result := nil;

    case routine_declaration_mode(PL) of
        forward_declaration: begin
            stack.new_var(N_(PL[1]), TVFunctionForwardDeclaration.Create, true);
            result := CreateTRUE;
        end;
        declaration: begin
            P := stack.look_var_or_nil(N_(PL[1]));
            if (P=nil) or not (P.V is TVFunctionForwardDeclaration)
            then P := stack.new_var(N_(PL[1]), nil, true);
            fun := TVFunction.Create(N_(PL[1]), L_(PL[2]), values_tail(PL, 3));
            f;
            replace_value(P.V, fun);
            fun.stack.new_ref(fun.nN, RefVariable(P));
            result := CreateTRUE;
        end;
        lambda: begin
            fun := TVFunction.Create(0, L_(PL[1]), values_tail(PL, 2));
            f;
            result := fun;
        end;
        invalid: raise ELE.Malformed('FUNCTION');
    end;
end;


function TEvaluationFlow.op_procedure                   (PL: TValues): TValue;
var proc: TVRoutine; P: PVariable;

    procedure proc_create(_N: integer; _sign, _body: TVList);
    begin
        case (PL[0] as TVOperator).op_enum of
            oeMACRO:    proc := TVMacro.Create(_n, _sign, _body);
            oePROCEDURE:proc := TVProcedure.Create(_n, _sign, _body);
        end;
    end;

begin
    result := nil;

    case routine_declaration_mode(PL) of
        forward_declaration: begin
            stack.new_var(N_(PL[1]), TVProcedureForwardDeclaration.Create, true);
            result := CreateTRUE;
        end;
        declaration: begin
            P := stack.look_var_or_nil(N_(PL[1]));
            if (P=nil) or not (P.V is TVProcedureForwardDeclaration)
            then P := stack.new_var(N_(PL[1]), nil, true);
            proc_create(N_(PL[1]), L_(PL[2]), values_tail(PL, 3));
            fill_procedure_stack(proc, extract_expression_symbols(PL));
            replace_value(P.V, proc);
            proc.stack.new_ref(proc.nN, RefVariable(P));
            result := CreateTRUE;
        end;
        lambda: begin
            proc_create(0, L_(PL[1]), values_tail(PL, 2));
            fill_procedure_stack(proc, extract_expression_symbols(PL));
            result := proc;
        end;
        invalid: raise ELE.Malformed('PROCEDURE/MACRO');
    end;
end;


function TEvaluationFlow.op_var                         (PL: TValues): TValue;
begin
    case Length(PL) of
        2: if tpName(PL[1])
            then stack.new_var(N_(PL[1]), TVList.Create)
            else raise ELE.Malformed('VAR');
        3: oph_bind(PL[1], eval_without_var_definitions(PL[2]), false);
        else raise ELE.malformed('VAR');
    end;

    result := CreateTRUE;
end;


function TEvaluationFlow.op_when                        (PL: TValues): TValue;
begin
    if Length(PL)<2 then raise ELE.malformed('WHEN');

    result := eval(PL[1]);

    if not tpNIL(result)
    then
        begin
            FreeAndNil(result);
            result := op_block(PL, 2)
        end;
end;



function TEvaluationFlow.op_while                       (PL: TValues): TValue;
var cond, V: TValue;
begin
    if Length(PL)<2 then raise ELE.malformed('WHILE');

    try
        result := TVList.Create(nil);
        V := nil;
        cond := nil;
        cond := eval_without_var_definitions(PL[1]);
        while not tpNIL(cond) do
            begin
                V := oph_loop_body(PL, 2);

                if tpBreak(V) then BREAK;
                if tpReturn(V) then
                    begin
                        result := V.Copy; //TODO: излишнее копирование
                        BREAK;
                    end;
            FreeAndNil(V);
            FreeAndNil(cond);
            cond := eval_without_var_definitions(PL[1]);
        end;
    finally
        FreeAndNil(cond);
        FreeAndNil(V);
    end;
end;


function TEvaluationFlow.op_with(PL: TValues; export_symbols: boolean): TValue;
var i: integer; name: unicodestring;
begin
    if (Length(PL)<2) then raise ELE.Malformed('WITH/USE');
    for i := 1 to high(PL) do
        if not (tpName(PL[i]) or tpString(PL[i]))
        then raise ELE.InvalidParameters;

    result := nil;

    for i := 1 to high(PL) do
        if vpStringPath(PL[i])
        then
            lisya_packages.AddPath(S_(PL[i]))

        else if tpString(PL[i])
        then
            oph_execute_file(DirSep(S_(PL[i])))

        else if vpSymbolQualified(PL[i])
        then
            begin
                if export_symbols
                then
                    begin
                        name := (PL[i] as TVSymbol).name;
                        stack.new_ref(
                            symbol_uname(N_(PL[i]))[Pos(':',name)+1..Length(name)],
                            stack.get_ref(N_(PL[i])))
    				end
	    		else
                    raise ELE.Create('invalid package name '+(PL[i] as TVSymbol).name, 'syntax')
            end

        else if tpName(PL[i])
        then
            oph_bind_package((PL[i] as TVSymbol).name, export_symbols);

    result := CreateTRUE;
end;


procedure TEvaluationFlow.fill_function_stack(sp: TVFunction; symbols: TIntegers);
var i: integer; P: PVariable;
begin
    for i := 0 to high(symbols) do begin
        P := stack.look_var(symbols[i]);
        if P.constant
        then sp.stack.new_ref(symbols[i], RefVariable(P))
        else sp.stack.new_var(symbols[i], P.V.Copy, true);
    end;
end;


procedure TEvaluationFlow.fill_procedure_stack(sp: TVRoutine; symbols: TIntegers);
var i: integer; P: PVariable;
begin
    for i := 0 to high(symbols) do begin
        P := stack.look_var_or_nil(symbols[i]);
        if P<>nil then sp.stack.new_ref(symbols[i], RefVariable(P));
    end;
end;


function TEvaluationFlow.call(expr: TValues): TValue;
var head: TValue; i: integer; params: TValues;
begin
    result := nil;
    head := expr[0];

    if head is TVInternalUnaryRoutine
    then
        result := call_internal_unary(expr)

    else if head is TVRoutine
    then
        begin
            params := Copy(expr);
            for i := 1 to high(params) do params[i] := params[i].Copy;
            result := call_routine(params)
		end

    else if head is TVInternalArrayRoutine
    then
        result := call_internal_array(expr)

    else if head is TVInternalRoutine
    then
        result := call_internal(expr)

    else if head is TVPredicate
    then
        result := call_predicate(expr)

    else
        raise ELE.InvalidParameters('call for '+head.AsString);
end;


function TEvaluationFlow.callv(PL: TValue): TValue;
begin
    result := call((PL as TVList).ValueList);
end;


function TEvaluationFlow.call_routine(PL: TValues): TValue;
var proc: TVRoutine; tmp_stack, proc_stack: TVSymbolStack;
    params, rest: TValues; i, last: integer; rest_L: TVList; msg: unicodestring;
    pure: boolean;
begin
    try
        tmp_stack := stack;
        proc_stack := nil;
        proc := PL[0] as TVRoutine;

        proc_stack := proc.stack.Copy as TVSymbolStack;

        ifh_bind_params(proc.sign, PL, params, rest);

        last := high(proc.sign);

        if proc.sign[0].mode=spmRest
        then
            begin
                if proc.rest=nil
                then
                    rest_L := TVList.Create()
                else
                    rest_L := proc.rest.Copy() as TVList;

                for i := 0 to high(rest) do rest_L.Add(rest[i]);

                  proc_stack.new_var(proc.sign[last].name, rest_L, true);
                  Dec(last);
            end;

        for i := 1 to last do
            if params[i-1]=nil
            then
                proc_stack.new_var(proc.sign[i].name, TVList.Create, true)
            else
                proc_stack.new_var(proc.sign[i].name, params[i-1], true);

        stack := proc_stack;

        try
            pure := pure_mode;
            if (proc is TVFunction) then pure_mode := true;
            result := op_block(proc.body.ValueList, 0);
        finally
            pure_mode := pure;
        end;

        if result is TVGo
        then
            begin
                if result is TVReturn
                then
                    Exit((result as TVReturn).Unpack())
                else if result is TVGoto
                then
                    begin
                        msg := 'Label '+symbol_uname((result as TVGoto).n)+' not reachable';
                        FreeAndNil(result);
                        raise ELE.Create(msg,'program/structure');
                    end;
                FreeAndNil(result);
                raise ELE.Create('unhandled break/continue', 'program/structure');
            end;
    finally
        stack := tmp_stack;
        proc_stack.Free;
    end;
end;


function TEvaluationFlow.call_internal_array(PL: TValues): TValue;
var params, rest: TValues; f: TVInternalArrayRoutine; i: integer; rl: TVList;
begin

    result := nil;
    f := PL[0] as TVInternalArrayRoutine;
    ifh_bind_params(f.sign, PL, params, rest);

    case f.sign[0].mode of

        spmRest:
            try
                params[high(params)] := TVListRest.Create(rest);
                result := f.body(params, call);
	    	finally
                params[high(params)].Free;
    		end;

        spmList:
            try
                //проверяется была ли функция с rest параметром каррирована
                //если да, то последний аргумент преобразуется из TVList в TVListRest;
                //TODO: вместо генерации ListRest, можно отправить ListBody?
                rl := params[high(params)] as TVList;
                params[high(params)] := TVListRest.Create(rl.ValueList);
                result := f.body(params, call);
	    	finally
                params[high(params)].Free;
    		end;

        else result := f.body(params, call);

	end;
end;


function TEvaluationFlow.call_internal(PL: TValues): TValue;
var binded: TVList; f: TVInternalRoutine; params, rest: TValues;
begin
    try
        result := nil;
        binded := nil;
        f := PL[0] as TVInternalRoutine;

        ifh_bind_params(f.sign, PL, params, rest);

        binded := TVList.CreateCopy(params);

        if f.sign[0].mode=spmRest then binded[-1] := TVList.CreateCopy(rest);

        result := f.body(binded, call);
    finally
        FreeAndNil(binded);
    end;
end;


function TEvaluationFlow.call_operator(PL: TValues): TValue;
var frame_start: integer; op: TOperatorEnum;
begin
    result := nil;
    frame_start:= stack.Count;
    op := (PL[0] as TVOperator).op_enum;
    if pure_mode and (op in impure_operators)
    then raise ELE.Create(PL[0].AsString+ ' in pure mode', 'impurity');

    try
        case op of
            oeAND       : result := op_and(PL);
            oeAPPEND    : result := op_append(PL);
            oeASSEMBLE  : result := op_assemble(PL);
            oeBLOCK     : result := op_block(PL);
            oeBREAK     : result := TVBreak.Create;
            oeCASE      : result := op_case(PL);
            oeCOND      : result := op_cond(PL);
            oeCONST     : result := op_const(PL);
            oeCONTINUE  : result := TVContinue.Create;
            oeDEBUG     : result := op_debug(PL);
            oeDEFAULT   : result := op_default(PL);
            oeELT       : result := op_elt(PL);
            oeERROR     : result := op_error(PL);
            oeEXECUTE_FILE: result := op_execute_file(PL);
            oeEXTRACT   : result := op_extract(PL);
            oeFOR       : result := op_for(PL);
            oeFUNCTION  : result := op_function(PL);
            oeGOTO      : result := op_goto(PL);
            oeIF        : result := op_if(PL);
            oeINSERT    : result := op_insert(PL);
            oeKEY       : result := op_key(PL);
            oeLET       : result := op_let(PL);
            oeMACRO     : result := op_procedure(PL);
            oeMACRO_SYMBOL: result := op_macro_symbol(PL);
            oeOR        : result := op_or(PL);
            oePACKAGE   : result := op_package(PL);
            oePROCEDURE : result := op_procedure(PL);
            oePUSH      : result := op_push(PL);
            oeQUOTE     : result := PL[1].Copy;
            //TODO: QUOTE не проверяет количество аргументов
            oeRETURN    : result := op_return(PL);
            oeSET       : result := op_set(PL);
            oeTRY       : result := op_try(PL);
            oeUSE       : result := op_with(PL, true);
            oeVAR       : result := op_var(PL);
            oeWHEN      : result := op_when(PL);
            oeWHILE     : result := op_while(PL);
            oeWITH      : result := op_with(PL, false);
            else raise ELE.Create('неизвестный оператор');
        end;
    finally
        if not (op in variable_defining_operators) then stack.clear_frame(frame_start);
    end;
end;


function TEvaluationFlow.call_predicate(PL: TValues): TValue;
var res: boolean; P: TVPredicate; A: TValue;
begin
    result := nil;
    if Length(PL)<>2 then raise ELE.Create('', 'invalid parameters/predicate/');

    P := PL[0] as TVPredicate;
    A := PL[1];
    res := P.body(A);

    if not P.fAssert    then bool_to_TV(res, result)

    else if res         then result := A.Copy

    else raise ELE.Create(A.AsString+' is not '+symbol_uname(P.nN), 'assertion/');
end;


function TEvaluationFlow.call_internal_unary(PL: TValues): TValue;
begin
    result := nil;
    if Length(PL)<>2 then raise ELE.Create('','invalid parameters/unary/');
    result := (PL[0] as TVInternalUnaryRoutine).body(PL[1]);
end;


function TEvaluationFlow.eval_routine(PL: TValues): TValue;
var proc: TVRoutine; params: TValues;
    i, frame_start: integer;
begin
    if stack<>nil then frame_start := stack.Count; //это костыль для чистого режима
    proc := PL[0] as TVRoutine;
    SetLength(params, Length(PL));
    FillByte(params[0], SizeOf(TValue)*Length(params), 0);
    params[0] := proc;
    result := nil;

    try
        try
            if proc is TVFunction
            then
                for i := 1 to high(PL) do params[i] := eval(PL[i])

            else if proc is TVProcedure
            then
                begin
                    for i := 1 to min(high(proc.sign), high(PL)) do
                        if proc.sign[i].req_mode=spmVar
                        then
                            params[i] := eval_link(PL[i])
                        else
                            params[i] := eval(PL[i]);

                    for i := min(high(proc.sign), high(PL))+1 to high(PL) do
                        params[i] := eval(PL[i]);
                end

            else if proc is TVMacro
            then
                for i := 1 to high(PL) do params[i] := PL[i].Copy;

		except
            for i := 1 to high(params) do FreeAndNil(params[i]);
            raise;
		end;

        result := call_routine(params);

    finally
        if stack<>nil then stack.clear_frame(frame_start);
    end;
end;


function TEvaluationFlow.eval_parameters(call: TCallProc; PL: TValues): TValue;
var params: TValues; i, frame_start: integer;
begin
    try
        frame_start := stack.Count;
        result := nil;

        SetLength(params, Length(PL));
        params[0] := PL[0];

        for i := 1 to high(params) do params[i] := eval(PL[i]);

        result := call(params);

    finally
        for i := 1 to high(params) do FreeAndNil(params[i]);
        stack.clear_frame(frame_start);
    end;
end;



//{$DEFINE EVAL_DEBUG_PRINT}

{$IFDEF EVAL_DEBUG_PRINT}
var eval_indent: integer=0;
procedure indent;
var i: integer;
begin
    for i := 1 to eval_indent do Write('¦   ');
end;
{$ENDIF}


//{$DEFINE EVALPARAMMOD}

function TEvaluationFlow.eval(V: TValue): TValue;
var PL: TValues;
    head: TValue;
    PV: PVariable;
    type_v: (selfEval, symbol, list);
    estack: unicodestring;
    {$IFDEF EVALPARAMMOD} h: DWORD; {$ENDIF}
begin try
    {$IFDEF EVAL_DEBUG_PRINT}
    indent; WriteLn('eval>> ',V.AsString);
    inc(eval_indent);
    {$ENDIF}

    {$IFDEF EVALPARAMMOD} h := V.hash;{$ENDIF}
    result := nil;

    if tpName(V)
    then type_v := symbol
    else
        if tpListNotEmpty(V)
        then type_v := list
        else
            type_v := selfEval;

    case type_v of

        selfEval: result := V.Copy;

        symbol: try
            PV := nil;
            PV := stack.get_ref(V as TVSymbol);

            if PV.V is TVChainPointer
            then result := (PV.V as TVChainPointer).GetTarget
            else result := PV.V.Copy;

            if result is TVMacroSymbol
            then
                result := eval_and_free(eval_routine(TValues.Create(result)));

        finally
            ReleaseVariable(PV);
        end;

        list: try
            PL := Copy((V as TVList).ValueList);
            head := nil;
            head := eval(PL[0]);
            PL[0] := head;

            if head is TVInternalUnaryRoutine
            then result := eval_parameters(call_internal_unary, PL)
            else

            if head is TVInternalArrayRoutine
            then result := eval_parameters(call_internal_array, PL)
            else

            if head is TVInternalRoutine
            then result := eval_parameters(call_internal, PL)
            else

            if head is TVPredicate
            then result := eval_parameters(call_predicate, PL)
            else

            if head is TVOperator
            then result := call_operator(PL)
			else

            if head is TVMacro
            then result := eval_and_free(eval_routine(PL))
            else

            if head is TVRoutine
            then result := eval_routine(PL)
            else

            raise ELE.Create(head.AsString, 'syntax/not subprogram');
        finally
            head.Free;
        end;

    end;


   if result=nil
   then
        raise ELE.Create('eval без результата', 'internal');
   {$IFDEF EVAL_DEBUG_PRINT}
   Dec(eval_indent);
   indent; writeLn('=  ',result.asString);
   {$ENDIF}
   {$IFDEF EVALPARAMMOD} if h<>V.hash then raise ELE.Create('модификация аргумента');{$ENDIF}
except
    on E:ELisyaError do begin
       E.EStack := V.AsString+LineEnding+'=> '+E.EStack;
       if debug_in_place then  debugger(E, V);
       raise;
    end;
    on E:Exception do begin
        try
            EStack := V.AsString+LineEnding+'=> ';
        except
            EStack := 'XXXX'+LineEnding+'=> ';
        end;
        if debug_in_place then debugger(ELE.Create(E.Message, 'native/'+E.ClassName, EStack), V);
        raise ELE.Create(E.Message, 'native/'+E.ClassName, EStack);
    end;
end;
end;


function TEvaluationFlow.eval_and_free(V: TValue): TValue;
begin
    try
        result := nil;
        result := eval(V);
    finally
        V.Free;
    end;
end;


function TEvaluationFlow.eval_without_var_definitions(V: TValue): TValue;
var frame_start: integer;
begin
    try
        frame_start := stack.Count;
        result := eval(V);
    finally
        stack.clear_frame(frame_start);
    end;
end;


initialization
    system.Randomize;
    create_persistent_internal_functions;
    InitCriticalSection(log_cs);
    InitCriticalSection(console_cs);


finalization
    log_file.Free;
    DoneCriticalSection(log_cs);
    DoneCriticalSection(console_cs);
    destroy_persistent_internal_functions;
end.
//4576 4431 4488 4643 4499 4701 5166 5096 5104 5337 5691 5580 6048 7426 9299


