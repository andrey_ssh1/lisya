unit mar_zemq;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils, ucomplex, mar, mar_math, mar_math_arrays, math;

type
    TZEMQMeasurements = array of record name: unicodestring; U, I: complex; end;

    TArcs = array of record n1, n2: integer; I: complex; end;
    TNodes = array of record U, Y, I: complex; end;
    TFolds = array of record saved, expelled, k: integer; end;

	{ TZEMQ }

    TZEMQ = class
    public
        constructor Create;
        procedure AddZ(_n1, _n2: unicodestring; _Z: complex);
        procedure AddG(_n1, _n2: unicodestring; _Z, _E: complex);
        procedure AddM(_n1, _n2: TStringArray; _M: TComplexes);
        procedure AddQ(_n1, _n2, _name: unicodestring);
        function Measurements(): TZEMQMeasurements;
    private
        nodes_catalog: array of
            record
                name: unicodestring;
			end;
        breakers_catalog: array of
            record
                name: unicodestring;
                a: integer;
                n1, n2: integer;
            end;

        nodes: TNodes;
        arcs: TArcs;

        generators: array of
            record
                a: integer;
                Z: complex;
                E: complex;
			end;
        resistors: array of
            record
                a: integer;
                Z: complex;
            end;
        mutuals: array of
            record
                Y: TComplexes;
                U: TComplexes;
                I: TComplexes;
                a: TIntegers;
            end;

        function add_node(_name: unicodestring): integer;

        procedure nodes_Y();
        procedure mutuals_Y();
        procedure add_nodes_Y(arc: integer; Z: complex);
        procedure fold_breakers();
        procedure unfold_breakers();
        procedure calc();
	end;


    EZEMQException = class (Exception) end;



implementation


type
    EZEMQ = EZEMQException;

const leak_Y = 0.000001;


function breakers_fold(var arcs: TArcs; tail: integer): TFolds;
var q, a, expelled, saved, polarity: integer;

    procedure add(_s, _e, _k: integer);
    var f: integer;
    begin
        f := Length(result);
        SetLength(result, f+1);
        result[f].saved := _s;    //сохраняемый узел/ветвь
        result[f].expelled := _e; //сворачиваемый узел/ветвь
        result[f].k := _k;        //коэффициент учёта тока (полярность)
	end;

begin
    result := nil;

    //пробег по концу списка ветвей, состоящему из выключателей
    for q := tail to high(arcs) do
        begin
            arcs[q].I := 0; //токи свёрнутых ыключателей равны нулю, поскольку оба конца на одном узле

            if arcs[q].n2 = 0 //избегает исключения нулевого узла из схемы
            then
                begin
                    expelled := arcs[q].n1;
                    saved := arcs[q].n2;
                    arcs[q].n1 := saved;
                    polarity := -1
				end
            else
                begin
                    expelled := arcs[q].n2;
                    saved := arcs[q].n1;
                    arcs[q].n2 := saved;
                    polarity := +1;
				end;

            add(saved, expelled, 0); //напряжения в узлах прилегающих к выключателю равны

            for a := 0 to high(arcs) do
                begin
                    if arcs[a].n1 = expelled
                    then
                        begin
                            arcs[a].n1 := saved;
                            add(a, q, +polarity);
						end;
                    if arcs[a].n2 = expelled
                    then
                        begin
                            arcs[a].n2 := saved;
                            add(a, q, -polarity)
						end;
				end;
		end;
end;


procedure breakers_unfold(var arcs: TArcs; var nodes: TNodes; folds: TFolds);
var f: integer;
begin
    for f := high(folds) downto 0 do
        with folds[f] do
            if k=0
            then nodes[expelled].U := nodes[saved].U
            else arcs[expelled].I := arcs[expelled].I + arcs[saved].I * k;
end;



{ TZEMQ }

//// Интерфейс /////////////////////////////////////////////////////////////////

constructor TZEMQ.Create;
begin
    breakers_catalog := nil;

    generators := nil;
    resistors := nil;
    mutuals := nil;

    SetLength(nodes_catalog, 1);
    SetLength(nodes, 1);
    nodes_catalog[0].name := '0';
    nodes[0].U := 0;
    arcs := nil;
end;


procedure TZEMQ.AddZ(_n1, _n2: unicodestring; _Z: complex);
begin
    //!!!
    SetLength(resistors, Length(resistors)+1);
    SetLength(arcs, Length(arcs)+1);
    with resistors[high(resistors)] do
        with arcs[high(arcs)] do
            begin
                a := high(arcs);
                n1 := add_node(_n1);
                n2 := add_node(_n2);
                Z := _Z;
                I := 0;
                add_nodes_Y(a, Z);
			end;
end;


procedure TZEMQ.AddG(_n1, _n2: unicodestring; _Z, _E: complex);
begin
    //!!!
    SetLength(generators, Length(generators)+1);
    SetLength(arcs, Length(arcs)+1);
    with generators[high(generators)] do
        with arcs[high(arcs)] do
            begin
                a := high(arcs);
                n1 := add_node(_n1);
                n2 := add_node(_n2);
                I := 0;
                E := _E;
                Z := _Z;
                add_nodes_Y(a, Z/1.618);
	    	end;
end;


procedure TZEMQ.AddM(_n1, _n2: TStringArray; _M: TComplexes);
var n, j,  w: integer; Z: complex;
begin
    w := Length(_n1);
    assert(w=Length(_n2), 'ZEMQ/induction group/nonconformant input and output nodes');
    assert(Length(_M)=w*w, 'ZEMQ/induction group/matrix nonconformant with nodes');


    SetLength(mutuals, Length(mutuals)+1);
    with mutuals[high(mutuals)] do
        begin
            SetLength(U, w);
            SetLength(I, w);
            SetLength(a, w);
            Y := matrix_inverse(_M);

            for n := 0 to w-1 do
                begin
                    a[n] := Length(arcs);
                    SetLength(arcs, Length(arcs)+1);
                    with arcs[a[n]] do
                        begin
                            I := 0;
                            n1 := add_node(_n1[n]);
                            n2 := add_node(_n2[n]);
						end;
				end;

            //вычисляется сопротивление рассеяния для каждой ветви и добавляется
            //в узловые проводимости
            for n := 0 to w-1 do
                begin
                    //вычисляются максимальные проводимости отдельных ветвей
                    //путём подачи единичного напряжения на одну ветвь
                    //результирующий ток ветви равен проводимости этой ветви
                    for j := 0 to w-1 do U[j] := 0;
                    U[n] := 1;
                    matrix_cross(Y, U, I);
                    //WriteLn(cexpstr(I[n]*1000));
                    add_nodes_Y(a[n], 1/(1.618*I[n]));
				end;
		end;
end;


procedure TZEMQ.AddQ(_n1, _n2, _name: unicodestring);
begin
    //!!!
    //выключатели добавляются только в справочник выключателей
    //в конец списка ветвей, они добавляются перед расчётом
    //это позволяет исключить выключатели из расчёта ограничив проход по ветвям
  //  SetLength(breakers, Length(breakers)+1);
  //  with breakers[high(breakers)] do
  //      begin
  //          n1 := add_node(_n1);
  //          n2 := add_node(_n2);
  //          name := _name;
  //          a := -1;
		//end;
end;


function TZEMQ.Measurements: TZEMQMeasurements;
var q: integer;
begin
    //!!!
    calc();



    SetLength(result, length(breakers_catalog));
    for q := 0 to high(breakers_catalog) do
        begin
            result[q].name := breakers_catalog[q].name;
            result[q].I := arcs[breakers_catalog[q].a].I;
            result[q].U := nodes[arcs[breakers_catalog[q].a].n1].U;
		end;

    SetLength(result, length(arcs));
    for q := 0 to high(arcs) do
        with result[q] do
            begin
                name := nodes_catalog[arcs[q].n1].name+'-'+nodes_catalog[arcs[q].n2].name;
                I := arcs[q].I;
                U := nodes[arcs[q].n1].U;
			end;

end;


//// Построение ////////////////////////////////////////////////////////////////

function TZEMQ.add_node(_name: unicodestring): integer;
var uname: unicodestring;
begin
    if _name='0' then Exit(0);

    uname := UnicodeUpperCase(_name);
    for result := high(nodes_catalog) downto 1 do
        if nodes_catalog[result].name = uname then Exit;

    SetLength(nodes_catalog, Length(nodes_catalog)+1);
    SetLength(nodes, Length(nodes_catalog));
    result := high(nodes);

    nodes_catalog[result].name := uname;
    nodes[result].U := 0;
    nodes[result].Y := leak_Y;
end;



//// Вычисления ////////////////////////////////////////////////////////////////

procedure TZEMQ.nodes_Y;
var a, n: integer;
begin
    //!!!
    //вычисление узловых проводимостей
  //  for n := 0 to high(nodes) do
  //      begin
  //          nodes[n].Y := leak_Y;
  //          nodes[n].I := 0;
		//end;
  //
  //  for a := 0 to high(arc_names) do
  //      begin
  //          nodes[arcs[a].n1].Y := nodes[arcs[a].n1].Y + cinv(arcs[a].Z);
  //          nodes[arcs[a].n2].Y := nodes[arcs[a].n2].Y + cinv(arcs[a].Z);
		//end;
end;


procedure TZEMQ.mutuals_Y;
var m: integer;
begin
    //!!!
    //проводимость условного узла, содержащего напряжение взаимоиндукции
    //for m := 0 to high(mutuals) do
    //    with mutuals[m] do
    //        Y := 1/M + 1/arcs[a1].Z + (K**2)/arcs[a2].Z;
end;


procedure TZEMQ.add_nodes_Y(arc: integer; Z: complex);
begin
    nodes[arcs[arc].n1].Y := nodes[arcs[arc].n1].Y + 1/Z;
    nodes[arcs[arc].n2].Y := nodes[arcs[arc].n2].Y + 1/Z;
end;


procedure TZEMQ.fold_breakers;
var a, q, i, expelled, saved, polarity: integer;

 //   procedure add(s, e: integer; k: complex);
 //   var f: integer;
 //   begin
 //       f := Length(folds);
 //       SetLength(folds, f+1);
 //       folds[f].saved := s;    //сохраняемый узел/ветвь
 //       folds[f].expelled := e; //сворачиваемый узел/ветвь
 //       folds[f].k := k;        //коэффициент учёта тока (полярность и Кт)
	//end;

begin
    //!!!
  //  if folds<>nil then raise EZEMQ.Create('ZEMQ/internal/repeated breakers folding');
  //
  //  //добавление выключателей в конец списка ветвей
  //  q := Length(arc_names);
  //  SetLength(arcs, Length(arc_names)+Length(breakers));
  //  for i := q to high(arcs) do
  //      begin
  //          arcs[i].n1 := breakers[i-q].n1;
  //          arcs[i].n2 := breakers[i-q].n2;
  //          arcs[i].U := 0;
  //          arcs[i].I := 0;
  //          arcs[i].Z := 0;
  //          breakers[i-q].a := i;
		//end;
  //
  //  //пробег по концу списка ветвей, состоящему из выключателей
  //  for q := Length(arc_names) to high(arcs) do
  //      begin
  //          if arcs[q].n2 = 0 //избегает исключения нулевого узла из схемы
  //          then
  //              begin
  //                  expelled := arcs[q].n1;
  //                  saved := arcs[q].n2;
  //                  arcs[q].n1 := saved;
  //                  polarity := -1
		//		end
  //          else
  //              begin
  //                  expelled := arcs[q].n2;
  //                  saved := arcs[q].n1;
  //                  arcs[q].n2 := saved;
  //                  polarity := +1;
		//		end;
  //
  //          add(saved, expelled, 0);
  //          for a := 0 to high(arcs) do
  //              begin
  //                  if arcs[a].n1 = expelled
  //                  then
  //                      begin
  //                          arcs[a].n1 := saved;
  //                          add(a, q, +polarity);
		//				end;
  //                  if arcs[a].n2 = expelled
  //                  then
  //                      begin
  //                          arcs[a].n2 := saved;
  //                          add(a, q, -polarity)
		//				end;
		//		end;
		//end;
end;


procedure TZEMQ.unfold_breakers;
var f, q: integer;
begin
    //!!!
    //for q := Length(arc_names) to high(arcs) do arcs[q].I := 0;
    //
    //for f := high(folds) downto 0 do
    //    with folds[f] do
    //        if k=0
    //        then nodes[expelled].U := nodes[saved].U
    //        else arcs[expelled].I := arcs[expelled].I + arcs[saved].I * k;
    //
    //folds := nil;
end;


procedure TZEMQ.calc;
var a, g, z, n, m, j, lim: integer; imbalance, treshold: real;
    folds: TFolds;
begin
    //!!!

    folds := breakers_fold(arcs, Length(arcs) - Length(breakers_catalog));

    lim := -1;

    repeat   //WriteLn(lim,'>');

        for g := 0 to high(generators) do
            with arcs[generators[g].a] do
                begin
                    I := (nodes[n1].U - nodes[n2].U + generators[g].E) / generators[g].Z;
                    nodes[n1].I := nodes[n1].I - I;
                    nodes[n2].I := nodes[n2].I + I;
                    //writeln('g=', g , ' I=', cexpstr(I), '  [', n1, '-', n2,']');
				end;

        for z := 0 to high(resistors) do
            with arcs[resistors[z].a] do
                begin
                    I := (nodes[n1].U - nodes[n2].U) / resistors[z].Z;
                    nodes[n1].I := nodes[n1].I - I;
                    nodes[n2].I := nodes[n2].I + I;
                    //writeln('z=', z , ' I=', cexpstr(I), '  n1=', n1, '  n2=', n2);
				end;

        for m := 0 to high(mutuals) do
            with mutuals[m] do
                begin
                    for j := 0 to high(a) do
                        U[j] := nodes[arcs[a[j]].n1].U - nodes[arcs[a[j]].n2].U;
                    matrix_cross(Y, U, I);
                    for j := 0 to high(a) do
                        begin
                            arcs[a[j]].I := I[j]; //лишняя операция до развёртывания выключателей
                            //writeln('m=', m ,'  a=',a[j], ' I=', cexpstr(I[j]), '  U=', cexpstr(U[j]), '  [', arcs[a[j]].n1, '-', arcs[a[j]].n2,']');
                            nodes[arcs[a[j]].n1].I := nodes[arcs[a[j]].n1].I - I[j];
                            nodes[arcs[a[j]].n2].I := nodes[arcs[a[j]].n2].I + I[j];
						end;
				end;

        imbalance := 0;

        for n := 1 to high(nodes) do
            with nodes[n] do
                begin
                    I := I - U * leak_Y; //утечка для привязки к земле неподключенных участков схемы
                    //WriteLn('U', n, '=', cexpstr(U), '  I=', cexpstr(I));
                    imbalance := max(imbalance, cmod(I/Y));
                    U := U + I/Y;
                    I := 0;
    		    end;

        //WriteLn('U', 0, '=', cexpstr(nodes[0].U), '  I=', cexpstr(nodes[0].I));
        nodes[0].I := 0;
 //
        dec(lim);
        if lim=0 then break;
 //
 //
	until imbalance<1;
 //
 //   //WriteLn(lim);
 //
 //
 //   unfold_breakers();
    breakers_unfold(arcs, nodes, folds);
end;

end.

