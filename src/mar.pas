﻿unit mar;

{$mode delphi}

interface

uses
    Classes, SysUtils, Math, ucomplex;

type
    TStringArray = array of unicodestring;
    TIntegers = array of integer;
    TIntegers2 = array of TIntegers;
    TIntegers64 = array of Int64;
    TDoubles = array of double;
    TPointers = array of Pointer;

function PosU(const ss, s: unicodestring; offset: integer = 1): integer;
function SplitString(S: unicodestring; separator: unicodestring = ' '): TStringArray;
procedure RemoveStrings(var sa: TStringArray; remove_value: unicodestring = '');
function FindInStringArray(const sa: TStringArray; find_value: unicodestring): integer;
function StringSubstitute(const src, a, b: unicodestring): unicodestring;
function end_of_string_is(s, se: unicodestring): boolean; inline;
function EndsWith(s, se: unicodestring): boolean; inline;
function StartsWith(s, sb: unicodestring): boolean; inline;

function PointerToStr(ptr: Pointer; digits: integer = -1): unicodestring;
function PointerToQWORD(ptr: Pointer): QWORD;
function DirSep(s: unicodestring): unicodestring;

function concat_integers(i1, i2: TIntegers): TIntegers;
procedure append_integers(var i1: TIntegers; i2: TIntegers); inline;
procedure append_integer(var i1: TIntegers; i: integer); inline;

function integers_div(i1: TIntegers; d: integer): TIntegers;
function integers_mul(i1: TIntegers; d: integer): TIntegers;
function pop_integer(var i: TIntegers): integer;
function integers_extract(var i1: TIntegers; n: integer): integer;
function integers_product(i: TIntegers): integer;
function integers_equal(a, b: TIntegers): boolean;
procedure append_string_array(var ss: TStringArray; s:unicodestring); inline; overload;
procedure append_string_array(var ss: TStringArray; s:TStringArray); inline; overload;

function crc8 (const data: TBytes; seed: byte): byte; overload;
function crc8 (const data: byte; seed: byte): byte; overload;

function cexpstr(x: complex): unicodestring;


type

    { TCountingObject }

    TCountingObject = class
    private
        ref_count: integer;
        CS: TRTLCriticalSection;
        res_cs: TRTLCriticalSection;
    public
        constructor Create;
        function Ref: TCountingObject;
        function Release: boolean;
        function {%H-}Description: unicodestring; virtual; abstract;
        property refs: integer read ref_count;
        procedure Lock; virtual;
        procedure Unlock; virtual;
    end;


type

	{ TPointersHashTable }

    TPointersHashTable = class
    private
        pointers: array of Pointer;
        keys: array of integer;
        sparsity: integer;
        function key(P: Pointer): integer;
    public
        constructor Create(count: integer = 1; _sparsity: integer = 8);
        destructor Destroy; override;
        procedure SetPointer(P: Pointer; n: integer);
        function GetIndex(P: Pointer): integer;
        procedure Expand;
        function AddPointer(P: Pointer): boolean;
        function Contains(P: Pointer): boolean;
	end;



implementation

function PosU(const ss, s: unicodestring; offset: integer = 1): integer;
var i, j: integer;
begin
    for i:=offset to length(s)-length(ss)+1 do begin
        result := i;
        for j := 1 to length(ss) do
            if ss[j]<>s[i+j-1] then begin
                result := 0;
                break;
            end;
        if result=i then exit;
    end;
    result := 0;
    //TODO: PosU возможно нужно переделать на CompareByte аналогично PosEx
end;


function SplitString(S: unicodestring; separator: unicodestring = ' '): TStringArray;
var p1, p2: integer;
begin
    SetLength(result, 0);
    p1 := 1;
    p2 := PosU(separator, S);
    while p2>0 do begin
        SetLength(result, Length(result)+1);
        result[high(result)] := S[p1..p2-1];
        p1 := p2+Length(separator);
        p2 := PosU(separator, S, p1);
    end;

    SetLength(result, Length(result)+1);
    if p1<=Length(s) then result[high(result)] := S[p1..Length(s)];
end;


procedure RemoveStrings(var sa: TStringArray; remove_value: unicodestring);
var i,j: integer; filtered: TStringArray;
begin
    SetLength(filtered, Length(sa));
    j  := 0;
    for i := 0 to high(sa) do
        if sa[i]<>remove_value
        then
            begin
                filtered[j] := sa[i];
                Inc(j);
			end;
	SetLength(filtered, j);
    sa := filtered;
end;


function FindInStringArray(const sa: TStringArray; find_value: unicodestring
			): integer;
begin
    for result := 0 to high(sa) do
        if sa[result]=find_value then Exit;
    result := -1;
end;


function StringSubstitute(const src, a, b: unicodestring): unicodestring;
var p1, p2: integer;
begin
    result := '';
    p1 := 1;
    p2 := PosU(a, src);
    while p2>0 do begin
        result := result + src[p1..p2-1] + b;
        p1 := p2 + Length(a);
        p2 := PosU(a,src, p1);
    end;
    if p1<=Length(src) then result := result + src[p1..Length(src)];
end;


function end_of_string_is(s, se: unicodestring): boolean;
begin
    result := (Length(s)>=Length(se))
        and (s[Length(s)-Length(se)+1..Length(s)]=se)
end;


function EndsWith(s, se: unicodestring): boolean;
begin
    result := (Length(s)>=Length(se))
        and (s[Length(s)-Length(se)+1..Length(s)]=se);
end;


function StartsWith(s, sb: unicodestring): boolean;
begin
    result := (Length(s)>=Length(sb))
        and (s[1..Length(sb)]=sb);
end;


function PointerToStr(ptr: Pointer; digits: integer): unicodestring;
var d: record case byte of 0: (p: pointer); 1: (b: array [1..SizeOf(ptr)] of Byte); end;
    i: integer;
begin
    d.p:=ptr;
    result := '';
    for i := low(d.b) to high(d.b) do begin
        result := IntToHex(d.b[i],2)+result;
        if i=4 then result := '_'+result;
    end;

    if (digits>0) and (digits<length(result))
    then result := result[Length(result)-digits+1..Length(result)];
end;


function PointerToQWORD(ptr: Pointer): QWORD;
var d: record case byte of 0: (p: pointer); 1: (b: array [0..SizeOf(ptr)-1] of Byte); end;
    i: integer;
begin
    result := 0;
    d.p := ptr;
    for i := 0 to high(d.b) do
        result := result +
            d.b[i]*(256**i);  //TODO: тормозит при освобождении памяти
end;


function DirSep(s: unicodestring): unicodestring;
var buf: rawbytestring;
begin
    buf := {%H-}s;
    DoDirSeparators(buf);
    result := buf;
end;

function concat_integers(i1, i2: TIntegers): TIntegers;
var i: integer;
begin
    result := nil;
    SetLength(result, Length(i1)+Length(i2));
    for i := 0 to high(i1) do result[i] := i1[i];
    for i := 0 to high(i2) do result[i+Length(i1)] := i2[i];
end;


procedure append_integers(var i1: TIntegers; i2: TIntegers);
var i, b: integer;
begin
    b := Length(i1);
    SetLength(i1, Length(i1)+Length(i2));
    for i:=0 to high(i2) do i1[b+i] := i2[i];
end;


procedure append_integer(var i1: TIntegers; i: integer);
begin
    SetLength(i1, Length(i1)+1);
    i1[high(i1)] := i;
end;


function count_integers(const ii: TIntegers; i: integer): integer;
var n: integer;
begin
    result := 0;
    for n := 0 to high(ii) do if ii[n]=i then Inc(result);
end;


function integers_div(i1: TIntegers; d: integer): TIntegers;
var i: integer;
begin
    SetLength(result, Length(i1));
    for i := 0 to high(i1) do result[i] := i1[i] div d;
end;


function integers_mul(i1: TIntegers; d: integer): TIntegers;
var i: integer;
begin
    SetLength(result, Length(i1));
    for i := 0 to high(i1) do result[i] := i1[i] * d;
end;


function pop_integer(var i: TIntegers): integer;
begin
    result := i[high(i)];
    SetLength(i, Length(i)-1);
end;


function integers_extract(var i1: TIntegers; n: integer): integer;
var i: integer;
begin
    result := i1[n];
    for i := n to high(i1)-1 do i1[i] := i1[i+1];
    SetLength(i1, Length(i1)-1);
end;

function integers_product(i: TIntegers): integer;
var n: integer;
begin
    result := 1;
    for n := 0 to high(i) do result := result * i[n];
end;


function integers_equal(a, b: TIntegers): boolean;
var i: integer;
begin
    if a=b then Exit(true);
    if Length(a)<>Length(b) then Exit(false);
    for i := 0 to high(a) do if a[i]<>b[i] then Exit(false);
    result := true;
end;


procedure append_string_array(var ss: TStringArray; s: unicodestring);
begin
    SetLength(ss, length(ss)+1);
    ss[high(ss)] := s;
end;

procedure append_string_array(var ss: TStringArray; s: TStringArray);
var i, l: integer;
begin
    l := Length(ss);
    SetLength(ss, length(ss)+length(s));
    for i := 0 to high(s) do ss[l+i] := s[i];
end;


function crc8 (const data: byte; seed: byte): byte;
var s_data: byte; bit: integer;
begin
    result := seed;
    s_data := data;
    for bit := 1 to 8 do begin
        if ((result xor s_data) and $01) = 0
        then result := result shr 1
        else begin
            result := result xor $18;
            result := result shr 1;
            result := result or $80;
        end;
        s_data := s_data shr 1;
    end;
end;


function crc8(const data: TBytes; seed: byte): byte;
var i: integer;
begin
    result := seed;
    for i := 0 to high(data) do result := crc8(data[i], result);
end;



function cexpstr(x: complex): unicodestring;
begin
    result := IntToStr(round(cmod(x)))+'∠'+IntToStr(round(carg(x)*180/pi))+'°';
end;


////////////////////////////////////////////////////////////////////////////////
///  Графоанализатор ///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

type
    TiNode = record local_refs, refs:integer; links: TIntegers; end;
    TiGraph = array of TiNode;

procedure print_iGraph(g: TiGraph);
var n,l: integer;
begin
    for n := 0 to high(g) do begin
        WriteLn(n,' [',g[n].local_refs,' / ',g[n].refs);
        for l := 0 to high(g[n].links) do WriteLn('    ', g[n].links[l]);
    end;
end;


procedure count_local_refs(var g: TiGraph);
var i,j: integer;
begin
    for i := 0 to high(g) do begin
        g[i].local_refs := 0;
        for j := 0 to high(g) do
            Inc(g[i].local_refs, count_integers(g[j].links, i));
    end;
end;



{ TPointersHashTable }

function TPointersHashTable.key(P: Pointer): integer;
var d: record case byte of
    0: (p: pointer);
    1: (i: {$IFDEF CPU64}QWORD{$ENDIF}{$IFDEF CPU32}DWORD{$ENDIF}); end;
begin
    d.p := P;
    result := (d.i div 32) mod length(keys);
end;


constructor TPointersHashTable.Create(count: integer; _sparsity: integer);
var i: integer;
begin
    sparsity := _sparsity;
    SetLength(pointers, count);
    SetLength(keys, (count+1) * sparsity);
    for i := 0 to high(keys) do keys[i] := -1;
end;


destructor TPointersHashTable.Destroy;
begin
    SetLength(pointers, 0);
    SetLength(keys, 0);
    inherited Destroy;
end;


procedure TPointersHashTable.SetPointer(P: Pointer; n: integer);
var k: integer;
begin
    pointers[n] := P;
    k := key(P);
    while keys[k]>=0 do
        k := (k+1) mod length(keys);
    keys[k] := n;
end;


function TPointersHashTable.GetIndex(P: Pointer): integer;
var k: integer;
begin
    k := key(P);
    while keys[k]>=0 do
        begin
            if pointers[keys[k]] = P then Exit(keys[k]);
            k := (k+1) mod length(keys);
		end;
    result := -1;
end;


procedure TPointersHashTable.Expand;
var p, k: integer;
begin
    SetLength(keys, length(pointers) * 2 * sparsity);

    for k := 0 to high(keys) do
        keys[k] := -1;

    for p := 0 to high(pointers) do
        SetPointer(pointers[p], p);
end;


function TPointersHashTable.AddPointer(P: Pointer): boolean;
begin
    if GetIndex(P)>=0 then Exit(false);

    if length(keys) div (length(pointers)+1) < sparsity then Expand;
    SetLength(pointers, Length(pointers)+1);
    SetPointer(P, high(pointers));
    result := true;
end;


function TPointersHashTable.Contains(P: Pointer): boolean;
begin
    result := GetIndex(P)>=0;
end;





    { TCountingObject }

constructor TCountingObject.Create;
begin
    ref_count := 1;
    InitCriticalSection(cs);
    InitCriticalSection(res_cs);
end;


function TCountingObject.Ref: TCountingObject;
begin
    EnterCriticalSection(cs);
    Inc(ref_count);
    LeaveCriticalSection(cs);
    result := self;
end;


function TCountingObject.Release: boolean;
begin
    EnterCriticalSection(cs);
    Dec(ref_count);
    result := ref_count=0;
    LeaveCriticalSection(cs);

    if result then begin
        DoneCriticalSection(cs);
        DoneCriticalSection(res_cs);
        self.Free;
    end;
end;


procedure TCountingObject.Lock;
begin
    EnterCriticalSection(res_cs);
end;

procedure TCountingObject.Unlock;
begin
    LeaveCriticalSection(res_cs);
end;


end.  //195

