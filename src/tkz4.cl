//комплексное умножение
float2 cmul(float2 a, float2 b)
{
	return (float2) ((a.x*b.x) - (a.y*b.y),
			         (a.x*b.y) + (a.y*b.x));
}


float2 csqr(float2 a)
{
    //if ((a.x == 1) && (a.y == 0)) return (float2) (1.0f, 0.0f);
    return (float2) (a.x * a.x - a.y * a.y,
                       2 * a.x * a.y);
}

//комплексное деление
float2 cdiv(float2 a, float2 b)
{
	if (fabs(b.x) > fabs(b.y))
	{
		float tmp = b.y / b.x;
		float denom = b.x + b.y*tmp;
		return (float2) ((a.x + a.y*tmp) / denom,
				         (a.y - a.x*tmp) / denom);
	}
	else
	{
		float tmp = b.x / b.y;
		float denom = b.y + b.x*tmp;
		return (float2) (( a.y + a.x*tmp) / denom,
				         (-a.x + a.y*tmp) / denom);
	}
}


float2 cdiv_s(float2 a, float2 b)
{
    float div = b.x*b.x+b.y*b.y;
    return (float2)(native_divide(( a.x*b.x+a.y*b.y) ,div),
                    native_divide((-a.x*b.y+a.y*b.x) ,div));
}


struct node
{
	float2 U; //float2 представляет комплексное число
	float2 I;
	float2 Y;
};  //24 B


struct arc
{
	float2 Yl;
	float2 Yc;
	float2 K;
	float2 E;
	float2 I;
	float2 I_[2];
	int node[2];
};  //64 B


struct arc_const
{
    float2 Yl;
	float2 Yc;
	float2 K;
	float2 E;
    int node[2];
};  //40 B


struct arc_var
{
    float2 I_[2];
};


struct arc_var arcCurrent(struct arc_var av, struct arc ac, float2 u1, float2 u2)
{
    float2 K = ac.K;
    float2 u2_ = cmul(u2, K) - ac.E;
    float2 du = u1 - u2_;
    float2 Yc = 0.5f*(ac.Yc);
    float2 I = cmul(du, ac.Yl);
    float2 I_b = I + cmul(u1, Yc);
    float2 I_e = cmul(K, cmul(u2, Yc) - I);
    av.I_[0] = I_b;
    av.I_[1] = I_e;
    return av;
};


struct node nodeVoltage(struct node n)
{
    n.U += cdiv_s(n.I, n.Y);
    //n.I = 0;
    //n.Y = 0;
    return n;
}


__kernel void tkz (const int nodesNumber,  const int arcsNumber, const int tasksNumber,
                   __global struct node* pNode,   __global struct arc* pArc,
                   __constant struct arc * Ac
                   )
{
    __local struct node     N[MAX_NODES*WORK_GROUP_SIZE];
    __local struct arc_var Av[MAX_ARCS*WORK_GROUP_SIZE];
    __local unsigned int balanced[WORK_GROUP_SIZE];

    const int global_task_id = get_global_id(0); //номер задачи
    const int local_task_id = get_local_id(0);
    const int bee = get_global_id(1);

    int i;

    const int global_baseNode = nodesNumber * global_task_id;
    const int global_baseArc  =  arcsNumber * global_task_id;

    const int baseNode = nodesNumber * local_task_id;
    const int baseArc  =  arcsNumber * local_task_id;

    const int n = baseNode + bee;
    const int a = baseArc + bee;


    if (bee < nodesNumber)
    {
        N[n] = pNode[global_baseNode+bee];
        N[n].I = 0;
        N[n].Y = 0;
    }

    //первая задача копирует описания ветвей в локальную память
    //if ((bee < arcsNumber) && (local_task_id == 0))
    //{
    //    Ac[bee] = pArc[global_baseArc+bee];
    //}

    //__private const struct arc Ap = Ac[bee];


    barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////////

	for (i=1; i<1000000; i++)
	{

        if (bee < arcsNumber)
        {
            Av[a] = arcCurrent(Av[a], Ac[bee], N[Ac[bee].node[0]].U, N[Ac[bee].node[1]].U);
            //Av[a] = arcCurrent(Av[a], Ap, N[Ap.node[0]].U, N[Ap.node[1]].U);
        }
        barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////

        if (bee < nodesNumber)
        {
            N[n].I = 0;
            N[n].Y = 0;
        }

        if (bee < 2)
        {
            int a_;
            for (a_=0; a_<arcsNumber; a_++)
            {
                //небалансы в узлах
                int n_ = baseNode + Ac[a_].node[bee];
                float2 K = Ac[a_].K;
                float2 Y = Ac[a_].Yl + 0.5f*Ac[a_].Yc;
                N[n_].I -= Av[a_].I_[bee];
                if (bee==0)
                    N[n_].Y += Y;
                else
                    N[n_].Y += cmul(csqr(K), Y);
            }
            N[baseNode].I = 0;
            N[baseNode].Y = 0;
            balanced[local_task_id] = 1;
        }
        barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////

        if ((0 < bee) && (bee < nodesNumber))
        {
                atomic_and(&(balanced[local_task_id]), fabs(N[n].I.x)<1.0f && fabs(N[n].I.y)<1.0f);
                N[n] = nodeVoltage(N[n]);
        }

        barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////

        N[0].I = (float2) ((float)i,0); // вернуть количество циклов
        if (balanced[local_task_id]) break;

	}

    if (bee < arcsNumber)
    {
        pArc[global_baseArc+bee].I = Av[a].I_[0];
        pArc[global_baseArc+bee].I_[0] = Av[a].I_[0];
        pArc[global_baseArc+bee].I_[1] = Av[a].I_[1];
    }

    if (bee < nodesNumber)
        pNode[global_baseNode+bee] = N[n];


    barrier(CLK_LOCAL_MEM_FENCE);

}
