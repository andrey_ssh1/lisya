﻿unit lisya_optimizer;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils
    , mar
    , lisya_predicates
    , lisya_exceptions
    , dlisp_values
    , lisya_symbols;


function extract_expression_symbols(expr: TValues): TIntegers;

implementation


function pExpressionOperator(V: TValue): boolean;
begin
    result := tpListNotEmpty(V) and ((V as TVList)[0] is TVOperator);
end;


function pExpressionVariableDeclaration(V: TValue): boolean;
begin
    result := vphExpressionOperator(V, [oeVAR, oeCONST, oeMACRO_SYMBOL],
                                            [vpVariableNames])
end;


function pExpressionSubprogramDeclaration(V: TValue): boolean;
begin
    result := vphExpressionOperator(V, [oeFUNCTION, oePROCEDURE, oeMACRO],
                                            [tpName]);
end;


function pExpressionLambda(V: TValue): boolean;
begin
    result := vphExpressionOperator(V,
        [oeFUNCTION, oePROCEDURE, oeMACRO],
        [tpListOfSymbols]);
end;


function extract_expression_symbols(expr: TValues): TIntegers;
var stack: TIntegers;

    procedure push(V: TValue);
    var i: integer; L: TVList;
    begin
        if tpName(V) then append_integer(stack, (V as TVSymbol).n);
        if tpList(V) then begin
            L := V as TVList;
            for i := 0 to L.high do push(L[i]);
        end;
    end;

    procedure add(S: TVSymbol);
    var i,n: integer;
    begin
        n := S.N;
        for i := high(stack)  downto 0 do if n=stack[i]  then Exit;
        for i := high(result) downto 0 do if n=result[i] then Exit;
        append_integer(result,n);
    end;

    procedure expression(E: TValue); forward;

    procedure impure_tail(E: TVList; start: integer);
    var i: integer;
    begin for i := start to E.high do expression(E[i]); end;

    procedure tail(E: TVList; start: integer);
    var sp: integer;
    begin sp := Length(stack); impure_tail(E, start); Setlength(stack,sp); end;

    procedure expression_assemble(E: TVList);
    var i: integer;
    begin
        for i := 0 to E.high do
            if vpListHeaded_VALUE(E[i]) or vpListHeaded_INSET(E[i])
            then impure_tail(E.L[i], 1)
            else if tpList(E[i]) then expression_assemble(E.L[i]);
    end;

    procedure expression_case(E: TVList);
    var i: integer;
    begin
        if E.Count<2 then raise ELE.Malformed(E.AsString);
        for i := 2 to E.high do if not tpListNotEmpty(E[i])
        then raise ELE.Malformed('CASE: '+E[i].AsString);
        expression(E[1]);
        for i := 2 to E.high do tail(E, 1);
    end;

    procedure expression_operator(E: TVList);
    var sp: integer;
    begin
        case (E[0] as TVOperator).op_enum of
            oeAND,oeAPPEND, oeBLOCK, oeCOND, oeDEFAULT, oeELT, oeERROR,
            oeEXECUTE_FILE, oeIF, oeINSERT, oeKEY, oeOR, oeEXTRACT,
            oePUSH, oeRETURN, oeSET, oeWHEN, oeWHILE:
                tail(E, 1);
            oeFOR:
                begin
                    sp := Length(stack);
                    expression(E[2]);
                    push(E[1]);
                    tail(E, 3);
                    SetLength(stack, sp);
                end;
            oeCONST, oeMACRO_SYMBOL, oeVAR:
                if pExpressionVariableDeclaration(E)
                then begin
                    tail(E, 2);
                    push(E[1]);
                end else raise ELE.Malformed(E.AsString);
            oeFUNCTION, oeMACRO, oePROCEDURE:
                if pExpressionSubprogramDeclaration(E)
                then begin
                    push(E[1]);
                    sp := Length(stack);
                    push(E[2]);
                    tail(E,3);
                    SetLength(stack, sp);
                end
                else if pExpressionLambda(E)
                then begin
                    sp := Length(stack);
                    push(E[1]);
                    tail(E, 2);
                    SetLength(stack, sp);
                end
                else raise ELE.Malformed(E.AsString);
            oeBREAK, oeCONTINUE, oeDEBUG, oeGOTO, oePACKAGE, oeQUOTE, oeUSE,
            oeWITH:
                ;
            oeASSEMBLE:
                begin
                    sp := Length(stack);
                    expression_assemble(E);
                    SetLength(stack, sp);
                end;
            oeLET:
                impure_tail(E, 1);
            oeTRY:
                begin
                    sp := Length(stack);
                    //TODO: почему двоеточия перед названиями переменных?
                    append_integer(stack, symbol_n(':EXCEPTION-CLASS'));
                    append_integer(stack, symbol_n(':EXCEPTION-MESSAGE'));
                    append_integer(stack, symbol_n(':EXCEPTION-STACK'));
                    tail(E, 1);
                    SetLength(stack, sp);
                end;
            oeCASE:
                begin
                    sp := Length(stack);
                    expression_case(E);
                    SetLength(stack, sp);
                end;
            else raise ELE.Create('Неопределённый оператор ' +E[0].AsString,'internal');
        end;
    end;

    procedure expression(E: TValue);
    begin
        if tpName(E) then add(E as TVSymbol)
        else
        if pExpressionOperator(E) then expression_operator(E as TVList)
        else
        if tpListNotEmpty(E) then tail(E as TVList, 0);
    end;

var E: TVList;
begin
    SetLength(stack, 0);
    SetLength(result, 0);
    E := TVList.CreateCopy(Expr); //TODO: костыльное создание списка
    expression(E);
    E.Free;
    SetLength(stack, 0);
end;

end.

