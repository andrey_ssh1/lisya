﻿unit lisya_type_converters;

{$mode delphi}{$H+}

interface

uses
    sysutils,
    dlisp_values, mar, lisya_exceptions, mar_math_arrays, mar_tensor, mar_math_graph;


function ComplexesToListOfComplexes(c: TComplexes): TVList;
function DoublesToListOfFloats(d: TDoubles): TVList;
function IntegersToListOfRanges(i: TIntegers): TVList;
function IntegersToListOfIntegers(i: TIntegers): TVList;
function StringArrayToListOfStrings(sa: TStringArray): TVList;

function ListOfNumbersToComplexes(L: TVList): TComplexes;
function ListOfIntegersToIntegers(L: TVList): TIntegers;
function ListOfRealsToDoubles(L: TVList): TDoubles;
function ListOfTimesToDoubles(L: TVList): TDoubles;
function ListOfTensorsToTensors(L: TVList): TTensors;
function ListOfCompoundsToGraph(L: TVList; n1, n2: TValue): TGraph;
function ListToStrings(L: TVList): TStringArray;

function ValuesToDoubles(L: TValues): TDoubles;
function ValuesToComplexes(L: TValues): TComplexes;


implementation

////////////////////////////////////////////////////////////////////////////////
/// ... TO LIST ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function ComplexesToListOfComplexes(c: TComplexes): TVList;
var i: integer;
begin
    result := TVList.Create;
    for i := 0 to high(c) do result.Add(TVComplex.Create(c[i]));
end;


function DoublesToListOfFloats(d: TDoubles): TVList;
var i: integer;
begin
    result := TVList.Create;
    for i := 0 to high(d) do result.Add(TVFloat.Create(d[i]));
end;


function IntegersToListOfRanges(i: TIntegers): TVList;
var n: integer;
begin
    if (Length(i) mod 2)<>0 then raise ELE.Create('internal/IntegersToListOfRanges/odd');
    result := TVList.Create;
    for n := 0 to (Length(i) div 2) - 1 do
        result.Add(TVRangeInteger.Create(i[n*2], i[n*2+1]));
end;


function IntegersToListOfIntegers(i: TIntegers): TVList;
var n: integer;
begin
    result := TVList.Create;
    for n := 0 to high(i) do result.Add(TVInteger.Create(i[n]));
end;


function StringArrayToListOfStrings(sa: TStringArray): TVList;
var i: integer;
begin
    result := TVList.Create;
    for i := 0 to high(sa) do result.Add(TVString.Create(sa[i]));
end;


////////////////////////////////////////////////////////////////////////////////
/// LIST TO ... ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function ListOfNumbersToComplexes(L: TVList): TComplexes;
var i: integer;
begin
    SetLength(result, L.Count);
    for i := 0 to L.high do result[i] := L.C[i];
end;


function ListOfIntegersToIntegers(L: TVList): TIntegers;
var i: integer; V: TValues;
begin
    V := LV_(L);
    SetLength(result, Length(V));
    for i := 0 to high(V) do result[i] := I_(V[i]);
end;


function ListOfRealsToDoubles(L: TVList): TDoubles;
var i: integer;
begin
    SetLength(result, L.Count);
    for i := 0 to L.high do result[i] := L.F[i];
end;


function ListOfTimesToDoubles(L: TVList): TDoubles;
var i: integer;
begin
    SetLength(result,L.Count);
    for i := 0 to high(result) do result[i] := (L[i] as TVTime).fDT;
end;


function ListOfTensorsToTensors(L: TVList): TTensors;
var i: integer;
begin
    SetLength(result, L.Count);
    for i := 0 to high(result) do result[i] := (L[i] as TVTensor).t;
end;


function ListOfCompoundsToGraph(L: TVList; n1, n2: TValue): TGraph;
var a: integer;
    function sr(V: TValue): unicodestring;
    begin
        if V is TVString then Exit(S_(V)) else Exit(V.AsString);
	end;
begin
    result.Init();
    for a := 0 to L.Count - 1 do
        with L[a] as TVCompound do
            result.Add(sr(items1[Index(n1)]), sr(items1[Index(n2)]));
end;


function ListToStrings(L: TVList): TStringArray;
var i: integer;
begin
    SetLength(result, L.Count);
    for i := 0 to high(result) do result[i] := L[i].AsString;
end;


function ValuesToDoubles(L: TValues): TDoubles;
var i: integer;
begin
    SetLength(result, Length(L));
    for i := 0 to high(result) do result[i] := F_(L[i]);
end;


function ValuesToComplexes(L: TValues): TComplexes;
var i: integer;
begin
    SetLength(result, Length(L));
    for i := 0 to high(result) do result[i] := C_(L[i]);
end;


end.

