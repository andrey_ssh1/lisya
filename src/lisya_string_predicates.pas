﻿unit lisya_string_predicates;

{$mode delphi}{$H+}

interface

uses
    {$IFDEF LINUX}
    cwstring,
    {$ENDIF}
    Classes, SysUtils;

function sp_integer(s: unicodestring): boolean;
function sp_float(s: unicodestring): boolean;
function sp_complex_alg(s: unicodestring): boolean;
function sp_complex_exp(s: unicodestring): boolean;
function sp_char(s: unicodestring): boolean;
function sp_duration(s: unicodestring): boolean;
function sp_date_time(s: unicodestring): boolean;
function sp_range_integer(s: unicodestring): boolean;
function sp_range_float(s: unicodestring): boolean;

function test_string_predicate(s: unicodestring): boolean;

implementation



function _end(const s: unicodestring; p: integer): boolean;
begin
    result := p>Length(s);
end;


function not_end(const s: unicodestring; p: integer): boolean;
begin
    result := p<=Length(s);
end;


function next(const s: unicodestring; var p: integer; w: unicodestring): boolean; overload;
begin
    result := copy(s, p, Length(w)) = w;
    if result then Inc(p, Length(w));
end;


function next(const s: unicodestring; var p: integer; words: array of unicodestring): boolean; overload;
var w: unicodestring;
begin
    if p>Length(s) then Exit(false);
    for w in words do
        if copy(s, p, Length(w)) = w
        then
            begin
                Inc(p, Length(w));
                Exit(true);
			end;
    result := false;
end;


function decimal(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    result := not_end(s,p) and (s[p] in ['0'..'9']);
    for i:=p to Length(s) do
        if s[i] in ['0'..'9']
        then
            Inc(p)
        else
            Exit;
end;


function hex(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    result := not_end(s,p) and (s[p] in ['0'..'9','A'..'F','a'..'f']);
    for i:=p to Length(s) do
        if s[i] in ['0'..'9','A'..'F','a'..'f']
        then
            Inc(p)
        else
            Exit;
end;


function hex_prefix(const s: unicodestring; var p: integer): boolean;
begin
    result := next(s,p,['$','0x']);
end;


function sign(const s: unicodestring; var p: integer): boolean;
begin
    result := not_end(s,p) and (s[p] in ['+','-']);
    if result then Inc(p);
end;


function optional_sign(const s: unicodestring; var p: integer): boolean;
begin
    result := true;
    sign(s,p);
end;


function separator(const s: unicodestring; var p: integer): boolean;
begin
    result := not_end(s,p) and (s[p]='_');
    if result then Inc(p);
end;


function optional_suffix_int(const s: unicodestring; var p: integer): boolean;
begin
    result := true;
    if not_end(s,p) then next(s,p,['к','k','М','M','Г','G','Т','T']);
end;


function suffix_decimal(const s: unicodestring; var p: integer): boolean;
begin
    result := next(s,p,[ 'к','k','М','M','Г','G','Т','T',
                         'п','p','н','n','u','мк','м','m']);
end;


function angle_unit(const s: unicodestring; var p: integer): boolean;
begin
    result := next(s,p,['°','гр','deg','pi']);
end;


function angle_symbol(const s: unicodestring; var p: integer): boolean;
begin
    result := (p<=Length(s))
        and ((s[p]='a') or (s[p]='у') or (s[p]='∠'));
    if result then Inc(p);
end;


function suffix_i(const s: unicodestring; var p: integer): boolean;
begin
    result := not_end(s,p) and ((s[p]='i') or (s[p]='м'));
    if result then Inc(p);
end;


function separated_decimal(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    result := decimal(s, p);
    i := p;
    while result do
        if separator(s,i) and decimal(s,i)
        then
            p := i
        else
            Exit;
end;


function separated_hex(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    result := hex(s, p);
    i := p;
    while result do
        if separator(s,i) and hex(s,i)
        then
            p := i
        else
            Exit;
end;


function decimal_int(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    i := p;
    result := optional_sign(s,i) and separated_decimal(s,i) and optional_suffix_int(s,i);
    if result then p := i;
end;


function hex_int(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    i := p;
    result := hex_prefix(s,i) and separated_hex(s,i);
    if result then p := i;
end;


function int(const s: unicodestring; var p: integer): boolean;
begin
    result := hex_int(s,p) or decimal_int(s,p);
end;


function decimal_separator(const s: unicodestring; var p: integer): boolean;
begin
    result := not_end(s,p) and (s[p] in ['.',',']);
    if result then Inc(p);
end;


function semicolon(const s: unicodestring; var p: integer): boolean;
begin
    result := not_end(s,p) and (s[p]=':');
    if result then Inc(p);
end;


function date_separator(const s: unicodestring; var p: integer): boolean;
begin
    result := not_end(s,p) and (s[p] in ['-','.']);
    if result then Inc(p);
end;


function date_time_separator(const s: unicodestring; var p: integer): boolean;
begin
    result := not_end(s,p) and (s[p] in ['_','T']);
    if result then Inc(p);
end;



function separated_float(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    result := separated_decimal(s,p);
    i := p;
    if result and decimal_separator(s,i) and separated_decimal(s,i)
    then
        p := i;
end;


function optional_seconds(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    result := true;
    i := p;
    if semicolon(s,i) and decimal(s,i)
    then
        begin
            p := i;
            if decimal_separator(s,i) and separated_decimal(s,i)
            then
                p := i;
		end;
end;


function minutes_and_seconds(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    i := p;
    result := semicolon(s,i) and decimal(s,i) and optional_seconds(s,i);
    if result then p := i;
end;


function optional_time(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    result := true;
    i := p;
    if date_time_separator(s,i) and decimal(s,i) and minutes_and_seconds(s,i)
    then
        p := i;
end;


function exp(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    i := p;
    result := not_end(s,i) and (s[i] in ['e','E']);
    if result then Inc(i) else Exit;
    result := optional_sign(s,i) and decimal(s,i);
    if result then p := i;
end;


function float_exp(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    i := p;
    result := separated_float(s,i) and (exp(s,i) or true);
    if result then p := i;
end;


function float(const s: unicodestring; var p: integer): boolean;
var i: integer;
begin
    i := p;
    result := optional_sign(s,i)
        and separated_float(s,i)
        and (exp(s,i)
            or angle_unit(s,i) //более длинные суффиксы проверяются первыми
            or suffix_decimal(s,i) //иначе "p" экранирует "pi"
            or true);
    if result then p := i;
end;


////////////////////////////////////////////////////////////////////////////////

function sp_char(s: unicodestring): boolean;
var p: integer;
begin
    p := 1;
    result := next(s,p,'#')
        and ((hex_prefix(s,p) and hex(s,p)) or decimal(s,p))
        and _end(s,p);
end;


function sp_integer(s: unicodestring): boolean;
var p: integer;
begin
    p := 1;
    result := int(s,p) and _end(s,p);
end;


function sp_float(s: unicodestring): boolean;
var p: integer;
begin
    p := 1;
    result := float(s,p) and _end(s,p);
end;


function sp_range_integer(s: unicodestring): boolean;
var p: integer;
begin
    p := 1;
    result := int(s,p) and next(s,p,'..') and int(s,p) and _end(s,p);
end;


function sp_range_float(s: unicodestring): boolean;
var p: integer;
begin
    p := 1;
    result := float(s,p) and next(s,p,'..') and float(s,p) and _end(s,p);
end;


function sp_complex_alg(s: unicodestring): boolean;
var p: integer;
begin
    p := 1;
    result := optional_sign(s,p) and float_exp(s,p) and sign(s,p)
        and suffix_i(s,p) and float_exp(s,p) and _end(s,p);
end;


function sp_complex_exp(s: unicodestring): boolean;
var p: integer;
begin
    p := 1;
    result := optional_sign(s,p) and separated_float(s,p)
        and (exp(s,p) or suffix_decimal(s,p) or true)
        and angle_symbol(s,p)
        and optional_sign(s,p) and separated_float(s,p) and angle_unit(s,p)
        and _end(s,p);
end;


function sp_duration(s: unicodestring): boolean;
var p: integer;
begin
    p := 1;
    result := optional_sign(s,p)
        and separated_decimal(s,p)
        and minutes_and_seconds(s,p)
        and _end(s,p);
end;


function sp_date_time(s: unicodestring): boolean;
var p: integer;
begin
    p := 1;
    result := decimal(s,p)
        and date_separator(s,p)
        and decimal(s,p)
        and date_separator(s,p)
        and decimal(s,p)
        and optional_time(s,p)
        and _end(s,p);
end;

////////////////////////////////////////////////////////////////////////////////


function test_string_predicate(s: unicodestring): boolean;
var p: integer;
begin
    p := 1;
    result := sp_char(s);
end;


end.

