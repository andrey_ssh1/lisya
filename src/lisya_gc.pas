unit lisya_gc;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils, dlisp_values, mar, math;


function NewVariable(_V: TValue = nil; _constant: boolean = false): PVariable;
function RefVariable(P: PVariable): PVariable;
procedure ReleaseVariable(var P: PVariable);
procedure ReleaseVariables(PP: PVariables); overload;
procedure ReleaseVariables(P: PVariable); overload;
procedure SetVariableValue(P: PVariable; _V: TValue); inline;



function separate(V: TValue; force_const: boolean=false): TValue;
procedure print_links(V: TValue);
procedure extract_block_values(var vv: TValues; V: TValue);
function extract_variables1(P: PVariable): PVariables;

var rrp: integer = 1;
    //0 - только подсчёт ссылок
    //1 - анализ корня графа памяти до обнаружения меток древовидности
    //2 - полный анализ графа памяти


//+1. Вывод графа с отметками древовидности
//+2. Анализатор графа, проставляющий древовидность
//+3. Фильтр удаляющий линки на деревья (при добавлении линков в граф)
//+4. Переключатель rrp отключающий оптимизацию с деревьями.
//+5. Удаление древовидных отметок при проходе TVChainPointer.TargetForModificatiоn/SetTarget
//+6. Тест на скорость повторяющегося обращения к списку
//+7. Тест на скорость повторяющегося обращения к хэш-таблице
//+8. Тест на простановку и удаление меток древовидности (с кольцами)
//+9. Ограниченный подграф для определения внешне доступных узлов

//1. Работа с rrp только в lisya_gc (убрать из clear_frame)
//2. Исключить пересоздания PointerHashTable при построении и анализе графов
//3. Построение подграфа внешне доступных узлов по графу памяти, а не по памяти



implementation

type
    TLinks = array of PPVariable;
    TNode = record P: PVariable; ref_count: integer; l: TLinks; end;
    TGraph = array of TNode;
    TINode = record acessible: boolean; links: TIntegers; end;
    TIGraph = array of TINode;


procedure add_value(var vv: TValues; v: TValue);
begin
    SetLength(vv, Length(vv)+1);
    vv[high(vv)] := v;
end;


procedure extract_block_links(var l: TLinks; V: TValue);

    procedure add_link(from_: PPVariable); inline;
    begin
        if from_^=nil then Exit;
        //возможность игнорирования связей с деревьями
        //if from_^^.tree then Exit;
        SetLength(l, Length(l)+1);
        l[high(l)] := from_;
    end;

    procedure el_SymbolStack(ss: TVSymbolStack);
    var i: integer;
    begin
        for i := 0 to high(ss.stack) do add_link(@ss.stack[i].V);
    end;

    procedure el_ListBody(lb: TVListBody);
    var i: integer;
    begin
        for i := 0 to high(lb.V) do extract_block_links(l, lb.V[i]);
    end;

    procedure el_List(ls: TVList);
    begin
        add_link(ls.get_body_link);
    end;

var proc: TVRoutine; rec: TVRecord; ht: TVHashTable;
    i: integer;
begin

    if V is TVSymbolStack
    then
        el_SymbolStack(V as TVSymbolStack)

    else if V is TVRoutine
    then
        begin
            proc := V as TVRoutine;
            el_SymbolStack(proc.stack);
            el_List(proc.body);
            if proc.rest<>nil then el_List(proc.rest);
        end

    else if V is TVChainPointer
    then
        add_link((V as TVChainPointer).get_link)

    else if V is TVListBody
    then
        el_ListBody(V as TVListBody)

    else if V is TVList
    then
        el_List(V as TVList)

    else if V is TVRecord
    then
        begin
            rec := V as TVRecord;
            for i := 0 to rec.count-1 do extract_block_links(l, rec[i]);
        end

    else if V is TVHashTable
    then
        begin
            ht := V as TVHashTable;
            extract_block_links(l, ht.data_list);
            extract_block_links(l, ht.keys_list);
        end

    else if V is TVReturn
    then
        extract_block_links(l, (V as TVReturn).value);
end;




procedure build_graph1(var b: TGraph; P: PVariable; root: boolean = false);
var PHT: TPointersHashTable; i: integer;

    procedure build(P: PVariable);
    var i, this: integer;
    begin
        if P = nil then Exit;
        if not PHT.AddPointer(P) then Exit;
        SetLength(b, Length(b)+1);
        this := high(b);
        b[this].P := P;
        b[this].ref_count := -1;
        b[this].l := nil;
        if root and P^.tree then Exit;
        extract_block_links(b[this].l, P^.V);
        for i := 0 to high(b[this].l) do build(b[this].l[i]^);
    end;

begin
    PHT := TPointersHashTable.Create(length(b));
    for i := 0 to high(b) do PHT.SetPointer(b[i].P, i);
    build(P);
    PHT.Free
end;



procedure count_internal_refs(var b: TGraph);
var i, j, k: integer;  PHT: TPointersHashTable;
begin
    PHT := TPointersHashTable.Create(length(b));
    for i := 0 to high(b) do
        begin
            b[i].ref_count:=0;
            PHT.SetPointer(b[i].P, i);
		end;

    for i := 0 to high(b) do
        for j := 0 to high(b[i].l) do
            begin
                k := PHT.GetIndex(b[i].l[j]^);
                if k>=0 then Inc(b[k].ref_count);
			end;

    PHT.Free;
end;


procedure accessible_nodes(var g: TGraph);  //TODO: не используется
var nga, i, j, k: integer; ig: TIGraph;
    function n(P: PVariable): integer;
    begin
        for result := 0 to high(g) do if Pointer(P)=g[result].P then Exit;
        raise Exception.Create('битый граф');
    end;
begin
    //преобразование графа в целочисленный (ссылки преобразуются в номера узлов в графе)
    nga := 0;
    SetLength(ig,Length(g));
    for i := 0 to high(g) do begin
        ig[i].acessible := g[i].P.ref_count>g[i].ref_count;
        if ig[i].acessible then Inc(nga);
        SetLength(ig[i].links, Length(g[i].l));
        for j := 0 to high(g[i].l) do ig[i].links[j] := n(g[i].l[j]^);
    end;

    while nga>0 do begin
        nga := 0;
        for i := 0 to high(ig) do begin
            if ig[i].acessible then begin
                for j := 0 to high(ig[i].links) do begin
                    k := ig[i].links[j];
                    if not ig[k].acessible then Inc(nga);
                    ig[k].acessible:=true;
                end;
            end;
        end;
    end;

    for i := 0 to high(g) do if not ig[i].acessible then g[i].ref_count:=-1;
end;


//граф проверяется на древовидность
//узлы не входящие в кольца помечаются как древовидные
//древовидными считаются листья и узлы указывающие только на древовидные узлы
procedure mark_tree_branches(var g: TGraph);
var n, l: integer;
label next_node;
begin
    for n := high(g) downto 0 do
        begin
            if g[n].P.tree then goto next_node;

            for l := 0 to high(g[n].l) do
                if not g[n].l[l]^.tree then goto next_node;

            g[n].P.tree := true;

			next_node:
		end;
end;

procedure show_graph(const b: TGraph);
var i,j,e: integer; c: array of qword; mask,n, a1, a2, nmask: qword;
    function is_tree (i: integer): string;
    begin if b[i].P.tree then result := ' / T' else result := ' / _' end;
label next_n;
begin
    if Length(b)=0 then Exit;

    SetLength(c, 0);
    for i := 0 to high(b) do begin
        a1 := PointerToqword(b[i].p);
        if a1>0 then begin
            SetLength(c, Length(c)+1);
            c[high(c)] := a1;
        end;
        for j := 0 to high(b[i].l) do begin
            SetLength(c, Length(c)+2);
            c[high(c)-1] := PointerToqword(b[i].l[j]);
            c[high(c)] := PointerToqword(b[i].l[j]^);
        end;
    end;

    n := 0;
    while true do begin
        next_n:
        Inc(n);
        nmask := 2**(n*4)-1;
        mask := not nmask;
        a1 := c[0] and mask;
        for i := 1 to high(c) do begin
            a2 := c[i] and mask;
            if a1<>a2 then goto next_n;
        end;
        Break;
    end;
    n := 8;

    for i := 0 to high(b) do
        begin
            if b[i].p<>nil
            then
                WriteLn(PointerToStr(b[i].p, n),
                    ' [', IntToStr(b[i].ref_count), ' / ',  IntToStr(b[i].P.ref_count), is_tree(i), ']',
                    '  ', b[i].p.V.AsString
                        //, ' -- @', PointerToStr(b[i].p.V, n)
                        )
            else
                WriteLn('-- nil ---');

        //if b[i].P.V is TVListBody
        //then
        //    for e := 0 to high((b[i].P.V as TVListBody).V) do
        //        begin
        //            WriteLn('                        @',
        //                    PointerToStr((b[i].P.V as TVListBody).V[e], n),
        //                    '  --  ', (b[i].P.V as TVListBody).V[e].AsString
        //                    );
        //    	end;

        for j := 0 to high(b[i].l) do
            WriteLn('        ', {PointerToStr(b[i].l[j], n),}' --> ', PointerToStr(b[i].l[j]^, n));
    end;
end;


procedure print_links(V: TValue);
var P: PVariable; b: TGraph;
begin
    New(P);
    P.V := V;
    P.ref_count := 0;
    SetLength(b, 0);
    build_graph1(b, P);
    show_graph(b);
    Dispose(P);
end;


procedure separate_block(V: TValue);
var proc: TVRoutine; ls: TVList; rec: TVRecord; ht: TVHashTable; ret: TVReturn;
    i: integer; lb: TVListBody;
begin
    //TODO: эта процедура на самом деле ничего не делает кроме
    //востановлениия кэширующей ссылки в списках
    if V is TVRoutine then begin
        proc := V as TVRoutine;
        separate_block(proc.body);
        separate_block(proc.rest);
    end

    else if V is TVListBody then begin
        lb := V as TVListBody;
        for i := 0 to high(lb.V) do separate_block(lb.V[i]);
    end

    else if V is TVList then begin
        ls := V as TVList;
        ls.restore_fast_link;
    end

    else if V is TVRecord then begin
        rec := V as TVRecord;
        for i := 0 to rec.count-1 do separate_block(rec[i]);
    end

    else if V is TVHashTable then begin
        ht := V as TVHashTable;
        separate_block(ht.data_list);
        separate_block(ht.keys_list);
    end

    else if V is TVReturn then begin
        ret := V as TVReturn;
        separate_block(ret.value);
    end;
end;


function separate(V: TValue; force_const: boolean): TValue;
var i,j,k: integer; b,nb: TGraph; P: PVariable;

    function variable_mirror(P: PVariable): PVariable;
    begin
        New(result);
        result.V := P.V.Copy;
        result.constant:= P.constant or force_const;
        result.ref_count := 0;
    end;

begin
    //первый проход - извлечение графа связей заданной переменной
    New(P);
    P.ref_count := 0;
    P.V := V;
    SetLength(b, 0);
    build_graph1(b, P);

    //второй проход - копирование блоков
    SetLength(nb, Length(b));
    for i := 0 to high(b) do begin
        SetLength(nb[i].l, 0);
        nb[i].P := variable_mirror(b[i].P);
        separate_block(nb[i].P.V);
        extract_block_links(nb[i].l, nb[i].P.V);
    end;

    //третий проход - перенаправление ссылок из нового графа в старый
    //(образовавшихся при копировании блоков) в новый граф
    for i := 0 to high(nb) do begin
        for j := 0 to high(nb[i].l) do begin
            for k := 1 to high(b) do if b[k].P=Pointer(nb[i].l[j]^) then Break;
            nb[i].l[j]^ := nb[k].P;
            Inc(nb[k].P.ref_count);
            Dec(b[k].P.ref_count);
        end;
        separate_block(nb[i].P.V);
    end;

    result := nb[0].P.V;
    Dispose(nb[0].P);
    Dispose(P);
end;



function create_graph_links_table(g: TGraph): TPointersHashTable;
var i, j: integer;
begin
    result := TPointersHashTable.Create();
    for i := 0 to high(g) do
        for j := 0 to high(g[i].l) do
            result.AddPointer(g[i].l[j]^);
end;

//{$define rrpd}


procedure ReleaseGraph(g: TGraph); //Используется только ReleaseVariables
var gbg: TGraph; i,j: integer; f: array of boolean; PHT: TPointersHashTable;
    function local(n: TNode): boolean;
    begin result := n.ref_count=n.P.ref_count; end;
begin
    count_internal_refs(g);
    {$IFDEF rrpd} WriteLn(); WriteLn('RRP 2'); show_graph(g);{$ENDIF}
    //построение графов начиная с доступных из вне узлов
    SetLength(gbg, 0);
    for i := 0 to high(g) do
        if not local(g[i]) then build_graph1(gbg, g[i].P, rrp=1);

    //маркировка деревьев
    if rrp=1 then mark_tree_branches(g); //маркировка деревьев проверена и работает
    //WriteLn('RV-------------');
    //show_graph(g);         //демаркировка модифицирующими операторами тоже

    {$IFDEF rrpd} WriteLn('-- global --'); show_graph(gbg);{$ENDIF}

    //поиск блоков под освобождение (недоступных из вне даже косвенно)
    PHT := create_graph_links_table(gbg);
    SetLength(f, Length(g));
    for i := 0 to high(g) do f[i] := local(g[i]) and not PHT.Contains(g[i].P);
    PHT.Free;

    {$IFDEF rrpd} for i := 0 to high(f) do Write(f[i],' '); WriteLn();{$ENDIF}
    {$IFDEF rrpd} WriteLn('-- local --'); show_graph(g);{$ENDIF}

    //удаление ссылок от освобождаемых объектов
    for i := 0 to high(g) do
        if f[i] then
            for j := 0 to high(g[i].l) do
                begin
                    Dec(g[i].l[j]^^.ref_count);
                    g[i].l[j]^ := nil;
                end;

    {$IFDEF rrpd} WriteLn('-- unlinked --'); count_internal_refs(g); show_graph(g);{$ENDIF}

    //освобождение
    for i := 0 to high(g) do if f[i] then g[i].P.V.Free;
    for i := 0 to high(g) do if f[i] then Dispose(g[i].P);
    {$IFDEF rrpd} WriteLn('rrp 2');{$endif}
end;



procedure ReleaseVariables(PP: PVariables);
var g: TGraph;  i: integer;
begin
    SetLength(g, 0);
    for i := 0 to high(PP) do
        if pp[i]<>nil
        then
            begin
                build_graph1(g, PP[i], rrp=1);
                dec(PP[i].ref_count);
            end;

    if Length(g)>0 then ReleaseGraph(g);
end;


procedure ReleaseVariables(P: PVariable); //эта не используется
var g: TGraph;
begin
    g := nil;
    if P<>nil
    then
        begin
            build_graph1(g, P);
            dec(P.ref_count);
            ReleaseGraph(g);
        end;
end;


procedure SetVariableValue(P: PVariable; _V: TValue); inline;
begin
    FreeAndNil(P.V);
    P.V := _V;
end;


procedure extract_block_values(var vv: TValues; V: TValue);

    procedure add (V: TValue); begin add_value(vv,v); end;

var proc: TVRoutine; ls: TVList; rec: TVRecord; ht: TVHashTable; ret: TVReturn;
    i: integer;
begin
    add(V);

    if V is TVRoutine then begin
        proc := V as TVRoutine;
        extract_block_values(vv, proc.rest);
        extract_block_values(vv, proc.body);
    end

    else if V is TVList then begin
        ls := V as TVList;
        for i := 0 to ls.high do extract_block_values(vv,ls[i]);
    end

    else if V is TVRecord then begin
        rec := V as TVRecord;
        for i := 0 to rec.count-1 do extract_block_values(vv,rec[i]);
    end

    else if V is TVHashTable then begin
        ht := V as TVHashTable;
        for i := 0 to ht.Count-1 do begin
            extract_block_values(vv, ht[i]);
            extract_block_values(vv, ht.look_key[i]);
        end;
    end

    else if V is TVReturn then begin
        ret := V as TVReturn;
        extract_block_values(vv, ret.value);
    end;
end;


function extract_variables1(P: PVariable): PVariables;
var b: TGraph; i: integer;
begin
    SetLength(b, 0);
    build_graph1(b, P);
    SetLength(result, Length(b));
    for i := 0 to high(b) do result[i] := b[i].P;
end;


{ TVariable }

function NewVariable(_V: TValue = nil; _constant: boolean = false): PVariable;
begin
    New(result);
    result.ref_count:=1;
    result.constant := _constant;
    result.V := _V;
    result.tree := false;
end;


function RefVariable(P: PVariable): PVariable;
begin
    result := P;
    if P<>nil then Inc(P.ref_count);
end;


procedure ReleaseVariable(var P: PVariable);
begin
    {$IFDEF rrpd}
        Write('Release ',PointerToStr(P), '  ');
        if P<>nil then WriteLn(P.ref_count, '  ', P.V.AsString) else WriteLn();
    {$ENDIF}
    if P<>nil
    then
        try
            if P.ref_count=1
            then
                begin
                    P.V.Free;
                    Dispose(P);
                end
            else
                Dec(P.ref_count);
        finally
            P := nil;
        end;
end;


initialization

finalization

end.

