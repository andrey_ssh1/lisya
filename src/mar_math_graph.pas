unit mar_math_graph;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils, mar;

type
    TGraphArc = record
        n1: integer;
        n2: integer;
	end;


	{ TGraph }

    TGraph = record
        names: TStringArray;
        arcs: array of TGraphArc;
        function find_node(name: unicodestring): integer;
        procedure Init();
        procedure Add(_n1, _n2: unicodestring); overload;
        procedure Add(_n1, _n2: integer); overload;
	end;



function Connectivity(g: TGraph): TIntegers2;
function Paths(g: TGraph; _from, _to: unicodestring): TIntegers2;


implementation


////////////////////////////////////////////////////////////////////////////////
//// Connectivity //////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


type TNodesMask = array of boolean;


function sub_graph(g: TGraph; nodes: TNodesMask): TIntegers; overload;
var a: integer;
begin
    result := nil;

    for a := 0 to high(g.arcs) do
        with g.arcs[a] do
            if nodes[n1] or nodes[n2] then append_integer(result, a);
end;


// Построение структуры узлы->узлы оптимальной для проверки связности
function NodesNeighbours(g: TGraph): TIntegers2;
var a, n: integer;
begin
    SetLength(result, Length(g.names));
    for n := 0 to high(result) do result[n] := nil;

    for a := 0 to high(g.arcs) do
        with g.arcs[a] do
            begin
                append_integer(result[n1], n2);
                append_integer(result[n2], n1);
		    end;
end;


function Connectivity(g: TGraph): TIntegers2;
var net: TIntegers2; nodes: TNodesMask; n, i: integer;

    procedure add_neighbours(n: integer);
    var nb: TIntegers; i: integer;
    begin
        nodes[n] := true;
        nb := net[n];
        net[n] := nil;
        for i := 0 to high(nb) do add_neighbours(nb[i]);
	end;

    function next_island(out n: integer): boolean;
    var i: integer;
    begin
        for i := 0 to high(net) do
            if net[i]<>nil then begin n := i; Exit(true); end;
        result := false;
	end;

begin
    result := nil;

    net := NodesNeighbours(g);
    SetLength(nodes, Length(net));

    while next_island(n) do
        begin
            for i := 0 to high(nodes) do nodes[i] := false;
            add_neighbours(n);

            SetLength(result, Length(result)+1);
            result[high(result)] := sub_graph(g, nodes);
		end;
end;


////////////////////////////////////////////////////////////////////////////////
//// Paths /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

type
    TGraphNodeLinks = record
        on_path: boolean;
        adjacent_arcs: TIntegers;  // каждая смежная ветвь указывает на
        adjacent_nodes: TIntegers; //конктертный смежный узел
    end;
    TGraphNodesLinks = array of TGraphNodeLinks;


// Построение структуры узлы-> связи оптимальной для поиска путей
function NodesLinks(g: TGraph): TGraphNodesLinks;
var n, a: integer;
begin
    SetLength(result, Length(g.names));
    for n := 0 to high(result) do
        with result[n] do
            begin
                on_path := false;
                adjacent_arcs := nil;
                adjacent_nodes := nil;
			end;

    for a := 0 to high(g.arcs) do
        with g.arcs[a] do
            begin
                append_integer(result[n1].adjacent_arcs, a);
                append_integer(result[n1].adjacent_nodes, n2);
                append_integer(result[n2].adjacent_arcs, a);
                append_integer(result[n2].adjacent_nodes, n1);
			end;
end;


function Paths(g: TGraph; _from, _to: unicodestring): TIntegers2;
var net: TGraphNodesLinks; path: TIntegers; stone, target: integer;

    procedure path_found();
    begin
        SetLength(result, Length(result) + 1);
        result[high(result)] := Copy(path, 0, stone+1);
	end;

    procedure from(n: integer);
    var a: integer;
    begin
        if net[n].on_path then Exit;
        if n = target then begin path_found(); Exit end;

        net[n].on_path := true;
        Inc(stone);
        for a := 0 to high(net[n].adjacent_arcs) do
            begin
                path[stone] := net[n].adjacent_arcs[a];
                from(net[n].adjacent_nodes[a]);
			end;
        Dec(stone);
		net[n].on_path := false;
	end;

begin
    SetLength(result, 0);
    net := NodesLinks(g);
    SetLength(path, Length(g.arcs)+1); //длина пути не может превышать количество ветвей
    stone := -1;
    target := g.find_node(_to);
    from(g.find_node(_from));
end;


////////////////////////////////////////////////////////////////////////////////
//// Graph /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

{ TGraph }


function TGraph.find_node(name: unicodestring): integer;
var n: integer;
begin
    for n := 0 to high(names) do
        if names[n] = name then Exit(n);

    append_string_array(names, name);
    result := high(names);
end;


procedure TGraph.Init;
begin
    names := nil;
    arcs := nil;
end;


procedure TGraph.Add(_n1, _n2: unicodestring);
begin
    Add(find_node(_n1), find_node(_n2));
end;


procedure TGraph.Add(_n1, _n2: integer);
begin
    SetLength(arcs, Length(arcs)+1);
    with arcs[high(arcs)] do
        begin
            n1 := _n1;
            n2 := _n2;
		end;
end;


end.  //166   146

