﻿unit mar_tensor;

{$mode delphi}{$H+}

interface

uses
    SysUtils, ucomplex, math, mar, mar_math_arrays, mar_math_sp;


type

	{ TTensor }

    TTensor = record
        dim: array of integer;
        e: array of double;
        function dim_high(_d: integer): integer;
        function dims_count(): integer;
        function size(): TIntegers; overload;
        function size(d: integer): integer; overload;
        function step(): TIntegers; overload;
        function step(d: integer): integer; overload;
        function complex_e(): TDoubles;
        procedure Init(_size: TIntegers; elsize: integer=1);
        procedure SetBufferLength();
        procedure Create(_dim: TIntegers; _e: TDoubles); overload;
        procedure Create(_dim: TIntegers; _e: TComplexes); overload;
        function GetR(idx: TIntegers): double;
        function GetC(idx: TIntegers): COMPLEX; overload;
        function GetC(idx: integer): COMPLEX; overload;
        function Complexes(): TComplexes;

        function str(): unicodestring;
	end;
    TTensors = array of TTensor;


    TTensorFoldFunctionR = function (a, b: real): real;
    TTensorFoldFunctionC = function (a, b: complex): complex;


function tffMax(a, b: real): real;
function tffMin(a, b: real): real;
function tffAdd(a, b: real): real;
function tffMul(a, b: real): real;

function tensor_new(_e: TDoubles): TTensor; overload;
function tensor_new(_e: TDoubles; w: integer): TTensor; overload;
function tensor_new(_e: TComplexes): TTensor; overload;
function tensor_new(_e: TComplexes; w: integer): TTensor; overload;
function tensor_stack(tensors: TTensors): TTensor;
function tensor_concatenate(tensors: TTensors): TTensor;
function tensor_add_dim(t: TTensor): TTensor;
function tensor_flat(t: TTensor): TTensor;
function tensor_split(t: TTensor; d: integer): TTensors;
function tensor_abs(t: TTensor): TTensor;
function tensor_arg(t: TTensor): TTensor;
function tensor_re(t: TTensor): TTensor;
function tensor_im(t: TTensor): TTensor;
function tensor_radtodeg(a: TTensor): TTensor;
function tensor_noisy(t: TTensor; m, a: real): TTensor;
function tensor_round_to(t: TTensor; digits: TRoundToRange): TTensor;
function tensor_equal(a, b: TTensor): boolean;
function tensor_fold(t: TTensor; d: integer; f: TTensorFoldFunctionR): TTensor;
function tensor_subseq(t: TTensor; _s, _e: integer): TTensor;

function tensor_add(a, b: TTensor): TTensor; overload;
function tensor_add(a: TTensor; b: complex): TTensor; overload;
function tensor_mul(a, b: TTensor): TTensor; overload;
function tensor_mul(a: TTensor; b: complex): TTensor; overload;
function tensor_sub(a, b: TTensor): TTensor;
function tensor_div(a, b: TTensor): TTensor; overload;
function tensor_div(a: TTensor; b: complex): TTensor; overload;


implementation


type
    ETensorOperation = class(Exception) end;
    ETO = ETensorOperation;


function dot(dim, idx: TIntegers): integer;
var i: integer;
begin
    result := 0;
    for i := 0 to high(idx) do result := result + dim[i]*idx[i];
end;


function same_geometry(a, b: TIntegers): boolean;
var d: integer;
begin
    if a=b then Exit(true);
    if Length(a)<>Length(b) then Exit(false);

    for d := Length(a) div 2 to high(a) do if a[d]<>b[d] then Exit(false);
    result := true;
end;


function common_dim(a, b: TIntegers; out c: TIntegers): integer; overload;
begin
    if not same_geometry(a, b) then raise ETO.Create('tensor/geometry mismatch');
    result := 10*a[0]+b[0];
    case result of
        11, 22, 21: c := a;
        12: c := b;
        else raise ETO.Create('tensor/invalid geometry');
	end;
end;


function common_dim(a: TIntegers; b: complex; out c: TIntegers): integer; overload;
var i: integer;
begin
    if b.im=0 then result := 10*a[0]+1 else result := 10*a[0]+2;
    case result of
        11, 22, 21: c := a;
        12: begin
            c := copy(a);
            for i := 0 to Length(c) div 2 -1 do c[i] := c[i]*2;
		end;
		else raise ETO.Create('tensor/invalid geometry');
	end;
end;


////////////////////////////////////////////////////////////////////////////////
//// folding ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function tffMax(a, b: real): real; begin result := max(a, b); end;
function tffMin(a, b: real): real; begin result := min(a, b); end;

function tffAdd(a, b: real): real; begin result := a + b; end;
function tffSub(a, b: real): real; begin result := a - b; end;
function tffMul(a, b: real): real; begin result := a * b; end;
function tffDiv(a, b: real): real; begin result := a / b; end;

function tffAddC(a, b: complex): complex; begin result := a + b; end;
function tffSubC(a, b: complex): complex; begin result := a - b; end;
function tffMulC(a, b: complex): complex; begin result := a * b; end;
function tffDivC(a, b: complex): complex; begin result := a / b; end;

////////////////////////////////////////////////////////////////////////////////
//// Multi Dimensional Counter /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

type
    TMDC = record
        step: TIntegers;
        size: TIntegers;
        idx: TIntegers;
        procedure Init(_step, _size: TIntegers; excl: integer);
        function Next(out offset: integer): boolean;
    end;


procedure TMDC.Init(_step, _size: TIntegers; excl: integer);
var d: integer;
begin
    step := _step;
    size := _size;
    size[excl] := 1;

    SetLength(idx, Length(_size));
    idx[0] := -1;
    for d := 1 to high(idx) do idx[d] := 0;
end;


function TMDC.Next(out offset: integer): boolean;
var d: integer;
begin
    Inc(idx[0]);
    for d := 0 to high(idx)-1 do
        if idx[d]>=size[d]
        then
            begin
                Inc(idx[d+1]);
                idx[d] := 0
            end;
    result := not (idx[high(idx)]>=size[high(idx)]);
    offset := dot(step, idx);
end;

////////////////////////////////////////////////////////////////////////////////
//// Tensor ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function tensor_new(_e: TDoubles): TTensor;
begin
    result.dim := TIntegers.Create(1, Length(_e));
    result.e := _e;
end;


function tensor_new(_e: TDoubles; w: integer): TTensor;
begin
//функция для совместимости с матрицами. Убрать после переделки
    result.dim := TIntegers.Create(1, w, w, Length(_e) div w);
    result.e := _e;
end;


function tensor_new(_e: TComplexes): TTensor;
var i: integer;
begin
    result.dim := TIntegers.Create(2, Length(_e));
    SetLength(result.e, Length(_e)*2);
    for i := 0 to high(_e) do
        PCOMPLEX(@(result.e[i*2]))^ := _e[i];
end;


function tensor_new(_e: TComplexes; w: integer): TTensor;
var i: integer;
begin
//функция для совместимости с матрицами. Убрать после переделки
    result.dim := TIntegers.Create(2, 2*w, w, Length(_e) div w);
    SetLength(result.e, Length(_e)*2);
    for i := 0 to high(_e) do
        PCOMPLEX(@(result.e[i*2]))^ := _e[i];
end;


//из тензоров одинаковой геометрии собирается тензор на одну размерность больше
function tensor_stack(tensors: TTensors): TTensor;
var t, d, dc, i: integer;
begin
    if Length(tensors)=0 then raise ETO.Create('Empty argument');

    for t := 1 to high(tensors) do
        if not integers_equal(tensors[0].dim, tensors[t].dim)
        then raise ETO.Create('Unrectangular');

    //установка размерности +1 к исходным
    dc := tensors[0].dims_count+1;
    SetLength(result.dim, 2*dc);
    for d := 0 to dc-2 do result.dim[d] := tensors[0].dim[d];
    result.dim[dc-1] := Length(tensors[0].e);
    for d := dc to 2*dc-2 do result.dim[d] := tensors[0].dim[d-1];
    result.dim[2*dc-1] := Length(tensors);

    SetLength(result.e, Length(tensors[0].e) * Length(tensors));
    for t := 0 to high(tensors) do
        for i := 0 to high(tensors[t].e) do
            result.e[t*Length(tensors[0].e)+i] := tensors[t].e[i];
end;


//из тензоров одинаковой геометрии (по измерениям кроме последнего)
//собирается тензор той же размерности
function tensor_concatenate(tensors: TTensors): TTensor;
var _dim: TIntegers; t, d, size, b: integer;
    function eq(a: TIntegers): boolean;
    var d: integer;
    begin
        if Length(a)<>Length(_dim) then Exit(false);
        for d := 0 to high(_dim)-1 do if a[d]<>_dim[d] then Exit(false);
        result := true;
    end;
begin
    if Length(tensors)=0 then raise ETO.Create('Empty argument');
    _dim := Copy(tensors[0].dim);
    for t := 1 to high(tensors) do
        if not eq(tensors[t].dim)
        then raise ETO.Create('Unrectangular')
        else _dim[high(_dim)] := _dim[high(_dim)] + tensors[t].dim[high(tensors[t].dim)];

    result.dim := _dim;

    size := _dim[0];
    for d := Length(_dim) div 2 to high(_dim) do size := size * _dim[d];
    SetLength(result.e, size);

    b := 0;
    for t := 0 to high(tensors) do
        begin
            Move(tensors[t].e[0], result.e[b], SizeOf(tensors[t].e[0])*Length(tensors[t].e));
            Inc(b, Length(tensors[t].e));
    	end;
end;


//при неизменном содержимом тензор получает дополнительную размерность
function tensor_add_dim(t: TTensor): TTensor;
var _dim: TIntegers;
begin
    _dim := t.size;
    SetLength(_dim, Length(_dim)+1);
    _dim[high(_dim)] := 1;
    result.init(_dim, t.dim[0]);
    result.e := t.e;
end;

//при неизмерном содержимом из тензора удаляются плоские измерения
function tensor_flat(t: TTensor): TTensor;
var _dim: TIntegers; d: integer;
begin
    _dim := t.size;
    for d := high(_dim) downto 0 do
        if _dim[d]=1 then integers_extract(_dim, d);

    result.Init(_dim, t.dim[0]);
    result.e := t.e;
end;


function tensor_split(t: TTensor; d: integer): TTensors;
var subsize, stripsize, i, j, offset, step: integer; _dim: TIntegers;
begin
    if (d<0) or (d>=t.dims_count) then raise ETO.Create('Dimension out of range '+IntToStr(d));

    SetLength(result, t.size()[d]);

    _dim := t.size();
    integers_extract(_dim, d);
    result[0].Init(_dim, t.dim[0]);
    for j := 1 to high(result) do result[j].dim := result[0].dim;

    subsize := Length(t.e) div t.size()[d];
    for j := 0 to high(result) do SetLength(result[j].e, subsize);

    for j := 0 to high(result) do //по результирующим "слоям"
        begin
            offset := j * t.dim[d];
            stripsize := t.dim[d];
            step := t.dim[d] * t.size()[d];
            for i := 0 to Length(t.e) div step - 1 do //по отрезкам
                Move(t.e[offset + i*step], result[j].e[i*stripsize], SizeOf(t.e[0])*stripsize);
    	end;
end;


function tensor_to_complex(t: TTensor): TTensor;
var i: integer;
begin
    if t.dim[0]=2 then Exit(t);

    result.dim := integers_mul(t.step(), 2);
    append_integers(result.dim, t.size());
    SetLength(result.e, 2*Length(t.e));
    for i := 0 to high(t.e) do
        begin
            result.e[2*i+0] := t.e[i];
            result.e[2*i+1] := 0;
    	end;
end;


function tensor_flat_dim0(t: TTensor): TTensor;
begin
    result.dim := integers_div(t.step(), t.dim[0]);
    append_integers(result.dim, t.size());

    SetLength(result.e, Length(t.e) div t.dim[0]);
end;


function tensor_re(t: TTensor): TTensor;
var i: integer;
begin
    if t.dim[0]=1 then Exit(t);

    result := tensor_flat_dim0(t);
    for i := 0 to high(result.e) do result.e[i] := t.e[2*i];
end;


function tensor_im(t: TTensor): TTensor;
var i: integer;
begin
    result := tensor_flat_dim0(t);

    if t.dim[0]=1
    then for i := 0 to high(result.e) do result.e[i] := 0
    else for i := 0 to high(result.e) do result.e[i] := t.e[2*i+1];
end;


function tensor_abs_complex(t: TTensor): TTensor;
var i: integer;
begin
    result := tensor_flat_dim0(t);
    for i := 0 to high(result.e) do
        result.e[i] := cmod(PCOMPLEX(@(t.e[2*i]))^);
end;


function tensor_abs(t: TTensor): TTensor;
var i: integer;
begin
    if t.dim[0]=2 then Exit(tensor_abs_complex(t));

    result.dim := t.dim;
    SetLength(result.e, Length(t.e));
    for i := 0 to high(t.e) do result.e[i] := abs(t.e[i]);
end;


function tensor_arg_complex(t: TTensor): TTensor;
var i: integer;
begin
    result := tensor_flat_dim0(t);
    for i := 0 to high(result.e) do
        result.e[i] := carg(PCOMPLEX(@(t.e[2*i]))^);
end;


function tensor_arg(t: TTensor): TTensor;
var i: integer;
begin
    if t.dim[0]=2 then Exit(tensor_arg_complex(t));

    result.dim := t.dim;
    SetLength(result.e, Length(t.e));
    for i := 0 to high(t.e) do
        if t.e[i]>=0 then result.e[i] := 0 else result.e[i] := pi;
end;


function tensor_radtodeg(a: TTensor): TTensor;
var i: integer;
begin
    if a.dim[0]<>1 then ETO.Create('tensor/complex argument');

    result.dim := a.dim;
    SetLength(result.e, Length(a.e));
    for i := 0 to high(a.e) do result.e[i] := math.radtodeg(a.e[i]);
end;


function tensor_noisy_complex(t: TTensor; m, a: real): TTensor;
var i: integer;
begin
    result.dim := t.dim;
    SetLength(result.e, Length(t.e));
    for i := 0 to length(result.e) div 2 - 1 do
            PCOMPLEX(@(result.e[i*2]))^ := noisy(PCOMPLEX(@(t.e[i*2]))^, m, a);
end;


function tensor_noisy(t: TTensor; m, a: real): TTensor;
begin
    if t.dim[0]=2 then Exit(tensor_noisy_complex(t, m, a));

    result.dim := t.dim;
    result.e := noisy(t.e, m, a);
end;


function tensor_round_to(t: TTensor; digits: TRoundToRange): TTensor;
begin
    result.dim := t.dim;
    result.e := doubles_round_to(t.e, digits);
end;


function tensor_equal(a, b: TTensor): boolean;
var i: integer; a_e, b_e: TDoubles;
begin
    if not same_geometry(a.dim, b.dim) then Exit(false);
    if a.e=b.e then Exit(true);

    if a.dim[0]=b.dim[0]
    then
        begin
            a_e := a.e;
            b_e := b.e;
        end
    else
        begin
            a_e := a.complex_e();
            b_e := b.complex_e();
        end;

    if Length(a_e)<>Length(b_e) then Exit(false);
    for i := 0 to high(a.e) do if a_e[i]<>b_e[i] then Exit(false);

    result := true;
end;


function tensor_fold(t: TTensor; d: integer; f: TTensorFoldFunctionR): TTensor;
var cnt: TMDC; i, offset: integer; acc: real;
begin
    cnt.Init(t.step(), t.size(), d);

    result.Init(cnt.size, 1);
    SetLength(result.e, Length(t.e) div t.size()[d]);

    while cnt.Next(offset) do
        begin
            acc := t.e[offset];
            for i := 1 to t.dim_high(d) do acc := f(acc, t.e[offset+i*t.dim[d]]);
            result.e[dot(result.dim, cnt.idx)] := acc;
        end;
end;


function tensor_subseq(t: TTensor; _s, _e: integer): TTensor;
var ln: integer;
begin
    if t.dims_count() <> 1 then ETO.Create('tensor subseq/invalid size');

    if _s<0 then _s := t.dim[1] + _s;
    if _e<0 then _e := t.dim[1] + _e;
    ln := _e-_s;
    if (_s>=t.dim[1]) or (_e>=t.dim[1]) or (_s<0) or (_e<0) or (ln<1)
    then raise ETO.Create('tensor subseq/out of range/');

    result.Init(TIntegers.Create(ln), t.dim[0]);
    SetLength(result.e, ln*t.dim[0]);
    move(t.e[_s*t.dim[0]], result.e[0], sizeof(t.e[0])*t.dim[0]*ln);
end;


//требует на 10-40% больше времени чем прямая операция
function tensor_by_element_op(a, b: TTensor;
                rop: TTensorFoldFunctionR; cop: TTensorFoldFunctionC): TTensor; overload;
var i: integer;
begin
    case common_dim(a.dim, b.dim, result.dim) of

        11: begin
            SetLength(result.e, length(a.e));
            for i := 0 to high(result.e) do
                result.e[i] := rop(a.e[i], b.e[i]);
            end;

        12: begin
            SetLength(result.e, Length(b.e));
            for i := 0 to Length(result.e) div 2 -1 do
                PCOMPLEX(@(result.e[2*i]))^ := cop(a.e[i], PCOMPLEX(@(b.e[2*i]))^);
		    end;

        21: begin
            SetLength(result.e, Length(a.e));
            for i := 0 to Length(result.e) div 2 -1 do
                PCOMPLEX(@(result.e[2*i]))^ := cop(PCOMPLEX(@(a.e[2*i]))^, b.e[i]);
		    end;

        22: begin
            SetLength(result.e, length(a.e));
            for i := 0 to Length(result.e) div 2 -1 do
                PCOMPLEX(@(result.e[2*i]))^ := cop(PCOMPLEX(@(a.e[2*i]))^, PCOMPLEX(@(b.e[2*i]))^);
		    end;
	end;
end;


function tensor_by_element_op(a: TTensor; b: complex;
                rop: TTensorFoldFunctionR; cop: TTensorFoldFunctionC): TTensor; overload;
var i: integer;
begin
    case common_dim(a.dim, b, result.dim) of

        11: begin
            SetLength(result.e, Length(a.e));
            for i := 0 to high(result.e) do
                result.e[i] := rop(a.e[i], b.re);
            end;

        12: begin
            SetLength(result.e, 2*Length(a.e));
            for i := 0 to Length(result.e) div 2 -1 do
                PCOMPLEX(@(result.e[2*i]))^ := cop(a.e[i], b);
		    end;

        21: begin
            SetLength(result.e, Length(a.e));
            for i := 0 to Length(result.e) div 2 -1 do
                PCOMPLEX(@(result.e[2*i]))^ := cop(PCOMPLEX(@(a.e[2*i]))^, b);
		    end;

        22: begin
            SetLength(result.e, Length(a.e));
            for i := 0 to Length(result.e) div 2 -1 do
                PCOMPLEX(@(result.e[2*i]))^ := cop(PCOMPLEX(@(a.e[2*i]))^, b);
		    end;
	end;
end;


//// add ///////////////////////////////////////////////////////////////////////

function tensor_add(a, b: TTensor): TTensor;
var i: integer;
begin
    case common_dim(a.dim, b.dim, result.dim) of

        11, 22: result.e := doubles_add(a.e, b.e);

        21: begin
            SetLength(result.e, Length(a.e));
            for i := 0 to Length(result.e) div 2 -1 do
                PCOMPLEX(@(result.e[2*i]))^ := PCOMPLEX(@(a.e[2*i]))^ + b.e[i];
		end;

        12: begin
            SetLength(result.e, Length(b.e));
            for i := 0 to Length(result.e) div 2 -1 do
                PCOMPLEX(@(result.e[2*i]))^ := a.e[i] + PCOMPLEX(@(b.e[2*i]))^;
		end;
	end;
end;


function tensor_add(a: TTensor; b: complex): TTensor;
var i: integer;
begin
    case common_dim(a.dim, b, result.dim) of
        11: result.e := doubles_add(a.e, b.re);
        12: begin
            SetLength(result.e, Length(a.e)*2);
            for i := 0 to high(a.e) do
                PComplex(@(result.e[2*i]))^ := a.e[i] + b;
		end;
        21, 22: begin
            SetLength(result.e, Length(a.e));
            for i := 0 to Length(a.e) div 2 -1 do
                PComplex(@(result.e[2*i]))^ := PComplex(@(a.e[2*i]))^ + b;
		end;
	end;
end;

//// mul ///////////////////////////////////////////////////////////////////////
function tensor_mul(a, b: TTensor): TTensor;
var i: integer;
begin
    case common_dim(a.dim, b.dim, result.dim) of
        11: result.e := doubles_mul(a.e, b.e);
        12: begin
            SetLength(result.e, Length(b.e));
            for i := 0 to Length(result.e) div 2 -1 do
                PCOMPLEX(@(result.e[2*i]))^ := a.e[i] * PCOMPLEX(@(b.e[2*i]))^;
		end;
        21: begin
            SetLength(result.e, Length(a.e));
            for i := 0 to Length(result.e) div 2 -1 do
                PCOMPLEX(@(result.e[2*i]))^ := PCOMPLEX(@(a.e[2*i]))^ * b.e[i];
		end;
        22: begin
            SetLength(result.e, Length(a.e));
            for i := 0 to Length(result.e) div 2 -1 do
                PCOMPLEX(@(result.e[2*i]))^ := PCOMPLEX(@(a.e[2*i]))^ * PCOMPLEX(@(b.e[2*i]))^;
		end;
	end;
end;


function tensor_mul(a: TTensor; b: complex): TTensor;
var i: integer;
begin
    case common_dim(a.dim, b, result.dim) of
        11: result.e := doubles_mul(a.e, b.re);
        12: begin
            SetLength(result.e, Length(a.e)*2);
            for i := 0 to high(a.e) do
                PComplex(@(result.e[2*i]))^ := a.e[i] * b;
  		end;
        21, 22: begin
            SetLength(result.e, Length(a.e));
              for i := 0 to Length(a.e) div 2 -1 do
                  PComplex(@(result.e[2*i]))^ := PComplex(@(a.e[2*i]))^ * b;
  		end;
  	end;
end;

//// sub ///////////////////////////////////////////////////////////////////////
function tensor_sub(a, b: TTensor): TTensor;
begin
    result := tensor_by_element_op(a, b, tffSub, tffSubC);
end;


function tensor_div(a, b: TTensor): TTensor;
begin
    result := tensor_by_element_op(a, b, tffDiv, tffDivC);
end;


function tensor_div(a: TTensor; b: complex): TTensor;
begin
    result := tensor_by_element_op(a, b, tffDiv, tffDivC);
end;


{ TTensor }

function TTensor.dim_high(_d: integer): integer;
begin
    result := dim[_d+Length(dim) div 2]-1;
end;


function TTensor.size: TIntegers;
begin
    result := Copy(dim, Length(dim) div 2, Length(dim) div 2);
end;


function TTensor.size(d: integer): integer;
begin
    if d>dims_count then Exit(0);
    result := dim[Length(dim) div 2 + d -1];
end;


function TTensor.step: TIntegers;
begin
    result := Copy(dim, 0, Length(dim) div 2);
end;


function TTensor.step(d: integer): integer;
begin
    if d>dims_count then Exit(0);
    result := dim[d -1];
end;


function TTensor.dims_count: integer;
begin
    result := Length(dim) div 2;
end;


function TTensor.complex_e: TDoubles;
var i: integer;
begin
    if dim[0]=2 then Exit(e);

    SetLength(result, 2*Length(e));
    for i := 0 to high(e) do
        begin
            result[2*i+0] := e[i];
            result[2*i+1] := 0;
		end;
end;


procedure TTensor.Init(_size: TIntegers; elsize: integer);
var d, step: integer;
begin
    SetLength(dim, 2* Length(_size));
    step := elsize;
    for d := 0 to high(_size) do
        begin
            dim[d] := step;
            step := step * _size[d];
            dim[d+Length(_size)] := _size[d];
    	end;
end;


procedure TTensor.SetBufferLength;
var ln, d: integer;
begin
    ln := dim[0];
    for d in size() do ln := ln*d;
    SetLength(e, ln);
end;


procedure TTensor.Create(_dim: TIntegers; _e: TDoubles);
begin
    Init(_dim, 1);
    e := _e;
end;


procedure TTensor.Create(_dim: TIntegers; _e: TComplexes);
var i: integer;
begin
    Init(_dim, 2);
    SetLength(e, 2*Length(_e));
    for i := 0 to high(_e) do
        begin
            e[i*2+0] := _e[i].re;
            e[i*2+1] := _e[i].im;
    	end;
end;


function TTensor.GetR(idx: TIntegers): double;
begin
    if Length(idx)<>(Length(dim) div 2) then raise ETO.Create('Invalid index');
    result := e[dot(dim, idx)];
end;


function TTensor.GetC(idx: TIntegers): COMPLEX;
var n: integer;
begin
    if Length(idx)<>(Length(dim) div 2) then raise ETO.Create('Invalid index');
    n := dot(dim, idx);
    if dim[0]=2
    then result := cinit(e[n], e[n+1])
    else result := e[n];
end;


function TTensor.GetC(idx: integer): COMPLEX;
begin
    if dim[0]=2
    then result := cinit(e[2*idx], e[2*idx+1])
    else result := e[2*idx];
end;


function TTensor.Complexes: TComplexes;
var i: integer;
begin
    if dim[0]=1 then Exit(doubles_to_complexes(e));

    SetLength(result, Length(e) div 2);
    for i := 0 to high(result) do
        result[i] := PCOMPLEX(@(e[i*2]))^;
end;


function TTensor.str: unicodestring;

    function cmp_str(re, im: real): unicodestring;
    begin
        result := FloatToStr(re);
        if im<0  then result := result+'-i'+FloatToStr(-im) else
        if im>=0 then result := result+'+i'+FloatToStr(+im);
    end;

    function str_row(offset, d: integer): unicodestring;
    var i: integer;
    begin
        result := '';
        for i := 0 to dim_high(d) do
            if dim[0]=1
            then result := result + '  ' + FloatToStr(e[offset+i*dim[d]])
            else result := result + '  ' + cmp_str(e[offset+i*dim[d]], e[offset+i*dim[d]+1]);
        result := result + ' |'
    end;

    function str_sub(offset, d: integer): unicodestring;
    var i: integer;
    begin
        if d=0
        then
            result := str_row(offset, 0)
        else
            begin
                result := '';
                for i := 0 to dim_high(d) do
                    result := result + str_sub(offset + i*dim[d], d-1);
                result := result + ' |';
        	end;
    end;

begin
    result := str_sub(0, dims_count-1);
end;



end.

