﻿unit lisya_sign;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils
    ,dlisp_values
    ,lisya_predicates
    ,mar
    ,lisya_symbols
    ,lisya_exceptions;



function ifh_build_sign(sign: TVList): TSignature;
function sign_to_list(sign: TSignature): TVList;
function usign_AsString(sign: TSignature): unicodestring;

var unary_sign: TSignature;


procedure ifh_bind_params(sign: TSignature; PL: TValues;
                            out params: TValues; out rest: TValues);
procedure delete_from_sign(var sign: TSignature; n: integer);

function plain_sign(S: TSignature): TSignature;



implementation


procedure error(msg: unicodestring; V: TValue = nil);
begin
    if V <> nil
    then raise ELE.Create(msg+ ' ('+V.AsString+')', 'syntax/subprogram/parameters')
    else raise ELE.Create(msg, 'syntax/subprogram/parameters');
end;


procedure unique(names: TIntegers; e: TVSymbol);
var i: integer;
begin
    for i := 0 to high(names) do
        if e.N=names[i] then error('duplicated signature element', e);
    append_integer(names,e.N);
end;


function select_param_mode(V: TValue): TSubprogramParmetersMode;
begin
    if vpKeyword_OPTIONAL(V) then result := spmOpt  else
    if vpKeyword_KEY(V)      then result := spmKey  else
    if vpKeyword_REST(V)     then result := spmRest else
    if vpOperator_VAR(V)     then result := spmVar  else
    error('invalid parameter mode', V);
end;


////////////////////////////////////////////////////////////////////////////////
//// USIGN /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


procedure h_error(main, cl: unicodestring; msg: unicodestring=''; V :TValue=nil);
var sV: unicodestring;
begin
    if V =nil then sV := '' else sV := ' - '+V.AsString;
    raise ELE.Create(msg+sV, main+'/'+cl);
end;


procedure p_error(cl: unicodestring; msg: unicodestring=''; V: TValue=nil);
begin
    h_error('syntax/subprogram/parameters', cl, msg, V);
end;


function ifh_build_sign(sign: TVList): TSignature;
var i: integer; _mode: TSubprogramParmetersMode;
    procedure add();
    var j, n: integer;
    begin
        n := (sign[i] as TVSYmbol).N;
        for j := 1 to high(result) do
            if result[j].name = n then p_error('duplicated', symbol_uname(n));
        SetLength(result, Length(result)+1);
        with result[high(result)] do
            begin
                name := n;
                case _mode of
                    spmReq, spmVar: req_mode := _mode;
                    spmKey: key := symbol_n(':'+symbol_uname(name));
                    else key := 0;
				end;
            end;
	end;
begin
    SetLength(result, 1);
    result[0].req_count := 0;

    if sign=nil then Exit;

    _mode := spmReq;
    for i := 0 to sign.high do
        case _mode of
            spmReq:
                if tpName(sign[i])
                then
                    begin
                        add();
                        Inc(result[0].req_count);
					end
				else
                    _mode := select_param_mode(sign[i]);

            spmVar:
                if tpName(sign[i])
                then
                    begin
                        add();
                        _mode := spmReq;
                        Inc(result[0].req_count);
					end
                else
                    p_error('invalid variable', '', sign[i]);

            spmOpt, spmKey:
                if tpName(sign[i])
                then
                    add()
                else
                    p_error('unexpected mode', '', sign[i]);

            spmRest:
                if tpName(sign[i]) and (i = sign.high)
                then
                    add()
                else
                    p_error('malformed rest', '', sign);
		end;

    result[0].mode := _mode;

    if (_mode<>spmReq) and (result[0].req_count=Length(result)-1)
    then
        p_error('missing non required', '', sign);
end;


procedure a_error(cl: unicodestring; msg: unicodestring=''; V: TValue=nil);
begin
    h_error('subprogram/arguments', cl, msg, V);
end;


procedure ifh_bind_params(sign: TSignature; PL: TValues;
                            out params: TValues; out rest: TValues);
    //PL должен иметь нулевой элемент, не являющийся параметром
    //игнорирует лишние ключи
    function find_by_key(key: integer): TValue;
    var k: integer;
    begin
        result := nil;
        k := high(PL)-1;
        while k>sign[0].req_count do
            if tpKeyword(PL[k]) and (N_(PL[k])=key)
            then Exit(PL[k+1])
            else Dec(k, 2);
    end;
var i: integer;
begin
    SetLength(params, Length(sign)-1);
    rest := nil;

    if High(PL)<sign[0].req_count then a_error('not enought');

    for i := 1 to sign[0].req_count do
        begin
            params[i-1] := PL[i];
            if (sign[i].req_mode=spmVar) and not vpPointerToVar(PL[i])
            then
                a_error('not a variable', symbol_uname(sign[i].name));
		end;

    case sign[0].mode of
        spmReq, spmList:
            if Length(PL)>Length(sign) then a_error('too many');
        spmOpt:
            begin
                if Length(PL)>Length(sign) then a_error('too many');

    			for i := sign[0].req_count+1 to high(sign) do
                    if i<Length(PL)
                    then
                        params[i-1] := PL[i]
                    else
                        params[i-1] := nil;
			end;
        spmKey:
            for i := sign[0].req_count+1 to high(sign) do
                params[i-1] := find_by_key(sign[i].key);
        spmRest:
            begin
                SetLength(rest, Length(PL)-1-sign[0].req_count);
                params[high(params)] := nil;
                for i := sign[0].req_count+1 to high(PL) do
                    rest[i-sign[0].req_count-1] := PL[i];
			end;
	end;
end;


function sign_to_list(sign: TSignature): TVList;
var i: integer;
begin
    result := TVList.Create;

    for i := 1 to sign[0].req_count do
        begin
            if sign[i].req_mode=spmVar then result.Add(TVOperator.Create(oeVAR));
            result.Add(TVSymbol.Create(sign[i].name));
		end;

    case sign[0].mode of
        spmOpt: result.Add(TVSymbol.Create(':OPTIONAL'));
        spmKey: result.Add(TVSymbol.Create(':KEY'));
        spmRest: result.Add(TVSymbol.Create(':REST'));
	end;

    for i := sign[0].req_count+1 to high(sign) do
        result.Add(TVSymbol.Create(sign[i].name));
end;


procedure delete_from_sign(var sign: TSignature; n: integer);
var i: integer;
begin
    for i := n+1 to high(sign) do sign[i-1] := sign[i];
    SetLength(sign, Length(sign)-1);
    if n<=sign[0].req_count then Dec(sign[0].req_count);
    if Length(sign)=sign[0].req_count then sign[0].mode := spmReq;
end;


function plain_sign(S: TSignature): TSignature;
var i: integer;
begin
    result := copy(S);
    result[0].mode := spmReq;
    result[0].req_count := Length(result)-1;
    for i := 1 to high(result) do result[i].req_mode := spmReq;
end;


function usign_AsString(sign: TSignature): unicodestring;
var i: integer;
begin
    result := '(';

    for i := 1 to sign[0].req_count do
        begin
            if sign[i].req_mode=spmVar then result := result + 'VAR ';
            result := result + symbol_uname(sign[i].name) + ' ';
		end;

    case sign[0].mode of
        spmOpt: result := result + ':OPTIONAL ';
        spmKey: result := result + ':KEY ';
        spmRest: result := result + ':REST ';
	end;

    for i := sign[0].req_count+1 to high(sign) do
        result := result + symbol_uname(sign[i].name) + ' ';

    if result[Length(result)] = ' '
    then result[Length(result)] := ')'
    else result := result+')';
end;



initialization
    SetLength(unary_sign, 2);
    unary_sign[0].mode := spmReq;
    unary_sign[0].req_count := 1;
    unary_sign[1].name := symbol_n('X');
    unary_sign[1].key := 0;
end.

