﻿unit dlisp_values;

{$IFDEF FPC} {$MODE Delphi} {$ENDIF}

{$ASSERTIONS ON}


interface

uses
    {$IFDEF LINUX}
    cwstring,
    {$ENDIF}
    SysUtils, Classes, Contnrs, ucomplex, crc, math,
    lisia_charset, mar, mar_math_arrays, mar_tensor, lisya_zip, lisya_exceptions, lisya_streams

    ,lisya_symbols
    ,lisya_protected_objects
    ,lisya_sql
    ,lisya_tkz_solver_poc
    ;


type

    { TValue }

    TValue = class
        function Copy: TValue; virtual; abstract;
        function AsString: unicodestring; virtual;
        function Description: unicodestring; virtual;
        function hash: DWORD; virtual;
        function equal({%H-}V: TValue): boolean; virtual;
        //procedure Release; virtual;
        //procedure Free;
    end;
    TValues = array of TValue;

    TEvalProc = function (V: TValue): TValue of object;

    { TVInternal }

    TVInternal = class (TValue)
        function Copy: TValue; override;
        function equal({%H-}V: TValue): boolean; override;
    end;

    { TVEndOfStream }

    TVEndOfStream = class (TValue)
        function Copy(): TValue; override;
    end;


    { TVariable }

    TVariable = record
        V: TValue;
        constant: boolean;
        ref_count: integer;
        tree: boolean;
    end;
    PVariable = ^TVariable;
    PPVariable = ^PVariable;
    PVariables = array of PVariable;
    PPVariables = ^PVariables;


    TStackRecord = record
        V: PVariable;
        N: integer;
    end;


    { TVNumber }

    TVNumber = class (TValue)
        function C: COMPLEX; virtual; abstract;
        function equal(V: TValue): boolean; override;
    end;

    { TVComplex }

    TVComplex = class (TVNumber)
        fC: COMPLEX;
        function C: COMPLEX; override;
        constructor Create(re, im: double); overload;
        constructor Create(_c: COMPLEX); overload;
        function Copy: TValue; override;
        function AsString: unicodestring; override;
        destructor Destroy; override;
        function hash: DWORD; override;
    end;

    { TVReal }

    TVReal = class (TVNumber)
        function F: double; virtual; abstract;
        function equal(V: TValue): boolean; override;
    end;


    { TVInteger }

    TVInteger = class (TVReal)
        fI: Int64;
        constructor Create(I: Int64);
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;

        function F: double; override;
        function C: COMPLEX; override;
    end;


    { TVFloat }

    TVFloat = class (TVReal)
        fF: double;
        constructor Create(F: double);
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;

        function F: double; override;
        function C: COMPLEX; override;
    end;


    { TVRange }

    TVRange = class (TValue)
        function f_low(): double; virtual; abstract;
        function f_high(): double; virtual; abstract;
        function f_length(): double; virtual; abstract;
        function equal(V: TValue): boolean; override;
    end;


    { TVRangeFloat }

    TVRangeFloat = class (TVRange)
        low, high: double;
        constructor Create(l,h: double);
        function f_low(): double; override;
        function f_high(): double; override;
        function f_length(): double; override;
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;
    end;


    { TVRangeInteger }

    TVRangeInteger = class (TVRange)
        low, high: Int64;
        constructor Create(l,h: Int64);
        function f_low(): double; override;
        function f_high(): double; override;
        function f_length(): double; override;
        function Count(): integer;
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;
    end;

    { TVTime }

    TVTime = class (TValue)
        fDT: TDateTime;
        function equal(V: TValue): boolean; override;

        //function year: integer;
        //function month: integer;
        //function day: integer;
        //function hours: integer;
        //function minutes: integer;
        //function seconds: integer;
        //function milliseconds: integer;
    end;

    { TVTimeInterval }

    TVDuration = class (TVTime)
        constructor Create(dt: TDateTime);
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;
    end;

    { TVDateTime }

    TVDateTime = class (TVTime)
        constructor Create(dt: TDateTime);
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;

        function AsSQLDateTime: unicodestring;
    end;




    { TVSymbol }

    TVSymbol = class (TValue)
    private
        fN: integer;
        fname: unicodestring;
        function fGetUname: unicodestring;
    public
        property name: unicodestring read fname;
        property uname: unicodestring read fGetUname;
        property N: integer read fN;

        constructor Create(S: unicodestring); overload;
        constructor Create(N: integer); overload;
        constructor Gensym;
        constructor CreateEmpty;
        destructor Destroy; override;
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;
    end;


    { TVKeyword }

    TVKeyword = class (TVSymbol)
        constructor CreateCopy(origin: TVKeyword);
        function Copy: TValue; override;
    end;

    { TVGo }

    TVGo = class (TVInternal)
    end;


    { TVGoto }

    TVGoto = class (TVGo)
        n: integer;
        constructor Create(_n: integer);
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;
    end;


    { TVBreak }

    TVBreak = class (TVGo)
        function hash: DWORD; override;
    end;


    { TVContinue }

    TVContinue = class (TVGo)
        function hash: DWORD; override;
    end;


    { TVReturn }

    TVReturn = class (TVGo)
        value: TValue;
        constructor Create(V: TValue);
        destructor Destroy; override;
        function Copy: TValue; override;
        function AsString: unicodestring; override;
        function hash: DWORD; override;

        function Unpack(): TValue;
    end;


    { TVChainPointer }
    TVCompound = class;
    TVList = class;

    TCallProc = function (V: TValues): TValue of object;

    TVChainPointer = class (TVInternal)
        P: PVariable;
        index: TIntegers;

        constructor Create(_P: PVariable); overload;
        constructor Create(_P: PVariable; indices: array of integer); overload;
        destructor Destroy; override;
        function Copy: TValue; override;
        function AsString: unicodestring; override;
        function hash: DWORD; override;

        function get_link: PPVariable;
        function constant: boolean;

        procedure add_index(i: integer);
        procedure set_last_index(i: integer);

        procedure EvalLink(selectors: TValues; call: TCallProc);
        function Penultimate(): TVCompound;
        function Target(): TValue;
        function TargetForModification(): TValue;
        procedure SetTarget(_V: TValue);
        function GetTarget(): TValue;
    end;


    {TVCompound}

    TVCompound = class (TValue)
    private
        function GetItem(index: integer): TValue; virtual; abstract;
        procedure SetItem(index: integer; _V: TValue); virtual; abstract;
        function Item(index: integer): TValue; virtual;
    public
        function Get(index: integer): TValue; overload;
        function Get(id: TValue): TValue; overload;
        property items1[index: integer]: TValue read Item write SetItem; default;
        function Count: integer; virtual; abstract;

        function Index(id: TValue): integer; virtual; abstract;
        procedure CopyOnWrite(); virtual;
    end;


    { TVSequence }

    TVSequence = class (TVCompound)
    protected
        function checked_index(i: integer): integer;
    public
        function high: integer;
        function subseq(istart: integer; iend: integer): TValue; virtual; abstract;
        function tail(istart: integer): TValue;
        function index(i: integer): integer; overload;
        function Index(id: TValue): integer; override; overload;
    end;

    { TVCompoundOfPrimitive }

	{ TVSequenceOfPrimitive }

    TVSequenceOfPrimitive = class (TVSequence)
        function Item({%H-}index: integer): TValue; override;
    end;


    { TVString }

    TVString = class (TVSequenceOfPrimitive)
    private
        function GetItem(index: integer): TValue; override;
        procedure SetItem(index: integer; _V: TValue); override;
    public
        S: unicodestring;
        constructor Create(S: unicodestring);
        destructor Destroy; override;
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;

        function Count: integer; override;
        function crc32: DWORD;

        function subseq(istart: integer; iend: integer): TValue; override;
        procedure Append(_s: TVString);
    end;


    { TVList }

    { TVListBody }

	{ TVListBase }

    TVListBase = class (TValue)
        V: TValues;
        function hash: DWORD; override;
        function equal({%H-}V: TValue): boolean; override;
	end;

	{ TVListRest }

    TVListRest = class (TVListBase)
    private
        procedure SetItem(Index: integer; _v: TValue);
        function GetItem(Index: integer): TValue;
    public
        property Items[Index: integer]: TValue read GetItem write SetItem; default;
        property Head: TValue index 0 read GetItem write SetItem;
        procedure Add(_v: TValue);
        constructor Create(VL: TValues);
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
	end;

    TVListBody = class (TVListBase)
        constructor Create();
        destructor Destroy; override;
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
    end;

    TVList = class (TVSequence)
    private
        fL: TVListBody;
        P: PVariable;
        function GetItem(index: integer): TValue; override;
        procedure SetItem(index: integer; _V: TValue); override;
        function Item(index: integer): TValue; override;

        function GetElementName(index: integer): unicodestring;
        function GetElementUName(index: integer): unicodestring;
        function GetElementI(index: integer): Int64;
        function GetElementF(index: integer): double;
        function GetElementS(index: integer): unicodestring;
        function GetElementL(index: integer): TVList;
        function GetElementC(index: integer): COMPLEX;
        function LookElementSYM(index: integer): TVSymbol;
    public
        constructor Create; overload;
        constructor Create(VL: array of TValue); overload;
        constructor CreateCopy(VL: array of TValue);
        constructor Create(body: PVariable); overload;
        destructor Destroy; override;
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;

        function Count: integer; override;

        function subseq(istart: integer; iend: integer): TValue; override;

        function copy_on_write: boolean;
        procedure restore_fast_link;
        procedure Add(V: TValue);
        procedure Append(VL: TVList); overload;
        procedure Append(VL: TValues); overload;


        property name[index: integer]: unicodestring read GetElementName;
        property uname[index: integer]: unicodestring read GetElementUName;
        property I[index: integer]: Int64 read GetElementI;
        property F[index: integer]: double read GetElementF;
        property S[index: integer]: unicodestring read GetElementS;
        property L[index: integer]: TVList read GetElementL;
        property C[index: integer]: COMPLEX read GetElementC;
        property SYM[index: integer]: TVSymbol read LookElementSYM;

        function extract(n: integer): TValue;
        function extract_subseq(_from, _to: integer): TVList;
        function GetValue(n: integer): TValue;
        procedure insert(n: integer; V: TValue);
        procedure Clear;
        function ValueList: TValues;
        function CdrValueList: TValues;
        function CAR: TValue;
        function CDR: TVList;
        function get_body_link: PPVariable;

        function KeyIndex(id: TValue): integer;
        function Index(id: TValue): integer; override;
        procedure CopyOnWrite(); override;
    end;


    { TVBytes }

    TVBytes = class (TVSequenceOfPrimitive)
    private
        function GetByte(Index: Integer): Int64;
        procedure SetByte(Index: Integer; V: Int64);

        function GetItem(index: integer): TValue; override;
        procedure SetItem(index: integer; _V: TValue); override;

    public
        fBytes: TBytes;

        constructor Create; overload;
        destructor Destroy; override;
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;

        property bytes[Index: integer]: Int64 read GetByte write SetByte; default;
        procedure SetCount(l: integer);
        procedure Add(b: Int64);

        function crc32: DWORD;

        function count: integer; override;
        function subseq(istart: integer; iend: integer): TValue; override;
        procedure append(BV: TVBytes);
    end;


    { TVRecord }

    TVRecord = class (TVCompound)
    private
        unames: array of integer;
        slots: TObjectList;
        function fGetSlot(index: unicodestring): TValue;
        procedure fSetSlot(index: unicodestring; V: TValue);
        function flook(index: unicodestring): TValue;

        function GetItem(index: integer): TValue; override;
        procedure SetItem(index: integer; _V: TValue); override;
        function Item(index: integer): TValue; override;
    public
        constructor Create(names: array of unicodestring); overload;

        constructor Create; overload;
        destructor Destroy; override;
        function Copy: TValue; override;
        function AsString: unicodestring; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;

        function GetSlot(nN: integer): TValue;
        procedure SetSlot(nN: integer; V: TValue);
        function LookSlot(nN: integer): TValue;

        property slot[nN: integer]:TValue read GetSlot write SetSlot;
        property look_name[n: unicodestring]: TValue read flook;
        function is_class(cl: TVRecord): boolean;
        procedure AddSlot(nN: integer; V: TValue); overload;
        procedure AddSlot(name: unicodestring; V: TValue); overload;
        procedure AddSlot(name: TVSymbol; V: TValue); overload;

        function count: integer; override;
        function name_n(n: integer): unicodestring;
        //TODO: сравнение записей должно быть независимым от порядка слотов
        function index_of(nN: integer): integer;
        function get_n_of(index: unicodestring): integer;

        function Index(id: TValue): integer; override;
    end;



    { TVHashTable }
    THashTableIndex = array of record h: DWORD; k:integer; end;

    //квадратичная зависимость от количества элементов
    TVHashTable = class (TVCompound)
    private
        data: TVList;
        index_table: THashTableIndex;
        keys: TVList;
        last_key: TValue;
        function GetItem(index: integer): TValue; override;
        procedure SetItem(index: integer; _V: TValue); override;
        function Item(index: integer): TValue; override;
        function LookKey(index: integer): TValue;

        procedure Expand;
        function FindEmpty(h: DWORD): integer;
        function find_key(_key: TValue): integer;
        function find_or_add_key(_key: TValue): integer;
        function store_last_key(): integer;
    public
        constructor Create;
        constructor CreateEmpty;
        destructor Destroy; override;
        function AsString: unicodestring; override;
        function Copy: TValue; override;
        function hash: DWORD; override;
        function equal({%H-}V: TValue): boolean; override;
        procedure print;

        procedure copy_on_write;
        procedure CopyKeys;
        property look_key[index: integer]: TValue read LookKey;
        property data_list: TVList read data;
        property keys_list: TVList read keys;

        function GetKeys: TVList;

        function Count: integer; override;

        function Index(id: TValue): integer; override;
        procedure CopyOnWrite(); override;
    end;


    { TVTensor }

    TVTensor = class(TValue)
        t: TTensor;
        constructor Create(_dims: TIntegers; _e: TDoubles); overload;
        constructor Create(_e: TDoubles); overload;
        constructor Create(_e: TComplexes); overload;
        constructor Create(_dims: TIntegers; _e: TComplexes); overload;
        constructor Create(_t: TTensor); overload;
        constructor CreateReal(_size: TIntegers; _e: TComplexes);
        destructor Destroy; override;

        function Copy: TValue; override;
        function AsString: unicodestring; override;
        function Description: unicodestring; override;
        function hash: DWORD; override;
        function equal({%H-}V: TValue): boolean; override;
    end;


    { TVSymbolStack }

    TVSymbolStack = class (TValue)
        parent: TVSymbolStack;
        stack: array of TStackRecord;

        function index_of(name: unicodestring): integer; overload;
        function index_of(n: integer): integer; overload;

        constructor Create;
        destructor Destroy; override;
        function Copy: TValue; override;
        function equal(V: TValue): boolean; override;

        function Count: integer;

        procedure Print(n: integer = -1); overload;

        procedure clear_frame(n: integer);
        procedure clear_top;

        function new_var(N: integer; V: TValue; c: boolean = false): PVariable; overload;
        function new_var(symbol: TVSymbol; V: TValue; c: boolean = false): PVariable; overload;
        procedure new_const(name: unicodestring; V: TValue); overload;
        procedure new_const(N: integer; V: TValue); overload;

        function find_var(symbol: TVSymbol): TValue; overload;
        function find_var(N: integer): TValue; overload;
        procedure set_var(symbol: TVSymbol; V: TValue);

        function get_ref(symbol: TVSymbol): PVariable; overload; //TODO: возможно лишнее
        function get_ref(N: integer): PVariable; overload;
        function look_var(N: integer): PVariable;
        procedure new_ref(name: unicodestring; P: PVariable); overload;
        procedure new_ref(symbol: TVSymbol; P: PVariable); overload;
        procedure new_ref(nN: integer; P: PVariable); overload;

        function look_var_or_nil(nN: integer): PVariable;
    end;


    { TVSubprogram }

    TSubprogramParmetersMode = (spmReq, spmOpt, spmKey, spmRest, spmVar, spmList);

    //первый элемент содержит режим передачи и количество обязательных элементов
    //остальные - только имена [и ключи] элементов
    TSignature = array of
        record case byte of
            0: (mode: TSubprogramParmetersMode;
                req_count: integer);
            1: (name: integer;
                case byte of
                    0: (req_mode: TSubprogramParmetersMode);
                    1: (key: integer););
        end;

    TVSubprogram = class (TValue)
        nN: integer;
        function AsString: unicodestring; override;
    end;


    { TVProcedureForwardDeclaration }

    TVProcedureForwardDeclaration = class (TValue)
        function Copy(): TValue; override;
    end;


    { TVFunctionForwardDeclaration }

    TVFunctionForwardDeclaration = class (TValue)
        function Copy(): TValue; override;
    end;


    TVSubprogramComplex = class (TVSubprogram)
        sign: TSignature;
	end;


    { TVRoutine }

    TVRoutine = class (TVSubprogramComplex)
        stack: TVSymbolStack;
        rest: TVList;
        body: TVList;
        constructor Create; overload;
        constructor Create(_n: integer; _sign, _body: TVList); overload;
        destructor Destroy; override;
        function Copy(): TValue; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;
        function AsString: unicodestring; override;
    end;


    TVProcedure = class (TVRoutine)
    end;


    TVFunction = class (TVRoutine)
    end;


    { TVMacro }

    TVMacro = class (TVRoutine)
        //макрос является обычной функцией за тем исключением, что
        //возвращаемый им результат выполняется в месте вызова
        //данный клас нужен, только для того, чтобы eval мог отличить макрос
        //от процедуры
    end;

    TVMacroSymbol = class (TVRoutine)
    end;



    TInternalFunctionBody = function (const PL: TVList; call: TCallProc): TValue;
    TInternalUnaryFunctionBody = function (const A: TValue): TValue;
    TInternalArrayFunctionBody = function (const PL: TValues; call: TCallProc): TValue;


    { TVInternalRoutine }

    TVInternalRoutine = class (TVSubprogramComplex)
    //TODO: нужно унифицировать интерфейсы внутренних функций,
        body: TInternalFunctionBody;
        constructor Create(_sign: TSignature;
                           _body: TInternalFunctionBody;
                           _nN: integer);
        destructor Destroy; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;
    end;


    { TVInternalFunction }

    TVInternalFunction = class (TVInternalRoutine)
        function Copy(): TValue; override;
    end;


    { TVInternalFunction }

    TVInternalProcedure = class (TVInternalRoutine)
        function Copy(): TValue; override;
    end;


	{ TVInternalArrayRoutine }

    TVInternalArrayRoutine = class (TVSubprogramComplex)
        body: TInternalArrayFunctionBody;
        constructor Create(_sign: TSignature;
                           _body: TInternalArrayFunctionBody;
                           _nN: integer);
        function equal(V: TValue): boolean; override;
    end;


	{ TVInternalArrayFunction }

    TVInternalArrayFunction = class (TVInternalArrayRoutine)
        function Copy(): TValue; override;
    end;

	{ TVInternalArrayProcedure }

    TVInternalArrayProcedure = class (TVInternalArrayRoutine)
        function Copy(): TValue; override;
    end;


    { TVPredicate }

    type TTypePredicate = function (V: TValue): boolean;

    TVPredicate = class (TVSubprogram)
        body: TTypePredicate;
        fAssert: boolean;
        constructor Create(name: unicodestring; _body: TTypePredicate; a: boolean); overload;
        constructor Create(_nN: integer; _body: TTypePredicate; a: boolean); overload;
        function Copy: TValue; override;
        function AsString: unicodestring; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;
    end;


    { TVInternalUnaryRoutine }

    TVInternalUnaryRoutine = class (TVSubprogram)
        body: TInternalUnaryFunctionBody;
        constructor Create(_body: TInternalUnaryFunctionBody;
                           _nN: integer);
        destructor Destroy; override;
        function AsString(): unicodestring; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;
    end;

    { TVInternalUnaryProcedure }

    TVInternalUnaryProcedure = class (TVInternalUnaryRoutine)
        function Copy(): TValue; override;
    end;

    { TVInternalUnaryFunction }

    TVInternalUnaryFunction = class (TVInternalUnaryRoutine)
        function Copy(): TValue; override;
    end;


    { TVOperator }

    type TOperatorEnum = (
            oeAND,
            oeAPPEND,      //mod
            oeASSEMBLE,
            oeBLOCK,
            oeBREAK,
            oeCASE,
            oeCOND,
            oeCONST,
            oeCONTINUE,
            oeDEBUG,
            oeDEFAULT,     //mod
            oeELT,
            oeERROR,
            oeEXECUTE_FILE,
            oeEXTRACT,     //mod
            oeFOR,
            oeFUNCTION,
            oeGOTO,
            oeIF,
            oeINSERT,      //mod
            oeKEY,
            oeLET,
            oeMACRO,
            oeMACRO_SYMBOL,
            oeOR,
            oePACKAGE,
            oePROCEDURE,
            oePUSH,         //mod
            oeQUOTE,
            oeRETURN,
            oeSET,          //mod
            oeTRY,
            oeUSE,
            oeVAR,
            oeWHEN,
            oeWHILE,
            oeWITH);

    const variable_defining_operators = [oeCONST, oeDEFAULT, oeEXECUTE_FILE,
        oeFUNCTION, oeLET, oeMACRO, oeMACRO_SYMBOL, oePROCEDURE, oeUSE, oeVAR,
        oeWITH];

    const impure_operators = [oeEXECUTE_FILE, oePACKAGE, oeWITH, oeUSE];

    const op_names: array[TOperatorEnum] of unicodestring = (
        'AND', 'APPEND', 'ASSEMBLE',
        'BLOCK', 'BREAK',
        'CASE', 'COND', 'CONST', 'CONTINUE',
        'DEBUG', 'DEFAULT',
        'ELT', 'ERROR', 'EXECUTE-FILE', 'EXTRACT',
        'FOR', 'FUNCTION',
        'GOTO',
        'IF', 'INSERT',
        'KEY',
        'LET',
        'MACRO', 'MACRO-SYMBOL',
        'OR',
        'PACKAGE', 'PROCEDURE', 'PUSH',
        'QUOTE',
        'RETURN',
        'SET',
        'TRY',
        'USE',
        'VAR',
        'WHEN', 'WHILE', 'WITH');

    type
    TVOperator = class (TVKeyword)
        op_enum: TOperatorEnum;

        constructor Create(name: unicodestring; en: TOperatorEnum); overload;
        constructor Create(_nN: integer; en: TOperatorEnum); overload;
        constructor Create(en: TOperatorEnum); overload;
        destructor Destroy; override;
        function Copy(): TValue; override;
        function AsString(): unicodestring; override;
        function equal(V: TValue): boolean; override;
    end;


    { TVZIPArchivePointer }

    TVZIPArchivePointer = class (TValue)
        Z: TZipArchive;

        constructor Create(_Z: TZipArchive);
        destructor Destroy; override;

        function Copy: TValue; override;
        function AsString: unicodestring; override;
        function hash: DWORD; override;
        function equal(V: TValue): boolean; override;

        procedure close_zip;
    end;


    { TVPointer }

    TVPointerBase = class (TValue);

    TVPointer<T: TCountingObject> = class (TVPointerBase)
        target: T;
        constructor Create(target_: T);
        destructor Destroy; override;

        function Copy: TValue; override;
        function AsString: unicodestring; override;
        function Hash: DWORD; override;
        function equal(V: TValue): boolean;override;
    end;

    TVQueue = TVPointer<TQueue>;

    TVSQL = TVPointer<TLSQL>;

    TVStream = TVPointer<TLStream>;

    TVGrid = TVPointer<TGrid>;



procedure Assign(var v1, v2: TValue);
function equal(v1, v2: TValue): boolean; inline;

function op_null(V: TValue): boolean;
function CreateTRUE(): TValue; inline;
function ValsToStr(VL: TValues): unicodestring;
function value_description(V: TValue): unicodestring;

function S_(V: TValue): unicodestring;
function I_(V: TValue): Int64;
function F_(V: TValue): real;
function C_(V: TValue): complex;
function L_(V: TValue): TVList;
function REST_(V: TValue): TValues;
function LV_(V: TValue): TValues;
function N_(V: TValue): integer;
function T_(V: TValue): TTensor;
procedure RNG_(V: TValue; out _low, _high: integer); overload;
procedure RNG_(V: TValue; out _low, _high: real); overload;



var
    NULL: TVList;
    _ : TVSymbol;
    kwFLAG, kwKEY, kwOPTIONAL, kwREST, kwTRUE: TVKeyword;


implementation

uses lisya_predicates, lisya_gc, lisya_sign;

function CreateTRUE(): TValue; inline;
begin
    result := kwTRUE.Copy;
end;


function ValsToStr(VL: TValues): unicodestring;
var i: integer;
begin
    result := '[';
    for i := 0 to high(VL) do
        if VL[i]=nil then result := result + 'nil '
        else result := result +VL[i].AsString + ' ';
    result := result + ']';
end;


function value_description(V: TValue): unicodestring;
begin
    if V=nil
    then result := '#<nil>'
    else result := V.Description;
end;


function S_(V: TValue): unicodestring;
begin
    result := (V as TVString).S;
end;


function I_(V: TValue): Int64;
begin
    result := (V as TVInteger).fI;
end;


function F_(V: TValue): real;
begin
    result := (V as TVReal).F;
end;


function C_(V: TValue): complex;
begin
    result := (V as TVNumber).C;
end;


function L_(V: TValue): TVList;
begin
    result := V as TVList;
end;

function REST_(V: TValue): TValues;
begin
    result := (V as TVListRest).V;
end;

function LV_(V: TValue): TValues;
begin
    result := (V as TVList).ValueList;
end;

function N_(V: TValue): integer;
begin
    result := (V as TVSymbol).N;
end;

function T_(V: TValue): TTensor;
begin
    result := (V as TVTensor).t;
end;

procedure RNG_(V: TValue; out _low, _high: integer);
begin
    with V as TVRangeInteger do
        begin
            _low := low;
            _high := high;
		end;
end;

procedure RNG_(V: TValue; out _low, _high: real);
begin
    with V as TVRange do
        begin
            _low := f_low;
            _high := f_high;
		end;
end;


procedure push_index(var il: TValues; V: TValue);
var L: TVList; i: integer;
begin

    if V is TVList
    then begin
        L := V as TVList;
        SetLength(il, Length(il)+L.Count);
        for i := 0 to L.high do il[high(il)-i] := L[i].Copy;
    end
    else begin
        SetLength(il, Length(il)+1);
        il[high(il)] := V.Copy;
    end;
end;


function pop_index(var il: TValues): TValue;
begin
    result := nil;
    if Length(il)=0 then raise ELE.InvalidParameters('not enought selectors');
    result := il[high(il)];
    SetLength(il, Length(il)-1);
end;


procedure Assign(var v1, v2: TValue);
begin
  v1.Free;
  v1 := v2;
  FreeAndNil(v2);
end;


function equal(v1, v2: TValue): boolean;
begin
    result := v1.equal(v2);
end;


function op_null(V: TValue): boolean;
begin
    result := (V is TVList) and ((V as TVList).count=0);
end;


function ComplexToStr(C: COMPLEX): unicodestring;
begin
    result := FloatToStr(C.re);
    if C.im<0       then result := result+'-i'+FloatToStr(-C.im)
    else if C.im>0 then result := result+'+i'+FloatToStr(+C.im);
end;


{ TVPointer }

constructor TVPointer<T>.Create(target_: T);
begin
    target := target_;
end;

destructor TVPointer<T>.Destroy;
begin
    target.release;
    inherited Destroy;
end;

function TVPointer<T>.Copy: TValue;
begin
    result := TVPointer<T>.Create(target.ref as T);
end;

//{$DEFINE P_REFS}
function TVPointer<T>.AsString: unicodestring;
begin
    result := '#<'+{$IFDEF P_REFS}IntToStr(target.refs)+'→'+{$ENDIF}target.description+'>'
end;

function TVPointer<T>.Hash: DWORD;
begin
    result := mar.PointerToQWORD(Pointer(target)) and $FFFFFFFF;
end;

function TVPointer<T>.equal(V: TValue): boolean;
begin
    result := (V is TVPointer<T>) and (target=(V as TVPointer<T>).target);
end;

{ TVSubprogram }

function TVSubprogram.AsString: unicodestring;
begin
    result := '#'+symbol_uname(nN);
end;


{ TVInternalArrayRoutine }

constructor TVInternalArrayRoutine.Create(_sign: TSignature;
			_body: TInternalArrayFunctionBody; _nN: integer);
begin
    sign := _sign;
    body := _body;
    nN := _nN;
end;

function TVInternalArrayRoutine.equal(V: TValue): boolean;
begin
    result := (V is TVInternalArrayRoutine) and (@body=@((V as TVInternalArrayRoutine).body));
end;



{ TVInternalArrayProcedure }

function TVInternalArrayProcedure.Copy: TValue;
begin
    result := TVInternalArrayProcedure.Create(sign, body, nN);
end;


{ TVInternalArrayFunction }


function TVInternalArrayFunction.Copy: TValue;
begin
    result := TVInternalArrayFunction.Create(sign, body, nN);
end;


{ TVInternalUnaryFunction }

function TVInternalUnaryFunction.Copy: TValue;
begin
    result := TVInternalUnaryFunction.Create(body,nN);
end;

{ TVInternalUnaryProcedure }

function TVInternalUnaryProcedure.Copy: TValue;
begin
    result := TVInternalUnaryProcedure.Create(body,nN);
end;

{ TVInternalUnaryRoutine }

constructor TVInternalUnaryRoutine.Create(_body: TInternalUnaryFunctionBody;
    _nN: integer);
begin
    inherited Create;
    body := _body;
    nN := _nN;
end;

destructor TVInternalUnaryRoutine.Destroy;
begin
    inherited Destroy;
end;

function TVInternalUnaryRoutine.AsString: unicodestring;
begin
    result := '#'+symbol_uname(nN);
end;

function TVInternalUnaryRoutine.hash: DWORD;
begin
    result := crc32(0, @body, SizeOf(body));
end;

function TVInternalUnaryRoutine.equal(V: TValue): boolean;
begin
    result := (V is TVInternalUnaryRoutine) and (@body=@((V as TVInternalUnaryRoutine).body));
end;


{ TVTensor }

constructor TVTensor.Create(_dims: TIntegers; _e: TDoubles);
begin
    t.Create(_dims, _e);
end;


constructor TVTensor.Create(_e: TDoubles);
begin
    t := tensor_new(_e);
end;


constructor TVTensor.Create(_e: TComplexes);
begin
    t := tensor_new(_e);
end;


constructor TVTensor.Create(_dims: TIntegers; _e: TComplexes);
begin
    t.Create(_dims, _e);
end;


constructor TVTensor.Create(_t: TTensor);
begin
    t := _t;
end;


constructor TVTensor.CreateReal(_size: TIntegers; _e: TComplexes);
begin
    //Вещественный тензор создаётся из комплексного агрумента.
    //Считыватель не может зарание знать тип литерала и по этому накапливает
    //данные в комплексном массиве. Если после считывания выясняется, что литерал
    //вещественный то используется этот конструктор.
    t.Create(_size, _e);
    t := tensor_re(t);
end;


destructor TVTensor.Destroy;
begin
    inherited Destroy;
end;


function TVTensor.Copy: TValue;
begin
  //тензоры иммутабельны и не требуют копирования данных
  result := TVTensor.Create(t);
end;


function TVTensor.AsString: unicodestring;
begin
    result := '{T' + t.str()+'}';
end;


function TVTensor.Description: unicodestring;
var i: integer;
begin
    if t.dim[0]=1
    then result := '#<Tensor S'
    else result := '#<Tensor C';
    for i in t.size() do result := result + ' ' + IntToStr(i);
    result := result + '>';
end;


function TVTensor.hash: DWORD;
begin
    result := crc.crc32(0, @t.dim[0], Length(t.dim)*SizeOf(t.dim[0]));
    result := crc.crc32(result, @t.e[0], Length(t.e)*SizeOf(t.e[0]));
end;


function TVTensor.equal(V: TValue): boolean;
var t2: TTensor;
begin
    if not (V is TVTensor) then Exit(false);
    t2 := (V as TVTensor).t;
    result := tensor_equal(t, t2);
end;


{ TVRangeFloat }

constructor TVRangeFloat.Create(l, h: double);
begin
    low := l; high := h;
end;

function TVRangeFloat.f_low: double;
begin
    result := low;
end;

function TVRangeFloat.f_high: double;
begin
    result := high;
end;


function TVRangeFloat.f_length: double;
begin
    result := high-low;
end;


function TVRangeFloat.Copy: TValue;
begin
    result := TVRangeFloat.Create(low, high);
end;

function TVRangeFloat.AsString: unicodestring;
begin
    result := FloatToStr(low)+'..'+FloatToStr(high);
end;

function TVRangeFloat.hash: DWORD;
begin
    result := crc32(0, @low, SizeOf(low));
    result := crc32(result, @high, SizeOf(high));
end;

{ TVRange }

function TVRange.equal(V: TValue): boolean;
begin
    result := (V is TVRange) and ((V as TVRange).f_low = f_low) and ((V as TVRange).f_high = f_high);
end;


{ TVCompound }

function TVCompound.Item(index: integer): TValue;
begin
    result := nil;
end;

function TVCompound.Get(index: integer): TValue;
begin
    result := GetItem(index);
end;


function TVCompound.Get(id: TValue): TValue;
begin
    result := GetItem(Index(id));
end;


procedure TVCompound.CopyOnWrite;
begin
    //
end;



{ TVFunctionForwardDeclaration }

function TVFunctionForwardDeclaration.Copy: TValue;
begin
    result := TVFunctionForwardDeclaration.Create;
end;


{ TListBody }

constructor TVListBody.Create;
begin
    V := nil;
end;

destructor TVListBody.Destroy;
var i: integer;
begin
    for i := 0 to high(V) do V[i].Free;
    V := nil;
end;


function TVListBody.Copy: TValue;
var res: TVListBody; i: integer;
begin
    res := TVListBody.Create();
    SetLength(res.V, Length(V));
    for i := 0 to high(V) do res.V[i] := V[i].Copy;
    result := res;
end;


function TVListBody.AsString: unicodestring;
begin
    result := '#<LB ';
    if Length(V)>0 then result := result + ' '+V[0].AsString;
    if Length(V)>1 then result := result + ' '+V[1].AsString;
    if Length(V)>2 then result := result + ' '+V[2].AsString;
    if Length(V)>3 then result := result + ' ... ';
    result := result + '>';
end;


{ TVListRest }

procedure TVListRest.SetItem(Index: integer; _v: TValue);
begin
    V[Index] := _v;
end;


function TVListRest.GetItem(Index: integer): TValue;
begin
    result := V[Index];
end;

procedure TVListRest.Add(_v: TValue);
begin
    SetLength(V, Length(V)+1);
    V[high(V)] := _v;
end;


constructor TVListRest.Create(VL: TValues);
begin
    V := VL;
end;


function TVListRest.{%H-}Copy: TValue;
begin
    raise ELE.Create('','internal/TVExpression.copy');
end;


function TVListRest.AsString: unicodestring;
begin
    result := '#<LR ';
    if Length(V)>0 then result := result + ' '+V[0].AsString;
    if Length(V)>1 then result := result + ' '+V[1].AsString;
    if Length(V)>2 then result := result + ' ... ';
    result := result + '>';
end;


{ TVListBase }

function TVListBase.hash: DWORD;
begin
    raise ELE.Create('','internal/TVListBody.hash/');
    result := 0;
end;


function TVListBase.equal(V: TValue): boolean;
begin
    raise ELE.Create('','internal/TVListBody.equal/');
    result := false;
end;


{ TVProcedureForwardDeclaration }

function TVProcedureForwardDeclaration.Copy: TValue;
begin
    result := TVProcedureForwardDeclaration.Create;
end;


{ TVSequenceOfPrimitive }

function TVSequenceOfPrimitive.Item(index: integer): TValue;
begin
    result := nil;
end;


{ TVInternal }

function TVInternal.Copy: TValue;
begin
    result := self.ClassType.Create as TValue;
end;

function TVInternal.equal(V: TValue): boolean;
begin
    result := false;
    raise ELE.Create('equal '+self.ClassName, 'internal');
end;


{ TValue }

function TValue.AsString: unicodestring;
begin
    result := '#<'+self.ClassName+'>';
end;


function TValue.Description: unicodestring;
begin
    result := AsString();
end;


function TValue.hash: DWORD;
begin
    result := 0;
end;


function TValue.equal(V: TValue): boolean;
begin
    result := false;
end;


//procedure TValue.Release;
//begin
//    Self.Destroy;
//end;
//
//
//procedure TValue.Free;
//begin
//    if self<>nil then self.Release;
//end;


{ TVZIPArchivePointer }

constructor TVZIPArchivePointer.Create(_Z: TZipArchive);
begin
    Z := _Z;
end;

destructor TVZIPArchivePointer.Destroy;
begin
    Z.Release;
    inherited Destroy;
end;

function TVZIPArchivePointer.Copy: TValue;
begin
    result := TVZIPArchivePointer.Create(Z.Ref as TZipArchive);
end;

function TVZIPArchivePointer.AsString: unicodestring;
begin
    result := '#<ZIP '+Z.description+'>';
end;

function TVZIPArchivePointer.hash: DWORD;
begin
    result := 10009;
end;

function TVZIPArchivePointer.equal(V: TValue): boolean;
begin
    result := (V is TVZIPArchivePointer) and (Z = (V as TVZIPArchivePointer).Z);
end;

procedure TVZIPArchivePointer.close_zip;
begin
    Z.Close;
end;



{ TVTime }

function TVTime.equal(V: TValue): boolean;
begin
    result := (V.ClassType=self.ClassType) and (fDT=(V as TVTime).fDT);
end;

{ TVReal }

function TVReal.equal(V: TValue): boolean;
begin
    if V is TVReal then result := (V as TVReal).F = F
    else
    if V is TVNumber then result := (V as TVNumber).C = C
    else
    result := false;
end;

{ TVNumber }

function TVNumber.equal(V: TValue): boolean;
begin
    if V is TVNumber then result := (V as TVNumber).C = C
    else
    result := false;
end;


{ TVHashTable }


function TVHashTable.GetItem(index: integer): TValue;
begin
    if index >= 0
    then result := data[index].Copy
    else result := TVList.Create(nil);
end;


procedure TVHashTable.SetItem(index: integer; _V: TValue);
begin
    if index >= 0
    then
        data[index] := _v
    else
        data[store_last_key] := _V;
end;


function TVHashTable.Item(index: integer): TValue;
begin
    if index < 0
    then result := data[store_last_key]
    else result := data[index];
end;


function TVHashTable.LookKey(index: integer): TValue;
begin
    result := keys[index];
end;


function TVHashTable.GetKeys: TVList;
var i: integer;
begin
    result := TVList.Create;
    for i := 0 to Count-1 do
        if tpTrue(data[i]) then result.Add(keys[i].Copy);
end;


procedure TVHashTable.Expand;
var old_index: THashTableIndex; i: integer;
begin
    if count<(Length(index_table) div 4) then Exit;

    old_index := System.Copy(index_table);
    SetLength(index_table, Length(old_index)*2);
    for i := 0 to high(index_table) do index_table[i].k:=-1;
    for i := 0 to high(old_index) do
        index_table[FindEmpty(old_index[i].h)] := old_index[i];
end;


function TVHashTable.FindEmpty(h: DWORD): integer;
var i, li: integer; size: integer;
begin
    size := Length(index_table);
    for i := h mod size to h mod size + size do
        begin
            li := i mod size;
            if index_table[li].k=-1 then Exit(li);
        end;

    raise ELE.Create('filfull hash-table', 'internal');
end;


function TVHashTable.find_key(_key: TValue): integer;
var h: DWORD; i, li, start, stop, size: integer;
begin
    size := Length(index_table);
    h := _key.hash;
    start := h mod size;
    stop :=  start + size;
    for i := start to stop do
        begin
            li := i mod size;
            if (index_table[li].h=h) and (index_table[li].k>=0) and _key.equal(keys[index_table[li].k])
            then
                Exit(index_table[li].k)
            else
                if index_table[li].k=-1 then Exit(-1);
		end;
	result := -1;
end;


function TVHashTable.find_or_add_key(_key: TValue): integer;
var h: DWORD; i, li, size: integer;
begin
    Expand;
    h := _key.hash;
    size := Length(index_table);
    for i := h mod size to (h mod size) + size do begin
        li := i mod size;
        if (index_table[li].h=h) and (index_table[li].k>=0) and _key.equal(keys[index_table[li].k])
        then
            Exit(index_table[li].k)
        else
            if index_table[li].k=-1 then begin
                keys.Add(_key.copy);
                data.Add(nil);
                index_table[li].h := h;
                index_table[li].k := data.high;
                result := data.high;
                Exit;
            end;
    end;
end;


function TVHashTable.store_last_key: integer;
var h: DWORD; i, li, size: integer;
begin
    Expand;
    h := last_key.hash;
    size := Length(index_table);
    for i := h mod size to (h mod size) + size do
        begin
            li := i mod size;
            if (index_table[li].h=h) and (index_table[li].k>=0) and last_key.equal(keys[index_table[li].k])
            then
                Exit(index_table[li].k)
            else
                if index_table[li].k=-1 then begin
                    keys.Add(last_key);
                    last_key := nil;
                    data.Add(TVList.Create);
                    index_table[li].h := h;
                    index_table[li].k := data.high;
                    result := data.high;
                    Exit;
                end;
        end;
end;


constructor TVHashTable.Create;
begin
    data := TVList.Create;
    keys := TVList.Create;
    SetLength(index_table, 2);
    index_table[0].k:=-1;
    index_table[1].k:=-1;
end;


constructor TVHashTable.CreateEmpty;
begin
    data := nil;
    keys := nil;
    SetLength(index_table, 0);
end;


destructor TVHashTable.Destroy;
begin
    FreeAndNil(last_key);
    keys.Free;
    data.Free;
    SetLength(index_table, 0);
    inherited Destroy;
end;


function TVHashTable.AsString: unicodestring;
begin
    result := '#<HASH-TABLE '+IntToStr(count)+'/'+IntToStr(Length(index_table))+'>';
end;

function TVHashTable.Copy: TValue;
begin
    result := TVHashTable.CreateEmpty;
    (result as TVHashTable).data := data.Copy as TVList;
    (result as TVHashTable).keys := keys.Copy as TVList;
    (result as TVHashTable).index_table := System.Copy(index_table);

    if last_key <> nil
    then (result as TVHashTable).last_key := last_key.Copy;
end;

function TVHashTable.hash: DWORD;
begin
    result := 10008;
end;

function TVHashTable.equal(V: TValue): boolean;
begin
    result := false;
    raise ELE.Create('TVHashTable.eqial','not implemented');
end;

procedure TVHashTable.print;
var i: integer;
begin
    WriteLn('--------',AsString,'--------');
    for i:=0 to Length(index_table)-1 do WriteLn(index_table[i].h, '   ', index_table[i].k);
    WriteLn('keys');
    for i := 0 to keys.high do WriteLn('    ', i, '  ', keys[i].AsString);
    WriteLn('values');
    for i := 0 to data.high do WriteLn('    ', i, '  ', data[i].AsString);
    WriteLn('--------',AsString,'--------');
end;

procedure TVHashTable.copy_on_write;
begin
    data.copy_on_write;
    keys.copy_on_write;
    //ключи не копируются при модификации значения ХЭШ таблицы,
    //поскольку они изменяются только при добавлении новых значений
end;

procedure TVHashTable.CopyKeys;
begin
    //этот метод нужен процедуре разделения памяти
    keys.copy_on_write;
end;


function TVHashTable.Count: integer;
begin
    result := data.Count;
end;


function TVHashTable.Index(id: TValue): integer;
begin
    result := find_key(id);
    if result < 0
    then
        begin
            FreeAndNil(last_key);
            last_key := id.Copy;
		end;
end;


procedure TVHashTable.CopyOnWrite;
begin
    copy_on_write;
end;


{ TVComplex }

function TVComplex.C: COMPLEX;
begin
   result := fC;
end;

constructor TVComplex.Create(re, im: double);
begin
    fC.re := re;
    fC.im := im;
end;

constructor TVComplex.Create(_c: COMPLEX);
begin
    fC := _c;
end;

function TVComplex.Copy: TValue;
begin
    result := TVComplex.Create(fC.re, fC.im);
end;

function TVComplex.AsString: unicodestring;
begin
    result := ComplexToStr(fC);
end;

destructor TVComplex.Destroy;
begin
    inherited Destroy;
end;

function TVComplex.hash: DWORD;
var i: Int64;
begin
    if fC.im=0
    then begin
        if fC.re<$7FFFFFFFFFFFFFFF then i := round(fC.re) else i := 0;
        if i = fC.re
        then result := crc32(0, @i, SizeOf(i))
        else result := crc32(0, @fC.re, SizeOf(fC.re))
    end
    else result := crc32(0, @fC, SizeOf(COMPLEX));
end;

{ TVPredicate }

constructor TVPredicate.Create(name: unicodestring; _body: TTypePredicate; a: boolean);
begin
    nN := symbol_n(name);
    body := _body;
    fAssert := a;
end;

constructor TVPredicate.Create(_nN: integer; _body: TTypePredicate; a: boolean);
begin
    nN := _nN;
    body := _body;
    fAssert := a;
end;


function TVPredicate.Copy: TValue;
begin
    result := TVPredicate.Create(nN, body, fAssert);
end;

function TVPredicate.AsString: unicodestring;
var a: unicodestring;
begin
    if fAssert then a := '!' else a := '?';
    result := '#'+symbol_uname(nN)+a;
end;

function TVPredicate.hash: DWORD;
begin
    if fAssert
    then result := crc32(1, @body, SizeOf(body))
    else result := crc32(0, @body, SizeOf(body));
end;

function TVPredicate.equal(V: TValue): boolean;
begin
    result := (V is TVPredicate)
        and (@body=@((V as TVPredicate).body))
        and (fAssert=(V as TVPredicate).fAssert);
end;

{ TVReturn }

constructor TVReturn.Create(V: TValue);
begin
    value := V;
end;

destructor TVReturn.Destroy;
begin
    value.Free;
    inherited Destroy;
end;

function TVReturn.Copy: TValue;
begin
    result := TVReturn.Create(value.Copy);
end;

function TVReturn.AsString: unicodestring;
begin
    result := '#<RETURN '+value.AsString+'>';
end;

function TVReturn.hash: DWORD;
var i: DWORD;
begin
    i := 4;
    result := crc32(value.hash, @i, SizeOf(i));
end;


function TVReturn.Unpack: TValue;
begin
    result := value;
    value := nil;
    Free;
end;


{ TVKeyword }

constructor TVKeyword.CreateCopy(origin: TVKeyword);
begin
    fN := origin.fN;
    fName := origin.fname;
end;

function TVKeyword.Copy: TValue;
begin
    result := TVKeyword.CreateCopy(self);
end;


{ TVSequence }

function TVSequence.checked_index(i: integer): integer;
begin
    if i < 0 then result := Count + i else result := i;

    if (result < 0) or (result > high)
    then
        raise ELE.Create(
            IntToStr(i)+' not in '+IntToStr(-Count)+'..'+IntToStr(Count),
            'index/out of range');
end;


function TVSequence.high: integer;
begin
    result := self.Count-1;
end;

function TVSequence.tail(istart: integer): TValue;
begin
    result := subseq(istart, count);
end;


function TVSequence.index(i: integer): integer;
begin
    if i < 0
    then result := Count + i
    else result := i;
end;


function TVSequence.Index(id: TValue): integer;
begin
    if id is TVInteger
    then result := checked_index((id as TVInteger).fI)
    else raise ELE.Create(id.AsString, 'index/type');
end;


{ TVChainPointer }

constructor TVChainPointer.Create(_P: PVariable);
begin
    P := _P;
    SetLength(index, 0);
end;

constructor TVChainPointer.Create(_P: PVariable; indices: array of integer);
var i: integer;
begin
    P := _P;
    SetLength(index, Length(indices));
    for i := 0 to High(indices) do index[i] := indices[i];
end;

destructor TVChainPointer.Destroy;
begin
    ReleaseVariable(P);
    SetLength(index,0);
    inherited;
end;

function TVChainPointer.Copy: TValue;
var i: integer;
begin
    result := TVChainPointer.Create(RefVariable(P));
    SetLength((result as TVChainPointer).index, Length(index));
    for i := 0 to high(index) do
        (result as TVChainPointer).index[i] := index[i];
end;

function TVChainPointer.AsString: unicodestring;
var i: integer;
begin
    result := '#<CHAIN-POINTER';
    for i := 0 to high(index) do result := result+' '+IntToStr(index[i]);
    result := result + '>';
end;

function TVChainPointer.hash: DWORD;
begin
    raise ELE.Create('HASH pointer', 'internal/');
    result := 0;
end;

function TVChainPointer.get_link: PPVariable;
begin
    result := @P;
end;

function TVChainPointer.constant: boolean;
begin
    result := P.constant;
end;


procedure TVChainPointer.add_index(i: integer);
begin
    SetLength(index, Length(index)+1);
    index[high(index)] := i;
end;


procedure TVChainPointer.set_last_index(i: integer);
begin
    index[high(index)] := i;
end;





procedure TVChainPointer.EvalLink(selectors: TValues; call: TCallProc);
var C: TVCompound; s, sl: integer; res, selector: TValue; L: TVList;
begin
    if selectors = nil then Exit;
    res := nil;

    try
        C := Target as TVCompound;
        for s := 0 to high(selectors) do
            begin
                selector := selectors[s];

                if selector is TVSubprogram
                then
                    begin
                        FreeAndNil(res);
                        res := call(TValues.Create(selector, C));
                        selector := res;
					end;

				if selector is TVList
                then
                    begin
                        L := selector as TVList;
                        for sl := 0 to L.high do
                            append_integer(index, C.Index(L[sl]));
					end
                else
                    append_integer(index, C.Index(selector));

                if s < high(selectors)
                then C := C[index[high(index)]] as TVCompound;
			end;

	finally
        FreeAndNil(res);
	end;
end;


function TVChainPointer.Target: TValue;
var i: integer;
begin
    result := P.V;
    for i := 0 to high(index) do
            result := (result as TVCompound)[index[i]];
end;


function TVChainPointer.TargetForModification: TValue;
var i: integer; C: TVCompound;
begin
    result := P.V;
    for i := 0 to high(index) do
        begin
            C := result as TVCompound;
            C.CopyOnWrite();
            result := C[index[i]];
		end;
end;


procedure TVChainPointer.SetTarget(_V: TValue);
begin
    if P.constant then raise ELE.TargetNotVariable('');

    if Length(index)=0
    then
        begin
            P.V.Free;
            P.V := _V;
        end
    else
        Penultimate()[index[high(index)]] := _V;
end;


function TVChainPointer.GetTarget: TValue;
var i: integer; C: TVCompound;
begin
    if Length(index)=0
    then
        result := P.V.Copy
    else
        begin
            if index[high(index)] = -1 then Exit(TVList.Create(nil));
            C := P.V as TVCompound;
            for i := 0 to high(index) - 1 do
                C := C[index[i]] as TVCompound;
            result := C.GetItem(index[high(index)]);
		end;
end;


function TVChainPointer.Penultimate: TVCompound;
var i: integer;
begin
    result := P.V as TVCompound;
    for i := 0 to high(index)-1 do
        begin
            result.CopyOnWrite();
            result := result[index[i]] as TVCompound;
		end;
end;


{ TVTimeInterval }

constructor TVDuration.Create(dt: TDateTime);
begin
    fDT := dt;
end;

function TVDuration.Copy: TValue;
begin
    result := TVDuration.Create(fDT);
end;

function TVDuration.AsString: unicodestring;
var hour,minute,second, ms: WORD; h: integer;
begin
    DecodeTime(fDT, hour, minute, second, ms);
    h := hour+round(abs(int(fDT))*24);
    result := format('%.0d:%.2d:%.2d.%.3d',[h, minute, second, ms]);
    if fDT<0 then result := '-'+result;
end;

function TVDuration.hash: DWORD;
begin
    result := crc32(1, @fDT, SizeOf(fDT));
end;

{ TVDateTime }

constructor TVDateTime.Create(dt: TDateTime);
begin
    fDT := dt;
end;

function TVDateTime.Copy: TValue;
begin
    result := TVDateTime.Create(fDT);
end;

function TVDateTime.AsString(): unicodestring;
var year,month,day,hour,minute,second, ms: WORD;
begin
    DecodeDate(fDT, year, month, day);
    DecodeTime(fDT, hour, minute, second, ms);
    result := format('%.4d.%.2d.%.2d_%.2d:%.2d:%.2d.%.3d',
                     [year,month,day,hour,minute,second,ms]);
end;

function TVDateTime.hash: DWORD;
begin
    result := crc32(2, @fDT, SizeOf(fDT));
end;

function TVDateTime.AsSQLDateTime: unicodestring;
var year,month,day,hour,minute,second, ms: WORD;
    function dd(i: integer): unicodestring;
    begin
        result := IntToStr(i);
        if Length(result)=1 then result := '0'+result;
    end;
    function ddd(i: integer): unicodestring;
    begin
        result := IntToStr(i);
        if Length(result)=2 then result := '0'+result;
        if Length(result)=1 then result := '00'+result;
    end;
begin
    DecodeDate(fDT, year, month, day);
    DecodeTime(fDT, hour, minute, second, ms);
    result := ''''+IntToStr(Year)+'-'+dd(month)+'-'+dd(day)+
        ' '+dd(hour)+':'+dd(minute)+':'+dd(second)+'.'+ddd(ms)+'''';
end;

{ TVBytes }

function TVBytes.GetByte(Index: Integer): Int64;
begin
    result := fBytes[index];
end;

procedure TVBytes.SetByte(Index: Integer; V: Int64);
begin
    if (V<0) or (V>255) then raise ELE.Create('byte', 'out of range');
    fBytes[Index] := V;
end;

function TVBytes.GetItem(index: integer): TValue;
begin
    result := TVInteger.Create(fBytes[index]);
end;

procedure TVBytes.SetItem(index: integer; _V: TValue);
begin
    fBytes[index] := (_V as TVInteger).fI;
    _V.Free;
end;

constructor TVBytes.Create;
begin
    SetLength(fBytes,0);
end;

destructor TVBytes.Destroy;
begin
    SetLength(fBytes,0);
    inherited Destroy;
end;


function TVBytes.Copy: TValue;
var i: integer; res: TVBytes;
begin
    res := TVBytes.Create;
    Setlength(res.fBytes, Length(fBytes));
    for i := 0 to Length(fBytes)-1 do res.fBytes[i] := fBytes[i];
    result := res;
end;

function TVBytes.AsString: unicodestring;
var i: integer;
begin
    result := '#B(';
    for i := 0 to Length(fBytes)-1 do result := result+' '+IntToHex(fBytes[i],2);
    result := result + ')';
end;

function TVBytes.hash: DWORD;
begin
    result := crc32;
end;

function TVBytes.equal(V: TValue): boolean;
var i: integer; BV: TVBytes;
begin
    result := V is TVBytes;
    if not result then Exit;
    BV := V as TVBytes;

    result := Length(fBytes) = Length(BV.fBytes);
    if not result then Exit;

    for i := 0 to Length(fBytes)-1 do begin
        result := fBytes[i]=BV.fBytes[i];
        if not result then exit;
    end;
end;

procedure TVBytes.SetCount(l: integer);
begin
    SetLength(fBytes, l);
end;

procedure TVBytes.Add(b: Int64);
begin
    if (b<0) or (b>255) then raise ELE.Create('byte', 'out of range');
    SetLength(fBytes, Length(fBytes)+1);
    fBytes[Length(fBytes)-1] := b;
end;

function TVBytes.subseq(istart: integer; iend: integer): TValue;
var i, s, e: integer;
begin
    s := index(istart);
    e := index(iend);
    result := TVBytes.Create;
    SetLength((result as TVBytes).fBytes, e-s);
    for i := s to e-1 do
        (result as TVBytes).fBytes[i-s] := fBytes[i];
end;

procedure TVBytes.append(BV: TVBytes);
var i, offset: integer;
begin
    offset := Length(fBytes);
    SetLength(fBytes, Length(fBytes)+Length(BV.fBytes));
    for i := 0 to Length(BV.fBytes)-1 do
        fBytes[i+offset] := BV.fBytes[i];

    BV.Free;
end;


function TVBytes.crc32: DWORD;
begin
    result := crc.crc32(0, @fBytes[0], Length(fBytes));
end;

function TVBytes.count: integer;
begin
    result := Length(fBytes);
end;


{ TVEndOfStream }

function TVEndOfStream.Copy: TValue;
begin
    result := TVEndOfStream.Create;
end;


{ TVSymbolStack }

function TVSymbolStack.index_of(name: unicodestring): integer;
begin
    result := index_of(symbol_n(name));
end;


function TVSymbolStack.index_of(n: integer): integer;
begin
    {$RANGECHECK OFF}
    for result := high(stack) downto 0 do
        if stack[result].N = n then
            if stack[result].V = nil
            then raise ELE.Create(symbol_uname(n)+' - nil bound', 'internal')
            else exit;

    raise ELE.Create(symbol_uname(n), 'symbol not bound');
    {$RANGECHECK ON}
end;


constructor TVSymbolStack.Create;
begin
    SetLength(stack,0);
end;


destructor TVSymbolStack.Destroy;
begin
    clear_frame(0);
    inherited Destroy;
end;


function TVSymbolStack.Copy: TValue;
var i: integer;
begin
    result := TVSymbolStack.Create;
    SetLength((result as TVSymbolStack).stack, Length(stack));
    for i := 0 to high(stack) do begin
        (result as TVSymbolStack).stack[i].N := stack[i].N;
        (result as TVSymbolStack).stack[i].V := RefVariable(stack[i].V)
    end;
end;


function TVSymbolStack.equal(V: TValue): boolean;
var i: integer; ss: TVSymbolStack;
begin
    result := V is TVSymbolStack;
    if not result then exit;
    ss := V as TVSymbolStack;

    result := Length(stack)=Length(ss.stack);
    if not result then Exit;
    for i := 0 to high(stack) do begin
        result := (stack[i].N=ss.stack[i].N) and (stack[i].V=ss.stack[i].V);
        if not result then Exit;
    end;
end;

function TVSymbolStack.Count: integer;
begin
    result := Length(stack);
end;


procedure TVSymbolStack.Print(n: integer);
var i, low_i: integer;
begin
    if n<0
    then low_i := 0
    else low_i := high(stack) - n;
    if low_i<0 then low_i := 0;
    WriteLn('-----------------------');
    for i := low_i to high(stack) do begin
        Write('  | ', symbol_uname(stack[i].N));
        if stack[i].V<> nil
        then WriteLn('  >(', stack[i].V.ref_count,')',
                        {%H-}Cardinal(stack[i].V),'>  ', stack[i].V.V.AsString)
        else WriteLn(' nil');
    end;
    WriteLn('-----------------------');

end;



function TVSymbolStack.new_var(N: integer; V: TValue; c: boolean): PVariable;
begin
    result := NewVariable;
    result.V := V;
    result.constant := c;
    SetLength(stack, Length(stack)+1);
    stack[high(stack)].N := N;
    stack[high(stack)].V := result;
end;


function TVSymbolStack.new_var(symbol: TVSymbol; V: TValue; c: boolean
    ): PVariable;
begin
    result := new_var(symbol.N, V, c);
end;


procedure TVSymbolStack.new_const(name: unicodestring; V: TValue);
begin
    new_var(symbol_n(name), V, true);
end;


procedure TVSymbolStack.new_const(N: integer; V: TValue);
begin
    new_var(N, V, true);
end;


function TVSymbolStack.find_var(symbol: TVSymbol): TValue;
begin
   result := stack[index_of(symbol.N)].V.V.Copy;
end;


function TVSymbolStack.find_var(N: integer): TValue;
begin
    result := stack[index_of(N)].V.V.Copy;
end;


procedure TVSymbolStack.set_var(symbol: TVSymbol; V: TValue);
var i: integer;
begin
    i := index_of(symbol.N);
    if stack[i].V.constant
    then raise ELE.Create(Symbol.name+' is not variable','program');
    stack[i].V.V.Free;
    stack[i].V.V := V;
end;


procedure TVSymbolStack.clear_frame(n: integer);
var i: integer; PP: PVariables;
begin
    if n = Length(stack) then Exit;

    if rrp>0 then begin  //TODO: убрать переключатель в gc
        Setlength(PP, high(stack)-n+1);
        for i := high(stack) downto n do PP[i-n] := stack[i].V;
        ReleaseVariables(PP);
    end
    else for i := high(stack) downto n do ReleaseVariable(stack[i].V);

    SetLength(stack, min(n, length(stack)));
end;


procedure TVSymbolStack.clear_top;
begin
    ReleaseVariable(stack[high(stack)].V);
    SetLength(stack,high(stack));
end;


function TVSymbolStack.get_ref(symbol: TVSymbol): PVariable;
begin
    result := RefVariable(stack[index_of(symbol.N)].V);
end;


function TVSymbolStack.get_ref(N: integer): PVariable;
begin
    result := RefVariable(stack[index_of(N)].V);
end;


function TVSymbolStack.look_var(N: integer): PVariable;
begin
    result := stack[index_of(N)].V;
end;

procedure TVSymbolStack.new_ref(name: unicodestring; P: PVariable);
begin
    SetLength(stack, Length(stack)+1);
    stack[high(stack)].N := symbol_n(name);
    stack[high(stack)].V := P;
end;

procedure TVSymbolStack.new_ref(symbol: TVSymbol; P: PVariable);
begin
    new_ref(symbol.N, P);
end;

procedure TVSymbolStack.new_ref(nN: integer; P: PVariable);
begin
    SetLength(stack, Length(stack)+1);
    stack[high(stack)].N := nN;
    stack[high(stack)].V := P;
end;


function TVSymbolStack.look_var_or_nil(nN: integer): PVariable;
var i: integer;
begin
    result := nil;
    for i := high(stack) downto 0 do
        if stack[i].N=nN then begin
            result := stack[i].V;
            exit;
        end;
end;


{ TVRangeInteger }

constructor TVRangeInteger.Create(l, h: Int64);
begin
    low := l;
    high := h;
end;

function TVRangeInteger.f_low: double;
begin
    result := low;
end;

function TVRangeInteger.f_high: double;
begin
    result := high;
end;


function TVRangeInteger.f_length: double;
begin
    result := high - low;
end;

function TVRangeInteger.Count: integer;
begin
    result := high - low;
end;


function TVRangeInteger.Copy: TValue;
begin
    result := TVRangeInteger.Create(low,high);
end;

function TVRangeInteger.AsString: unicodestring;
begin
    result := IntToStr(low)+'..'+IntToStr(high);
end;

function TVRangeInteger.hash: DWORD;
begin
    result := crc32(0, @low, SizeOf(low));
    result := crc32(result, @high, SizeOf(high));
end;

function TVRangeInteger.equal(V: TValue): boolean;
begin
    if (V is TVRangeInteger)
    then result := (low=(V as TVRangeInteger).low) and (high=(V as TVRangeInteger).high)
    else result := inherited equal(V);
end;


{ TVRecord }

function TVRecord.fGetSlot(index: unicodestring): TValue;
begin
    result := (slots[index_of(symbol_n(index))] as TValue).copy;
end;

procedure TVRecord.fSetSlot(index: unicodestring; V: TValue);
begin
    slots[index_of(symbol_n(index))] := V;
end;

function TVRecord.flook(index: unicodestring): TValue;
begin
    //TODO: обращение к несуществующему слоту структуры вызывает необрабатываемую ошибку
    result := slots[index_of(symbol_n(index))] as TValue;
end;

function TVRecord.GetItem(index: integer): TValue;
begin
    result := (slots[index] as TValue).Copy;
end;

procedure TVRecord.SetItem(index: integer; _V: TValue);
begin
    slots[index] := _V;
end;

function TVRecord.Item(index: integer): TValue;
begin
    result := slots[index] as TValue;
end;

function TVRecord.index_of(nN: integer): integer;
var i: integer;
begin
    for i := 0 to high(unames) do
        if unames[i]=nN then begin
            result := i;
            Exit;
        end;
    raise ELE.Create('slot '+symbol_uname(nN)+' not found');
end;

constructor TVRecord.Create(names: array of unicodestring);
var i: integer;
begin
    SetLength(unames, Length(names));
    slots := TObjectList.create(true);
    slots.Capacity := Length(names);
    for i := low(names) to high(names) do begin
        unames[i] := symbol_n(names[i]);
        slots.Add(TVList.Create);
    end;
end;

constructor TVRecord.Create;
begin
    SetLength(unames, 0);
    slots := TObjectList.Create(true);
end;

destructor TVRecord.Destroy;
begin
    SetLength(unames, 0);
    slots.Free;
    inherited Destroy;
end;

function TVRecord.Copy: TValue;
var i: integer;
begin
    result := TVRecord.Create;

    SetLength((result as TVRecord).unames, Length(unames));
    (result as TVRecord).slots.capacity := slots.count;
    for i := low(unames) to high(unames) do begin
        (result as TVRecord).unames[i] := unames[i];
        (result as TVRecord).slots.Add((slots[i] as TValue).copy);
    end;
end;

function TVRecord.AsString: unicodestring;
var i: integer;
begin
    result := '#R(';
    if slots.Count>0
    then result := result + symbol_uname(unames[0]) + ' ' + (slots[0] as TValue).AsString();
    for i := 1 to slots.Count - 1 do
    result := result + ' ' + symbol_uname(unames[i]) + ' ' + (slots[i] as TValue).AsString();
    result := result + ')';
end;

function TVRecord.hash: DWORD;
var i: integer;
begin
    result := 0;
    for i := 0 to high(unames) do
        result := result +
            crc32((slots[i] as TValue).hash,@unames[i], SizeOf(unames[i]))
            div Length(unames);
end;

function TVRecord.equal(V: TValue): boolean;
var i, j: integer; VR: TVRecord;
begin
    result := V is TVRecord;
    if not result then Exit;
    VR := V as TVRecord;

    result := count=VR.count;
    if not result then Exit;

    for i := 0 to high(unames) do begin
        result := false;
        for j := 0 to high(VR.unames) do
            if (unames[i]=VR.unames[j])
                and ((slots[i] as TValue).equal(VR.slots[j] as TValue))
            then result := true;
        if not result then Exit;
    end;

    result := true;
end;

function TVRecord.is_class(cl: TVRecord): boolean;
var i, j: integer; m: boolean;
begin
    result := false;
    for i := 0 to High(cl.unames) do
        begin
            m := false;
            for j := 0 to high(unames) do
                if cl.unames[i]=unames[j] then m := true;
            if not m then exit;
        end;
    result := true;
end;

procedure TVRecord.AddSlot(nN: integer; V: TValue);
var i: integer;
begin
    for i := 0 to high(unames) do
        if unames[i]=nN then raise ELE.Create('Not unique slot name '+symbol_uname(nN), 'syntax');
    SetLength(unames, Length(unames)+1);
    unames[high(unames)] := nN;
    slots.Add(V);
end;

procedure TVRecord.AddSlot(name: unicodestring; V: TValue);
begin
    AddSlot(symbol_n(name), V);
end;

procedure TVRecord.AddSlot(name: TVSymbol; V: TValue);
begin
    AddSlot(name.N, V);
end;

function TVRecord.GetSlot(nN: integer): TValue;
begin
    result := (slots[index_of(nN)] as TValue).Copy;
end;

procedure TVRecord.SetSlot(nN: integer; V: TValue);
begin
    slots[index_of(nN)] := V;
end;

function TVRecord.LookSlot(nN: integer): TValue;
begin
    result := slots[index_of(nN)] as TValue;
end;

function TVRecord.count: integer;
begin
    result := Length(unames);
end;

function TVRecord.name_n(n: integer): unicodestring;
begin
    result := symbol_uname(unames[n]);
end;

function TVRecord.get_n_of(index: unicodestring): integer;
begin
    result := index_of(symbol_n(index));
end;


function TVRecord.Index(id: TValue): integer;
var nN: integer;
begin
    if id is TVSymbol
    then nN := (id as TVSymbol).N
    else raise ELE.Create(id.ClassName+'('+id.AsString+')', 'invalid index/type/');

    for result := 0 to high(unames) do
        if unames[result] = nN
        then Exit;

    raise ELE.Create(id.AsString, 'invalid index/not found');
end;



{ TVOperator }

constructor TVOperator.Create(name: unicodestring; en: TOperatorEnum);
begin
    self.fN := symbol_n(name);
    self.op_enum := en;
end;

constructor TVOperator.Create(_nN: integer; en: TOperatorEnum);
begin
    self.fN := _nN;
    self.op_enum := en;
end;

constructor TVOperator.Create(en: TOperatorEnum);
begin
    self.fN := symbol_n(op_names[en]);
    self.op_enum := en;
end;

destructor TVOperator.Destroy;
begin
    inherited Destroy;
end;

function TVOperator.Copy: TValue;
begin
    result := TVOperator.Create(fN, op_enum);
end;

function TVOperator.AsString: unicodestring;
begin
    result := symbol_uname(fN);
end;

function TVOperator.equal(V: TValue): boolean;
begin
    result := (V is TVOperator) and (op_enum=(V as TVOperator).op_enum);
end;


{ TVInternalRoutine }

constructor TVInternalRoutine.Create(_sign: TSignature;
			_body: TInternalFunctionBody; _nN: integer);
begin
  inherited Create;
  sign := _sign;
  self.body := _body;
  self.nN := _nN;
end;


destructor TVInternalRoutine.Destroy;
begin
    //сигнатура не должна освобождаться, поскольку все экземпляры встроенной
    //функции используют общую сигнатуру, освобождаемую при завершении программы
    inherited Destroy;
end;


//procedure TVInternalRoutine.Release;
//begin
//    //встроенные функции никогда не освобождаются
//end;


function TVInternalRoutine.AsString: unicodestring;
begin
    result := '#'+symbol_uname(nN);
end;


function TVInternalRoutine.hash: DWORD;
begin
    result := crc32(0, @body, SizeOf(body));
end;


function TVInternalRoutine.equal(V: TValue): boolean;
begin
    result := (V is TVInternalRoutine) and (@body=@((V as TVInternalRoutine).body));
end;


{ TVInternalFunction }


function TVInternalFunction.Copy: TValue;
begin
    result := TVInternalFunction.Create(sign, body, nN);
    //result := self;
end;


{ TVInternalProcedure }


function TVInternalProcedure.Copy: TValue;
begin
    result := TVInternalProcedure.Create(sign, body, nN);
    //result := self;
end;


{ TVContinue }


function TVContinue.hash: DWORD;
begin
    result := 10003;
end;

{ TVBreak }

function TVBreak.hash: DWORD;
begin
    result := 10002;
end;


{ TVRoutine }

constructor TVRoutine.Create;
begin
    inherited;
    nN := 0;
    body := nil;
    stack := nil;
    rest := nil;
    sign := nil;
end;


constructor TVRoutine.Create(_n: integer; _sign, _body: TVList);
begin
    inherited Create;
    nN := _n;
    body := _body;
    stack := TVSymbolStack.Create;
    rest := nil;
    sign := ifh_build_sign(_sign);
end;


destructor TVRoutine.Destroy;
begin
    stack.Free;
    body.Free;
    rest.Free;
    sign := nil;
    inherited Destroy;
end;


function TVRoutine.Copy: TValue;
begin
    result := self.ClassType.Create as TValue;

    with result as TVRoutine do
        begin
            body := self.body.Copy() as TVList;
            stack := self.stack.Copy as TVSymbolStack;
            sign := self.sign;
            nN := self.nN;
            if self.rest <> nil then rest := self.rest.Copy as TVList;
		end;
end;


function TVRoutine.hash: DWORD;
begin
    result := (body.hash div 2);//TODO: хэш от процедур вычисляется без учёта сигнатуры
end;


function TVRoutine.equal(V: TValue): boolean;
var proc: TVRoutine;
begin
    result := self.ClassType=V.ClassType;
    if not result then Exit;
    proc := V as TVRoutine;
    //TODO: равенство процедур вычисляется без учёта сигнатуры
    result := //sign.equal(proc.sign)
        //and
        body.equal(proc.body)
        and rest.equal(proc.rest)
        and stack.equal(proc.stack);
end;


function TVRoutine.AsString: unicodestring;
begin
    if nN=0
    then result := '#Λ'+usign_AsString(sign)
    else result := '#'+symbol_uname(nN);
end;


{ TVGoto }

constructor TVGoto.Create(_n: integer);
begin
    n := _n;
end;


function TVGoto.Copy: TValue;
begin
    result := TVGoto.Create(n);
end;


function TVGoto.AsString: unicodestring;
begin
    result := '#<GOTO '+symbol_uname(N)+'>';
end;


function TVGoto.hash: DWORD;
begin
    result := crc32(3, @n, SizeOf(n));
end;



{ TVFloat }

constructor TVFloat.Create(F: double);
begin
    fF := F;
end;

function TVFloat.Copy: TValue;
begin
    result := TVFloat.Create(self.F);
end;

function TVFloat.AsString: unicodestring;
begin
    result := FloatToStr(F);
end;

function TVFloat.hash: DWORD;
var i: Int64;
begin
    if abs(fF)<$7FFFFFFFFFFFFFFF then i := round(fF) else i:=0;
    if fF = i
    then result := crc32(0, @i, SizeOf(i))
    else result := crc32(0, @fF, SizeOf(fF));
end;

function TVFloat.F: double;
begin
    result := fF;
end;

function TVFloat.C: COMPLEX;
begin
    result.im := 0;
    result.re := fF;
end;


{ TVString }

function TVString.GetItem(index: integer): TValue;
begin
    result := TVString.Create(S[index+1]);
end;


procedure TVString.SetItem(index: integer; _V: TValue);
begin
    S[index+1] := (_V as TVString).S[1];
    _V.Free;
end;

constructor TVString.Create(S: unicodestring);
begin
    self.S := S;
end;

destructor TVString.Destroy;
begin
    self.S := '';
    inherited;
end;


function TVString.Copy: TValue;
begin
    result := TVString.Create(self.S);
end;


function TVString.AsString: unicodestring;
var i :integer;
begin
    result := '"';
    for i:= 1 to Length(S) do
        case s[i] of
            '"', '\': result := result + '\' + s[i];
            else result := result + s[i];
        end;
    result := result + '"';
end;

function TVString.hash: DWORD;
begin
    result := crc32;
end;

function TVString.equal(V: TValue): boolean;
begin
    result := (V is TVString) and (S=(V as TVString).S);
end;

function TVString.Count: integer;
begin
    result := Length(S);
end;

function TVString.crc32: DWORD;
var i, cp: DWORD;
begin
    result := 10005;
    for i := 1 to Length(S) do begin
        cp := Ord(S[i]);
        result := crc.crc32(result, @cp, SizeOf(cp));
    end;
end;

function TVString.subseq(istart: integer; iend: integer): TValue;
var b, e: integer;
begin
    b := index(istart);
    e := index(iend);
    if b>=e
    then result := TVString.Create('')
    else result := TVString.Create(S[b+1..index(iend)]);
end;


procedure TVString.Append(_s: TVString);
begin
    S := S + _s.S;
    _s.Free;
end;


{ TVInteger }

function    TVInteger.Copy: TValue;
begin
    result := TVInteger.Create(fI);
end;

function    TVInteger.AsString: unicodestring; begin result := IntToStr(fI); end;

function TVInteger.hash: DWORD;
begin
    result := crc32(0, @fI, SizeOf(fI));
end;

function TVInteger.equal(V: TValue): boolean;
begin
    if V is TVInteger then result := (V as TVInteger).fI = fI
    else
    if V is TVReal then result := (V as TVReal).F = F
    else
    if V is TVNumber then result := (V as TVNumber).C = C
    else
    result := false;
end;

function TVInteger.F: double;
begin
    result := fI;
end;

function TVInteger.C: COMPLEX;
begin
    result.im := 0;
    result.re := fI;
end;

constructor TVInteger.Create(I: Int64); begin fI := I; end;


  { TVSymbol }

function TVSymbol.Copy: TValue;
begin
    result := TVSymbol.CreateEmpty;
    (result as TVSymbol).fname := self.fname;
    (result as TVSymbol).fN := self.fN;
end;

function TVSymbol.AsString: unicodestring;
begin
    result := fname;
end;

function TVSymbol.hash: DWORD;
begin
    if n>=0
    then result := N
    else result := $FFFFFFFF+N;
end;

function TVSymbol.equal(V: TValue): boolean;
begin
    result := (V is TVSymbol) and (N=(V as TVSymbol).N);
end;



constructor TVSymbol.Gensym;
begin
    fN := gensym_n;
    fName := symbol_uname(fN);
end;

constructor TVSymbol.CreateEmpty;
begin

end;

function TVSymbol.fGetUname: unicodestring;
begin
    if fN<0
    then result := fname
    else result := symbol_uname(fN);
end;

constructor TVSymbol.Create(S: unicodestring);
begin
    fname := S;
    fN := symbol_n(fname);
end;

constructor TVSymbol.Create(N: integer);
begin
    fname := symbol_uname(N);
    fN := N;
end;

destructor TVSymbol.Destroy;
begin
    inherited;
end;


{ TVList }

procedure TVList.Add(V: TValue);
begin
    copy_on_write;
    SetLength(fL.V, Length(fL.V)+1);
    fL.V[system.high(fL.V)] := V;
end;


procedure TVList.Append(VL: TValues);
var base: integer;
begin
    copy_on_write;

    base := Length(fL.V);
    SetLength(fL.V, Length(fL.V) + Length(VL));
    {$R-}move(VL.[0], fL.V[base], SizeOf(TValue)*Length(VL));{$R+}
end;


procedure TVList.Append(VL: TVList);
begin
    VL.copy_on_write;

    Append(VL.fL.V);

    SetLength(VL.fL.V, 0);

    VL.Free;
end;


function TVList.GetElementName(index: integer): unicodestring;
begin
    result := (fL.V[index] as TVSymbol).name;
end;


function TVList.GetElementUName(index: integer): unicodestring;
begin
    result := (fL.V[index] as TVSymbol).uname;
end;


function TVList.GetElementI(index: integer): Int64;
begin
    result := (fL.V[index] as TVInteger).fI;
end;


function TVList.GetElementF(index: integer): double;
begin
    result := (fL.V[index] as TVReal).F;
end;


function TVList.GetElementS(index: integer): unicodestring;
begin
    result := (fL.V[index] as TVString).S;
end;


function TVList.GetElementL(index: integer): TVList;
begin
    result := fL.V[index] as TVList;
end;


function TVList.GetElementC(index: integer): COMPLEX;
begin
    result := (fL.V[index] as TVNumber).C;
end;


function TVList.LookElementSYM(index: integer): TVSymbol;
begin
    result := fL.V[index] as TVSymbol;
end;


function TVList.Count: integer;
begin
    if P <> nil
    then result := Length(fL.V)
    else result := 0;
end;


function TVList.extract(n: integer): TValue;
var i, nn: integer;
begin
    copy_on_write;

    if n = -1 then nn := system.high(fL.V) else nn := n;

    result := fL.V[nn];

    for i := nn+1 to system.high(fL.V) do fL.V[i-1] := fL.V[i];

    SetLength(fL.V, length(fL.V)-1);
end;


//извлекает значение, оставляя пустое место
//требуется для избежание излишнего копирования при работе деструктурирующей привязки
function TVList.GetValue(n: integer): TValue;
begin
    result := fL.V[n];
    fl.V[n] := nil;
end;


function TVList.extract_subseq(_from, _to: integer): TVList;
var i, a, b, d: integer;
begin
    copy_on_write;
    result := nil;

    a := checked_index(_from);
    b := checked_index(_to)-1;
    d := b-a+1;

    result := TVList.Create(system.Copy(fL.V, a, d));

    for i := b+1 to system.High(fL.V) do fL.V[i-d] := fl.V[i];
    SetLength(fL.V, Length(fL.V)-d);
end;



procedure TVList.insert(n: integer; V: TValue);
var i: integer;
begin
    copy_on_write;

    SetLength(fL.V, Length(fL.V)+1);

    for i := system.High(fL.V) downto n+1 do fL.V[i] := fL.V[i-1];
    fL.V[n] := V;
end;


function TVList.Copy: TValue;
begin
    result := TVList.Create(RefVariable(P))
end;


function TVList.AsString: unicodestring;
var i: integer;
    function str(V: TValue): unicodestring;
    begin if V=nil then result := '#<nil>' else result := V.AsString; end;
begin
    if self=nil then Exit('nil');

    if count=0
    then
        result := 'NIL'
    else
        begin
            result := '(';
            for i := 0 to system.High(fL.V) do result := result + str(fL.V[i]) + ' ';
            result[length(result)]:=')';
        end;
end;


function TVList.hash: DWORD;
var h: DWORD; i: integer;
begin
    result := 10006;
    for i := 0 to high do begin
        h := fL.V[i].hash;
        result := crc32(result, @h, SizeOf(h));
    end;
end;


function TVList.equal(V: TValue): boolean;
var VL: TVList; i: integer;
begin
    result := V is TVList;
    if not result then Exit;

    VL := V as TVList;
    result := fL=VL.fL;
    if result then Exit;

    result := Count=VL.Count;
    if not result then Exit;

    for i := 0 to high do begin
        result := fL.V[i].equal(VL[i]);
        if not result then Exit;
    end;
end;


constructor TVList.Create;
begin
    fL := TVListBody.Create();
    P := NewVariable(fL);
end;


constructor TVList.Create(VL: array of TValue);
begin
    fL := TVListBody.Create();
    SetLength(fL.V, Length(VL));
    P := NewVariable(fL);
    {$R-} move(VL[0], fL.V[0], sizeOf(TValue)*Length(VL)); {$R+}
end;


constructor TVList.CreateCopy(VL: array of TValue);
var i: integer;
begin
    fL := TVListBody.Create();
    SetLength(fL.V, Length(VL));
    P := NewVariable(fL);
    for i := 0 to system.high(fl.V) do if VL[i]<>nil then fL.V[i] := VL[i].Copy;
end;


constructor TVList.Create(body: PVariable);
begin
    P := body;
    if body <> nil
    then fL := body.V as TVListBody
    else fL := nil;
end;


destructor TVList.Destroy;
begin
    fL := nil;
    ReleaseVariable(P);
    inherited;
end;


function TVList.GetItem(index: integer): TValue;
begin
    result := fL.V[index].copy;
end;


procedure TVList.SetItem(index: integer; _V: TValue);
var i: integer;
begin
    copy_on_write;

    if index<0 then i := Length(fL.V)+index else i := index;

    fL.V[i].Free;
    fL.V[i] := _V;
end;


function TVList.Item(index: integer): TValue;
begin
    result := fL.V[index];
end;


function TVList.subseq(istart: integer; iend: integer): TValue;
var i, s, e : integer; res: TVList;
begin
    s := index(istart);
    e := index(iend);
    res := TVList.Create;
    SetLength(res.fL.V, e - s);
    for i := s to e - 1 do
        res.fL.V[i-s] := fL.V[i].Copy;
    Exit(res);
end;


function TVList.copy_on_write: boolean;
var P_old: PVariable;
begin
    if P = nil
    then
        begin
            fL := TVListBody.Create();
            P := NewVariable(fL);
            Exit(false);
    	end;

    if P.ref_count=1
    then
        begin
            P.tree := false;
            Exit(false);
		end;

    if P.ref_count>1
    then
        begin
            P_old := P;
            P := NewVariable(fL.Copy);
            ReleaseVariable(P_old);
            fL := P.V as TVListBody;
            Exit(true);
        end;

    result := false;
end;


procedure TVList.restore_fast_link;
begin
    fL := P.V as TVListBody;
end;


procedure TVList.Clear;
var i: integer;
begin
    copy_on_write;
    for i := 0 to system.High(fL.V) do fL.V[i].Free;
    fL.V := nil;
end;


function TVList.ValueList: TValues;
begin
    result := fL.V;
end;


function TVList.CdrValueList: TValues;
begin
    SetLength(result, Length(fL.V)-1);
    {$R-}move(fL.V[1], result[0], Length(result)*SizeOf(TValue));{$R+}
end;


function TVList.CAR: TValue;
begin
    if fL.V=nil
    then result := TVList.Create
    else result := fL.V[0].copy;
end;


function TVList.CDR: TVList;
var i: integer;
begin
    result := TVList.Create;
    SetLength(result.fL.V, Length(fL.V)-1);
    for i:=1 to system.high(fL.V) do result.fL.V[i-1] := fL.V[i].Copy;
end;


function TVList.get_body_link: PPVariable;
begin
    result := @P;
end;



function TVList.KeyIndex(id: TValue): integer;
begin
    result := high;
    while result > 0 do
        begin
            if id.equal(self[result-1]) then Exit;
            Dec(result, 2);
		end;

    result := -1;
end;


function TVList.Index(id: TValue): integer;
begin
    if id is TVInteger then Exit(index((id as TVInteger).fI));

    result := KeyIndex(id);

    if result < 0 then raise ELE.Create(id.AsString, 'invalid index/not found');
end;


procedure TVList.CopyOnWrite;
begin
    copy_on_write;
end;



initialization
    NULL := TVList.Create(nil);
    _ := TVSymbol.Create('_');
    kwFLAG := TVKeyword.Create(':FLAG');
    kwKEY := TVKeyword.Create(':KEY');
    kwOPTIONAL := TVKeyword.Create(':OPTIONAL');
    kwREST := TVKeyword.Create(':REST');
    kwTRUE := TVkeyword.Create('TRUE');

finalization
    kwTRUE.Free;
    kwREST.Free;
    kwOPTIONAL.Free;
    kwKEY.Free;
    kwFLAG.Free;
    _.Free;
    NULL.Free;

end.  //3477 3361 3324 3307 2938  2911 2988 3079 2885 3150
