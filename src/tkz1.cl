//комплексное умножение
float2 cmul(float2 a, float2 b)
{
	return (float2) ((a.x*b.x) - (a.y*b.y),
			         (a.x*b.y) + (a.y*b.x));
}

//комплексное деление
float2 cdiv(float2 a, float2 b)
{
	if (fabs(b.x) > fabs(b.y))
	{
		float tmp = b.y / b.x;
		float denom = b.x + b.y*tmp;
		return (float2) ((a.x + a.y*tmp) / denom,
				         (a.y - a.x*tmp) / denom);
	}
	else
	{
		float tmp = b.x / b.y;
		float denom = b.y + b.x*tmp;
		return (float2) (( a.y + a.x*tmp) / denom,
				         (-a.x + a.y*tmp) / denom);
	}
}


float2 cdiv_s(float2 a, float2 b)
{
    float div = b.x*b.x+b.y*b.y;

    return (float2)(( a.x*b.x+a.y*b.y) /div,
                    (-a.x*b.y+a.y*b.x) /div);
}


struct node
{
	float2 U; //float2 представляет комплексное число
	float2 I;
	float2 Y;
};


struct arc
{
	float2 Yl;
	float2 Yc;
	float2 K;
	float2 E;
	float2 I;
	float2 I_b;
	float2 I_e;
	int node1;
    int node2;
};


__kernel void tkz (const int nodesNumber,  const int arcsNumber, const int tasksNumber,
                   __global struct node* pNode,   __global struct arc* pArc
                   )
{
    int global_id = get_global_id(0);

    if (global_id >= tasksNumber) return;

    int i;
    int a;
    const int baseNode = nodesNumber * global_id;
    const int baseArc  =  arcsNumber * global_id;

    __private struct arc A[32];
    __private struct node N[32];

    for (a=0 ; a<arcsNumber; a++)
        A[a] = pArc[baseArc+a];

    for (a=0 ; a<nodesNumber; a++)
        N[a] = pNode[baseNode+a];

    for (a=0; a<nodesNumber; a++)
    {
        N[a].I = 0;
        N[a].Y = 0;
    }

	for (i=0; i<1000000; i++)
	{
        for (a=0; a<arcsNumber; a++)
        {
            int n1 = A[a].node1;
            int n2 = A[a].node2;
            //токи в ветвях
            float2 K = A[a].K;
            float2 u1 = N[n1].U;
            float2 u2 = N[n2].U;
            float2 u2_ = cmul(u2, K) - A[a].E;
            float2 du = u1 - u2_;
            float2 Yc = 0.5f*A[a].Yc;
            float2 I = cmul(du, A[a].Yl);
            float2 I_b = I + cmul(u1, Yc);
            float2 I_e = cmul(K, cmul(u2, Yc) - I);
            A[a].I = I;
            A[a].I_b = I_b;
            A[a].I_e = I_e;
            //небалансы в узлах
            N[n1].I -= I_b;
            N[n1].Y += A[a].Yl + Yc;
            N[n2].I -= I_e;
            N[n2].Y += cmul(cmul(K,K), A[a].Yl + Yc);
        }

        N[0].I = 0;
        N[0].Y = 0;

        bool balanced = true;

        for (a=1; a<nodesNumber; a++)
        {
            //условие завершения - отсутствие значительных небалансов в узлах
            //balanced = balanced && (length(N[a].I)<1.0f);
            balanced = balanced && (fabs(N[a].I.x)<1.0f) && (fabs(N[a].I.y)<1.0f);
            //корректировка узловых напряжений
            N[a].U += cdiv_s(N[a].I, N[a].Y);
            N[a].I = 0;
            N[a].Y = 0;
        }

        if (balanced)
        {
            N[0].I = (float2) ((float)i,0); // вернуть количество циклов
            break;
        }
	}

    for (a=0 ; a<arcsNumber; a++)
        pArc[baseArc+a] = A[a];

    for (a=0; a<nodesNumber; a++)
        pNode[baseNode+a] = N[a];


}
