unit oracle_module;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils, oracleconnection, sqldb, FileUtil, lisya_sql;

type

    { TOracle_m }

    TOracle_m = class(TDataModule)
        Connector: TOracleConnection;
        Query: TSQLQuery;
        SQLTransaction1: TSQLTransaction;
    private
        { private declarations }
    public
        { public declarations }
    end;


    { TLSQLoracle }

    TLSQLoracle = class (TLSQL)
        module: Toracle_m;
        constructor Connect(host,database,username,password: unicodestring);
        destructor Destroy; override;

        function description: unicodestring; override;
    end;

var
    Oracle_m: TOracle_m;

implementation

{$R *.lfm}

{ TLSQLoracle }

constructor TLSQLoracle.Connect(host, database, username,
    password: unicodestring);
begin
    inherited Create;
    module := Toracle_m.Create(nil);

    module.connector.DatabaseName:=database;
    module.Connector.HostName:=host;
    module.Connector.UserName:=username;
    module.Connector.Password:=password;

    query := module.Query;
end;

destructor TLSQLoracle.Destroy;
begin
    module.Free;
    inherited Destroy;
end;

function TLSQLoracle.description: unicodestring;
begin
    result := module.connector.UserName+'@'+module.connector.DatabaseName;
end;

end.

