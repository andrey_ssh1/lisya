﻿unit lisya_form;

{$mode delphi}{$H+}

interface

uses
    {$IFDEF LINUX}
    cthreads,
    cwstring,
    {$ENDIF}
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs;

type
    TLisyaForm = class(TForm)
    private
        { private declarations }
    public
        { public declarations }
    end;

var
    LisyaForm: TLisyaForm;



implementation

{$R *.lfm}

type

    TPointFloat = record x,y: real; end;
    TMatrix = array[1..3,1..3] of real;

const
    identity: TMatrix = ((1,0,0),(0,1,0),(0,0,1));

type

    { TGO }

    TGO = class
        fcolor: TColor;
        ftransform: TMatrix;
        procedure paint; virtual; abstract;
    end;


    { TGAxis }

    TGAxis = class (TGO)
        step: real;
    end;

    { TGAxisX }

    TGAxisX = class (TGAxis)
        //procedure paint; override;
    end;

    { TGAxisY }

    TGAxisY = class (TGAxis)
        //procedure paint; override;
    end;

    { TGGrid }

    TGGrid = class (TGO)
        stepX, stepY: real;
        //procedure paint; override;
    end;

    { TGMark }

    TGMark = class (TGO)
        fp: TPointFloat;
        text: unicodestring;
        //procedure paint; override;
    end;

    { TGPoint }

    TGPoint = class (TGO)
        x, y: real;
        //procedure paint; override;
    end;

    { TGPolyline }

    TGPolyline = class (TGO)
        points: array of TPointFloat;
        //procedure paint; override;
    end;

    TGPlot = class (TGPolyline)
    end;

var
    lo_x: real = -1;
    hi_x: real = 1;
    a_x: real = 2;
    lo_y: real = -1;
    hi_y: real = 1;
    a_y: real = 2;
    cw,ch: integer;
    frame: integer;
    cnv: TCanvas;
    equal_scale: boolean = true;
    color: TColor = clBlack;
    transform: TMatrix = ((1,0,0),(0,1,0),(0,0,1));
    tm: TMatrix = ((1,0,0),(0,1,0),(0,0,1));




end.

