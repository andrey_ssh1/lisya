unit mar_math;

{$mode delphi}{$H+}

interface

uses
    sysutils, math, ucomplex, mar, mar_math_arrays, mar_math_sp, mar_tensor;


function matrix_cross(a, b: TTensor): TTensor; overload;
procedure matrix_cross(m: TComplexes; v: TComplexes; var res: TComplexes); overload;
function matrix_identity(w: integer): TTensor;
function matrix_diagonal(d: TComplexes): TTensor; overload;
function matrix_diagonal(d: TDoubles): TTensor; overload;

function matrix_minor(m: TTensor; i, j: integer): TTensor; overload;
function matrix_determinant(e: TDoubles; w: integer): double; overload;
function matrix_determinant_c(e: TDoubles; w: integer): complex;

function matrix_inverse(m: TTensor): TTensor; overload;
function matrix_inverse(m: TComplexes): TComplexes; overload;
function matrix_transpose(m: TTensor): TTensor; overload;


function mar_RoundTo(const AValue: Double; const Digits: TRoundToRange): Double;


implementation


type
    EMatrixOperation = class(Exception) end;
    EMO = EMatrixOperation;


type TDoubles2 = array of TDoubles;


function mar_RoundTo(const AValue: Double; const Digits: TRoundToRange): Double;
var RV , f: Double;
begin
  RV:=IntPower(10, Digits);
  f := AValue/RV;
  if (f <= 9223372036854775807) and (f >= -9223372036854775808)
  then Result:=Round(f)*RV
  else Result := AValue;
end;


////////////////////////////////////////////////////////////////////////////////
///  Matrix  ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


//// Init //////////////////////////////////////////////////////////////////////


function matrix_identity(w: integer): TTensor;
var i: integer;
begin
    result.Init(TIntegers.Create(w, w), 1);
    SetLength(result.e, w*w);
    for i := 0 to high(result.e) do
        if (i mod (w+1))=0 then result.e[i] := 1 else result.e[i] := 0;
end;


function matrix_diagonal(d: TDoubles): TTensor;
var i, w: integer;
begin
    w := Length(d);
    result.Init(TIntegers.Create(w, w), 1);
    SetLength(result.e, w*w);
    for i := 0 to high(result.e) do
        if (i mod (w+1))=0 then result.e[i] := d[i div (w+1)] else result.e[i] := 0;
end;


function matrix_diagonal(d: TComplexes): TTensor;
var i, w: integer;
begin
    w := Length(d);
    result.Init(TIntegers.Create(w, w), 2);
    SetLength(result.e, w*w*2);
    for i := 0 to high(result.e) div 2 do
        if (i mod (w+1))=0
        then PCOMPLEX(@result.e[i*2])^ := d[i div (w+1)]
        else PCOMPLEX(@result.e[i*2])^ := 0;
end;


//// Arithmetic ////////////////////////////////////////////////////////////////

function matrix_cross(a, b: TTensor): TTensor;
var w, h, l, i, j, k, es :integer; r: real; c: complex;
begin
    if (not a.dims_count()=2) or (not b.dims_count()=2)
        or (a.size(1)<>b.size(2)) then EMO.Create('matrix cross/nonconformant arguments');

    w := b.size(1);
    h := a.size(2);
    l := a.size(1);

    es := max(a.dim[0], b.dim[0]);

    result.Init(TIntegers.Create(w, h), es);
    result.SetBufferLength();

    case 10*a.dim[0]+b.dim[0] of
        11: for j := 0 to h-1 do
                for i := 0 to w-1 do
                    begin
                        r := 0;
                        for k := 0 to l-1 do r := r + a.e[j*l+k] * b.e[k*w+i];
                        result.e[j*w+i] := r;
		        	end;
        12: for j := 0 to h-1 do
                for i := 0 to w-1 do
                    begin
                        c := 0;
                        for k := 0 to l-1 do c := c + a.e[j*l+k] * PCOMPLEX(@b.e[2*(k*w+i)])^;
                        PCOMPLEX(@result.e[2*(j*w+i)])^ := c;
					end;
        21: for j := 0 to h-1 do
                for i := 0 to w-1 do
                    begin
                        c := 0;
                        for k := 0 to l-1 do c := c + PCOMPLEX(@a.e[2*(j*l+k)])^ * b.e[k*w+i];
                        PCOMPLEX(@result.e[2*(j*w+i)])^ := c;
					end;
        22: for j := 0 to h-1 do
                for i := 0 to w-1 do
                    begin
                        c := 0;
                        for k := 0 to l-1 do c := c + PCOMPLEX(@a.e[2*(j*l+k)])^ * PCOMPLEX(@b.e[2*(k*w+i)])^;
                        PCOMPLEX(@result.e[2*(j*w+i)])^ := c;
					end;
	end;
end;

//умножение квадратной матрицы на вектор столбец
procedure matrix_cross(m: TComplexes; v: TComplexes; var res: TComplexes);
var i, j, w: integer; e: complex;
begin
    w := Length(v);
    for j := 0 to w-1 do
        begin
            e := 0;
            for i := 0 to w-1 do e := e + m[j*w+i]*v[i];
            res[j] := e;
        end;
end;


//// minor /////////////////////////////////////////////////////////////////////

function div_f(a, b: integer): integer; inline;
begin
    result := a div b;
    if a<0 then Dec(result);
end;


function minor(e: TComplexes; w, i, j: integer): TComplexes; overload;
var k: integer;
begin
    //минор матрицы
    SetLength(result, (w-1)**2+1);//выполняется одно лишнее копирование если исключается последний столбец
    for k := 0 to high(e)-(w*(w-j)) do result[div_f((k-i)*(w-1), w) + i] := e[k];
    for k := w*(j+1) to high(e) do result[div_f((k-i)*(w-1), w) + i - w+1] := e[k];
    SetLength(result, (w-1)**2);
end;


function minor(e: TDoubles; w, i, j: integer): TDoubles; overload;
var k: integer;
begin
    //минор матрицы
    SetLength(result, (w-1)**2+1);//выполняется одно лишнее копирование если исключается последний столбец
    for k := 0 to high(e)-(w*(w-j)) do result[div_f((k-i)*(w-1), w) + i] := e[k];
    for k := w*(j+1) to high(e) do result[div_f((k-i)*(w-1), w) + i - w+1] := e[k];
    SetLength(result, (w-1)**2);
end;


function matrix_minor(m: TTensor; i, j: integer): TTensor;
var c: TComplexes;
begin
    if m.size(1)<>m.size(2) then raise EMO.Create('minor/not square/');

    result.init(TIntegers.Create(m.size(1)-1, m.size(2)-1), m.dim[0]);
    if m.dim[0]=1
    then
        result.e := minor(m.e, m.size(1), i, j)
    else
        begin
            //TODO: излишнее копирование буферов
            result.SetBufferLength();
            c := minor(m.Complexes(), m.size(1), i, j);
            move(c[0], result.e[0], sizeof(result.e[0])*Length(result.e));
		end;
end;


//// transpose /////////////////////////////////////////////////////////////////


function matrix_transpose(m: TTensor): TTensor;
var i, j, es, w, h: integer;
begin
    es := m.dim[0];
    w := m.size(1);
    h := m.size(2);
    result.Init(TIntegers.create(h, w), es);
    result.SetBufferLength();
    for i := 0 to w - 1 do
        for j := 0 to h-1 do
            move(m.e[es*(w*j+i)], result.e[es*(h*i+j)], es*sizeof(m.e[0]));
end;


//// determinant ///////////////////////////////////////////////////////////////

function max_last_element_row(d: TDoubles2; w: integer; out row: integer): boolean; overload;
var m: double; r: integer;
begin
    //ищет в матрице строку с максимальным по абсолютному значению последним элементом
    row := 0;
    //это должно повысить устойчивость вычислений
    m := 0;
    for r := 0 to w-1 do
        if abs(d[r][w-1])>m
        then
            begin
                row := r;
                m := abs(d[r][w-1]);
			end;
    result := m>0;
end;


function max_last_element_row(d: array of TComplexes; w: integer; out row: integer): boolean; overload;
var m: double; r: integer;
    function sqmod(c: complex): real; begin sqmod := c.re*c.re + c.im*c.im; end;
begin
    //ищет в матрице строку с максимальным по абсолютному значению последним элементом
    //это должно повысить устойчивость вычислений
    m := 0;
    row := 0;
    for r := 0 to w-1 do
        if sqmod(d[r][w-1])>m
        then
            begin
                row := r;
                m := sqmod(d[r][w-1]);
			end;
    result := m>0;
end;


procedure swap_rows(var d: TDoubles2; r1, r2: integer); overload;
var tmp: TDoubles;
begin
    tmp := d[r1];
    d[r1] := d[r2];
    d[r2] := tmp;
end;


procedure swap_rows(var M: array of TComplexes; r1, r2: integer); overload;
var tmp: TComplexes;
begin
    tmp := M[r1];
    M[r1] := M[r2];
    M[r2] := tmp;
end;


function determinant_2(e: TComplexes): complex; overload;
begin
    result := e[0]*e[3]-e[1]*e[2];
end;


function determinant_2(e: TDoubles): double; overload;
begin
    result := e[0]*e[3]-e[1]*e[2];
end;


function determinant_3(e: TComplexes): complex; overload;
begin
    result :=   e[0]*e[4]*e[8] + e[1]*e[5]*e[6] + e[3]*e[2]*e[7]
              - e[2]*e[4]*e[6] - e[0]*e[5]*e[7] - e[1]*e[3]*e[8];
end;


function determinant_3(e: TDoubles): double; overload;
begin
    result :=   e[0]*e[4]*e[8] + e[1]*e[5]*e[6] + e[3]*e[2]*e[7]
              - e[2]*e[4]*e[6] - e[0]*e[5]*e[7] - e[1]*e[3]*e[8];
end;


function matrix_determinant(e: TDoubles; w: integer): double; overload;
var row, col: integer; d2: TDoubles2; base, k: double;
begin
    if Length(e) div w <> w then raise EMO.Create('determinant/nonsquare argument/');

    case w of
        1: EXIT(e[0]);
        2: EXIT(determinant_2(e));
        3: EXIT(determinant_3(e));
	end;

	Setlength(d2, w);
    for row := 0 to w-1 do
        begin
            SetLength(d2[row], w);
            {$R-}Move(e[row*w], d2[row][0], SizeOf(Double)*w);{$R+}
  	    end;

    result := 1;

    while w>1 do
        begin
            if not max_last_element_row(d2, w, row) then EXIT(0);
            if row<>w-1
            then
                begin
                    result := result * -1;
                    swap_rows(d2, w-1, row);
				end;

            base := d2[w-1][w-1];
            result := result * base;

            for row := 0 to w-2 do
                begin
                    k := - d2[row][w-1]/base;
                    for col := 0 to w-2 do
                        d2[row][col] := d2[row][col] + k*d2[w-1][col];
                    //последний столбец не обрабатывается, поскольку не влияет
                    //на результат - вычёркивается при извлечении минора
				end;
            Dec(w);
		end;

    result := result * d2[0][0];
end;


function matrix_determinant(var M: array of TComplexes): complex; overload;
var w, row, col: integer; base, k: complex;
begin
    w := Length(M);

    result := 1;

    while w>1 do
        begin
            if not max_last_element_row(M, w, row) then EXIT(0);
              if row<>w-1
              then
                  begin
                      result := result * -1;
                      swap_rows(M, w-1, row);
  				end;

              base := M[w-1][w-1];
              result := result * base;

              for row := 0 to w-2 do
                  begin
                      k := - M[row][w-1]/base;
                      for col := 0 to w-2 do
                          M[row][col] := M[row][col] + k*M[w-1][col];
                      //последний столбец не обрабатывается, поскольку не влияет
                      //на результат - вычёркивается при извлечении минора
  				end;
              Dec(w);
  		end;

      result := result * M[0][0];
end;


function matrix_determinant(e: TComplexes; w: integer): complex; overload;
var row: integer; M: array of TComplexes;
begin
    if Length(e) div w <> w then raise EMO.Create('determinant/nonsquare argument/');

    case w of
        1: EXIT(e[0]);
        2: EXIT(determinant_2(e));
        3: EXIT(determinant_3(e));
	end;

	Setlength(M, w);
    for row := 0 to w-1 do
        begin
            SetLength(M[row], w);
            {$R-}Move(e[row*w], M[row][0], SizeOf(complex)*w);{$R+}
  	    end;

    result := matrix_determinant(M);
end;


function matrix_determinant_c(e: TDoubles; w: integer): complex;
var row: integer; M: array of TComplexes;
begin
    if Length(e) div w <> 2*w then raise EMO.Create('determinant/nonsquare argument/');

    case w of
        1: EXIT(cinit(e[0], e[1]));
	end;

	Setlength(M, w);
    for row := 0 to w-1 do
        begin
            SetLength(M[row], w);
            {$R-}Move(e[row*w*2], M[row][0], SizeOf(complex)*w);{$R+}
  	    end;

    result := matrix_determinant(M);
end;



//// inverse ///////////////////////////////////////////////////////////////////

function A (c: TComplexes; w, i, j: integer): complex; overload;
begin
    result := ((-1)**(i+j))*matrix_determinant(minor(c,w,i,j),w-1);
end;


function A (r: TDoubles; w, i, j: integer): double; overload;
begin
    result := ((-1)**(i+j))*matrix_determinant(minor(r,w,i,j),w-1);
end;


function matrix_inverse_r(M: TTensor): TTensor;
var D: double; i, w: integer;
begin
    D := matrix_determinant(M.e, M.size(1));
    if D=0 then raise EMO.Create('matrix/inverse/zero determinant');
    result.dim := M.dim;
    result.Init(m.size(), m.dim[0]);
    result.SetBufferLength();
    w := m.size(1);

    if w=1
    then
        result.e[0] := 1/M.e[0]
    else
        for i := 0 to high(result.e) do
            result.e[i] := A(m.e, w, i div w, i mod w) / D
end;


function matrix_inverse_c(M: TTensor): TTensor;
var D: complex; i, w: integer; e: TComplexes;
begin
    w := M.size(1);
    D := matrix_determinant_c(M.e, w);
    if D=0 then raise EMO.Create('matrix/inverse/zero determinant');
    result.dim := M.dim;
    result.SetBufferLength();

    e := m.Complexes();

    if w=1
    then
        PCOMPLEX(@result.e[0])^ := cinv(e[0])
    else
        for i := 0 to high(e) do
            PCOMPLEX(@result.e[i*2])^ := A(e, w, i div w, i mod w) / D
end;


function matrix_inverse(m: TTensor): TTensor;
begin
    if m.size(1)<>m.size(2) then raise EMO.Create('matrix/inverse/nonsquare');

    case m.dim[0] of
        1: result := matrix_inverse_r(m);
	    2: result := matrix_inverse_c(m);
	end;
end;



function matrix_inverse(m: TComplexes): TComplexes;
var D: complex; i, w: integer;
begin
    w := round(sqrt(Length(m)));
    assert(Length(m)=w*w, 'matrix/inverse/nonsquare');
    D := matrix_determinant(m, w);
    assert(D<>0, 'matrix/inverse/zero determinant');
    SetLength(result, w*w);

    if w=1
    then
        result[0] := cinv(m[0])
    else
        for i := 0 to high(m) do
            result[i] := A(m, w, i div w, i mod w) / D;
end;


end.   //1521  1390

