﻿unit mar_opencl;

{$mode delphi}{$H+}

//ошибка линковки возникает поскольку в /usr/lib/x86_64-linux-gnu
//лежат libOpenCL.so.1.0.0 c номером версии, а в модуле cl.pp имя библиотеки
//указано без версии
//можно создать ссылку
//#ln -s libOpenCL.so.1.0.0 libOpenCL.so

interface

uses
    Classes, SysUtils, cl, ctypes;

const
    CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE = $11B3;
    CL_KERNEL_PRIVATE_MEM_SIZE                   = $11B4;

    CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF        = $1034;
    CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR           = $1036;
    CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT          = $1037;
    CL_DEVICE_NATIVE_VECTOR_WIDTH_INT            = $1038;
    CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG           = $1039;
    CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT          = $103A;
    CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE         = $103B;
    CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF           = $103C;
    CL_DEVICE_HOST_UNIFIED_MEMORY                = $1035;   // deprecated
    CL_DEVICE_LINKER_AVAILABLE                   = $103E;
    CL_DEVICE_BUILT_IN_KERNELS                   = $103F;
    CL_DEVICE_IMAGE_MAX_BUFFER_SIZE              = $1040;
    CL_DEVICE_IMAGE_MAX_ARRAY_SIZE               = $1041;
    CL_DEVICE_PARENT_DEVICE                      = $1042;
    CL_DEVICE_PARTITION_MAX_SUB_DEVICES          = $1043;
    CL_DEVICE_PARTITION_PROPERTIES               = $1044;
    CL_DEVICE_PARTITION_AFFINITY_DOMAIN          = $1045;
    CL_DEVICE_PARTITION_TYPE                     = $1046;
    CL_DEVICE_PREFERRED_INTEROP_USER_SYNC        = $1048;
    CL_DEVICE_PRINTF_BUFFER_SIZE                 = $1049;
    CL_DEVICE_PARTITION_EQUALLY                  = $1086;
    CL_DEVICE_PARTITION_BY_COUNTS                = $1087;
    CL_DEVICE_PARTITION_BY_COUNTS_LIST_END       = $0;
    CL_DEVICE_PARTITION_BY_AFFINITY_DOMAIN       = $1088;

    CL_DEVICE_QUEUE_ON_HOST_PROPERTIES           = $102A;
    CL_DEVICE_IMAGE_PITCH_ALIGNMENT              = $104A;
    CL_DEVICE_IMAGE_BASE_ADDRESS_ALIGNMENT       = $104B;
    CL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS          = $104C;
    CL_DEVICE_MAX_GLOBAL_VARIABLE_SIZE           = $104D;
    CL_DEVICE_QUEUE_ON_DEVICE_PROPERTIES         = $104E;
    CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE     = $104F;
    CL_DEVICE_QUEUE_ON_DEVICE_MAX_SIZE           = $1050;
    CL_DEVICE_MAX_ON_DEVICE_QUEUES               = $1051;
    CL_DEVICE_MAX_ON_DEVICE_EVENTS               = $1052;
    CL_DEVICE_SVM_CAPABILITIES_INFO              = $1053;
    CL_DEVICE_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE=$1054;
    CL_DEVICE_MAX_PIPE_ARGS                      = $1055;
    CL_DEVICE_PIPE_MAX_ACTIVE_RESERVATIONS       = $1056;
    CL_DEVICE_PIPE_MAX_PACKET_SIZE               = $1057;
    CL_DEVICE_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT= $1058;
    CL_DEVICE_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT  = $1059;
    CL_DEVICE_PREFERRED_LOCAL_ATOMIC_ALIGNMENT   = $105A;

    CL_DEVICE_SVM_COARSE_GRAIN_BUFFER            = (1 shl 0);
    CL_DEVICE_SVM_FINE_GRAIN_BUFFER              = (1 shl 1);
    CL_DEVICE_SVM_FINE_GRAIN_SYSTEM              = (1 shl 2);
    CL_DEVICE_SVM_ATOMICS                        = (1 shl 3);

    CL_DEVICE_AFFINITY_DOMAIN_NUMA               = (1 shl 0);
    CL_DEVICE_AFFINITY_DOMAIN_L4_CACHE           = (1 shl 1);
    CL_DEVICE_AFFINITY_DOMAIN_L3_CACHE           = (1 shl 2);
    CL_DEVICE_AFFINITY_DOMAIN_L2_CACHE           = (1 shl 3);
    CL_DEVICE_AFFINITY_DOMAIN_L1_CACHE           = (1 shl 4);
    CL_DEVICE_AFFINITY_DOMAIN_NEXT_PARTITIONABLE = (1 shl 5);

    CL_FP_SOFT_FLOAT                             = (1 shl 6);
    CL_FP_CORRECTLY_ROUNDED_DIVIDE_SQRT          = (1 shl 7);

    CL_DEVICE_TYPE_CUSTOM                        = (1 shl 4);

type
    cl_device_partition_property = intptr_t;
    cl_device_affinity_domain = cl_bitfield;
    cl_device_svm_capabilities = cl_bitfield;

type
    cl_platform_id_array = array of cl_platform_id;
    cl_device_id_array = array of cl_device_id;
    csize_t_array = array of csize_t;
    cl_device_partition_property_array = array of cl_device_partition_property;

    TKernelWorkGroupInfo = record
        WORK_GROUP_SIZE: csize_t;
        COMPILE_WORK_GROUP_SIZE: csize_t_array;
        LOCAL_MEM_SIZE: cl_uint;
        PREFERRED_WORK_GROUP_SIZE_MULTIPLE: csize_t;
        PRIVATE_MEM_SIZE: cl_ulong;
    end;

    { EOpenCL }

    EOpenCL = class (Exception)
        constructor Create(code: cl_int; msg: unicodestring); overload;
    end;

    { ECL_INVALID_VALUE }

    ECL_INVALID_VALUE = class (EOpenCL)
        constructor Create(msg: unicodestring);
    end;


function mclPlatforms: cl_platform_id_array;
function mclPlatform(name: string): cl_platform_id;
function mclGetPlatformInfo_STRING(platform: cl_platform_id; param_name: cl_platform_info): string;

function mclDevices(platform: cl_platform_id; device_type: cl_device_type = CL_DEVICE_TYPE_ALL): cl_device_id_array; overload;
function mclDevices(platform_name: string;    device_type: cl_device_type = CL_DEVICE_TYPE_ALL): cl_device_id_array; overload;
function mclDevices(                          device_type: cl_device_type = CL_DEVICE_TYPE_ALL): cl_device_id_array; overload;
function mclDevice(name: string; platform: cl_platform_id = nil): cl_device_id; overload;
function mclDevice(name: string; platform: string              ): cl_device_id; overload;


function mclGetDeviceInfo_STRING(device: cl_device_id; param_name: cl_device_info): string;
function mclGetDeviceInfo_UINT(device: cl_device_id; param_name: cl_device_info): cl_uint;
function mclGetDeviceInfo_ULONG(device: cl_device_id; param_name: cl_device_info): cl_ulong;
function mclGetDeviceInfo_BOOL(device: cl_device_id; param_name: cl_device_info): boolean;
function mclGetDeviceInfo_SIZE_T(device: cl_device_id; param_name: cl_device_info): csize_t;
function mclGetDeviceInfo_BITFIELD(device: cl_device_id; param_name: cl_device_info): cl_bitfield;
function mclGetDeviceInfo_PARTITION_PROPERTY_ARRAY(device: cl_device_id; param_name: cl_device_info): cl_device_partition_property_array;
function mclGetDeviceMaxWorkItemSizes(device: cl_device_id): csize_t_array;
function mclGetDeviceGlobalMemCacheType(device: cl_device_id): cl_device_mem_cache_type;
function mclGetDeviceLocalMemType(device: cl_device_id): cl_device_local_mem_type;
function mclGetDevicePlatform(device: cl_device_id): cl_platform_id;



function mclCreateContext(device: cl_device_id; notify: TContextNotify = nil; user_data: Pointer = nil): cl_context;
procedure mclReleaseContext(context: cl_context);

function mclCreateCommandQueue(context: cl_context; device: cl_device_id): cl_command_queue;
procedure mclFinish(command_queue: cl_command_queue);
procedure mclReleaseCommandQueue(queue: cl_command_queue);

function mclCreateBuffer(context: cl_context; flags: cl_mem_flags;
                         size: csize_t; host_ptr: Pointer): cl_mem;
procedure mclEnqueueWriteBuffer(command_queue: cl_command_queue; buffer: cl_mem;
                                cb: csize_t; ptr: pointer);
procedure mclEnqueueReadBuffer(command_queue: cl_command_queue; buffer: cl_mem;
                               cb: csize_t; ptr: pointer);


procedure mclReleaseMemObject(memobj: cl_mem);

function mclCreateProgramWithSource(context: cl_context; code: string): cl_program;
function mclGetProgramBuildLog(program_: cl_program; device: cl_device_id): string;
procedure mclBuildProgram(program_: cl_program; device: cl_device_id; options: string = '');
procedure mclReleaseProgram(program_: cl_program);


function mclCreateKernel(program_: cl_program; name: string): cl_kernel;
function mclGetKernelInfo_STRING(kernel: cl_kernel; param_name: cl_kernel_info): string;
function mclGetKernelInfo_UINT  (kernel: cl_kernel; param_name: cl_kernel_info): string;
function mclGetKernelContext(kernel: cl_kernel): cl_context;
function mclGetKernelProgram(kernel: cl_kernel): cl_program;

function mclGetKernelWorkGroupInfo_SIZE_T(      kernel: cl_kernel;
                                                device: cl_device_id;
                                                param_name: cl_kernel_work_group_info)
                                                : csize_t;
function mclGetKernelWorkGroupInfo_SIZE_T_ARRAY(kernel: cl_kernel;
                                                device: cl_device_id;
                                                param_name: cl_kernel_work_group_info)
                                                : csize_t_array;
function mclGetKernelWorkGroupInfo_ULONG(       kernel: cl_kernel;
                                                device: cl_device_id;
                                                param_name: cl_kernel_work_group_info)
                                                : cl_ulong;
function mclGetKernelWorkGroupInfo(             kernel: cl_kernel;
                                                device: cl_device_id)
                                                : TKernelWorkGroupInfo;
procedure mclSetKernelArg(kernel: cl_kernel; arg_index: cl_uint; arg_size: csize_t; arg_value: pointer);
procedure mclEnqueueKernel(command_queue: cl_command_queue; kernel: cl_kernel;
                           work_dim: cl_int;
                           global_size: array of csize_t;
                           local_size: array of csize_t;
                           event: Pcl_event = nil);
procedure mclReleaseKernel(kernel: cl_kernel);


procedure enqueueTask(command_queue: cl_command_queue; kernel: cl_kernel);



type
    TEventProfilingInfo = record
        _QUEUED, _SUBMIT, _START, _END: cl_ulong;
    end;


function mclGetEventProfilingInfo(event: cl_event): TEventProfilingInfo;

procedure mclWaitForEvent(event: cl_event);

function file_text(fn: unicodestring): string;

implementation


procedure CEC(code: cl_int; msg: unicodestring = '');
begin
    case code of
        0: ;
        CL_INVALID_VALUE: raise ECL_INVALID_VALUE.Create(msg);
        else raise EOpenCL.Create(code, msg);
    end;
end;


//// Platforms /////////////////////////////////////////////////////////////////

function mclPlatforms: cl_platform_id_array;
var num_platforms: cl_uint;
begin
    CEC(clGetPlatformIDs(0, nil, @num_platforms), 'mclPlatforms');
    SetLength(result, num_platforms);
    CEC(clGetPlatformIDs(num_platforms, @result[0], @num_platforms), 'mclPlatforms');
end;


function mclPlatform(name: string): cl_platform_id;
var p: integer; platforms: cl_platform_id_array;
begin
    platforms := mclPlatforms;
    for p := 0 to high(platforms) do
        if Pos(name, mclGetPlatformInfo_STRING(platforms[p], CL_PLATFORM_NAME))>0
        then
            Exit(platforms[p]);
    result := nil;
end;


function mclGetPlatformInfo_STRING(platform: cl_platform_id;
                                   param_name: cl_platform_info): string;
var size_ret: csize_t;
begin
    CEC(clGetPlatformInfo(platform, param_name, 0, nil, size_ret));
    SetLength(result, size_ret);
    CEC(clGetPlatformInfo(platform, param_name, size_ret, @result[1], size_ret));
    result := result[1..size_ret];
end;


//// Devices ///////////////////////////////////////////////////////////////////

function mclDevices(platform: cl_platform_id;
                    device_type: cl_device_type = CL_DEVICE_TYPE_ALL): cl_device_id_array;
var num_devices: cl_uint; errcode: cl_int;
begin
    SetLength(result, 0);
    errcode := clGetDeviceIDs(platform, device_type, 0, nil, @num_devices);
    if errcode = CL_DEVICE_NOT_FOUND then Exit;
    CEC(errcode);
    SetLength(result, num_devices);
    CEC(clGetDeviceIDs(platform, device_type, num_devices, @result[0], @num_devices));
end;


function mclDevices(platform_name: string;
                    device_type: cl_device_type = CL_DEVICE_TYPE_ALL): cl_device_id_array;
begin
    result := mclDevices(mclPlatform(platform_name), device_type);
end;


function mclDevices(device_type: cl_device_type = CL_DEVICE_TYPE_ALL): cl_device_id_array;
var platforms: cl_platform_id_array;
    platform_devices: cl_device_id_array;
    p, start: integer;
begin
    platforms := mclPlatforms();
    SetLength(result, 0);
    for p := 0 to high(platforms) do
    begin
        platform_devices := mclDevices(platforms[p], device_type);
        start := Length(result);
        SetLength(result, Length(result) + Length(platform_devices));
        if Length(platform_devices) > 0
        then Move(platform_devices[0], result[start], SizeOf(cl_device_id)*Length(platform_devices));
    end;
end;


function mclDevice(name: string; platform: cl_platform_id = nil): cl_device_id;
var d: integer;
    devices: cl_device_id_array;
    device_type: cl_device_type;
begin

    if      name = 'CPU' then device_type := CL_DEVICE_TYPE_CPU
    else if name = 'GPU' then device_type := CL_DEVICE_TYPE_GPU
    else                      device_type := CL_DEVICE_TYPE_ALL;

    if platform <> nil
    then devices := mclDevices(platform, device_type)
    else devices := mclDevices(device_type);

    for d := 0 to high(devices) do
        if Pos(name, mclGetDeviceInfo_STRING(devices[d], CL_DEVICE_NAME))>0
        then
            Exit(devices[d]);

    raise EOpenCL.Create('device '+name+' not found');
end;


function mclDevice(name: string; platform: string): cl_device_id;
begin
    result := mclDevice(name, mclPlatform(platform));
end;


function mclGetDeviceGlobalMemCacheType(device: cl_device_id)
                                                     : cl_device_mem_cache_type;
var size: csize_t;
begin
    CEC(clGetDeviceInfo(device,
        CL_DEVICE_GLOBAL_MEM_CACHE_TYPE, SizeOf(result), @result, size));
end;


function mclGetDeviceLocalMemType(device: cl_device_id)
                                                     : cl_device_local_mem_type;
var size: csize_t;
begin
    CEC(clGetDeviceInfo(device,
        CL_DEVICE_LOCAL_MEM_TYPE_INFO, SizeOf(result), @result, size));
end;


function mclGetDevicePlatform(device: cl_device_id): cl_platform_id;
var size: csize_t;
begin
    CEC(clGetDeviceInfo(device, CL_DEVICE_PLATFORM, SizeOf(result), @result, size));
end;


function mclGetDeviceInfo_STRING(device: cl_device_id;
                                 param_name: cl_device_info): string;
var size: csize_t;
begin
    CEC(clGetDeviceInfo(device, param_name, 0, nil, size));
    SetLength(result, size);
    CEC(clGetDeviceInfo(device, param_name, size, @result[1], size));
    result := result[1..size];
end;


function mclGetDeviceInfo_UINT(device: cl_device_id; param_name: cl_device_info): cl_uint;
var size: csize_t;
begin
    CEC(clGetDeviceInfo(device, param_name, SizeOf(result), @result, size));
end;


function mclGetDeviceInfo_ULONG(device: cl_device_id; param_name: cl_device_info): cl_ulong;
var size: csize_t;
begin
  CEC(clGetDeviceInfo(device, param_name, SizeOf(result), @result, size));
end;


function mclGetDeviceInfo_BOOL(device: cl_device_id; param_name: cl_device_info): boolean;
var size: csize_t; ret: cl_bool;
begin
    CEC(clGetDeviceInfo(device, param_name, SizeOf(ret), @ret, size));
    result := ret = CL_TRUE;
end;


function mclGetDeviceInfo_SIZE_T(device: cl_device_id; param_name: cl_device_info): csize_t;
var size: csize_t;
begin
    result := 0;
    CEC(clGetDeviceInfo(device, param_name, SizeOf(result), @result, size));
end;


function mclGetDeviceInfo_BITFIELD(device: cl_device_id; param_name: cl_device_info): cl_bitfield;
var size: csize_t;
begin
    result := 0;
    CEC(clGetDeviceInfo(device, param_name, SizeOf(result), @result, size));
end;


function mclGetDeviceInfo_PARTITION_PROPERTY_ARRAY(device: cl_device_id;
    param_name: cl_device_info): cl_device_partition_property_array;
var size: csize_t;
begin
    CEC(clGetDeviceInfo(device, param_name, 0, nil, size));
    SetLength(result, size div SizeOf(cl_device_partition_property));
    //if size > 0
    //then
        CEC(clGetDeviceInfo(device, param_name, size, @result[0], size));
end;


function mclGetDeviceMaxWorkItemSizes(device: cl_device_id): csize_t_array;
var size: csize_t; dimensions: cl_uint;
begin
    dimensions := mclGetDeviceInfo_UINT(device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS);
    SetLength(result, dimensions);
    CEC(clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_SIZES, SizeOf(result[0])* dimensions, @result[0], size));
end;



//// Context ///////////////////////////////////////////////////////////////////

function mclCreateContext(device: cl_device_id;
                          notify: TContextNotify = nil;
                          user_data: Pointer = nil): cl_context;
var context_properties: array[0..2] of cl_context_properties =
        (CL_CONTEXT_PLATFORM_INFO, 0, 0);
    errcode: cl_int;
begin
    context_properties[1] := cl_context_properties(mclGetDevicePlatform(device));
    result := clCreateContext(@context_properties[0],
                              1, @device,
                              notify, user_data,
                              errcode);
    CEC(errcode);
end;


procedure mclReleaseContext(context: cl_context);
begin
    CEC(clReleaseContext(context));
end;


//// Program ///////////////////////////////////////////////////////////////////

function mclCreateProgramWithSource(context: cl_context; code: string): cl_program;
var errcode: cl_int;
    Pcode: PChar;
begin
    Pcode := PChar(code);
    result := clCreateProgramWithSource(context,1, @Pcode, nil, errcode);
    CEC(errcode);
end;


procedure mclBuildProgram(program_: cl_program; device: cl_device_id;
                         options: string = '');
var errcode: cl_int;
begin
    errcode := clBuildProgram(program_, 1, @device, PChar(options), nil, nil);
    if errcode < 0
    then
        CEC(errcode, mclGetProgramBuildLog(program_, device));
end;


function mclGetProgramBuildLog(program_: cl_program; device: cl_device_id
    ): string;
var size: csize_t; build_log: string;
begin
    CEC(clGetProgramBuildInfo(program_, device, CL_PROGRAM_BUILD_LOG,
                                                    0, nil, size));
    SetLength(build_log, size);
    CEC(clGetProgramBuildInfo(program_, device, CL_PROGRAM_BUILD_LOG,
                                                    size, @build_log[1], size));
    result := build_log[1..size];
end;


function mclGetProgramInfo_UINT(program_: cl_program;
                                param_name: cl_program_info): cl_uint;
var size_ret: csize_t;
begin
    CEC(clGetProgramInfo(program_, param_name, SizeOf(result), @result, size_ret));
end;


procedure mclReleaseProgram(program_: cl_program);
begin
    CEC(clReleaseProgram(program_));
end;


//// Kernel ////////////////////////////////////////////////////////////////////

function mclCreateKernel(program_: cl_program; name: string): cl_kernel;
var errcode: cl_int;
begin
    result := clCreateKernel(program_, PChar(name), errcode);
    CEC(errcode);
end;


function mclGetKernelInfo_STRING(kernel: cl_kernel; param_name: cl_kernel_info): string;
var size: csize_t; buf: string;
begin
    CEC(clGetKernelInfo(kernel, param_name, 0, nil, size));
    SetLength(buf, size);
    CEC(clGetKernelInfo(kernel, param_name, size, @buf[1], size));
    result := buf[1..size];
end;


function mclGetKernelInfo_UINT(kernel: cl_kernel; param_name: cl_kernel_info): string;
var size: csize_t;
begin
    CEC(clGetKernelInfo(kernel, param_name, SizeOf(result), @result, size));
end;


function mclGetKernelContext(kernel: cl_kernel): cl_context;
var size: csize_t;
begin
    CEC(clGetKernelInfo(kernel, CL_KERNEL_CONTEXT, SizeOf(result), @result, size));
end;


function mclGetKernelProgram(kernel: cl_kernel): cl_program;
var size: csize_t;
begin
    CEC(clGetKernelInfo(kernel, CL_KERNEL_PROGRAM, SizeOf(result), @result, size));
end;


procedure mclReleaseKernel(kernel: cl_kernel);
begin
    CEC(clReleasekernel(kernel));
end;


function mclGetKernelWorkGroupInfo_SIZE_T(kernel: cl_kernel;
                                          device: cl_device_id;
                                          param_name: cl_kernel_work_group_info):
                                          csize_t;
begin
    CEC(clGetKernelWorkGroupInfo(kernel, device, param_name,
                                 SizeOf(result), @result, nil));
end;


function mclGetKernelWorkGroupInfo_SIZE_T_ARRAY(kernel: cl_kernel;
                                                device: cl_device_id;
                                                param_name: cl_kernel_work_group_info):
                                                csize_t_array;
var size: csize_t;
begin
    CEC(clGetKernelWorkGroupInfo(kernel, device, param_name, 0, nil, @size));
    SetLength(result, size div SizeOf(csize_t));
    CEC(clGetKernelWorkGroupInfo(kernel, device, param_name,
                                 size, @result[0], nil));
end;


function mclGetKernelWorkGroupInfo_ULONG(kernel: cl_kernel;
                                         device: cl_device_id;
                                         param_name: cl_kernel_work_group_info):
                                         cl_ulong;
begin
    CEC(clGetKernelWorkGroupInfo(kernel, device, param_name,
                                 SizeOf(result), @result, nil));
end;


function mclGetKernelWorkGroupInfo(kernel: cl_kernel; device: cl_device_id)
                                                        : TKernelWorkGroupInfo;
begin
    with result do
    begin
        WORK_GROUP_SIZE :=                    mclGetKernelWorkGroupInfo_SIZE_T(
                                                               kernel, device,
                    CL_KERNEL_WORK_GROUP_SIZE);

        COMPILE_WORK_GROUP_SIZE :=            mclGetKernelWorkGroupInfo_SIZE_T_ARRAY(
                                                               kernel, device,
                    CL_KERNEL_COMPILE_WORK_GROUP_SIZE);

        LOCAL_MEM_SIZE :=                     mclGetKernelWorkGroupInfo_ULONG(
                                                               kernel, device,
                    CL_KERNEL_LOCAL_MEM_SIZE);

        PREFERRED_WORK_GROUP_SIZE_MULTIPLE := mclGetKernelWorkGroupInfo_SIZE_T(
                                                               kernel, device,
                    CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE);

        PRIVATE_MEM_SIZE :=                   mclGetKernelWorkGroupInfo_ULONG(
                                                               kernel, device,
                    CL_KERNEL_PRIVATE_MEM_SIZE);
    end;
end;


procedure mclSetKernelArg(kernel: cl_kernel; arg_index: cl_uint; arg_size: csize_t;
    arg_value: pointer);
begin
    CEC(clSetKernelArg(kernel, arg_index, arg_size, arg_value))
end;


procedure mclEnqueueKernel(command_queue: cl_command_queue; kernel: cl_kernel;
                           work_dim: cl_int;
                           global_size: array of csize_t;
                           local_size: array of csize_t;
                           event: Pcl_event = nil);
begin
    CEC(clEnqueueNDRangeKernel( command_queue,
                                kernel,
                                work_dim,
                                nil,
                                @global_size[0],
                                @local_size[0],
                                0, nil, event));
end;


procedure enqueueTask(command_queue: cl_command_queue; kernel: cl_kernel);
begin
    CEC(clEnqueueTask(command_queue, kernel, 0 ,nil, nil));
end;


//// Queue /////////////////////////////////////////////////////////////////////

function mclCreateCommandQueue(context: cl_context; device: cl_device_id): cl_command_queue;
var errcode: cl_int;
begin
    result := clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE,
                                   errcode);
    CEC(errcode);
end;


procedure mclFinish(command_queue: cl_command_queue);
begin
    CEC(clFinish(command_queue));
end;


procedure mclReleaseCommandQueue(queue: cl_command_queue);
begin
    CEC(clReleaseCommandQueue(queue));
end;


//// Mem Object ////////////////////////////////////////////////////////////////

function mclCreateBuffer(context: cl_context; flags: cl_mem_flags; size: csize_t;
    host_ptr: Pointer): cl_mem;
var errcode: cl_int;
begin
    result := clCreateBuffer(context, flags, size, host_ptr, errcode);
    CEC(errcode);
end;


procedure mclEnqueueWriteBuffer(command_queue: cl_command_queue; buffer: cl_mem;
                                cb: csize_t; ptr: pointer);
begin
    CEC(clEnqueueWriteBuffer(command_queue, buffer, CL_FALSE, 0, cb, ptr,
                             0, nil, nil));
end;


procedure mclEnqueueReadBuffer(command_queue: cl_command_queue; buffer: cl_mem;
                               cb: csize_t; ptr: pointer);
begin
    CEC(clEnqueueReadBuffer(command_queue, buffer, CL_FALSE, 0, cb, ptr,
                            0, nil, nil));
end;


procedure mclReleaseMemObject(memobj: cl_mem);
begin
    CEC(clReleaseMemObject(memobj));
end;


//// Event /////////////////////////////////////////////////////////////////////

function mclGetEventProfilingInfo(event: cl_event): TEventProfilingInfo;
var size_ret: csize_t;
begin
    CEC(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_QUEUED,
                                SizeOf(result._QUEUED), @result._QUEUED, size_ret));
    CEC(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_SUBMIT,
                                SizeOf(result._SUBMIT), @result._SUBMIT, size_ret));
    CEC(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START,
                                SizeOf(result._START), @result._START, size_ret));
    CEC(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END,
                                SizeOf(result._END), @result._END, size_ret));
end;


procedure mclWaitForEvent(event: cl_event);
begin
    CEC(clWaitForEvents(1, @event));
end;


procedure mclWaitForEvents(events: array of cl_event);
begin
    CEC(clWaitForEvents(Length(events), @events[0]));
end;


function file_text(fn: unicodestring): string;
var sl: TStringList;
begin
    try
        sl := TStringList.Create;
        sl.LoadFromFile(fn);
        result := sl.Text;
    finally
        sl.Free;
    end;
end;



{ ECL_INVALID_VALUE }

constructor ECL_INVALID_VALUE.Create(msg: unicodestring);
begin
    inherited Create(CL_INVALID_VALUE, msg);
end;




{ EOpenCL }

constructor EOpenCL.Create(code: cl_int; msg: unicodestring);
begin
    if code = 0
    then Message := 'CL_SUCCESS'
    else Message:= clErrorText(code) + LineEnding + msg;
end;

end.

