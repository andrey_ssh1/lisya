﻿unit lisya_directory;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils, mar, fileutil, LazFileUtils, math;

type
    TFileVisibility = (fvVisible,fvHidden,fvAny);

function ifhh_path(dir: unicodestring): unicodestring;

function ifh_directory(path, mask: unicodestring;
        dir_only: boolean = false; visibility: TFileVisibility = fvAny): TStringArray;


function ifh_directory_tree(path, mask: unicodestring;
    dir_only: boolean = false; visibility: TFileVisibility = fvAny): TStringArray;

procedure ifh_copy_file(src, dest: unicodestring);

implementation

function ifhh_path(dir: unicodestring): unicodestring;
begin
    result := IncludeTrailingPathDelimiter(ExpandFileName(DirSep(dir)));
end;

function ifh_directory(path, mask: unicodestring;
    dir_only: boolean = false; visibility: TFileVisibility = fvAny): TStringArray;
var sr: TUnicodeSearchRec; found: boolean; fn: unicodestring;
label next;
begin
    SetLength(result,0);
    try
        found := findfirst(path+mask, faAnyFile, sr)=0;
        while found do begin
            fn := path+sr.name;
            if (sr.name='..') or (sr.name='.') then goto next;
            if dir_only and ((sr.Attr and faDirectory)=0) then goto next;
            case visibility of
                fvVisible: if (sr.Attr and faHidden)>0 then goto next;
                fvHidden: if (sr.Attr and faHidden)=0 then goto next;
                fvAny:;
            end;

            if (sr.Attr and faDirectory)<>0
            then fn := IncludeTrailingPathDelimiter(fn);

            append_string_array(result, fn);

            next:
            found := findNext(sr)=0;
        end;
    finally
        FindClose(sr);
    end;


end;


function ifh_directory_tree(path, mask: unicodestring;
    dir_only: boolean = false; visibility: TFileVisibility = fvAny): TStringArray;
var dirs: TStringArray; i: integer;
begin
    result := ifh_directory(path, mask, dir_only, visibility);
    dirs := ifh_directory(path, '*', true, visibility);
    for i := 0 to high(dirs) do append_string_array(result,
        ifh_directory_tree(dirs[i],mask,dir_only,visibility));
end;


procedure ifh_copy_file(src, dest: unicodestring);
var target: unicodestring;
begin
    if end_of_string_is(dest,DirectorySeparator)
    then target := dest+ExtractFileName(src)
    else target := dest;

    CopyFile(ExpandFileName(src), ExpandFileName(target),
            [cffOverwriteFile,cffCreateDestDirectory,cffPreserveTime],true);
end;


end.

