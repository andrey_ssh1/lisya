﻿program lisya;


uses
    {$IFDEF LINUX}
    cthreads,
    cwstring,
    //cmem,
    {$ENDIF}
    {$IFDEF GUI}
    Interfaces, Forms , lisya_form,
    {$ENDIF}
    sysutils, lisya_repl, dlisp_read, dlisp_eval, dlisp_values, lisya_streams,
	lisya_exceptions, pipes, lisya_string_predicates, lisya_sign, lisya_symbols,
	lisya_gc, lisya_optimizer, lisya_heuristic, lisya_protected_objects,
	lisya_regexp, lisya_ifh_subprograms, lisya_directory, lisya_sql,
	lisya_xml_entities, lisya_type_converters, TKZ, TKZ_types,
	lisya_tkz_solver_opencl, lisya_tkz_solver_poc, mar_math_sp, mar_math_arrays,
	mar_tensor, mar_math_graph, mar_zemq
    //,mar
    ;

//определения
//MySQL55
//ODBC
//ORACLE
//GUI
//NETWORK



begin
    {$if declared(UseHeapTrace)}
    if FileExists('lisya.trc') then DeleteFile('lisya.trc');
    SetHeaptraceOutput('lisya.trc');
    {$ifend}

    {$IFDEF GUI}
    RequireDerivedFormResource := True;
    //Application.Initialize;
    {$ENDIF}

    if (ParamCount=0) or not EXEC(paramStr(1)) then REPL;
end.



