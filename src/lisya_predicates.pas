﻿unit lisya_predicates;
{$mode delphi}
{$ASSERTIONS ON}
interface

uses
    {$IFDEF LINUX}
    cwstring,
    {$ENDIF}
    process, Classes, SysUtils, math, ucomplex, mar_math
    ,dlisp_values, lisya_streams, lisya_string_predicates;


function tpAny                                      ({%H-}V: TValue): boolean;

function tpAtom                                     (V: TValue): boolean;

function tpBreak                                    (V: TValue): boolean;

function tpBytes                                    (V: TValue): boolean;

function tpCharacter                                (V: TValue): boolean;

function tpComplex                                  (V: TValue): boolean;

function tpComplexOrNIL                             (V: TValue): boolean;

function tpData                                     (V: TValue): boolean;

function tpSequence                                 (V: TValue): boolean;

function tpContinue                                 (V: TValue): boolean;

function tpCompound                                 (V: TValue): boolean;

function tpDateTime                                 (V: TValue): boolean;

function tpFloat                                    (V: TValue): boolean;

function tpFunction                                 (V: TValue): boolean;

function tpGo                                       (V: TValue): boolean;

function tpGoto                                     (V: TValue): boolean;

function tpGrid                                     (V: TValue): boolean;

function tpHashTable                                (V: TValue): boolean;

function tpInteger                                  (V: tValue): boolean;

function tpIntegerOrNIL                             (V: tValue): boolean;

function tpInternalFunction                         (V: TValue): boolean;

function tpInternalPureComplexRoutine               (V: TValue): boolean;

function tpInternalRoutine                          (V: TValue): boolean;

function tpInternalSubprogramComplex                (V: TValue): boolean;

function tpInternalUnaryRoutine                     (V: TValue): boolean;

function tpKeyword                                  (V: TValue): boolean;

function tpKeywordOrNIL                             (V: TValue): boolean;

function tpList                                     (V: TValue): boolean;

function tpListOrNIL                                (V: TValue): boolean;

function tpListNotEmpty                             (V: TValue): boolean;

function tpListOfByteses                            (V: TValue): boolean;

function tpListOfCompounds                          (V: TValue): boolean;

function tpListOfDateTimes                          (V: TValue): boolean;

function tpListOfIntegers                           (V: TValue): boolean;

function tpListOfKeywords                           (V: TValue): boolean;

function tpListOfLists                              (V: TValue): boolean;

function tpListOfNumbers                            (V: TValue): boolean;

function tpListOfRanges                             (V: TValue): boolean;

function tpListOfRangesInteger                      (V: TValue): boolean;

function tpListOfReals                              (V: TValue): boolean;

function tpListOfRecords                            (V: TValue): boolean;

function tpListOfTimes                              (V: TValue): boolean;

function tpListOfDurations                          (V: TValue): boolean;

function tpListOfNames                              (V: TValue): boolean;

function tpListOfStrings                            (V: TValue): boolean;

function tpListOfSubprograms                        (V: TValue): boolean;

function tpListOfSubprogramsPure                    (V: TValue): boolean;

function tpListOfSymbols                            (V: TValue): boolean;

function tpListOfTensors                            (V: TValue): boolean;

function tpListOrSymbol                             (V: TValue): boolean;

function tpListOrQueue                              (V: TValue): boolean;

function tpListXMLnode                              (V: TValue): boolean;

function tpMacro                                    (V: TValue): boolean;

function tpName                                     (V: TValue): boolean;

function tpNativNIL                                 (V: TValue): boolean;

function tpNIL                                      (V: TValue): boolean;

function tpNILnil                                   (V: TValue): boolean;

function tpNumber                                   (V: TValue): boolean;

function tpNumberOrNIL                              (V: TValue): boolean;

function tpOperator                                 (V: TValue): boolean;

function tpQueue                                    (V: TValue): boolean;

function tpPredicate                                (V: TValue): boolean;

function tpProcedure                                (V: TValue): boolean;

function tpRoutine                                  (V: TValue): boolean;

function tpProcess                                  (V: TValue): boolean;

function tpRange                                    (V: TValue): boolean;

function tpRangeOrNIL                               (V: TValue): boolean;

function tpRangeInteger                             (V: TValue): boolean;

function tpReal                                     (V: TValue): boolean;

function tpRealOrNIL                                (V: TValue): boolean;

function tpRecord                                   (V: TValue): boolean;

function tpReturn                                   (V: TValue): boolean;

function tpSelfEvaluating                           (V: TValue): boolean;

function tpSingleDateTime                           (V: TValue): boolean;

function tpSQL                                      (V: TValue): boolean;

function tpStream                                   (V: TValue): boolean;

function tpStreamFile                               (V: TValue): boolean;

function tpStreamSerial                             (V: TValue): boolean;

function tpString                                   (V: TValue): boolean;

function tpStringOrNIL                              (V: TValue): boolean;

function tpSubprogram                               (V: TValue): boolean;

function tpSubprogramPure1                           (V: TValue): boolean;

function tpSymbol                                   (V: TValue): boolean;

function tpTensor                                   (V: TValue): boolean;

function tpTensor1d                                 (V: TValue): boolean;

function tpTensor2d                                 (V: TValue): boolean;

function tpTensorReal                               (V: TValue): boolean;

function tpTensorReal1d                             (V: TValue): boolean;

function tpTensorReal2d                             (V: TValue): boolean;

function tpTime                                     (V: TValue): boolean;

function tpDuration                                 (V: TValue): boolean;

function tpTreeOfNumbers                            (V: TValue): boolean;

function tpTreeOfReal                               (V: TValue): boolean;

function tpTrue                                     (V: TValue): boolean;

function tpZIPArchivePointer                        (V: TValue): boolean;


/////////////////////////////////////
/// Value Predicates ////////////////
/////////////////////////////////////

function vphExpressionOperator(V: TValue;
    const op: array of TOperatorEnum;
    const tp: array of TTypePredicate): boolean;


function vpComplexNotZero                           (V: TValue): boolean;


function vpDurationNotNegative                      (V: TValue): boolean;


function vpEmpty                                    (V: TValue): boolean;


function vpExpression_CONST                         (V: TValue): boolean;

function vpExpression_FUNCTION_lambda               (V: TValue): boolean;

function vpExpression_PACKAGE                       (V: TValue): boolean;

function vpExpression_VAR                           (V: TValue): boolean;

function vpInteger8                                 (V: TValue): boolean;

function vpInteger16                                (V: TValue): boolean;

function vpInteger32                                (V: TValue): boolean;

function vpInteger60Range                           (V: TValue): boolean;

function vpInteger60RangeOrNIL                      (V: TValue): boolean;

function vpInteger1000Range                         (V: TValue): boolean;

function vpInteger1000RangeOrNIL                    (V: TValue): boolean;

function vpIntegerAbsOne                            (V: TValue): boolean;

function vpIntegerByte                              (V: TValue): boolean;

function vpIntegerDWORD                             (V: TValue): boolean;

function vpIntegerNegative                          (V: TValue): boolean;

function vpIntegerNotZero                           (V: TValue): boolean;

function vpIntegerPositive                          (V: TValue): boolean;

function vpIntegerPositiveOrNIL                     (V: TValue): boolean;

function vpIntegerRoundToRange                      (V: TValue): boolean;

function vpIntegerRoundToRangeOrNIL                 (V: TValue): boolean;

function vpIntegerWORD                              (V: TValue): boolean;

function vpIntegerWORDorNIL                         (V: TValue): boolean;

function vpIntegerZero                              (V: TValue): boolean;


function vpKeyword__                                (V: TValue): boolean;

function vpKeyword_ALL                              (V: TValue): boolean;

function vpKeyword_APPEND                           (V: TValue): boolean;

function vpKeyword_ARRAY                            (V: TValue): boolean;

function vpKeyword_BOM                              (V: TValue): boolean;

function vpKeyword_BY_HEAD                          (V: TValue): boolean;

function vpKeyword_CAPTION                          (V: TValue): boolean;

function vpKeyword_CAPTURE                          (V: TValue): boolean;

function vpKeyword_CAPTURED                         (V: TValue): boolean;

function vpKeyword_CASE_INSENSITIVE                 (V: TValue): boolean;

function vpKeyword_CENTER                           (V: TValue): boolean;

function vpKeyword_CP1251                           (V: TValue): boolean;

function vpKeyword_CP1252                           (V: TValue): boolean;

function vpKeyword_CP866                            (V: TValue): boolean;

function vpKeyword_CPU_COUNT                        (V: TValue): boolean;

function vpKeyword_CSV                              (V: TValue): boolean;

function vpKeyword_DEFLATE                          (V: TValue): boolean;

function vpKeyword_EQUAL                            (V: TValue): boolean;

function vpKeyword_FIRST                            (V: TValue): boolean;

function vpKeyword_FLAG                             (V: TValue): boolean;

function vpKeyword_GENERATOR                        (V: TValue): boolean;

function vpKeyword_HIDDEN                           (V: TValue): boolean;

function vpKeyword_HTML                             (V: TValue): boolean;

function vpKeyword_INTEGER                          (V: TValue): boolean;

function vpKeyword_KEY                              (V: TValue): boolean;

function vpKeyword_KEY_VALUE                        (V: TValue): boolean;

function vpKeyword_KOI8R                            (V: TValue): boolean;

function vpKeyword_LATIN1                           (V: TValue): boolean;

function vpKeyword_LEFT                             (V: TValue): boolean;

function vpKeyword_LESS                             (V: TValue): boolean;

function vpKeyword_LINE                             (V: TValue): boolean;

function vpKeyword_MACROEXPAND                      (V: TValue): boolean;

function vpKeyword_MEAN                             (V: TValue): boolean;

function vpKeyword_MORE                             (V: TValue): boolean;

function vpKeyword_OFF                              (V: TValue): boolean;

function vpKeyword_ON                               (V: TValue): boolean;

function vpKeyword_OPTIONAL                         (V: TValue): boolean;

function vpKeyword_PROLOGUE                         (V: TValue): boolean;

function vpKeyword_RANDOM                           (V: TValue): boolean;

function vpKeyword_READ                             (V: TValue): boolean;

function vpKeyword_REST                             (V: TValue): boolean;

function vpKeyword_RESULT                           (V: TValue): boolean;

function vpKeyword_RIGHT                            (V: TValue): boolean;

function vpKeyword_SECOND                           (V: TValue): boolean;

function vpKeyword_TRUE                             (V: TValue): boolean;

function vpKeyword_UTF16                            (V: TValue): boolean;

function vpKeyword_UTF16BE                          (V: TValue): boolean;

function vpKeyword_UTF16LE                          (V: TValue): boolean;

function vpKeyword_UTF32                            (V: TValue): boolean;

function vpKeyword_UTF32BE                          (V: TValue): boolean;

function vpKeyword_UTF32LE                          (V: TValue): boolean;

function vpKeyword_UTF8                             (V: TValue): boolean;

function vpKeyword_WIDTH                            (V: TValue): boolean;

function vpKeyword_WRITE                            (V: TValue): boolean;

function vpKeyword_VISIBLE                          (V: TValue): boolean;

function vpKeywordAlign                             (V: TValue): boolean;

function vpKeywordAlignOrNil                        (V: TValue): boolean;

function vpKeywordEncoding                          (V: TValue): boolean;

function vpKeywordEncodingOrNIL                     (V: TValue): boolean;

function vpKeywordFileAttributes                    (V: TValue): boolean;

function vpKeywordFileMode                          (V: TValue): boolean;

function vpKeywordFileModeOrNIL                     (V: TValue): boolean;

function vpKeywordFoldFunction                      (V: TValue): boolean;

function vpKeywordGridArc                           (V: TValue): boolean;

function vpKeywordTableModeOrNIL                    (V: TValue): boolean;

function vpKeywordThinningModeOrNIL                 (V: TValue): boolean;

function vpKeywordVisibilityOrNIL                   (V: TValue): boolean;



function vpListEvenLength                           (V: TValue): boolean;

function vpListHeaded_ELSE                          (V: TValue): boolean;

function vpListHeaded_ELT                           (V: TValue): boolean;

function vpListHeaded_EXCEPTION                     (V: TValue): boolean;

function vpListHeaded_INS                           (V: TValue): boolean;

function vpListHeaded_INSET                         (V: TValue): boolean;

function vpListHeaded_SUBSEQ                        (V: TValue): boolean;

function vpListHeaded_THEN                          (V: TValue): boolean;

function vpListHeaded_VALUE                         (V: TValue): boolean;

function vpListHeadedByString                       (V: TValue): boolean;

function vpListHeadedByListOfStrings                (V: TValue): boolean;

function vpListLength_1_2                           (V: TValue): boolean;
function vpListLength_2                             (V: TValue): boolean;
function vpListLength_3                             (V: TValue): boolean;
function vpListLength_4_6                           (V: TValue): boolean;
function vpListLength_5_6                           (V: TValue): boolean;


function vpListGraph                                (V: TValue): boolean;

function vpListGraphArc                             (V: TValue): boolean;

function vpListGridArc                              (V: TValue): boolean;

function vpListGridRandomization                    (V: TValue): boolean;

function vpListKeywordValue                         (V: TValue): boolean;

function vpListKeyStringValue                       (V: TValue): boolean;

function vpListLambdaExpression                     (V: TValue): boolean;

function vpListMatrix                               (V: TValue): boolean;

function vpListMatrixOfNumbers                      (V: TValue): boolean;

function vpListMatrixOfIntegers                     (V: TValue): boolean;

function vpListOfByte                               (V: TValue): boolean;

function vpListOfDurationsForMul                    (V: TValue): boolean;

function vpListOfFileAttributes                     (V: TValue): boolean;

function vpListOfIndices_1d                         (V: TValue): boolean;

function vpListOfIndices_2d                         (V: TValue): boolean;

function vpListOfNaturals                           (V: TValue): boolean;

function vpListOfNumbers                            (V: TValue): boolean;

function vpListOfStringsEvenLength                  (V: TValue): boolean;

function vpListOfSymbolValuePairs                   (V: TValue): boolean;

function vpListOfSummableTimes                      (V: TValue): boolean;

function vpListRoutineExpression                    (V: TValue): boolean;

function vpListSymbolValue                          (V: TValue): boolean;

function vpListVariableExpression                   (V: TValue): boolean;

function vpListWithLastList                         (V: TValue): boolean;


function vpNatural                                  (V: TValue): boolean;

function vpNaturalOrNIL                             (V: TValue): boolean;

function vpNumberNotZero                            (V: TValue): boolean;


function vpOperator_KEY                             (V: TValue): boolean;

function vpOperator_VAR                             (V: TValue): boolean;

function vpOperatorVarDeclaration                   (V: TValue): boolean;

function vpOperatorVarOrConst                       (V: TValue): boolean;

function vpOperatorRoutine                          (V: TValue): boolean;



function vpPairListNatural                          (V: TValue): boolean;

function vpPairNaturalList                          (V: TValue): boolean;

function vpPairPositiveList                         (V: TValue): boolean;

function vpPointerToVar                             (V: TValue): boolean;


function vpRangeIntegerNotNegative                  (V: TValue): boolean;


function vpRealAbsOneOrMore                         (V: TValue): boolean;

function vpRealNegative                             (V: TValue): boolean;

function vpRealNotNegative                          (V: TValue): boolean;

function vpRealNotNegativeOrNIL                     (V: TValue): boolean;

function vpRealNotZero                              (V: TValue): boolean;

function vpRealPositive                             (V: TValue): boolean;

function vpRealPositiveOrNIL                        (V: TValue): boolean;

function vpRealZero                                 (V: TValue): boolean;


function vpSequenceNotEmpty                         (V: TValue): boolean;


function vpStream                                   (V: TValue): boolean;

function vpStreamEnd                                (V: TValue): boolean;


function vpStringDateTime                           (V: TValue): boolean;

function vpStringDuration                           (V: TValue): boolean;

function vpStringEmpty                              (V: TValue): boolean;

function vpStringInteger                            (V: TValue): boolean;

function vpStringNumber                             (V: TValue): boolean;

function vpStringPath                               (V: TValue): boolean;

function vpStringRange                              (V: TValue): boolean;

function vpStringRangeInteger                       (V: TValue): boolean;

function vpStringReal                               (V: TValue): boolean;

function vpStringTestPredicate                      (V: TValue): boolean;


function vpSymbol__                                 (V: TValue): boolean;

function vpSymbol_ELSE                              (V: TValue): boolean;

function vpSymbol_IN                                (V: TValue): boolean;

function vpSymbol_LIST                              (V: TValue): boolean;

function vpSymbol_RANGE                             (V: TValue): boolean;

function vpSymbolQualified                          (V: TValue): boolean;


function vpVariableNames                            (V: TValue): boolean;


////////////////////////////////////////////////////////////////////////////////
////// KEYWORD PREDICATES //////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////
/// Rest Predicates /////////////////
/////////////////////////////////////

function rphOf(V: TValue; tp: TTypePredicate): boolean;

function rpAny                                      (V: TValue): boolean;

function rpNIL                                      (V: TValue): boolean;

function rpIntegers                                 (V: TValue): boolean;

function rpLists                                    (V: TValue): boolean;

function rpNumbers                                  (V: TValue): boolean;

function rpOneTensor                                (V: TValue): boolean;

function rpOneTensorReal                            (V: TValue): boolean;

function rpReals                                    (V: TValue): boolean;

function rpTimes                                    (V: TValue): boolean;

function rpDateTimes                                (V: TValue): boolean;

function rpDurations                                (V: TValue): boolean;



implementation /////////////////////////////////////////////////////////////////

uses lisya_protected_objects;

function tphListOf(V: TValue; p: TTypePredicate): boolean; inline;
var i: integer;
begin
    result := false;
    if V is TVList
    then begin
        for i := 0 to (V as TVList).high do
            if not p((V as TVList)[i]) then Exit;
        result := true;
    end;
end;

function tphSingle(V: TValue; p: TTypePredicate): boolean; inline;
var L: TVList;
begin
    result := V is TVList;
    if not result then Exit;
    L := V as TVList;
    result := (L.Count=1) and p(L[0]);
end;

function tpAny({H-}V: TValue): boolean;
begin
    result := true;
end;

function tpAtom(V: TValue): boolean;
begin
    result := (not (V is TVList)) or ((V as TVList).Count=0);
end;

function tpBreak(V: TValue): boolean;
begin
    result := V is TVBreak;
end;

function tpBytes(V: TValue): boolean;
begin
    result := V is TVBytes;
end;

function tpCharacter(V: TValue): boolean;
begin
    result := (V is TVString) and ((V as TVString).Count=1);
end;

function tpComplex(V: TValue): boolean;
begin
    result := V is TVComplex;
end;

function tpComplexOrNIL(V: TValue): boolean;
begin
    result := (V is TVComplex) or tpNIL(V);
end;

function tpData(V: TValue): boolean;
begin
    result := tpNumber(V) or tpTensor(V) or tpList(V);
end;

function tpSequence(V: TValue): boolean;
begin
    result := (V is TVSequence);
end;

function tpContinue(V: TValue): boolean;
begin
    result := V is TVContinue;
end;

function tpCompound(V: TValue): boolean;
begin
    result := V is TVCompound;
end;

function tpDateTime(V: TValue): boolean;
begin
    result := (V is TVDateTime);
end;

function tpFloat(V: TValue): boolean;
begin
    result := V is TVFloat;
end;

function tpFunction(V: TValue): boolean;
begin
    result := V is TVFunction;
end;

function tpGo(V: TValue): boolean;
begin
    result := V is TVGo;
end;

function tpGoto(V: TValue): boolean;
begin
    result := V is TVGoto;
end;

function tpGrid(V: TValue): boolean;
begin
    result := V is TVGrid;
end;

function tpHashTable(V: TValue): boolean;
begin
    result := V is TVHashTable;
end;

function tpInteger(V: tValue): boolean;
begin
    result := V is TVInteger;
end;

function tpIntegerOrNIL(V: tValue): boolean;
begin
    result := tpInteger(V) or tpNIL(V);
end;



function tpInternalFunction(V: TValue): boolean;
begin
    result := V is TVInternalFunction;
end;

function tpInternalPureComplexRoutine(V: TValue): boolean;
begin
    result := V is TVInternalFunction
        or V is TVInternalArrayFunction;
end;


function tpInternalRoutine(V: TValue): boolean;
begin
    result := V is TVInternalRoutine;
end;


function tpInternalSubprogramComplex(V: TValue): boolean;
begin
    result := (V is TVInternalArrayRoutine)
        or (V is TVInternalRoutine);
end;


function tpInternalUnaryRoutine(V: TValue): boolean;
begin
    result := V is TVInternalUnaryRoutine;
end;

function tpKeyword(V: TValue): boolean;
begin
    result := V is TVKeyword;
end;

function tpKeywordOrNIL(V: TValue): boolean;
begin
    result := tpKeyword(V) or tpNIL(V);
end;

function tpList(V: TValue): boolean;
begin
    result := V is TVList;
end;

function tpListOrNIL(V: TValue): boolean;
begin
    result := (V=nil) or (V is TVList);
end;

function tpListNotEmpty(V: TValue): boolean;
begin
    result := (V is TVList) and ((V as TVList).Count>0);
end;

function tpListOfByteses(V: TValue): boolean;
begin
    result := tphListOf(V, tpBytes);
end;

function tpListOfCompounds(V: TValue): boolean;
begin
    result := tphListOf(V, tpCompound);
end;

function tpListOfDateTimes(V: TValue): boolean;
begin
    result := tphListOf(V, tpDateTime);
end;

function tpListOfIntegers(V: TValue): boolean;
begin
    result := tphListOf(V, tpInteger);
end;

function tpListOfKeywords(V: TValue): boolean;
begin
    result := tphListOf(V, tpKeyword);
end;

function tpListOfLists(V: TValue): boolean;
begin
    result := tphListOf(V, tpList);
end;

function tpListOfNumbers(V: TValue): boolean;
begin
    result := tphListOf(V, tpNumber);
end;


function tpListOfRanges(V: TValue): boolean;
begin
    result := tphListOf(V, tpRange);
end;


function tpListOfRangesInteger(V: TValue): boolean;
begin
    result := tphListOf(V, tpRangeInteger);
end;


function tpListOfReals(V: TValue): boolean;
begin
    result := tphListOf(V, tpReal);
end;

function tpListOfRecords(V: TValue): boolean;
begin
    result := tphListOf(V, tpRecord);
end;

function tpListOfTimes(V: TValue): boolean;
begin
    result := tphListOf(V, tpTime);
end;

function tpListOfDurations(V: TValue): boolean;
begin
    result := tphListOf(V, tpDuration);
end;

function tpListOfNames(V: TValue): boolean;
begin
    result := tphListOf(V, tpName);
end;

function tpListOfStrings(V: TValue): boolean;
begin
    result := tphListOf(V, tpString);
end;

function tpListOfSubprograms(V: TValue): boolean;
begin
    result := tphListOf(V, tpSubprogram);
end;

function tpListOfSubprogramsPure(V: TValue): boolean;
begin
    result := tphListOf(V, tpSubprogramPure1);
end;

function tpListOfSymbols(V: TValue): boolean;
begin
    result := tphListOf(V, tpSymbol);
end;

function tpListOfTensors(V: TValue): boolean;
begin
    result := tphListof(V, tpTensor);
end;


function tpListOrSymbol(V: TValue): boolean;
begin
    result := (V is TVList) or (V is TVSymbol);
end;


function tpListOrQueue(V: TValue): boolean;
begin
    result := (V is TVList) or (V is TVQueue);
end;


function tpListXMLnode(V: TValue): boolean;
begin
    result := (V is TVList)
        and ((V as TVList).Count>=2)
        and tpString((V as TVList)[0])
        and vpListEvenLength((V as TVList)[1])
        and tpListOfStrings((V as TVList)[1]);
end;


function tpMacro(V: TValue): boolean;
begin
    result := V is TVMacro;
end;



function tpName(V: TValue): boolean;
begin
    result := (V is TVSymbol) and not (V is TVKeyword);
end;


function tpNativNIL(V: TValue): boolean;
begin
    result := V = nil;
end;


function tpNIL(V:  TValue): boolean;
begin
    result := (V=nil)
        or (V is TVList) and ((V as TVList).Count=0);
end;


function tpNILnil(V:  TValue): boolean;
begin
    result := (V = nil) or tpNIL(V);
end;


function tpNumber(V: TValue): boolean;
begin
    result := V is TVNumber;
end;


function tpNumberOrNIL(V: TValue): boolean;
begin
    result := (V is TVNumber) or tpNIL(V);
end;


function tpOperator(V: TValue): boolean;
begin
    result := V is TVOperator;
end;


function tpQueue(V: TValue): boolean;
begin
    result := V is TVQueue;
end;

function tpPredicate(V: TValue): boolean;
begin
    result := V is TVPredicate;
end;

function tpProcedure(V: TValue): boolean;
begin
    result := V is TVProcedure;
end;

function tpRoutine(V: TValue): boolean;
begin
    result := V is TVRoutine;
end;

function tpProcess(V: TValue): boolean;
begin
    result := (V is TVStream) and ((V as TVStream).target is TLProcess);
end;

function tpRange(V: TValue): boolean;
begin
    result := V is TVRange;
end;


function tpRangeOrNIL(V: TValue): boolean;
begin
    result := (V=nil) or tpRange(V) or tpNIL(V);
end;


function tpRangeInteger(V: TValue): boolean;
begin
    result := V is TVRangeInteger;
end;

function tpReal(V: TValue): boolean;
begin
    result := (V is TVReal);
end;

function tpRealOrNIL(V: TValue): boolean;
begin
    result := tpReal(V) or tpNIL(V);
end;

function tpRecord(V: TValue): boolean;
begin
    result := V is TVRecord;
end;

function tpReturn(V: TValue): boolean;
begin
    result := V is TVReturn;
end;

function tpSelfEvaluating(V: TValue): boolean;
begin
    result := not (tpListNotEmpty(V) or tpName(V));
end;

function tpSingleDateTime(V: TValue): boolean;
begin
    result := tphSingle(V, tpDateTime);
end;

function tpSQL(V: TValue): boolean;
begin
    result := V is TVSQL;
end;

function tpStream(V: TValue): boolean;
begin
    result := V is TVStream;
end;

function tpStreamFile(V: TValue): boolean;
begin
  result := (V is TVStream)
      and ((V as TVStream).target is TLFileStream);
end;

function tpStreamSerial(V: TValue): boolean;
begin
    result := (V is TVStream)
        and ((V as TVStream).target is TLSerialStream);
end;

function tpString(V: TValue): boolean;
begin
    result := V is TVString;
end;

function tpStringOrNIL(V: TValue): boolean;
begin
    result := tpString(V) or tpNIL(V);
end;

function tpSubprogram(V: TValue): boolean;
begin
    result := V is TVSubprogram;
end;

function tpSubprogramPure1(V: TValue): boolean;
begin
    result := (V is TVFunction) or (V is TVInternalFunction) or (V is TVPredicate)
        or (V is TVInternalUnaryFunction) or (V is TVInternalArrayFunction);
end;

function tpSymbol(V: TValue): boolean;
begin
    result := V is TVSymbol;
end;

function tpTensor(V: TValue): boolean;
begin
    result := V is TVTensor;
end;

function tpTensor1d(V: TValue): boolean;
begin
    if not (V is TVTensor) then Exit(false);
    result := Length(T_(V).dim) = 2;
end;

function tpTensor2d(V: TValue): boolean;
begin
    if not (V is TVTensor) then Exit(false);
    result := Length(T_(V).dim) = 4;
end;


function tpTensorReal(V: TValue): boolean;
begin
    result := (V is TVTensor) and ((V as TVTensor).t.dim[0]=1);
end;


function tpTensorReal1d(V: TValue): boolean;
begin
    if V is TVTensor
    then with V as TVTensor do result := (t.dim[0]=1) and (Length(t.dim)=2)
    else result := false;
end;


function tpTensorReal2d(V: TValue): boolean;
begin
    if V is TVTensor
    then with V as TVTensor do result := (t.dim[0]=1) and (Length(t.dim)=4)
    else result := false;
end;


function tpTime(V: TValue): boolean;
begin
    result := V is TVTime;
end;

function tpDuration(V: TValue): boolean;
begin
    result := V is TVDuration
end;

function tpTreeOfNumbers(V: TValue): boolean;
var i: integer; L: TValues;
begin
    result := tpListOfNumbers(V);
    if result then Exit;
    result := tpList(V);
    if not result then Exit;
    L := LV_(V);
    for i := 0 to high(L) do
        if not tpTreeOfNumbers(L[i]) then Exit(false);
end;

function tpTreeOfReal(V: TValue): boolean;
var i: integer; L: TValues;
begin
    result := tpListOfReals(V);
    if result then Exit;
    result := tpList(V);
    if not result then Exit;
    L := LV_(V);
    for i := 0 to high(L) do
        if not tpTreeOfReal(L[i]) then Exit(false);
end;



function tpTrue(V:  TValue): boolean;
begin
    result := not tpNIL(V);
end;

function tpZIPArchivePointer(V: TValue): boolean;
begin
    result := V is TVZipArchivePointer;
end;


/////////////////////////////////////
/// Value Predicates ////////////////
/////////////////////////////////////

function vphKeywordName(V: TValue; const n: unicodestring): boolean;
begin
    result := (V is TVKeyword) and ((V as TVKeyword).uname = n);
end;

function vphKeywordNames(V: TValue; const n: array of unicodestring): boolean;
var i: integer;
begin
    result := true;
    if V is TVKeyword then for i := 0 to high(n) do
        if (V as TVKeyword).uname=n[i] then Exit;

    result := false;
end;

function vphSymbolName(V: TValue; const n: unicodestring): boolean;
begin
    result := (V is TVSymbol) and ((V as TVSymbol).uname = n);
end;

function vphListHeaded(V: TValue; const n: unicodestring): boolean;
begin
    result := (V is TVList)
        and ((V as TVList).Count>0)
        and tpSymbol((V as TVList)[0])
        and ((V as TVList).uname[0]=n);
end;

function vphListOpCall(V: TValue; const n: unicodestring;
                                                op_en: TOperatorEnum): boolean;
begin
    result := (V is TVList)
        and ((V as TVList).Count>0)
        and (vphSymbolName((V as TVList)[0], n)
            or (((V as TVList)[0] is TVOperator)
                and (((V as TVList)[0] as TVOperator).op_enum = op_en)));
end;

function vphPair(V: TValue; e0, e1: TTypePredicate): boolean;
var L: TVList;
begin
    result := V is TVList;
    if not result then exit;
    L := V as TVList;
    result := (L.Count=2) and e0(L[0]) and e1(L[1]);
end;


function vphExpressionOperator(V: TValue;
    const op: array of TOperatorEnum;
    const tp: array of TTypePredicate): boolean;
var i: integer;  L: TVList; oe: TOperatorEnum; in_op: boolean;
begin
    result := false;
    if not (V is TVList) then Exit;
    L := V as TVList;
    if L.Count<(1+Length(tp)) then Exit;
    if not (L[0] is TVOperator) then Exit;
    oe := (L[0] as TVOperator).op_enum;
    in_op := false;
    for i := 0 to high(op) do if oe=op[i] then in_op := true;
    if not in_op then Exit;
    for i := 0 to high(tp) do if not tp[i](L[i+1]) then Exit;
    result := true;
end;


function vpComplexNotZero                           (V: TValue): boolean;
begin
    result := (V is TVComplex) and
        (((V as TVComplex).fC.re<>0) or ((V as TVComplex).fC.im<>0));
end;

function vpDurationNotNegative(V: TValue): boolean;
begin
    result := (V is TVDuration) and ((V as TVDuration).fDT>=0);
end;

function vpEmpty(V: TValue): boolean;
begin
    result := ((V is TVCompound) and ((V as TVCompound).Count=0))
        or ((V is TVQueue) and (V as TVQueue).target.empty)
        or ((V is TVRange) and ((V as TVRange).f_length()=0));
end;


function vpExpression_CONST(V: TValue): boolean;
begin
    result := vphExpressionOperator(V, oeCONST, [vpVariableNames])
        and ((V as TVList).Count=3);
end;

function vpExpression_FUNCTION_lambda(V: TValue): boolean;
begin
    result := vphExpressionOperator(V, oeFUNCTION, [tpListOfSymbols]);
end;

function vpExpression_PACKAGE(V: TValue): boolean;
begin
    result := vphExpressionOperator(V, oePackage, [tpName, tpListOfSymbols]);
end;

function vpExpression_VAR(V: TValue): boolean;
begin
    result := vphExpressionOperator(V, oeVAR, [vpVariableNames])
        and ((V as TVList).Count in [2..3]);
end;

function vpInteger8(V: TValue): boolean;
begin
    result := (V is TVInteger)
        and ((V as TVInteger).fI>=-128)
        and ((V as TVInteger).fI<=127);
end;

function vpInteger16(V: TValue): boolean;
begin
    result := (V is TVInteger)
        and ((V as TVInteger).fI>=-32768)
        and ((V as TVInteger).fI<=32767);
end;

function vpInteger32(V: TValue): boolean;
begin
    result := (V is TVInteger)
        and ((V as TVInteger).fI>=-2147483648)
        and ((V as TVInteger).fI<=2147483647);
end;

function vpInteger60Range(V: TValue): boolean;
begin
    result := (V is TVInteger)
        and ((V as TVInteger).fI>=0)
        and ((V as TVInteger).fI<60);
end;

function vpInteger60RangeOrNIL(V: TValue): boolean;
begin
    result := vpInteger60Range(V) or tpNIL(V);
end;

function vpInteger1000Range(V: TValue): boolean;
begin
    result := (V is TVInteger)
        and ((V as TVInteger).fI>=0)
        and ((V as TVInteger).fI<1000);
end;

function vpInteger1000RangeOrNIL(V: TValue): boolean;
begin
    result := vpInteger1000Range(V) or tpNIL(V);
end;

function vpIntegerAbsOne                            (V: TValue): boolean;
begin
    result := (V is TVInteger) and (abs((V as TVInteger).fI) = 1);
end;

function vpIntegerByte                              (V: TValue): boolean;
begin
    result := (V is TVInteger)
        and ((V as TVInteger).fI>=0)
        and ((V as TVInteger).fI<256);
end;

function vpIntegerDWORD(V: TValue): boolean;
begin
    result := (V is TVInteger)
        and ((V as TVInteger).fI>=0)
        and ((V as TVInteger).fI<=$FFFF);
end;

function vpIntegerNegative(V: TValue): boolean;
begin
    result := (V is TVInteger) and ((V as TVInteger).fI < 0);
end;

function vpIntegerNotZero                           (V: TValue): boolean;
begin
    result := (V is TVInteger) and ((V as TVInteger).fI <> 0);
end;

function vpIntegerPositive(V: TValue): boolean;
begin
    result := (V is TVInteger) and ((V as TVInteger).fI > 0);
end;

function vpIntegerPositiveOrNIL(V: TValue): boolean;
begin
    result := (V=nil) or vpIntegerPositive(V) or tpNIL(V);
end;

function vpIntegerRoundToRange                      (V: TValue): boolean;
begin
    result := (V is TVInteger)
        and Math.InRange((V as TVInteger).fI,
            low(math.TRoundToRange),
            high(math.TRoundToRange));
end;

function vpIntegerRoundToRangeOrNIL(V: TValue): boolean;
begin
    result := (V=nil) or vpIntegerRoundToRange(V) or tpNIL(V);
end;

function vpIntegerWORD(V: TValue): boolean;
begin
    result := (V is TVInteger)
        and ((V as TVInteger).fI>=0)
        and ((V as TVInteger).fI<(256*256));
end;

function vpIntegerWORDorNIL(V: TValue): boolean;
begin
    result := vpIntegerWORD(V) or tpNIL(V);
end;

function vpIntegerZero(V: TValue): boolean;
begin
    result := (V is TVInteger) and ((V as TVInteger).fI=0);
end;


function vpKeyword__                                (V: TValue): boolean;
begin
    result := (V is TVKeyword) and ((V as TVSymbol).N = _.N);
end;

function vpKeyword_ALL                              (V: TValue): boolean;
begin
    result := vphKeywordNames(V, [':ALL', ':ВСЕ', ':ВСЁ']);
end;

function vpKeyword_APPEND                           (V: TValue): boolean;
begin
    result := (V is TVKeyword) and ((V as TVSymbol).uname = ':APPEND');
end;

function vpKeyword_ARRAY(V: TValue): boolean;
begin
  result := vphKeywordName(V, ':ARRAY');
end;

function vpKeyword_BOM                              (V: TValue): boolean;
begin
    result := (V is TVKeyword) and ((V as TVSymbol).uname = ':BOM');
end;

function vpKeyword_BY_HEAD(V: TValue): boolean;
begin
    result := vphKeywordName(V, ':BY-HEAD');
end;

function vpKeyword_CAPTION(V: TValue): boolean;
begin
    result := vphKeywordName(V, ':CAPTION');
end;

function vpKeyword_CAPTURE                          (V: TValue): boolean;
begin
    result := (V is TVKeyword) and ((V as TVSymbol).uname = ':CAPTURE');
end;

function vpKeyword_CAPTURED                         (V: TValue): boolean;
begin
    result := (V is TVKeyword) and ((V as TVSymbol).uname = ':CAPTURED');
end;

function vpKeyword_CASE_INSENSITIVE(V: TValue): boolean;
begin
    result := vphKeywordName(V,':CASE-INSENSITIVE');
end;

function vpKeyword_CENTER                           (V: TValue): boolean;
begin
    result := vphKeywordName(V, ':CENTER');
end;

function vpKeyword_CP1251                           (V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
           ((V as TVSymbol).uname = ':CP1251')
        or ((V as TVSymbol).uname = ':WINDOWS-1251'));
end;

function vpKeyword_CP1252                           (V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
           ((V as TVSymbol).uname = ':CP1252')
        or ((V as TVSymbol).uname = ':WINDOWS-1252'));
end;

function vpKeyword_CP866(V: TValue): boolean;
begin
    result := vphKeywordNames(V, [':CP866',':DOS']);
end;

function vpKeyword_CPU_COUNT(V: TValue): boolean;
begin
    result := vphSymbolName(V, ':CPU-COUNT');
end;

function vpKeyword_CSV(V: TValue): boolean;
begin
    result := vphSymbolName(V, ':CSV');
end;

function vpKeyword_DEFLATE                          (V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
           ((V as TVSymbol).uname = ':DEFLATE'));
end;

function vpKeyword_EQUAL                            (V: TValue): boolean;
begin
    result := vphKeywordName(V, ':LESS');
end;

function vpKeyword_FIRST                            (V: TValue): boolean;
begin
    result := (V is TVKeyword) and ((V as TVSymbol).uname = ':FIRST');
end;

function vpKeyword_FLAG                             (V: TValue): boolean;
begin
    result := V.equal(kwFLAG);
end;

function vpKeyword_GENERATOR(V: TValue): boolean;
begin
    result := vphKeywordName(V, ':GENERATOR');
end;

function vpKeyword_HIDDEN(V: TValue): boolean;
begin
    result := vphKeywordName(V, ':HIDDEN');
end;

function vpKeyword_HTML(V: TValue): boolean;
begin
    result := vphKeywordName(V, ':HTML');
end;

function vpKeyword_INTEGER(V: TValue): boolean;
begin
    result := vphKeywordNames(V, [':INTEGER',':ЦЕЛОЕ']);
end;

function vpKeyword_KEY                              (V: TValue): boolean;
begin
    result := V.equal(kwKEY);
end;

function vpKeyword_KEY_VALUE                        (V: TValue): boolean;
begin
   result := vphKeywordNames(V, [':KV', ':KEY-VALUE']);
end;

function vpKeyword_KOI8R                            (V: TValue): boolean;
begin
    result := vphKeywordNames(V, [':KOI8R',':KOI8-R', ':КОИ8']);
end;


function vpKeyword_LATIN1(V: TValue): boolean;
begin
    result := vphKeywordNames(V, [':LATIN1',':LATIN-1', ':ISO-8859-1', ':CP819']);
end;

function vpKeyword_LEFT                             (V: TValue): boolean;
begin
    result := vphKeywordName(V, ':LEFT');
end;

function vpKeyword_LESS                             (V: TValue): boolean;
begin
    result := vphKeywordName(V, ':LESS');
end;

function vpKeyword_LINE(V: TValue): boolean;
begin
    result := vphKeywordName(V, ':LINE');
end;

function vpKeyword_MACROEXPAND(V: TValue): boolean;
begin
    result := vphKeywordName(V, ':MACROEXPAND');
end;

function vpKeyword_MEAN(V: TValue): boolean;
begin
    result := vphKeywordName(V, ':MEAN');
end;

function vpKeyword_MORE                             (V: TValue): boolean;
begin
    result := vphKeywordName(V, ':MORE');
end;

function vpKeyword_OFF(V: TValue): boolean;
begin
    result := vphKeywordNames(V, [':OFF', ':ОТКЛ', ':ВЫКЛ']);
end;

function vpKeyword_ON(V: TValue): boolean;
begin
    result := vphKeywordNames(V, [':ON', ':ВКЛ']);
end;

function vpKeyword_OPTIONAL                         (V: TValue): boolean;
begin
    result := V.equal(kwOPTIONAL);
end;

function vpKeyword_PROLOGUE(V: TValue): boolean;
begin
    result := vphKeywordName(V, ':PROLOGUE');
end;

function vpKeyword_RANDOM                           (V: TValue): boolean;
begin
    result := vphKeywordName(V, ':RANDOM');
end;

function vpKeyword_READ                             (V: TValue): boolean;
begin
    result := vphKeywordName(V, ':READ');
end;

function vpKeyword_REST                             (V: TValue): boolean;
begin
    result := V.equal(kwREST);
end;

function vpKeyword_RESULT                           (V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
           ((V as TVSymbol).uname = ':RESULT'));
end;

function vpKeyword_RIGHT                            (V: TValue): boolean;
begin
    result := vphKeywordName(V, ':RIGHT');
end;

function vpKeyword_SECOND                           (V: TValue): boolean;
begin
    result := (V is TVKeyword) and ((V as TVSymbol).uname = ':SECOND');
end;

function vpKeyword_TRUE(V: TValue): boolean;
begin
    result := vphKeywordName(V, 'TRUE');
end;

function vpKeyword_UTF16(V: TValue): boolean;
begin
  result := (V is TVKeyword) and (
         ((V as TVSymbol).uname = ':UTF16')
      or ((V as TVSymbol).uname = ':UTF-16'));
end;

function vpKeyword_UTF16BE                          (V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
           ((V as TVSymbol).uname = ':UTF16BE')
        or ((V as TVSymbol).uname = ':UTF-16BE'));
end;

function vpKeyword_UTF16LE                          (V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
           ((V as TVSymbol).uname = ':UTF16LE')
        or ((V as TVSymbol).uname = ':UTF-16LE'));
end;

function vpKeyword_UTF32(V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
           ((V as TVSymbol).uname = ':UTF32')
        or ((V as TVSymbol).uname = ':UTF-32'));
end;

function vpKeyword_UTF32BE                          (V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
           ((V as TVSymbol).uname = ':UTF32BE')
        or ((V as TVSymbol).uname = ':UTF-32BE'));
end;

function vpKeyword_UTF32LE                          (V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
           ((V as TVSymbol).uname = ':UTF32LE')
        or ((V as TVSymbol).uname = ':UTF-32LE'));
end;

function vpKeyword_UTF8                             (V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
           ((V as TVSymbol).uname = ':UTF8')
        or ((V as TVSymbol).uname = ':UTF-8'));
end;

function vpKeyword_WIDTH(V: TValue): boolean;
begin
    result := vphKeywordName(V, ':WIDTH');
end;

function vpKeyword_WRITE                            (V: TValue): boolean;
begin
    result := (V is TVKeyword) and ((V as TVSymbol).uname = ':WRITE');
end;

function vpKeyword_VISIBLE(V: TValue): boolean;
begin
    result := vphKeywordName(V, ':VISIBLE');
end;

function vpKeywordAlign                             (V: TValue): boolean;
begin
    result := vpKeyword_LEFT(V) or vpKeyword_RIGHT(V) or vpKeyword_CENTER(V);
end;

function vpKeywordAlignOrNil                        (V: TValue): boolean;
begin
    result := tpNIL(V) or vpKeywordAlign(V);
end;

function vpKeywordEncoding                          (V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
            ((V as TVSymbol).uname = ':BOM')
        or vpKeyword_UTF8(V)
        or vpKeyword_CP1251(V)
        or vpKeyword_CP1252(V)
        or vpKeyword_CP866(V)
        or vpKeyword_KOI8R(V)
        or vpKeyword_UTF16(V)
        or vpKeyword_UTF16LE(V)
        or vpKeyword_UTF16BE(V)
        or vpKeyword_UTF32LE(V)
        or vpKeyword_UTF32BE(V)
        or vpKeyword_UTF32(V)
        or vpKeyword_LATIN1(V)
        );
end;

function vpKeywordEncodingOrNIL                     (V: TValue): boolean;
begin
    result := tpNIL(V) or vpKeywordEncoding(V);
end;

function vpKeywordFileAttributes(V: TValue): boolean;
begin
    result := vphKeywordNames(V, [':READ-ONLY','HIDDEN','SYS-FILE','VOLUME-ID','DIRECTORY','ARCHIVE','SYM-LINK']);
end;

function vpKeywordFileMode                          (V: TValue): boolean;
begin
    result := (V is TVKeyword) and (
        ((V as TVSymbol).uname = ':READ')
        or ((V as TVSymbol).uname = ':WRITE')
        or ((V as TVSymbol).uname = ':APPEND'));
end;

function vpKeywordFileModeOrNIL                     (V: TValue): boolean;
begin
    result := tpNil(V) or vpKeywordFileMode(V);
end;

function vpKeywordFoldFunction(V: TValue): boolean;
begin
    result := vphKeywordNames(V, [':MAX', ':MIN', ':ADD', ':MUL']);
end;

function vpKeywordGridArc(V: TValue): boolean;
begin
    result := tpNil(V) or vphKeywordNames(V, [':G', ':W', ':T', ':Q']);
end;

function vpKeywordParametersMode(V: TValue): boolean;
begin
    result := vphKeywordNames(V, [':OPTIONAL',':KEY',':REST']);
end;

function vpKeywordTableModeOrNIL(V: TValue): boolean;
begin
    result := tpNil(V) or vphKeywordNames(V, [':CSV', ':HTML']);
end;


function vpKeywordThinningModeOrNIL(V: TValue): boolean;
begin
    result := tpNIL(V) or vphKeywordNames(V, [':MEAN']);
end;


function vpKeywordVisibilityOrNIL(V: TValue): boolean;
begin
    result := tpNIL(V) or vpKeyword_VISIBLE(V) or vpKeyword_HIDDEN(V);
end;



function vpListEvenLength                           (V: TValue): boolean;
begin
    result := (V is TVList) and (((V as TVList).count mod 2) = 0);
end;

function vpListHeaded_ELSE                          (V: TValue): boolean;
begin
    result := vphListHeaded(V, 'ELSE');
end;

function vpListHeaded_ELT                           (V: TValue): boolean;
begin
    result := vphListHeaded(V, 'ELT');
end;

function vpListHeaded_EXCEPTION                     (V: TValue): boolean;
begin
    result := vphListHeaded(V, 'EXCEPTION');
end;

function vpListHeaded_INS                           (V: TValue): boolean;
begin
    result := vphListHeaded(V, 'INS');
end;

function vpListHeaded_INSET(V: TValue): boolean;
begin
    result := vphListHeaded(V, 'INSET');
end;

function vpListHeaded_SUBSEQ(V: TValue): boolean;
begin
    result := vphListHeaded(V, 'SUBSEQ');
end;

function vpListHeaded_THEN                          (V: TValue): boolean;
begin
    result := vphListHeaded(V, 'THEN');
end;

function vpListHeaded_VALUE(V: TValue): boolean;
begin
    result := vphListHeaded(V, 'VALUE');
end;

function vpListHeadedByString(V: TValue): boolean;
begin
    result := (V is TVList)
            and ((V as TVList).Count>0)
            and ((V as TVList)[0] is TVString);
end;


function vpListHeadedByListOfStrings(V: TValue): boolean;
begin
    result := (V is TVList)
        and ((V as TVList).Count>0)
        and tpListOfStrings((V as TVList)[0]);
end;


function vpListLength_1_2(V: TValue): boolean;
begin
    result := (V is TVList) and ((V as TVList).Count<=2) and ((V as TVList).Count>=1);
end;


function vpListLength_2(V: TValue): boolean;
begin
    result := (V is TVList) and ((V as TVList).Count=2);
end;


function vpListLength_3(V: TValue): boolean;
begin
    result := (V is TVList) and ((V as TVList).Count=3);
end;


function vpListLength_5_6(V: TValue): boolean;
begin
    result := (V is TVList) and ((V as TVList).Count>=5) and ((V as TVList).Count<=6);
end;

function vpListLength_4_6(V: TValue): boolean;
begin
    result := (V is TVList) and ((V as TVList).Count>=4) and ((V as TVList).Count<=6);
end;

function vpListGraph(V: TValue): boolean;
begin
    result := tphListOf(V, vpListGraphArc);
end;

function vpListGraphArc(V: TValue): boolean;
begin
    result := (V is TVList) and ((V as TVList).Count>=3);
end;

function vpListGridArc                              (V: TValue): boolean;
var i: integer; L: TVList;
begin
    result := vpListLength_4_6(V);
    if not result then Exit;

    L := V as TVList;
    result := vpKeywordGridArc(L[0]);
    if result then
        for i :=  5 to L.high do
            if not tpNumber(L[i]) then Exit(false);
end;


function vpListGridRandomization                      (V: TValue): boolean;
var L: TVList;
begin
    result := vpListLength_3(V);
    if not result then Exit;

    L := V as TVList;
    result := vpKeyword_RANDOM(L[0]) and tpRange(L[2]);
end;


function vpListKeywordValue                         (V: TValue): boolean;
var i: integer;
begin
    result := vpListEvenLength(V);
    if result then
        for i :=  0 to (V as TVList).Count div 2 - 1 do
            if not tpKeyword((V as TVList)[i*2]) then begin
                result := false;
                exit;
            end;
end;


function vpListKeyStringValue                       (V: TValue): boolean;
var i: integer;
begin
    //Список КЛЮЧ - ЗНАЧЕНИЕ, в котором ключами являются строки
    result := vpListEvenLength(V);
    if result then
        for i :=  0 to (V as TVList).Count div 2 - 1 do
            if not tpString((V as TVList)[i*2]) then begin
                result := false;
                exit;
            end;
end;


function vpListLambdaExpression(V: TValue): boolean;
var L: TVList;
begin
    result := V is TVList;
    if not result then Exit;
    L := V as TVList;
    result := (L.Count>=2) and (L[0] is TVOperator)
        and ((L[0] as TVOperator).op_enum in [oePROCEDURE, oeFUNCTION, oeMACRO])
        and tpList(L[1]);
end;


function vpListMatrix(V: TValue): boolean;
var L: TVList; i, ln: integer;
begin
    result := V is TVList;
    if not result then exit;
    L := V as TVList;

    if L.Count=0 then Exit;

    result := tpList(L[0]);
    if not result then Exit;

    ln := L.L[0].Count;

    for i := 1 to L.high do begin
        result := tpList(L[i]) and (L.L[i].Count=ln);
        if not result then exit;
    end;
end;


function vpListMatrixOfIntegers(V: TValue): boolean;
var L: TVList; i: integer;
begin
    result := vpListMatrix(V);
    if not result then Exit;
    L := V as TVList;
    for i := 0 to L.high do begin
        result := tpListOfIntegers(L.L[i]);
        if not result then Exit;
    end;
end;


function vpListMatrixOfNumbers(V: TValue): boolean;
var L: TVList; i: integer;
begin
    result := vpListMatrix(V);
    if not result then Exit;
    L := V as TVList;
    for i := 0 to L.high do begin
        result := tpListOfNumbers(L.L[i]);
        if not result then Exit;
    end;
end;


function vpListOfByte(V: TValue): boolean;
begin
    result := tphListOf(V, vpIntegerByte);
end;

function vpListOfDurationsForMul(V: TValue): boolean;
var is_dt: boolean; L: TVList; i: integer;
begin
    result := V is TVList;
    if not result then Exit;
    L := V as TVList;
    is_dt := false;
    for i := 0 to L.high do begin
        result := (L[i] is TVDuration) or (L[i] is TVReal);
        if not result then Exit;
        result := not ((L[i] is TVDuration) and is_dt);
        if not result then Exit;
        is_dt := is_dt or (L[i] is TVDuration);
    end;
    result := is_dt;
end;

function vpListOfFileAttributes(V: TValue): boolean;
begin
    result := tphListOf(V, vpKeywordFileAttributes);
end;

function vpListOfIndices_1d(V: TValue): boolean;
begin
  result := tphListOf(V, vpNatural);
  if not result then Exit;
  result := (V as TVList).Count=1;
end;


function vpListOfIndices_2d(V: TValue): boolean;
begin
    result := tphListOf(V, vpNatural);
    if not result then Exit;
    result := (V as TVList).Count=2;
end;

function vpListOfNaturals(V: TValue): boolean;
begin
    result := tphListOf(V, vpNatural);
end;

function vpListOfNumbers(V: TValue): boolean;
begin
    result := tphListOf(V, tpNumber);
end;

function vpListOfStringsEvenLength(V: TValue): boolean;
begin
    result := vpListEvenLength(V) and tpListOfStrings(V);
end;


function vpListOfSymbolValuePairs                   (V: TValue): boolean;
var i: integer; L: TVList;
begin
    result := false;
    if V is TVList then begin
        L := V as TVList;
        for i :=  0 to L.high do
            if not (tpList(L[i])
                and (L.L[i].count=2)
                and tpName(L.L[i][0]))
            then exit;
    end
    else exit;
    result := true;
end;


function vpListOfSummableTimes(V: TValue): boolean;
var is_dt: boolean; L: TVList; i: integer;
begin
    result := V is TVList;
    if not result then Exit;
    L := V as TVList;
    is_dt := false;
    for i := 0 to L.high do begin
        result := (L[i] is TVTime);
        if not result then Exit;
        result := not ((L[i] is TVDateTime) and is_dt);
        if not result then Exit;
        is_dt := is_dt or (L[i] is TVDateTime);
    end;
    result := is_dt;
end;


function vpListRoutineExpression(V: TValue): boolean;
var L: TVList;
begin
    result := V is TVList;
    if not result then Exit;
    L := V as TVList;
    result := (L.Count>=3) and (L[0] is TVOperator)
        and ((L[0] as TVOperator).op_enum in [oePROCEDURE, oeFUNCTION, oeMACRO])
        and tpName(L[1])
        and tpList(L[2]);
end;


function vpListSymbolValue                          (V: TValue): boolean;
var i: integer;
begin
    result := vpListEvenLength(V);
    if result then
        for i :=  0 to (V as TVList).Count div 2 - 1 do
            if not tpSymbol((V as TVList)[i*2]) then begin
                result := false;
                exit;
            end;
end;


function vpListVariableExpression(V: TValue): boolean;
var L: TVList;
begin
    result := V is TVList;
    if not result then Exit;
    L := V as TVList;
    result := (L.Count>=2) and (L[0] is TVOperator)
        and ((L[0] as TVOperator).op_enum in [oeCONST, oeVAR, oeMACRO_SYMBOL])
        and tpName(L[1])
        and tpList(L[2]);
end;

function vpListWithLastList(V: TValue): boolean;
begin
    result := (V is TVList)
        and ((V as TVList).Count>0)
        and tpList((V as TVList)[(V as TVList).high]);
end;


function vpNatural                                  (V: TValue): boolean;
begin
    result := (V is TVInteger) and ((V as TVInteger).fI >= 0);
end;

function vpNaturalOrNIL                             (V: TValue): boolean;
begin
    result := tpNIL(V) or ((V is TVInteger) and ((V as TVInteger).fI >= 0));
end;

function vpNumberNotZero(V: TValue): boolean;
begin
    result := (V is TVNumber) and not ((V as TVNumber).C = _0);
end;


function vpOperator_KEY(V: TValue): boolean;
begin
    result := (V is TVOperator)
        and ((V as TVOperator).op_enum = oeKEY);
end;


function vpOperator_VAR(V: TValue): boolean;
begin
    result := (V is TVOperator)
        and ((V as TVOperator).op_enum = oeVAR);
end;


function vpOperatorVarDeclaration(V: TValue): boolean;
begin
    result := (V is TVOperator)
        and ((V as TVOperator).op_enum in
            [oeVAR, oeCONST, oePROCEDURE, oeMACRO, oeMACRO_SYMBOL, oeFUNCTION]);
end;

function vpOperatorVarOrConst(V: TValue): boolean;
begin
    result := (V is TVOperator) and ((V as TVOperator).op_enum in [oeVAR, oeCONST]);
end;

function vpOperatorRoutine(V: TValue): boolean;
begin
    result := (V is TVOperator)
        and ((V as TVOperator).op_enum in
            [oePROCEDURE, oeMACRO, oeMACRO_SYMBOL, oeFUNCTION]);
end;

function vpPairListNatural(V: TValue): boolean;
begin
    result := vphPair(V, tpList, vpNatural);
end;

function vpPairNaturalList(V: TValue): boolean;
begin
    result := vphPair(V, vpNatural, tpList);
end;

function vpPairPositiveList(V: TValue): boolean;
begin
    result := vphPair(V, vpIntegerPositive, tpList);
end;


function vpPointerToVar(V: TValue): boolean;
begin
    result := (V is TVChainPointer) and not (V as TVChainPointer).constant;
end;


function vpRangeIntegerNotNegative                  (V: TValue): boolean;
begin
    result := (V is TVRangeInteger) and ((V as TVRangeInteger).high>=(V as TVRangeInteger).low);
end;


function vpRealAbsOneOrMore                         (V: TValue): boolean;
begin
    result := (V is TVReal) and (abs((V as TVReal).F) >= 1);
end;

function vpRealNegative                             (V: TValue): boolean;
begin
    result := (V is TVReal) and ((V as TVReal).F < 0);
end;

function vpRealNotNegative                          (V: TValue): boolean;
begin
    result := (V is TVReal) and ((V as TVReal).F >= 0);
end;

function vpRealNotNegativeOrNIL(V: TValue): boolean;
begin
    result := vpRealNotNegative(V) or tpNIL(V);
end;

function vpRealNotZero                              (V: TValue): boolean;
begin
    result := (V is TVReal) and ((V as TVReal).F <> 0);
end;

function vpRealPositive                             (V: TValue): boolean;
begin
    result := (V is TVReal) and ((V as TVReal).F > 0);
end;

function vpRealPositiveOrNIL(V: TValue): boolean;
begin
    result := vpRealPositive(V) or tpNIL(V);
end;

function vpRealZero                                 (V: TValue): boolean;
begin
    result := (V is TVReal) and ((V as TVReal).F = 0);
end;

function vpSequenceNotEmpty(V: TValue): boolean;
begin
    result := (V is TVSequence) and ((V as TVSequence).Count>0);
end;


function vpStream                                   (V: TValue): boolean;
begin
    result := (V is TVStream) and ((V as TVStream).target<>nil);
end;

function vpStreamEnd(V: TValue): boolean;
begin
    result :=
    (V as TVStream).target.Position = (V as TVStream).target.Size;
end;



function vpStringDateTime(V: TValue): boolean;
begin
    result := (V is TVString) and sp_date_time(S_(V));
end;


function vpStringDuration(V: TValue): boolean;
begin
    result := (V is TVString) and sp_duration(S_(V));
end;


function vpStringEmpty                              (V: TValue): boolean;
begin
    result := (V is TVString) and ((V as TVString).S = '');
end;


function vpStringInteger(V: TValue): boolean;
begin
    result := (V is TVString) and sp_integer((V as TVString).S);
end;


function vpStringNumber(V: TValue): boolean;
begin
    result := (V is TVString)
        and (sp_float(S_(V)) or sp_complex_alg(S_(V)) or sp_complex_exp(S_(V)));
end;

function vpStringPath(V: TValue): boolean;
var s: unicodestring;
begin
    if not (V is TVString) then Exit(false);
    s := S_(V);
    result := s[high(s)] = DirectorySeparator;
end;

function vpStringRange(V: TValue): boolean;
begin
    result := (V is TVString) and sp_range_float(S_(V));
end;


function vpStringRangeInteger(V: TValue): boolean;
begin
    result := (V is TVString) and sp_range_integer(S_(V));
end;


function vpStringReal(V: TValue): boolean;
begin
    result := (V is TVString) and sp_float(S_(V));
end;


function vpStringTestPredicate(V: TValue): boolean;
begin
    result := (V is TVString) and test_string_predicate((V as TVString).S);
end;




function vpSymbol__                                 (V: TValue): boolean;
begin
    result := V.equal(_);
end;

function vpSymbol_ELSE(V: TValue): boolean;
begin
    result := vphSymbolName(V, 'ELSE');
end;

function vpSymbol_IN                                (V: TValue): boolean;
begin
    result := vphSymbolName(V, 'IN');
end;

function vpSymbol_LIST                              (V: TValue): boolean;
begin
    result := vphSymbolName(V, 'LIST');
end;


function vpSymbol_RANGE                             (V: TValue): boolean;
begin
    result := vphSymbolName(V, 'RANGE');
end;


function vpSymbolQualified(V: TValue): boolean;
begin
    result := tpName(V) and (Pos(':', (V as TVSymbol).name)>1);
end;





function vpVariableNames(V: TValue): boolean;
var i: integer; L: TVList; no_keyword_yet: boolean;
    function mode_keyword(x: TValue): boolean;
    begin
        result := no_keyword_yet and (
                vpKeyword_OPTIONAL(x)
                or vpKeyword_KEY(x)
                or (vpKeyword_REST(x) and (i=L.high-1)));
        if result then no_keyword_yet := false;
    end;
begin
    //пригодность аргумента в качестве имени переменной для VAR и CONST
    if tpName(V) then Exit(true);
    if not tpList(V) then Exit(false);
    L := V as TVList;
    no_keyword_yet := true;
    result := false;
    for i := 0 to L.high do
        if not (mode_keyword(L[i]) or vpVariableNames(L[i])) then Exit;
    result := true;
end;


////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////
/// Rest Predicates /////////////////
/////////////////////////////////////

function rphOf(V: TValue; tp: TTypePredicate): boolean;
var i: integer; VL: TValues;
begin
    result := V is TVListRest;
    if not result then Exit;

    VL := (V as TVListRest).V;
    for i := 0 to high(VL) do if not tp(VL[i]) then EXIT(false);
end;


function rpAny                                              (V: TValue): boolean;
begin
    result := (V is TVListRest);
end;


function rpNIL                                              (V: TValue): boolean;
begin
    result := (V is TVListRest) and (length((V as TVListRest).V)=0);
end;


function rpIntegers                                         (V: TValue): boolean;
begin
    result := rphOf(V, tpInteger);
end;


function rpLists(V: TValue): boolean;
begin
    result := rphOf(V, tpList);
end;


function rpNumbers(V: TValue): boolean;
begin
    result := rphOf(V, tpNumber);
end;


function rpOneTensor(V: TValue): boolean;
begin
    result := (Length(REST_(V))=1) and tpTensor(REST_(V)[0])
end;


function rpOneTensorReal(V: TValue): boolean;
begin
    result := (Length(REST_(V))=1) and tpTensorReal(REST_(V)[0])
end;


function rpReals                                            (V: TValue): boolean;
begin
    result := rphOf(V, tpReal);
end;


function rpTimes(V: TValue): boolean;
begin
    result := rphOf(V, tpTime);
end;


function rpDateTimes(V: TValue): boolean;
begin
    result := rphOf(V, tpDateTime);
end;


function rpDurations(V: TValue): boolean;
begin
    result := rphOf(V, tpDuration);
end;


end.  //955 1157

