//комплексное умножение
float2 cmul(float2 a, float2 b)
{
	return (float2) ((a.x*b.x) - (a.y*b.y),
			         (a.x*b.y) + (a.y*b.x));
}

//комплексное деление
float2 cdiv(float2 a, float2 b)
{
	if (fabs(b.x) > fabs(b.y))
	{
		float tmp = b.y / b.x;
		float denom = b.x + b.y*tmp;
		return (float2) ((a.x + a.y*tmp) / denom,
				         (a.y - a.x*tmp) / denom);
	}
	else
	{
		float tmp = b.x / b.y;
		float denom = b.y + b.x*tmp;
		return (float2) (( a.y + a.x*tmp) / denom,
				         (-a.x + a.y*tmp) / denom);
	}
}


struct node
{
	float2 U; //float2 представляет комплексное число
	float2 I;
	float2 Y;
};


struct arc
{
	float2 Yl;
	float2 Yc;
	float2 K;
	float2 E;
	float2 I;
	float2 I_b;
	float2 I_e;
	int node1;
    int node2;
};


__kernel void tkz (int nodesNumber,  int arcsNumber, int tasksNumber,
                   __global struct node* pNode,   __global struct arc* pArc)
{
    int global_id = get_global_id(0);

    if (global_id >= tasksNumber) return;

    int i;
    int a;
    int baseNode = nodesNumber * global_id;
    int baseArc  =  arcsNumber * global_id;

    for (a=baseNode; a<(baseNode+nodesNumber); a++)
    {
        pNode[a].I = 0;
        pNode[a].Y = 0;
    }

	for (i=0; i<1000000; i++)
	{
        for (a=baseArc; a<(baseArc+arcsNumber); a++)
        {
            int n1 = pArc[a].node1 + baseNode;
            int n2 = pArc[a].node2 + baseNode;
            //токи в ветвях
            float2 K = pArc[a].K;
            float2 u1 = pNode[n1].U;
            float2 u2 = pNode[n2].U;
            float2 u2_ = cmul(u2, K) - pArc[a].E;
            float2 du = u1 - u2_;
            float2 Yc = 0.5f*pArc[a].Yc;
            float2 I = cmul(du, pArc[a].Yl);
            float2 I_b = I + cmul(u1, Yc);
            float2 I_e = cmul(K, cmul(u2, Yc) - I);
            pArc[a].I = I;
            pArc[a].I_b = I_b;
            pArc[a].I_e = I_e;
            //небалансы в узлах
            pNode[n1].I -= I_b;
            pNode[n1].Y += pArc[a].Yl + Yc;
            pNode[n2].I -= I_e;
            pNode[n2].Y += cmul(cmul(K,K), pArc[a].Yl + Yc);
        }

        pNode[baseNode].I = 0;
        pNode[baseNode].Y = 0;

        bool balanced = true;

        for (a=baseNode+1; a<(baseNode+nodesNumber); a++)
        {
            //условие завершения - отсутствие значительных небалансов в узлах
            balanced = balanced && (length(pNode[a].I)<1.0f);
            //корректировка узловых напряжений
            pNode[a].U += cdiv(pNode[a].I, pNode[a].Y);
            pNode[a].I = 0;
            pNode[a].Y = 0;
        }

        if (balanced) break;
	}
}
