unit lisya_ifh;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils, dlisp_values, lisya_predicates, mar,  math
    ,lisya_symbols
    , lisya_exceptions;


function ifh_member(const L: TVList; const E: TValue): boolean;

function ifh_reverse(L: TVList): TVList;
function ifh_transpose(L: TVList): TVList;

function ifh_difference         (const A, B: TVList): TVList;

function ifh_union              (const L: TVList): TVList; overload;
function ifh_union              (const L: TValues): TVList; overload;

function ifh_intersection       (const L: TVList): TVList;
function ifh_set_include        (const A, B: TVList): boolean;
function ifh_equal_sets         (const A, B: TVList): boolean;

function ifh_list_substitute(L, sub: TVList): TVList;

function ifh_map(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;
function ifh_map_tail(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;
function ifh_map_tree(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;
procedure ifh_at_depth(d: integer; P: TVSubprogram; L: TVList; call: TCallProc);
function ifh_filter(const PL: TVList; call: TCallProc; P: TTypePredicate): TValue; inline;
function ifh_fold(call: TCallProc; P: TVSubprogram; PL: TVList; b,e: integer): TValue;
function ifh_count(call: TCallProc; P: TVSubprogram; PL: TVList): integer;
function ifh_interlaced(L: TVList): TVList;
function ifh_group(L: TVList; P: TVList; call: TCallProc): TVList;
function ifh_group_by(L: TVList; P: TVSubprogram; call: TCallProc; key: boolean = false): TVList; overload;
function ifh_group_by(L: TVList; P: TVSubprogram; call: TCallProc; class_count: integer): TVList; overload;
function ifh_group_by(L: TVList; f: TVSymbol; key: boolean = false): TVList; overload;
function ifh_group_by(L: TVList; f: TVSymbol; class_count: integer): TVList; overload;

function ifh_like (str1, str2: unicodestring): integer;
function ifh_strings_mismatch(s1,s2: unicodestring): integer;
function ifh_single_spaces(s: unicodestring): unicodestring;
function ifh_max_double(dd: TDoubles): integer;
function ifh_min_double(dd: TDoubles): integer;

function ifh_matrix_mul(w1: integer; m1: TIntegers64; m2: TIntegers64): TIntegers64; overload;

function ListContainKeyword(L: TVList; key: unicodestring): boolean;

function decode_base64(s: unicodestring): TBytes;
function decode_base64_2(s: unicodestring): TBytes;
function encode_base64(b: TBytes): unicodestring;

function ifh_range_integer_intersection(PL: TVList): TValue;
function ifh_range_float_intersection(PL: TVList): TValue;


function ifh_times_sum(VL: TValues): TVTime;


implementation

type THashes = array of DWORD;
type THashesList = array of THashes;

procedure ifhh_hash_list(L: TVList; out hashes: THashes); inline;
var i: integer;
begin
    SetLength(hashes, L.Count);
    for i := 0 to L.high do hashes[i] := L[i].hash;
end;

procedure ifhh_hash_lists(L: TVList; out hashes: THashesList); overload;
var i: integer;
begin
    SetLength(hashes, L.Count);
    for i := 0 to L.high do ifhh_hash_list(L.L[i], hashes[i]);
end;

procedure ifhh_hash_lists(L: TValues; out hashes: THashesList); overload;
var i: integer;
begin
    SetLength(hashes, Length(L));
    for i := 0 to high(L) do ifhh_hash_list(L_(L[i]), hashes[i]);
end;

function ifhh_member_hashed(const hL: THashes; L: TVList;
                                  hE: DWORD; E: TValue): boolean;
var i: integer;
begin
    result := true;
    for i := 0 to L.high do
        if (hE=hL[i]) and equal(L[i], E) then Exit;

    result := false;
end;


////////////////////////////////////////////////////////////////////////////////
/// неупорядоченные множества //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function ifh_member(const L: TVList; const E: TValue): boolean;
var i: integer;
begin
    result := false;
    for i := 0 to L.High do
        if equal(L[i], E) then begin
            result := true;
            break;
        end;
end;



//------------------------------------------------------------------------------
function ifh_difference         (const A, B: TVList): TVList;
var i: integer;
begin
    result := TVList.Create;
    for i := 0 to A.high do
        if not ifh_member(B, A[i])
        then result.add(A[i].Copy);
end;
//------------------------------------------------------------------------------
function ifh_union             (const L: TVList): TVList;
var i, j: integer;
    hashes: THashesList;
    res: THashes;
begin
    result := TVList.Create;
    if L.Count=0 then exit;

    ifhh_hash_lists(L, hashes);
    SetLength(res, 0);

    for i := 0 to L.high do
        for j := 0 to L.L[i].high do
            if not ifhh_member_hashed(res, result, hashes[i][j], L.L[i][j])
            then begin
                result.Add(L.L[i][j].Copy);
                SetLength(res, Length(res)+1);
                res[high(res)] := hashes[i][j];
        end;

    for i := 0 to high(hashes) do SetLength(hashes[i], 0);
    SetLength(hashes, 0);
    SetLength(res, 0);
end;


function ifh_union             (const L: TValues): TVList;
var i, j: integer;
    hashes: THashesList;
    res: THashes;
begin
    result := TVList.Create;
    if Length(L)=0 then exit;

    ifhh_hash_lists(L, hashes);
    SetLength(res, 0);

    for i := 0 to high(L) do
        for j := 0 to L_(L[i]).high do
            if not ifhh_member_hashed(res, result, hashes[i][j], L_(L[i])[j])
            then begin
                result.Add(L_(L[i])[j].Copy);
                SetLength(res, Length(res)+1);
                res[high(res)] := hashes[i][j];
        end;

    for i := 0 to high(hashes) do SetLength(hashes[i], 0);
    SetLength(hashes, 0);
    SetLength(res, 0);
end;

//------------------------------------------------------------------------------
function ifh_intersection       (const L: TVList): TVList;
var hashes: array of array of DWORD;
    res: array of DWORD;
    i,j, min_length, res_count :integer;
    m: boolean;

    procedure add_in_res(v: integer);
    var k: integer;
    begin
        for k := 0 to res_count-1 do
            if (hashes[0][v]=res[k]) and equal(L.L[0][v],result[k])
            then exit;
        res[res_count] := hashes[0][v];
        result.Add(L.L[0][v].Copy);
        inc(res_count);
    end;

begin
    //TODO: intersection может быть ускорена если предварительно отсортировать
    //множества по длине
    //левая свёртка этой функцией работает на 10% быстрее чем сама функция
    //от множества аргументов
    result := TVList.Create;
    if (L.Count=0) then Exit;

    //вычисление хэшей
    ifhh_hash_lists(L, hashes);

    //результат не может быть больше самого маленького из множеств
    min_length := Length(hashes[0]);
    for i := 1 to High(hashes) do
        if Length(hashes[i])<min_length then min_length := Length(hashes[i]);
    SetLength(res, min_length);
    res_count := 0;

    for i := 0 to L.L[0].high do begin
        for j := 1 to L.high do begin
            m := ifhh_member_hashed(hashes[j], L.L[j], hashes[0][i], L.L[0][i]);
            if not m then break;
        end;
        if m then add_in_res(i);
    end;

    for i := 0 to high(hashes) do SetLength(hashes[i], 0);
    SetLength(hashes, 0);
    SetLength(res, 0);
end;
//------------------------------------------------------------------------------
function ifh_set_include        (const A, B: TVList): boolean;
var hashes_A, hashes_B: THashes; i: integer;
begin
    ifhh_hash_list(A, hashes_A);
    ifhh_hash_list(B, hashes_B);

    result := true;
    for i := 0 to B.high do begin
        result := ifhh_member_hashed(hashes_A, A, hashes_B[i], B[i]);
        if not result then exit;
    end;
end;
//------------------------------------------------------------------------------
function ifh_equal_sets(const A, B: TVList): boolean;
var i: integer; hashes_A, hashes_B: THashes;
begin
    ifhh_hash_list(A, hashes_A);
    ifhh_hash_list(B, hashes_B);

    result := true;
    for i := 0 to A.high do begin
        result := ifhh_member_hashed(hashes_B, B, hashes_A[i], A[i]);
        if not result then Exit;
    end;

    for i := 0 to B.high do begin
        result := ifhh_member_hashed(hashes_A, A, hashes_B[i], B[i]);
        if not result then Exit;
    end;
end;

////////////////////////////////////////////////////////////////////////////////
///         ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function ifh_fold(call: TCallProc; P: TVSubprogram; PL: TVList; b,e: integer): TValue;
var expr: TValues; i: integer; tmp: TValue;
begin
    expr := TValues.Create(P, PL[b], PL[b+1]);
    result := call(expr);
    tmp := result;
    for i := b+2 to e do
        begin
            expr[1] := result;
            expr[2] := PL[i];
            result := call(expr);
            tmp.Free;
            tmp := result;
        end;
end;


function ifh_count(call: TCallProc; P: TVSubprogram; PL: TVList): integer;
var i: integer; expr: TValues; res: TValue;
begin
    result := 0;
    res := nil;
    expr := TValues.Create(P, nil);
    for i := 0 to PL.high do
        try
            expr[1] := PL[i];
            res := call(expr);
            if tpTRUE(res) then Inc(result);
		finally
            FreeAndNil(res);
		end;
end;


function ifh_interlaced(L: TVList): TVList;
var i, j: integer; done: boolean;
begin
    result := TVList.Create();
    i := 0;
    done := false;
    repeat
        done := true;
        for j := 0 to L.high do
            if i<L.L[j].Count then begin
                done := false;
                result.Add(L.L[j][i].Copy);
            end;
        Inc(i);
    until done;
end;


function ifh_group(L: TVList; P: TVList; call: TCallProc): TVList;
var src: array of TValue; i,j: integer; expr: TValues; res: TValue;
begin
    SetLength(src, L.Count);
    for i := 0 to L.high do src[i] := L[i].Copy;
    result := TVList.Create;
    for i := 0 to P.high+1 do result.Add(TVList.Create);

    expr := TValues.Create(nil, nil);
    res := nil;

    for i := 0 to P.high do
        begin
            expr[0] := P[i];
            for j := 0 to high(src) do
                if src[j]<>nil
                then
                    try
                        expr[1] := src[j];
                        res := call(expr);
                        if tpTrue(res) then
                            begin
                                result.L[i].Add(src[j]);
                                src[j] := nil;
                            end;
                    finally
                        FreeAndNil(res);
                    end;
        end;

    for j := 0 to high(src) do
        if src[j]<>nil then result.L[P.Count].Add(src[j]);
end;


function ifh_group_by(L: TVList; P: TVSubprogram; call: TCallProc; key: boolean): TVList;
var src: TValues; keys, ukeys: TValues; i: integer; expr: TValues;
    procedure add_key(k: TValue);
    var j: integer;
    begin
        for j := 0 to high(ukeys) do if ukeys[j].equal(k) then Exit;
        SetLength(ukeys, Length(ukeys)+1);
        ukeys[high(ukeys)] := k;
    end;
    function find_key(k: TValue): integer;
    begin
        for result := 0 to high(ukeys) do if ukeys[result].equal(k) then Exit;
    end;

begin
    try
        src := L.ValueList;
        result := TVList.Create;
        expr := TValues.Create(P, nil);
        SetLength(ukeys, 0);
        SetLength(keys, Length(src));
        for i := 0 to high(src) do keys[i] := nil;
        for i := 0 to high(src) do
            begin
                expr[1] := src[i];
                keys[i] := call(expr);
                add_key(keys[i]);
            end;
        if key
        then
            begin
                for i := 0 to high(ukeys) do
                    begin
                        result.Add(ukeys[i].Copy);
                        result.Add(TVList.Create);
                    end;
                for i := 0 to high(src) do
                    result.L[2*find_key(keys[i])+1].Add(src[i].Copy);
            end
        else
            begin
                for i := 0 to high(ukeys) do result.Add(TVList.Create);
                for i := 0 to high(src) do result.L[find_key(keys[i])].Add(src[i].Copy);
            end;
    finally
        for i := 0 to high(src) do keys[i].Free;
    end;
end;


function ifh_group_by(L: TVList; f: TVSymbol; key: boolean): TVList;
var src: TValues; keys, ukeys: TValues; i: integer;
    procedure add_key(k: TValue);
    var j: integer;
    begin
        for j := 0 to high(ukeys) do if ukeys[j].equal(k) then Exit;
        SetLength(ukeys, Length(ukeys)+1);
        ukeys[high(ukeys)] := k;
    end;
    function find_key(k: TValue): integer;
    begin
        for result := 0 to high(ukeys) do if ukeys[result].equal(k) then Exit;
    end;

begin
    src := L.ValueList;
    result := TVList.Create;
    SetLength(ukeys, 0);
    SetLength(keys, Length(src));

    for i := 0 to high(src) do
        begin
            keys[i] := (src[i] as TVRecord).LookSlot(f.N);
            add_key(keys[i]);
        end;

    if key
    then
        begin
            for i := 0 to high(ukeys) do
                begin
                    result.Add(ukeys[i].Copy);
                    result.Add(TVList.Create);
                end;
            for i := 0 to high(src) do
                result.L[2*find_key(keys[i])+1].Add(src[i].Copy);
        end
    else
        begin
            for i := 0 to high(ukeys) do result.Add(TVList.Create);
            for i := 0 to high(src) do result.L[find_key(keys[i])].Add(src[i].Copy);
        end;
end;


//Элемент включается в группу с указанным номером
procedure ifhh_in_group_n(G: TVList; E, N: TValue);
var class_n: integer;
begin
    if not (N is TVInteger)
    then raise ELE.InvalidParameters('Invalid group id ('+N.AsString+')');

    class_n := I_(N);
    if (class_n<0) or (class_n>=G.Count)
    then raise ELE.InvalidParameters('Group id out of range ('+IntToStr(class_n)+')');

    G.L[class_n].Add(E.Copy);
end;


//Группировка элементов по номеру класса, возвращаемому предикатом.
//Возвращается список, позиция в котором соответствует номеру класса.
function ifh_group_by(L: TVList; P: TVSubprogram; call: TCallProc;
			class_count: integer): TVList;
var i: integer; src, expr: TValues; key: TValue;
begin
    result := TVList.Create;
    for i := 0 to class_count - 1 do result.Add(TVList.Create);
    expr := TValues.Create(P, nil);
    src := L.ValueList;
    key := nil;

    for i := 0 to high(src) do
        try
            expr[1] := src[i];
            key := call(expr);
            ifhh_in_group_n(result, src[i], key);
		finally
            key.Free;
		end;
end;


//Группировка элементов по номеру класса содержащемуся в указанном поле.
//Возвращается список, позиция в котором соответствует номеру класса.
function ifh_group_by(L: TVList; f: TVSymbol; class_count: integer): TVList;
var i: integer; src: TValues;
begin
    result := TVList.Create;
    for i := 0 to class_count - 1 do result.Add(TVList.Create);
    src := L.ValueList;

    for i := 0 to high(src) do
        ifhh_in_group_n(result, src[i], (src[i] as TVRecord).LookSlot(f.N));
end;



function ifh_reverse(L: TVList): TVList;
var i: integer;
begin
    result := TVList.Create;
    for i := L.high downto 0 do result.Add(L[i].Copy);
end;


function ifh_transpose(L: TVList): TVList;
var i, j, w, h: integer;
begin
    h := L.Count;
    w := L.L[0].Count; //транспонируются только непустые таблицы
    result := TVList.Create();
    for i := 0 to w-1 do result.Add(TVList.Create);
    for i := 0 to w-1 do
        for j := 0 to h-1 do
            result.L[i].Add(L.L[j][i].Copy);
    //TODO: можно оптимизировать, извлекая данные из аргумента массивом, а не
    //по одному
end;


function ifh_list_substitute(L, sub: TVList): TVList;
    function pair(a: TValue; out p: TValue): boolean;
    var i: integer;
    begin
        result := false;
        p := nil;
        for i := 0 to (sub.Count div 2) - 1 do
            if equal(a, sub[i*2]) then begin
                result := true;
                p := sub[i*2+1].Copy;
                Exit;
            end;
    end;
var p: TValue; i: integer;
begin
    result := L.Copy as TVList;
    for i := 0 to L.high do if pair(L[i], p) then result[i] := p;
end;

//------------------------------------------------------------------------------
function ifh_map(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;
var expr: TValues; i, j: integer;
begin
    try
        result := TVList.Create;
        if PL.Count=0 then Exit;

        SetLength(expr, PL.Count + 1);
        expr[0] := P;

        for i := 0 to PL.L[0].high do
            begin
                for j := 0 to PL.high do expr[j+1] := PL.L[j][i];
                result.Add(call(expr));
            end;

    except
        FreeAndNil(result);
        raise;
    end;
end;


function ifh_map_tail(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;
var expr: TValues; i, j: integer; tails: TValues;
begin
    try
        result := TVList.Create;
        if PL.Count=0 then Exit;

        SetLength(expr, PL.Count + 1);
        expr[0] := P;
        SetLength(tails, PL.Count);

        for i := 0 to PL.L[0].high do
            try
                for j := 0 to PL.high do
                    begin
                        //TODO: излишне копируется содержимое хвостов
                        tails[j] := PL.L[j].tail(i);
                        expr[j+1] := tails[j];
					end;
				result.Add(call(expr));
			finally
                for j := 0 to PL.high do FreeAndNil(tails[j])
			end;

    except
        FreeAndNil(result);
        raise;
    end;
end;


function ifh_map_tree(call: TCallProc; P: TVSubprogram; PL: TVList): TVList;
var params: TVList; expr: TValues;
    i, j: integer;
begin
    try
        try
            params := nil;
            result := TVList.Create;
            if PL.Count=0 then Exit;

            SetLength(expr, PL.Count + 1);
            expr[0] := P;

            params := TVList.Create([]);
            for i := 0 to PL.high do params.Add(nil);

            for i := 0 to PL.L[0].high do
                if tpList(PL.L[0][i])
                then
                    begin
                        for j := 0 to PL.high do params[j] := PL.L[j][i].Copy;
                        params.copy_on_write;
                        if not vpListMatrix(params) then raise ELE.Create(params.AsString,'map-tree/unmatched structures');
                        result.Add(ifh_map_tree(call, P, params))
                    end
                else
                    begin
                        for j := 0 to PL.high do expr[j+1] := PL.L[j][i];
                        result.Add(call(expr))
                    end;
        except
            FreeAndNil(result);
            raise;
        end;
    finally
        params.Free;
    end;
end;


procedure ifh_at_depth(d: integer; P: TVSubprogram; L: TVList; call: TCallProc);
var expr: TValues; i: integer;
begin
    L.copy_on_write;  //это необходимо, поскольку процедура обрабатывает копию
                    //аргумента, а не создаёт результат с нуля
    if d=1
    then
        begin
            expr := TValues.Create(P, nil);
            for i := 0 to L.high do
                begin
                    expr[1] := L[i];
                    L[i] := call(expr);
                end;
        end
        else
            for i := 0 to L.high do
                if tpList(L[i]) then ifh_at_depth(d-1, P, L.L[i], call);
end;


//------------------------------------------------------------------------------
function ifh_filter(const PL: TVList; call: TCallProc; P: TTypePredicate): TValue;
var expr: TValues; c: TValue; i: integer;
begin
    c := nil;
    result := TVList.Create;
    expr := TValues.Create(PL[0], nil);
    for i := 0 to PL.L[1].high do
        try
            expr[1] := PL.L[1][i];
            c := call(expr);
            if P(c) then (result as TVList).Add(PL.L[1][i].Copy);
        finally
            c.Free;
        end;
end;

////////////////////////////////////////////////////////////////////////////////
/// строки /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
function ifh_like (str1, str2: unicodestring): integer;
var v: array of record s: unicodestring; n: array[1..2] of integer; end;
    procedure add_point(n: byte; p: unicodestring);
    var i: integer;
    begin
        for i := 0 to high(v) do
            if v[i].s=p then begin Inc(v[i].n[n]); Exit; end;
        SetLength(v, Length(v)+1);
        v[high(v)].s:=p;
        v[high(v)].n[1]:=0;
        v[high(v)].n[2]:=0;
        v[high(v)].n[n]:=1;
    end;

    procedure vectorize(n: byte; s: unicodestring);
    var i: integer;
    begin
        for i:=1 to length(s)   do add_point(n, s[i..i]);
        for i:=1 to length(s)-1 do add_point(n, s[i..i+1]);
        for i:=1 to length(s)-2 do add_point(n, s[i..i+2]);
        for i:=1 to length(s)-2 do add_point(n, s[i]+#1+s[i+2]);
    end;

    function distance: integer;
    var i: integer;
    begin
        result := 0;
        for i := 0 to high(v) do result := result+min(v[i].n[1],v[i].n[2]);
    end;

    function abbr(s: unicodestring): unicodestring;
    var i: integer; word_start: boolean;
    begin
        result := '';
        word_start := true;
        for i := 1 to Length(s) do
            case s[i] of
                ' ': word_start:=true;
                else
                    if word_start
                    then begin result := result+s[i]; word_start:=false; end;
            end;
    end;

var s1, s2: unicodestring;
begin
    s1 := UnicodeUpperCase(str1);
    s2 := UnicodeUpperCase(str2);
    if (Length(s1)=0) or (Length(s2)=0) then begin result:=0; Exit; end;
    //построение вектора
    SetLength(v, 0);
    vectorize(1, s1);
    vectorize(1, abbr(s1));
    vectorize(2, s2);
    vectorize(2, abbr(s2));

    //for i := 0 to high(v) do WriteLn(v[i].s, #9, #9, v[i].n[1], #9, v[i].n[2]);

    result := distance;
    if length(s1)=length(s2) then Inc(result);
    if (PosU(str1, str2)>0) or (PosU(str2, str1)>0) then Inc(result);
    if s1=s2 then Inc(result);
    if str1=str2 then Inc(result);
end;

function ifh_strings_mismatch(s1, s2: unicodestring): integer;
var i, mm: integer; s_long, s_short: unicodestring;
begin
    if Length(s1)>Length(s2)
    then begin s_long := s1; s_short := s2; end
    else begin s_long := s2; s_short := s1; end;
    mm := 0;
    for i := 1 to Length(s_short) do
        if s_short[i]<>s_long[i] then begin mm := i; break; end;
    if (mm=0) and (Length(s1)<>Length(s2)) then mm := Length(s_short)+1;

    result := mm-1;
end;


function ifh_single_spaces(s: unicodestring): unicodestring;
var mode: (space, char);
    i: integer;
    src: unicodestring;
    function space_char(ch: unicodechar): boolean;
    begin result := (ch=' ') or (ch=#9); end;
begin
    mode := space;
    result := '';
    src := trim(s);
    for i := 1 to Length(src) do
        case mode of
            space: if not space_char(src[i]) then begin
                result := result + src[i];
                mode := char;
            end;
            char: if space_char(src[i]) then begin
                    result := result + ' ';
                    mode := space;
            end
            else result := result + src[i];
        end;
end;

function ifh_max_double(dd: TDoubles): integer;
var mv: double; i: integer;
begin
    if dd=nil then raise ELE.Create('invalid parameters/max/empty/');
    mv := dd[0];
    result := 0;
    for i := 1 to high(dd) do if dd[i]>mv then begin mv := dd[i]; result := i; end;
end;

function ifh_min_double(dd: TDoubles): integer;
var mv: double; i: integer;
begin
    if dd=nil then raise ELE.Create('invalid parameters/max/empty/');
    mv := dd[0];
    result := 0;
    for i := 1 to high(dd) do if dd[i]<mv then begin mv := dd[i]; result := i; end;
end;


////////////////////////////////////////////////////////////////////////////////
// matrix //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
function ifh_matrix_mul(w1: integer; m1: TIntegers64; m2: TIntegers64): TIntegers64;
var i,j, k,h1, h2, w2, h, w: integer;
begin
    h1 := length(m1) div w1;
    h2 := w1;
    w2 := length(m2) div h2;
    h := h1;
    w := w2;
    SetLength(result, w*h);
    for j := 0 to h-1 do
        for i := 0 to w-1 do begin
            result[i*w+j] := 0;
            for k := 0 to w1-1 do
            result[i*w+j] := result[i*w+j]+m1[j*w1+k]*m2[w2*k+i];
        end;
end;




function ListContainKeyword(L: TVList; key: unicodestring): boolean;
var i: integer;
begin
    result := true;
    for i := 0 to L.high do if L.uname[i]=key then Exit;
    result := false;
end;


////////////////////////////////////////////////////////////////////////////////
/// base64 /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

const base64_alphabet: array[0..63] of unicodechar = (
'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
'Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f',
'g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
'w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/');

function encode_base64(b: TBytes): unicodestring;
var i: integer; acc: integer;
begin
    result := '';
    for i := 0 to length(b) div 3 -1 do begin
        acc := (b[i*3+0] shl 16) or (b[i*3+1] shl 8) or b[i*3+2];
        result := result
            + base64_alphabet[(acc shr 18) and $3F]
            + base64_alphabet[(acc shr 12) and $3F]
            + base64_alphabet[(acc shr  6) and $3F]
            + base64_alphabet[acc and $3F];
    end;
    if (length(b) mod 3)=2 then begin
        acc := (b[length(b)-2] shl 16) or (b[length(b)-1] shl 8);
        result := result
            + base64_alphabet[(acc shr 18) and $3F]
            + base64_alphabet[(acc shr 12) and $3F]
            + base64_alphabet[(acc shr  6) and $3F]
            + '=';
    end;
    if (length(b) mod 3)=1 then begin
        acc := b[length(b)-1] shl 16;
        result := result
            + base64_alphabet[(acc shr 18) and $3F]
            + base64_alphabet[(acc shr 12) and $3F]
            + '==';
    end;
end;



function decode_base64(s: unicodestring): TBytes;
var i,j, acc: integer;
    function digit(d: unicodechar): integer;
    var a: integer;
    begin
      if (d='=') //or (d=' ') or (d=#8) or (d=#13) or (d=#10)
      then exit(0);
      for a := low(base64_alphabet) to high(base64_alphabet) do
        if base64_alphabet[a]=d then begin result := a; Exit; end;
      raise ELE.Create('Invalid base64 digit '+d,'Invalid data');
    end;
begin
    SetLength(result,0);
    j := 0;
    acc := 0;
    for i := 0 to length(s) div 4 -1 do begin
        if (s[i*4+3]<>'=') and (s[i*4+4]<>'=') then begin
            acc :=  (digit(s[i*4+1]) shl 18) or
                    (digit(s[i*4+2]) shl 12) or
                    (digit(s[i*4+3]) shl 6) or
                    digit(s[i*4+4]);
            SetLength(result, Length(result)+3);
            result[Length(result)-1] := acc mod 256;
            result[Length(result)-2] := (acc div 256) mod 256;
            result[Length(result)-3] := (acc div (256*256)) mod 256;
        end
        else if s[i*4+3]<>'=' then begin
            acc :=  (digit(s[i*4+1]) shl 18) or
                    (digit(s[i*4+2]) shl 12) or
                    (digit(s[i*4+3]) shl 6);
            SetLength(result, Length(result)+2);
            result[Length(result)-1] := (acc div 256) mod 256;
            result[Length(result)-2] := (acc div (256*256)) mod 256;
        end
        else begin
            acc :=  (digit(s[i*4+1]) shl 18) or
                    (digit(s[i*4+2]) shl 12);
            SetLength(result, Length(result)+1);
            result[Length(result)-1] := (acc div (256*256)) mod 256;
        end;

    end;
end;


function decode_base64_2(s: unicodestring): TBytes;
var i, j, acc: integer;
    function digit(d: unicodechar): integer;
    var a: integer;
    begin
      for a := low(base64_alphabet) to high(base64_alphabet) do
        if base64_alphabet[a]=d then begin result := a; Exit; end;
    end;

    procedure add_block;
    begin
      case j of
        4: begin
            SetLength(result, Length(result)+3);
            result[length(result)-3] := (acc shr 16) and $FF;
            result[length(result)-2] := (acc shr  8) and $FF;
            result[length(result)-1] := (acc shr  0) and $FF;
        end;
        3: begin
           SetLength(result, Length(result)+2);
           result[length(result)-2] := (acc shr 16) and $FF;
           result[length(result)-1] := (acc shr  8) and $FF;
        end;
        2: begin
           SetLength(result, Length(result)+1);
           result[length(result)-1] := (acc shr 16) and $FF;
        end;
      end;
      j := 0;
      acc := 0;
    end;

    procedure add_char(ch: unicodechar);
    begin
        acc := acc or (digit(ch) shl (6*(3-j)));
        Inc(j);
        if j=4 then add_block;
    end;

begin
    SetLength(result,0);
    j := 0;
    acc := 0;
    for i := 1 to length(s) do
        case s[i] of
            'A'..'Z','a'..'z','0','1'..'9','+','/': add_char(s[i]);
            '=': add_block;
            ' ',#8,#13,#10:;
            else raise ELE.Create(s[i], 'base64/invalid digit');
        end;
    add_block;
end;


////////////////////////////////////////////////////////////////////////////////
///// RANGE ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function range_intersection(var a1, b1: integer; a2, b2: integer):boolean; overload;
begin
    if b1<a1 then raise ELE.Create(IntToStr(a1)+'..'+IntToStr(b1), 'range intersection/negative range');
    if b2<a2 then raise ELE.Create(IntToStr(a2)+'..'+IntToStr(b2), 'range intersection/negative range');

    if b1<a2 then Exit(false);
    if b2<a1 then Exit(false);

    if (a2>=a1) and (b2<=b1)
    then
        begin
            a1 := a2;
            b1 := b2;
            Exit(true);
		end;

    if (a1>=a2) and (b1<=b2) then Exit(true);

    if (a2>=a1) and (b2>=b1) and (b1>=a2)
    then
        begin
            a1 := a2;
            Exit(true);
		end;

    if (a1>=a2) and (b1>=b2) and (b2>=a1)
    then
        begin
            b1 := b2;
            Exit(true);
		end;

    raise ELE.Create(IntToStr(a1)+'..'+IntToStr(b1)+' * '
                    +IntToStr(a2)+'..'+IntToStr(b2), 'range intersection');
end;


function range_intersection(var a1, b1: double; a2, b2: double):boolean; overload;
begin
    if b1<a1 then raise ELE.Create(FloatToStr(a1)+'..'+FloatToStr(b1), 'range intersection/negative range');
    if b2<a2 then raise ELE.Create(FloatToStr(a2)+'..'+FloatToStr(b2), 'range intersection/negative range');

    if b1<a2 then Exit(false);
    if b2<a1 then Exit(false);

    if (a2>=a1) and (b2<=b1)
    then
        begin
            a1 := a2;
            b1 := b2;
            Exit(true);
		end;

    if (a1>=a2) and (b1<=b2) then Exit(true);

    if (a2>=a1) and (b2>=b1) and (b1>=a2)
    then
        begin
            a1 := a2;
            Exit(true);
		end;

    if (a1>=a2) and (b1>=b2) and (b2>=a1)
    then
        begin
            b1 := b2;
            Exit(true);
		end;

    raise ELE.Create(FloatToStr(a1)+'..'+FloatToStr(b1)+' * '
                    +FloatToStr(a2)+'..'+FloatToStr(b2), 'range intersection');
end;


function ifh_range_integer_intersection(PL: TVList): TValue;
var a1,b1, a2,b2, i: integer; r: TVRangeInteger;
begin
    result := nil;
    r := PL[0] as TVRangeInteger;
    a1 := r.low;
    b1 := r.high;
    for i:=1 to PL.high do
        begin
            r := PL[i] as TVRangeInteger;
            a2 := r.low;
            b2 := r.high;
            if not range_intersection(a1, b1, a2, b2) then Exit(TVList.Create());
		end;

    result := TVRangeInteger.Create(a1, b1);
end;


function ifh_range_float_intersection(PL: TVList): TValue;
var a1,b1, a2,b2: double; i: integer; r: TVRange;
begin
    result := nil;
    r := PL[0] as TVRange;
    a1 := r.f_low();
    b1 := r.f_high();
    for i:=1 to PL.high do
        begin
            r := PL[i] as TVRange;
            a2 := r.f_low();
            b2 := r.f_high();
            if not range_intersection(a1, b1, a2, b2) then Exit(TVList.Create());
		end;

    result := TVRangeFloat.Create(a1, b1);
end;


////////////////////////////////////////////////////////////////////////////////

function ifh_times_sum(VL: TValues): TVTime;
var i: integer; f: TDateTime; date: boolean;
begin
    f := 0;
    date := false;
    for i := 0 to high(VL) do
        begin
            f := f + (VL[i] as TVTime).fDT;
            if VL[i] is TVDateTime
            then
                if date
                then raise ELE.Create('','invalid parameters')
                else date := true;
		end;

    if date
    then result := TVDateTime.Create(f)
    else result := TVDuration.Create(f);
end;


end. //542 471 462

