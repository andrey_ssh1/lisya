unit odbc_module;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils
    , odbcconn
    , sqldb
    , FileUtil, lisya_sql;

type

    { Todbc_m }

    Todbc_m = class(TDataModule)
        connector: TODBCConnection;
        Query: TSQLQuery;
        SQLTransaction1: TSQLTransaction;
    private
        { private declarations }
    public
        { public declarations }
    end;

    { TLSQLodbc }

    TLSQLodbc = class (TLSQL)
        module: Todbc_m;
        constructor Connect(DSN: unicodestring);
        destructor Destroy; override;

        function description: unicodestring; override;
    end;

var
    odbc_m: Todbc_m;

implementation

{$R *.lfm}

{ TLSQLmy }

constructor TLSQLodbc.Connect(DSN: unicodestring);
begin
    inherited Create;
    module := Todbc_m.Create(nil);

    module.connector.DatabaseName:={%H-}DSN;

    query := module.Query;
end;

destructor TLSQLodbc.Destroy;
begin
    module.Free;
    inherited Destroy;
end;

function TLSQLodbc.description: unicodestring;
begin
    result := module.connector.UserName+'@'+module.connector.FileDSN;
end;

end.

