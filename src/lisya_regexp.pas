﻿unit lisya_regexp;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils, mar,
    regexpr in './fpc_backport/regexpr.pas';

function regexp_match(pattern, source: unicodestring; start: integer = 1): TStringArray;
function regexp_is(pattern, source: unicodestring): boolean;

function regexp_replace(source, pattern, new: unicodestring; start: integer = 1): unicodestring;

function contains_words(words, source: unicodestring): boolean;


implementation


function regexp_match(pattern, source: unicodestring; start: integer = 1): TStringArray;
var re: TRegExpr; s: unicodestring;
begin
    SetLength(result,0);
    if start>Length(source) then Exit;
    s := source[start..Length(source)];
    try
        re := TRegExpr.Create(pattern{%H-});
        if re.Exec(s) then begin
            append_string_array(result, re.Match[0]);
            while re.ExecNext do append_string_array(result, re.Match[0]);
        end;
    finally
        re.Free;
    end;
end;


function regexp_is(pattern, source: unicodestring): boolean;
var re: TRegExpr;
begin
    try
        re := TRegExpr.Create(pattern{%H-});
        result := re.Exec(source);
    finally
        re.Free;
    end;
end;

function regexp_replace(source, pattern, new: unicodestring; start: integer
    ): unicodestring;
var re: TRegExpr; s: unicodestring;
begin
    result := '';
    if start>Length(source) then Exit;
    s := source[start..Length(source)];
    try
        re := TRegExpr.Create(pattern{%H-});
        result := re.Replace(s, new, false);
    finally
        re.Free;
    end;
end;


function contains_words(words, source: unicodestring): boolean;
var w: TStringArray; i: integer;
begin
    w := SplitString(words);
    for i := 0 to high(w) do
        if Pos(w[i], source)<1 then Exit(false);
    result := true;
end;


end.

