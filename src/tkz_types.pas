﻿unit TKZ_types;

{$H+}

{$mode DELPHI}

interface

uses
    Classes, SysUtils, ucomplex, lisya_exceptions;

type
    TTKZParameterType = (tptString, tptInteger, tptReal, tptComplex);
    TTKZParameter = record
        name: unicodestring;
        pt: TTKZparameterType;
        s: unicodestring;
        i: integer;
        r: real;
        c: complex;
    end;

    TTKZParameters = array of TTKZParameter;


function NewParameter(_name: unicodestring; _s: unicodestring): TTKZParameter; overload;
function NewParameter(_name: unicodestring; _i: integer):       TTKZParameter; overload;
function NewParameter(_name: unicodestring; _r: Real):          TTKZParameter; overload;
function NewParameter(_name: unicodestring; _c: complex):       TTKZParameter; overload;

function ParameterAsString  (_p: TTKZParameter): unicodestring;
function ParameterAsInteger (_p: TTKZParameter): integer;
function ParameterAsReal    (_p: TTKZParameter): real;
function ParameterAsComplex (_p: TTKZParameter): complex;

procedure PushParameter(var _pp: TTKZParameters; _p: TTKZParameter); overload;
procedure PushParameter(var _pp: TTKZParameters; _name: unicodestring; _s: unicodestring); overload;
procedure PushParameter(var _pp: TTKZParameters; _name: unicodestring; _i: integer); overload;
procedure PushParameter(var _pp: TTKZParameters; _name: unicodestring; _r: real); overload;
procedure PushParameter(var _pp: TTKZParameters; _name: unicodestring; _c: complex); overload;


type
    ETKZ              = class (ELE)
    end;

    ETKZParameter     = class (ETKZ)
    end;

    complexes = array of complex;
    complexes2 = array of complexes;
    complexes3 = array of complexes2;

	{ scomplex }

    scomplex = packed record
        re, im: single;
        class operator Implicit(a: complex): scomplex;
        class operator Implicit(a: real): scomplex;
        class operator Implicit(a: scomplex): complex;
    end;
    scomplexes = array of scomplex;




    function scinit(re, im: single): scomplex; inline; overload;
    function scinit(c: complex): scomplex; inline; overload;
    function dcinit(c: scomplex): complex; inline;
    function scsqr(c: scomplex): scomplex; inline;

    function cdiv_s(a, b: complex): complex; inline;



type
    TArcKind = (akUniversal,
                akResistor,
                akLine,
                akTransformer,
                akGenerator,
                akBreaker);

    TArc = record
        name: unicodestring;
        kind: TArcKind;
        Yl: complex;   //продольная проводимость (приведено к началу)
        Yc: complex;   //поперечная проводимость (приведена к началу)
        K: complex;    //коэффициент трансформации (в конце ветви)
        E: complex;    //ЭДС (приведена к началу)
        node1, node2: integer;  //номера начального и конечного узлов
        I: array[0..1] of complex; // ток в начале и конце (в ветвь)
    end;
    TArcs = array of TArc;



    { TNode }

    TNode = record
        name: unicodestring;
        U: complex;
        zone: integer;
        procedure Init(_name: unicodestring);
    end;
    TNodes = array of TNode;


    TNodeOrArc = (Node, Arc);

    TMeasurementPoint = record
        source: TNodeOrArc;
        index: integer;
    end;
    TMeasurementPoints = array of TMeasurementPoint;


    TArcIndex = array of
        record
            target: TArcKind;
            id: integer;
        end;


    TGridDescription = record
        arcs: TArcs;
        nodes: TNodes;
        measurement_points: TMeasurementPoints;
	end;


    TFaultPoint = record
        zone: integer;
        target: TNodeOrArc;
        id: integer;
        R: real;
	end;
    TFaultPoints = array of TFaultPoint;


    TProtection = record
        name: unicodestring;
        U_mp_id: integer;  //measurement point index
        I_mp_id: integer;
	end;
    TProtections = array of TProtection;


    TMeasurement = record
        U: complex;
        I: complex;
	end;
    TMeasurements = array of TMeasurement;
    TMeasurements2 = array of TMeasurements;



implementation


function NewParameter(_name: unicodestring; _s: unicodestring): TTKZParameter;
begin
    with result do
        begin
            name := _name;
            pt := tptString;
            s := _s;
        end;
end;


function NewParameter(_name: unicodestring; _i: integer): TTKZParameter;
begin
    with result do
        begin
            name := _name;
            pt := tptInteger;
            i := _i;
        end;
end;


function NewParameter(_name: unicodestring; _r: Real): TTKZParameter;
begin
    with result do
        begin
            name := _name;
            pt := tptReal;
            r := _r;
        end;
end;


function NewParameter(_name: unicodestring; _c: complex): TTKZParameter;
begin
  with result do
        begin
            name := _name;
            pt := tptComplex;
            c := _c;
        end;
end;


function ParameterAsString(_p: TTKZParameter): unicodestring;
begin
    case _p.pt of
        tptString: result := _p.s;
        else raise ETKZParameter.Create('Параметр '+_p.name+'не строковый');
    end;
end;


function ParameterAsInteger(_p: TTKZParameter): integer;
begin
    case _p.pt of
        tptInteger: result := _p.i;
        else raise ETKZParameter.Create('Параметр '+_p.name+'не целый');
    end;
end;


function ParameterAsReal(_p: TTKZParameter): real;
begin
    case _p.pt of
        tptInteger: result := _p.i;
        tptReal:    result := _p.r;
        else raise ETKZParameter.Create('Параметр '+_p.name+'не вещественный');
    end;
end;


function ParameterAsComplex(_p: TTKZParameter): complex;
begin
    case _p.pt of
        tptInteger: result := _p.i;
        tptReal:    result := _p.r;
        tptComplex: result := _p.c;
        else raise ETKZParameter.Create('Параметр '+_p.name+'не комплексный');
    end;
end;


procedure PushParameter(var _pp: TTKZParameters; _p: TTKZParameter);
begin
    SetLength(_pp, Length(_pp)+1);
    _pp[high(_pp)] := _p;
end;


procedure PushParameter(var _pp: TTKZParameters; _name: unicodestring; _s: unicodestring);
begin
    PushParameter(_pp, NewParameter(_name, _s));
end;


procedure PushParameter(var _pp: TTKZParameters; _name: unicodestring; _i: integer);
begin
    PushParameter(_pp, NewParameter(_name, _i));
end;


procedure PushParameter(var _pp: TTKZParameters; _name: unicodestring; _r: real);
begin
    PushParameter(_pp, NewParameter(_name, _r));
end;


procedure PushParameter(var _pp: TTKZParameters; _name: unicodestring; _c: complex);
begin
    PushParameter(_pp, NewParameter(_name, _c));
end;


//// SCOMPLEX //////////////////////////////////////////////////////////////////


function scinit(re, im: single): scomplex;
begin
    result.re := re;
    result.im := im;
end;


function scinit(c: complex): scomplex;
begin
    result.re := c.re;
    result.im := c.im;
end;


function dcinit(c: scomplex): complex;
begin
    result.re := c.re;
    result.im := c.im;
end;


function scsqr(c: scomplex): scomplex;
begin
    scsqr.re := c.re * c.re - c.im * c.im;
    scsqr.im := 2 * c.re * c.im;
end;


function cdiv_s(a, b: complex): complex;
var d: real;
begin
    d := b.re*b.re+b.im*b.im;
    result.re := ( a.re*b.re + a.im*b.im) / d;
    result.im := (-a.re*b.im + a.im*b.re) / d;
end;


{ scomplex }

class operator scomplex.Implicit(a: complex): scomplex;
begin
    result.re := a.re;
    result.im := a.im;
end;


class operator scomplex.Implicit(a: real): scomplex;
begin
    result.re := a;
    result.im := 0;
end;


class operator scomplex.Implicit(a: scomplex): complex;
begin
    result.re := a.re;
    result.im := a.im;
end;




{ TNode }


procedure TNode.Init(_name: unicodestring);
begin
    name := _name;
    U := 0;
end;



end.

