//комплексное умножение
float2 cmul(float2 a, float2 b)
{
	return (float2) ((a.x*b.x) - (a.y*b.y),
			         (a.x*b.y) + (a.y*b.x));
}

float2 csqr(float2 a)
{
      return (float2) (a.x * a.x - a.y * a.y,
                       2 * a.x * a.y);
}


float2 cdiv(float2 a, float2 b)
{
    float div = b.x*b.x+b.y*b.y;
    return (float2)(native_divide(( a.x*b.x+a.y*b.y) ,div),
                    native_divide((-a.x*b.y+a.y*b.x) ,div));
}


struct node
{
	float2 U; //float2 представляет комплексное число
	float2 I;
	float2 Y;
};  //24 B


struct arc
{
	float2 Yl;
	float2 Yc;
	float2 K;
	float2 E;
	float2 I;
	float2 I_[2];
	int node[2];
};  //64 B


struct arc arcCurrent(struct arc a, float2 u1, float2 u2)
{
    float2 K = a.K;
    float2 u2_ = cmul(u2, K) - a.E;
    float2 du = u1 - u2_;
    float2 Yc = 0.5f*(a.Yc);
    float2 I = cmul(du, a.Yl);
    float2 I_b = I + cmul(u1, Yc);
    float2 I_e = cmul(K, cmul(u2, Yc) - I);
    a.I = I;
    a.I_[0] = I_b;
    a.I_[1] = I_e;
    return a;
};


struct node nodeVoltage(struct node n)
{
    n.U += cdiv(n.I, n.Y);
    n.I = 0;
    n.Y = 0;
    return n;
}


__kernel void tkz (const int nodesNumber,  const int arcsNumber, const int tasksNumber,
                   __global struct node* pNode,   __global struct arc* pArc
                   )
{
    __local struct node N[MAX_NODES*WORK_GROUP_SIZE];
    __local struct arc  A[MAX_ARCS*WORK_GROUP_SIZE];
    __local unsigned int balanced;

    const int global_task_id = get_global_id(0); //номер задачи
    const int local_task_id = get_local_id(0);
    const int bee = get_global_id(1);
    const int tasks_number = get_global_size(0);

    int i;

    const int global_baseNode = nodesNumber * global_task_id;
    const int global_baseArc  =  arcsNumber * global_task_id;

    const int baseNode = nodesNumber * local_task_id;
    const int baseArc  =  arcsNumber * local_task_id;

    const int n = baseNode + bee;
    const int a = baseArc + bee;

    if (bee < nodesNumber)
    {
        N[n] = pNode[global_baseNode+bee];
        N[n].I = 0;
        N[n].Y = 0;
    }

    if (bee < arcsNumber)
    {
        A[a] = pArc[global_baseArc+bee];
    }

    barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////////

	for (i=0; i<1000000; i++)
	{

        if (bee < arcsNumber)
        {
            A[a] = arcCurrent(A[a], N[A[a].node[0]].U, N[A[a].node[1]].U);
        }
        barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////


        if (bee < 2)
        {
            int a_;
            for (a_=0; a_<arcsNumber; a_++)
            {
                //небалансы в узлах
                int n_ = baseNode + A[a_].node[bee];
                float2 K = A[a_].K;
                float2 Y = A[a_].Yl + 0.5f*A[a_].Yc;
                N[n_].I -= A[a_].I_[bee];
                if (bee==0)
                    N[n_].Y += Y;
                else
                    N[n_].Y += cmul(csqr(K), Y);
            }
            N[baseNode].I = 0;
            N[baseNode].Y = 0;
            balanced = 1;
        }
        barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////

        if ((0 < bee) && (bee < nodesNumber))
        {
                atomic_and(&balanced, fabs(N[n].I.x)<1.0f && fabs(N[n].I.y)<1.0f);
                N[n] = nodeVoltage(N[n]);
        }

        barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////

        N[0].I = (float2) ((float)i,0); // вернуть количество циклов
        if (balanced) break;

	}

    if (bee < arcsNumber)
        pArc[global_baseArc+bee] = A[a];

    if (bee < nodesNumber)
        pNode[global_baseNode+bee] = N[n];


    barrier(CLK_LOCAL_MEM_FENCE);

}
