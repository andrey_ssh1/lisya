//комплексное умножение
float2 cmul(float2 a, float2 b)
{
	return (float2) ((a.x*b.x) - (a.y*b.y),
			         (a.x*b.y) + (a.y*b.x));
}

float2 csqr(float2 a)
{
      return (float2) (a.x * a.x - a.y * a.y,
                       2 * a.x * a.y);
}

//комплексное деление
float2 cdiv(float2 a, float2 b)
{
	if (fabs(b.x) > fabs(b.y))
	{
		float tmp = b.y / b.x;
		float denom = b.x + b.y*tmp;
		return (float2) ((a.x + a.y*tmp) / denom,
				         (a.y - a.x*tmp) / denom);
	}
	else
	{
		float tmp = b.x / b.y;
		float denom = b.y + b.x*tmp;
		return (float2) (( a.y + a.x*tmp) / denom,
				         (-a.x + a.y*tmp) / denom);
	}
}


float2 cdiv_s(float2 a, float2 b)
{
    float div = b.x*b.x+b.y*b.y;
    return (float2)(native_divide(( a.x*b.x+a.y*b.y) ,div),
                    native_divide((-a.x*b.y+a.y*b.x) ,div));
}


struct node
{
	float2 U; //float2 представляет комплексное число
	float2 I;
	float2 Y;
};  //24 B


struct arc
{
	float2 Yl;
	float2 Yc;
	float2 K;
	float2 E;
	float2 I;
	float2 I_[2];
	int node[2];
};  //64 B


struct arc arcCurrent(struct arc a, float2 u1, float2 u2)
{
    float2 K = a.K;
    float2 u2_ = cmul(u2, K) - a.E;
    float2 du = u1 - u2_;
    float2 Yc = 0.5f*(a.Yc);
    float2 I = cmul(du, a.Yl);
    float2 I_b = I + cmul(u1, Yc);
    float2 I_e = cmul(K, cmul(u2, Yc) - I);
    a.I = I;
    a.I_[0] = I_b;
    a.I_[1] = I_e;
    return a;
};


struct node nodeVoltage(struct node n)
{
    n.U += cdiv_s(n.I, n.Y);
    n.I = 0;
    n.Y = 0;
    return n;
}


__kernel void tkz (const int nodesNumber,  const int arcsNumber, const int tasksNumber,
                   __global struct node* pNode,   __global struct arc* pArc
                   )
{
    const int global_id = get_global_id(0);
    const int local_id = get_local_id(0);
    const int group_id = get_group_id(0);

    if (group_id >= tasksNumber) return;

    int i;
    int a;
    const int baseNode = nodesNumber * group_id;
    const int baseArc  =  arcsNumber * group_id;

    __local struct node N[MAX_NODES];
    __local struct arc  A[MAX_ARCS];
    __local unsigned int balanced;

    if (local_id<nodesNumber)
    {
        N[local_id] = pNode[baseNode+local_id];
        N[local_id].I = 0;
        N[local_id].Y = 0;
    }

    if (local_id<arcsNumber)
    {
        A[local_id] = pArc[baseArc+local_id];
    }


    barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////////

	for (i=0; i<1000000; i++)
	{
        a = local_id;
        if (local_id < arcsNumber)
        {
            A[a] = arcCurrent(A[a], N[A[a].node[0]].U, N[A[a].node[1]].U);
        }
        barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////


        if (local_id < 2)
        {
            for (a=0; a<arcsNumber; a++)
            {
                //небалансы в узлах
                int n = A[a].node[local_id];
                float2 K = A[a].K;
                float2 Y = A[a].Yl + 0.5f*A[a].Yc;
                N[n].I -= A[a].I_[local_id];
                if (local_id==0)
                    N[n].Y += Y;
                else
                    N[n].Y += cmul(csqr(K), Y);
            }
            N[0].I = 0;
            N[0].Y = 0;
            balanced = 1;
        }
        barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////

        a = local_id;
        if ((0 < a) && (a < nodesNumber))
        {
                atomic_and(&balanced, fabs(N[a].I.x)<1.0f && fabs(N[a].I.y)<1.0f);
                N[a] = nodeVoltage(N[a]);
        }

        barrier(CLK_LOCAL_MEM_FENCE); //////////////////////////////////////////

        N[0].I = (float2) ((float)i,0); // вернуть количество циклов
        if (balanced) break;

	}

    if (local_id<arcsNumber)
        pArc[baseArc+a] = A[a];

    if (local_id<nodesNumber)
        pNode[baseNode+a] = N[a];


    barrier(CLK_LOCAL_MEM_FENCE);

}
