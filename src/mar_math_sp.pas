﻿unit mar_math_sp;

{$mode delphi}{$H+}

interface

uses
    mar, ucomplex, sysutils, math, mar_math_arrays;



function noisy(V, m, a: real): real; overload;
function noisy(V: COMPLEX; m, a: real): COMPLEX; overload;
function noisy(V: TDoubles; m, a: real): TDoubles; overload;
function noisy(V: TComplexes; m, a: real): TComplexes; overload;

function trigger_over (sv: TDoubles; threshold, return: real): TIntegers;
function trigger_under(sv: TDoubles; threshold, return: real): TIntegers;
function trigger_out(sv: TDoubles; t_l, t_h, r_l, r_h: real): TIntegers;

function limited(sv: TDoubles; lt: boolean; top: double; lb: boolean; bottom: double): TDoubles;

function correlation (a, b: TDoubles): real;

function sp_convolution(sv, core: TDoubles; normalization: boolean): TDoubles;

function harmonic(sv: TDoubles; window: integer; frequency: real): TComplexes;
function DFT(sv: TDoubles; window, harmonic: integer): TComplexes;

function sp_diff(sv: TDoubles): TDoubles;
function sp_rms(sv:TDoubles; w: integer): TDoubles;


function thinned(sv: TDoubles; n: integer): TDoubles;
function thinned_mean(sv: TDoubles; n: integer): TDoubles;

function sp_stretch(sv: TDoubles; scale: real; order: integer): TDoubles;

function sp_sample(sv: TDoubles; n: real; order: integer=1): real; overload;
function sp_sample(sv: PComplexes; cnt: integer; n:real; order: integer): complex; overload;




implementation

type
    ESignalOperation = class(Exception) end;
    ESO = ESignalOperation;


////////////////////////////////////////////////////////////////////////////////
//// noisy /////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function random_complex(): COMPLEX;
begin
    result := Random()*cexp(cinit(0, 2*pi*Random()));
end;


function noisy(V, m, a: real): real;
begin
    result := V + V*m*(2*Random()-1) + a*(2*Random()-1);
end;


function noisy(V: COMPLEX; m, a: real): COMPLEX;
begin
    result := V + cmod(V)*m*random_complex() + a*random_complex();
end;


function noisy(V: TDoubles; m, a: real): TDoubles; overload;
var i: integer;
begin
    SetLength(result, Length(V));
    for i := 0 to high(V) do result[i] := noisy(V[i], m, a);
end;


function noisy(V: TComplexes; m, a: real): TComplexes; overload;
var i: integer;
begin
    SetLength(result, Length(V));
    for i := 0 to high(V) do result[i] := noisy(V[i], m, a);
end;


////////////////////////////////////////////////////////////////////////////////
//// trigger ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function trigger_over(sv: TDoubles; threshold, return: real): TIntegers;
var t: boolean; i: integer;
begin
    if return>threshold then raise ESO.Create('trigger over/return>treshold');

    SetLength(result, 0);
    t := false;
    for i := 0 to high(sv) do
        case t of
            false: if sv[i]>=threshold then
                begin
                    t := true;
                    append_integer(result, i);
                end;
            true: if sv[i]<return then
                begin
                    t := false;
                    append_integer(result, i);
                end;
        end;

    if t then append_integer(result, Length(SV));
end;


function trigger_under(sv: TDoubles; threshold, return: real): TIntegers;
var t: boolean; i: integer;
begin
    if return<threshold then raise ESO.Create('trigger under/return<treshold');

    SetLength(result, 0);
    t := false;
    for i := 0 to high(sv) do
      case t of
        false: if sv[i]<=threshold then
            begin
                t := true;
                append_integer(result, i);
            end;
        true: if sv[i]>return then
            begin
                t := false;
                append_integer(result, i);
            end;
      end;

    if t then append_integer(result, Length(sv));
end;


function trigger_out(sv: TDoubles; t_l, t_h, r_l, r_h: real): TIntegers;
var t: boolean; i: integer;
begin
    if  (t_l>r_l) or (t_h<r_h) or (t_l>=t_h) or (r_l>=r_h)
    then raise ESO.Create('trigger out/return>treshold');

    SetLength(result, 0);
    t := false;
    for i := 0 to high(sv) do
      case t of
        false: if (sv[i]>=t_h) or (sv[i]<=t_l) then
            begin
                t := true;
                append_integer(result, i);
            end;
        true: if (sv[i]<r_h) and (sv[i]>r_l) then
            begin
                t := false;
                append_integer(result, i);
            end;
      end;

    if t then append_integer(result, Length(sv));
end;


////////////////////////////////////////////////////////////////////////////////
//// limitation ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function limited(sv: TDoubles; lt: boolean; top: double; lb: boolean;
			bottom: double): TDoubles;
var i: integer;
begin
    if lt and lb and (top<bottom) then raise ESO.Create('limited/top<bottom');

    Result := copy(sv);

    if lt
    then
        for i := 0 to high(sv) do
            if result[i]>top then result[i] := top;

    if lb
    then
        for i := 0 to high(sv) do
            if result[i]<bottom then result[i] := bottom;
end;


////////////////////////////////////////////////////////////////////////////////
//// correlation ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function correlation(a, b: TDoubles): real;
var a_mean, b_mean, cov, a_d, b_d: real; i: integer;
begin
    if Length(a)<>Length(b) then raise ESO.Create('correlation/different sizes');

    a_mean := mean(a);
    b_mean := mean(b);

    cov := 0;
    a_d := 0;
    b_d := 0;

    for i := 0 to high(a) do
        begin
            cov := cov + (a[i] - a_mean) * (b[i] - b_mean);
            a_d := a_d + (a[i] - a_mean)**2;
            b_d := b_d + (b[i] - b_mean)**2;
		end;

    if a_d*b_d = 0 then raise ESO.Create('signal/correlation/constant argument');

    result := cov / sqrt(a_d * b_d);
end;


////////////////////////////////////////////////////////////////////////////////
//// convolution ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


function sp_convolution(sv, core: TDoubles; normalization: boolean): TDoubles;
var i, j, l: integer; a, w: double;
begin
    if Length(sv)<Length(core) then raise ESO.Create('signal/convolution/small source/');

    result := nil;
    l := Length(sv)-Length(core)+1;
    SetLength(result, l);

    if normalization
    then
        for i := 0 to l-1 do
            begin
                a := 0;
                w := 0;
                for j := 0 to high(core) do
                    begin
                        a := a + sv[i+j]*core[j];
                        w := w + abs(sv[i+j]);
                    end;
                result[i] := a/w;
            end
    else
        for i := 0 to l-1 do
            begin
                a := 0;
                for j := 0 to high(core) do
                    a := a + sv[i+j]*core[j];
                result[i] := a;
			end;
end;


////////////////////////////////////////////////////////////////////////////////
//// fourier ///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


function harmonic(sv: TDoubles; window: integer; frequency: real): TComplexes;
var i, p: integer; re, im, darg, w_re, w_im: double;
begin
    if frequency>=0.5 then raise ESO.Create('fourier/low resolution');

    SetLength(result, Length(sv)-window+1);
    darg := 2*pi*frequency;

    for p := 0 to high(result) do
        begin
            result[p] := 0;
            w_re := 0;
            w_im := 0;
            for i := p to p+window-1 do
                begin
                    sincos(darg*i, im, re);
                    w_re := w_re + abs(re);
                    w_im := w_im + abs(im);
                    result[p].re := result[p].re + sv[i]*re;
                    result[p].im := result[p].im - sv[i]*im;
                end;
            result[p].re := result[p].re / w_re;
            result[p].im := result[p].im / w_im;
		end;
end;


function DFT(sv: TDoubles; window, harmonic: integer): TComplexes;
var i, p: integer; sint, cost: TDoubles; w: real;
begin
    SetLength(sint, window);
    SetLength(cost, window);
    for i := 0 to window-1 do sincos(2*pi*i/window, sint[i], cost[i]);

    if harmonic=0 then w := 1 else w := 2;

    SetLength(result, Length(sv)-window+1);

    for p := 0 to high(result) do
        begin
            result[p] := 0;
            for i := p to p+window-1 do
                begin
                    result[p].re := result[p].re + sv[i]*cost[i*harmonic mod window];
                    result[p].im := result[p].im - sv[i]*sint[i*harmonic mod window];
				end;
            result[p] := result[p] * w / window;
		end;
end;


////////////////////////////////////////////////////////////////////////////////
//// Differentiation ///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function sp_diff(sv: TDoubles): TDoubles;
var i: integer;
begin
    if Length(sv)<2 then raise ESO.Create('sp_diff/too short arg');

    SetLength(result, Length(sv)-1);
    for i:= 1 to high(sv) do
        result[i-1] := sv[i]-sv[i-1];
end;


////////////////////////////////////////////////////////////////////////////////
//// Moving average ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function sp_rms(sv: TDoubles; w: integer): TDoubles;
var sqsum: double; i, j: integer;
begin
    if (w<1) or (w>Length(sv)) then raise ESO.Create('RMS/invalid window/');

    SetLength(result,Length(sv)-w+1);
    //TODO: неэффективный RMS
    for i := 0 to high(result) do
    begin
        sqsum := 0;
        for j := 0 to w-1 do sqsum := sqsum + (sv[i+j])**2;
        result[i] := sqrt(sqsum/w);
    end;
end;


////////////////////////////////////////////////////////////////////////////////
//// Thinning //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function thinned(sv: TDoubles; n: integer): TDoubles;
var i: integer;
begin
    SetLength(result, Length(sv) div n);
    for i := 0 to high(result) do result[i] := sv[i*n];
end;


function thinned_mean(sv: TDoubles; n: integer): TDoubles;
var i, j: integer;
begin
    SetLength(result, Length(sv) div n);
    for i := 0 to high(result) do
        begin
            result[i] := 0;
            for j := 0 to n-1 do result[i] := result[i] + sv[i*n+j];
            result[i] := result[i] / n;
		end;
end;


function sp_stretch(sv: TDoubles; scale: real; order: integer): TDoubles;
var i, l: integer;
begin
    result := nil;
    //при удвоении, удваивается не количество семлов а количество интервалов
    //во избежание взятия семпла за пределами исходной последовательности
    l := floor((length(sv)-1)*scale) + 1;
	SetLength(result, l);
    for i := 0 to l-1 do result[i] := sp_sample(sv, i/scale, order);
end;

////////////////////////////////////////////////////////////////////////////////
//// Interpolation /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////



function sp_sample(sv: TDoubles; n: real; order: integer): real;
var n1, n2: integer;
begin
    case order of
        0: begin
            n1 := round(n);
            if (n1<0) or (n1>high(sv)) then raise ESO.Create('interpolation/out of range/'+FloatToStr(n));
            result := sv[n1];
		end;
        1: begin
            n1 := floor(n);
            n2 := ceil(n);
            if (n1<0) or (n2>high(sv)) then raise ESO.Create('interpolation/out of range/'+FloatToStr(n));
            result := sv[n1] + (sv[n2]-sv[n1])*frac(n);
	    end;
        else ESO.Create('interpolated sample/unsupported order/'+FloatToStr(order));
	end;
end;


function sp_sample(sv: PComplexes; cnt: integer; n:real; order: integer): complex;
var n1, n2: integer;
begin
    case order of
        0: begin
            n1 := round(n);
            if (n1<0) or (n1>cnt) then raise ESO.Create('interpolation/out of range/'+FloatToStr(n));
            {$R-}result := sv^[n1];{$R+}
		end;
        1: begin
            n1 := floor(n);
            n2 := ceil(n);
            if (n1<0) or (n2>cnt) then raise ESO.Create('interpolation/out of range/'+FloatToStr(n));
            {$R-}result := sv^[n1] + (sv^[n2]-sv^[n1])*frac(n);{$R+}
		end;
        else ESO.Create('interpolated sample/unsupported order/'+FloatToStr(order));
	end;
end;

end.


