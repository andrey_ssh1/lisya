﻿(package utils (
    inc
    dec
    cexp
    successfull
    parsed
    subprocess
    table-from-CSV
    table-to-CSV
    save
    сохранить
    load
    загрузить
    load-to
    загрузить-в
    macroexpand)


(procedure inc (i) (set i (+ i 1)))

(procedure dec (i) (set i (- i 1)))

(function cexp (x)
    (const a (round (abs x)))
    (if (> a 1000)
        (str (div a 1000) "_" (FIXED (mod a 1000) 0 3) "∠" (round (deg (arg x))) "°")
        (str a "∠" (round (deg (arg x))) "°")))


(macro successfull (:rest expression)
    "Возвращает результат вычисления последнего переданного выражения.
    В случае возникновения исключения, подавляет его, и возвращает NIL"
    (assemble try (block @expression) ("")))


(procedure tokens1 (s rules i mode)
    (var tokens)
    (var acc "")
    (while t
        (for selector (associations rules mode :by-head)
            (const expr (concatenate "^" (head selector)))
            (const token
                (if (and (= i (length s)) (= expr "^$"))
                    ""
                    (head (regexp:match expr s :from i))))
            (when token
                (set i (+ i (length token)))
                (for cmd (subseq selector 1)
                    (cond
                        ((keyword? cmd) (set mode cmd))
                        ((string? cmd) (append acc cmd))
                        ((integer? cmd) (set i (+ i cmd)))
                        ((= \begin cmd) (push tokens (tokens1 s rules i mode)))
                        ((= \return cmd) (set i (- i (length acc))))
                        ((= \accumulate cmd) (append acc token))
                        ((= \clear cmd) (set acc ""))
                        ((= \token cmd) (when (not (empty? acc))
                                                (push tokens acc) (set acc "")))
                        ((= \read cmd) (push tokens (read-from-string acc)) (set acc ""))
                        ((= \error cmd) (error "utils/parser" acc))
                        ((= \trim cmd) (set acc (trim acc)))
                        ((= \end cmd) (return tokens))
                        (t (error "utils/parsed" "command " cmd " not supported"))))
                (break)))
            (if (= i (length s)) (break)))
    tokens)


(function parsed (s rules)
    "Разбирает переданную строку в соответствии с заданными правилами
    ((:state1 (\"regexp\" cmd*)*)+)
    cmd ::=
        :keyword - переключение состояния
        \"string\" - добавление произвольной строки к аккумулятору
        123 - перемещение по строке на заданное количество символов
        begin - рекурсивный вызов парсера
        return - возврат на длину текущего аккумулятора
        accumulate - добавление текущего символа к аккумулятору
        clear - очистка аккумулятора
        token -добавление аккумулятора к списку и очистка
        read - считывание аккумулятора, добавление считанного в список и очистка
        error - генерация ошибки с текущим аккумулятором в качестве сообщения
        trim - удаление пробельных символов из аккумулятора
        end - завершение разбора"
    (var mode (head (head rules)))
    (var i 0)
    (tokens1 s rules i mode))


(macro subprocess (name :rest body)
    "Создаёт процесс интерпретатора и передаёт ему команды на исполнение"
    (var p (process
        (fmt t executable-path "lisya -")
        :encoding (if (platform "windows") :cp866 :utf8)))
    (for c body (write p c))
    (assemble var /name /p))


(procedure table-from-CSV (filename :key encoding cell-separator row-separator)
    "Считывает данные из CSV файла и возвращает их в виде списка строк/ячеек"
    (default cell-separator ";")
    (default row-separator NL)
    (default encoding :utf8)
    (map
        (function (r) (split-string r cell-separator))
        (remove
            (split-string
                (text-from-file filename encoding)
                row-separator)
            "")))


(procedure table-to-CSV (filename table :key encoding cell-separator row-separator)
    "Сохраняет список строк/ячеек в файл формата CSV"
    (default cell-separator ";")
    (default row-separator NL)
    (default encoding :utf8)
    (var f (file filename :write encoding))
    (for s table (fmt f (LST s cell-separator) row-separator)))


(macro save (v)
    "Сохраняет указанную переменную в файл с соответствующим именем"
    (assemble /write (/file /(str v ".txt") :WRITE) /v))

(const сохранить save)


(macro load (v)
    "Объявляет переменную с указанным именем и загружает в неё данные из файла с
    соответствующим именем"
    (assemble var /v (/read (/file /(str v ".txt")))))

(const загрузить load)


(macro load-to (v)
    "Загружает данные в указанную переменную из файла с соответствующим именем"
    (assemble set /v (/read (/file /(str v ".txt")))))

(const загрузить-в load-to)


(macro macroexpand (expression)
    (print nil expression)
    (fmt nil "=>" NL)
    (assemble log (debug :macroexpand /expression)))



)
