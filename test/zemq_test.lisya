﻿(fmt nil "ZEMQ ... ")




(function cexp (x)
    (str (round (abs x)) "∠" (round (deg (arg x))) "°"))

(procedure print-measurements (m)
    (log)
    (for m m
        (log (COL 20 m/0) (COL 20 (cexp m/1)) (COL 20 (cexp m/2)))))


(function ~ (U a I b)
    (const res
        (and
            (< (abs (- U a)) (+ 100 (* 0.01 (+ (abs U) (abs a)))))
            (< (abs (- I b)) (+ 10  (* 0.01 (+ (abs I) (abs b)))))))
    (if (not res) (log "U = " (cexp U) "  /  I = " (cexp I)))
    res)



(block "basic"

    ;генератор на холостом ходу
    (const m1 (ZEMQ \(
        (:G 1 0 0+i10 10k)
        )))
;    (print-measurements m1)
    (const ((_ U I)) m1)
    (assertion (~ U -10k I 0) "generator idle")

    ;изолированный генератор на холостом ходу
    (const m2 (ZEMQ \(
        (:G 1 2 0+i10 10k)
        )))
;    (print-measurements m2)
    (const ((_ U I)) m2)
    (assertion (~ U -5k I 0) "generator isolated idle")

    ;замыкание
    (const m3 (ZEMQ \(
        (:G 0 1 0+i10 10k)
        (:Z 1 0 0+i30)
        )))
;    (print-measurements m3)
    (const (_ (_ U I)) m3)
    (assertion (~ U 7500 I 250∠-90°) "short circuit")

    ;короткое замыкание генератора
    (const m4 (ZEMQ \(
        (:G 0 0 0+i10 10k)
        )))
;    (print-measurements m4)
    (const ((_ U I)) m4)
    (assertion (~ U 0 I 1000∠-90°) "generator short circuit")

    ;короткое замыкание изолированного генератора
    (const m5 (ZEMQ \(
        (:G 1 1 0+i10 10k)
        )))
;    (print-measurements m5)
    (const ((_ U I)) m5)
    (assertion (~ U 0 I 1000∠-90°) "generator isolated short circuit")

    ;трансформатор на холостом ходу
    (const m6 (ZEMQ \(
        (:G 0 1 0+i10 10k)
        (:M (1 2) (0 0) {T  0+i1_010   0+i1_000 |
                            0+i1_000   0+i1_010 | |})
        )))
;    (print-measurements m6)
    (const (_ (_ U1 I1) (_ U2 I2)) m6)
    (assertion (~ U1 9902 I1 10∠-90°) "transformer idle primary")
    (assertion (~ U2 9804 I2 0∠0°) "transformer idle secondary")

    ;изолированный трансформатор на холостом ходу
    (const m7 (ZEMQ \(
        (:G 0 1 0+i10 10k)
        (:M (1 2) (0 3) {T  0+i1_010 0+i1_000 |
                            0+i1_000 0+i1_010 | |})
        )))
;    (print-measurements m7)
    (const (_ (_ U1 I1) (_ U2 I2)) m7)
    (assertion (~ U1 9902 I1 10∠-90°) "transformer isolated idle primary")
    (assertion (~ U2 4902 I2 0∠0°) "transformer isolated idle secondary")

    ;трансформатор под КЗ
    (const m8 (ZEMQ \(
        (:G 0 1 0+i10 10k)
        (:M (1 0) (0 0) {T  0+i1_010 0+i1_000 |
                            0+i1_000 0+i1_010 | |})
        )))
;    (print-measurements m8)
    (const (_ (_ U1 I1) (_ U2 I2)) m8)
    (assertion (~ U1 6656 I1 334∠-90°) "transformer short circuit primary")
    (assertion (~ U2 0 I2 331∠90°) "transformer short circuit secondary")


    ;изолированный трансформатор под КЗ
    (const m9 (ZEMQ \(
        (:G 0 1 0+i10 10k)
        (:M (1 0) (0 0) {T  0+i1_010 0+i1_000 |
                            0+i1_000 0+i1_010 | |})
        )))
;    (print-measurements m9)
    (const (_ (_ U1 I1) (_ U2 I2)) m9)
    (assertion (~ U1 6656 I1 334∠-90°) "transformer short circuit primary")
    (assertion (~ U2 0 I2 331∠90°) "transformer short circuit secondary")

    ;автотрансформатор
    (const m10 (ZEMQ \(
        (:G 0 1 0+i10 10k)
        (:M (1 2) (2 0) {T  0+i1_010   0+i1_000 |
                            0+i1_000   0+i1_010 | |})
        (:Z 2 0 0+i40)
        )))
;    (print-measurements m10)
    (const (_ (_ U1 I1) _ (_ U2 I2)) m10)
    (assertion (~ U1 9451 I1 55∠-90°) "autotransformer primary")
    (assertion (~ U2 4201 I2 105∠-90°) "autotransformer secondary")

    ;автотрансформатор повышающий
    (const m11 (ZEMQ \(
        (:G 0 2 0+i10 10k)
        (:M (1 2) (2 0) {T  0+i1_010 0+i1_000 |
                            0+i1_000 0+i1_010 | |})
        (:Z 1 0 0+i160)
        )))
;    (print-measurements m11)
    (const ((_ _ I1) _ (_ U1 _) (_ U2 I2)) m11)
    (assertion (~ U1 8130  I1 187∠-90°)  "autotransformer 2 primary")
    (assertion (~ U2 14389 I2  90∠-90°) "autotransformer 2 secondary")

    ;трёхфазная линия
    (const m12 (ZEMQ \(
        (:G 0 1 0+i10 10000a0deg)
        (:G 0 2 0+i10 10000a240deg)
        (:G 0 3 0+i10 10000a120deg)
        (:M (1 2 3) (0 0 0) {T 0+i60 0+i20 0+i20 |
                               0+i20 0+i60 0+i20 |
                               0+i20 0+i20 0+i60 | |})
        )))
;    (print-measurements m12)
    (const (_ _ _ (_ Ua Ia) (_ Ub Ib) (_ Uc Ic) ) m12)
    (assertion (~ Ua 8000∠0°   Ia 200∠-90°) "line A")
    (assertion (~ Ub 8000∠240° Ib 200∠150°) "line B")
    (assertion (~ Uc 8000∠120° Ic 200∠30°)  "line C")

    ;трансформатор понижающий на холостом ходу
    (const m13 (ZEMQ \(
        (:G 0 1 0+i10 10k)
        (:M (1 2) (0 0) {T  0+i1008   0+i500 |
                             0+i500   0+i252 | |})
        )))
;    (print-measurements m13)
    (const (_ (_ U1 I1) (_ U2 I2)) m13)
    (assertion (~ U1 9902 I1 10∠-90°) "transformer lowering idle primary")
    (assertion (~ U2 4912 I2 0∠-90°) "transformer lowering idle secondary")

    ;трансформатор понижающий
    (const m14 (ZEMQ \(
        (:G 0 1 0+i10 10k)
        (:M (1 2) (0 0) {T  0+i1008   0+i500 |
                             0+i500   0+i252 | |})
        (:Z 2 0 0+i40)
        )))
;    (print-measurements m14)
    (const (_ (_ U1 I1) _ (_ U2 I2)) m14)
    (assertion (~ U1 9382 I1 62∠-90°) "transformer lowering primary")
    (assertion (~ U2 4232 I2 106∠-90°) "transformer lowering idle secondary")

    )


(block "line"

    ;трёхфазная линия
    ;К3
    (const m1 (ZEMQ \(
        (:G 0 11 0,666+i4,5 69700a0deg)
        (:G 0 12 0,666+i4,5 69700a240deg)
        (:G 0 13 0,666+i4,5 69700a120deg)
        (:M (11 12 13) (0 0 0) {T
            9.333+i31.173   2.853+i15.813   2.853+i15.813 |
            2.853+i15.813   9.333+i31.173   2.853+i15.813 |
            2.853+i15.813   2.853+i15.813   9.333+i31.173 | |})
        )))
;    (print-measurements m1)
    (const (_ _ _ (_ Ua Ia) (_ Ub Ib) (_ Uc Ic)) m1)
    (assertion (~ Ua 55050∠-3°   Ia 3302∠-70°) "3ph line ABC0 A")
    (assertion (~ Ub 55050∠-123° Ib 3302∠170°) "3ph line ABC0 B")
    (assertion (~ Uc 55050∠117°  Ic 3302∠50°)  "3ph line ABC0 C")

    ;трёхфазная линия
    ;К1
    (const m2 (ZEMQ \(
        (:G 0 11 0,666+i4,5 69700a0deg)
        (:G 0 12 0,666+i4,5 69700a240deg)
        (:G 0 13 0,666+i4,5 69700a120deg)
        (:M (11 12 13) (0 22 23) {T
            9.333+i31.173   2.853+i15.813   2.853+i15.813 |
            2.853+i15.813   9.333+i31.173   2.853+i15.813 |
            2.853+i15.813   2.853+i15.813   9.333+i31.173 | |})
        )))
;    (print-measurements m2)
    (const (_ _ _ (_ Ua Ia) (_ Ub Ib) (_ Uc Ic)) m2)
    (assertion (~ Ua 61220∠-1°   Ia 1881∠-74°) "3ph line A0 A")
    (assertion (~ Ub 69700∠-120° Ib 0∠0°)      "3ph line A0 B")
    (assertion (~ Uc 69700∠120°  Ic 0∠0°)      "3ph line A0 C")
    (const 3U0 (+ Ua Ub Uc))
    (const 3I0 (+ Ia Ib Ic))
    (assertion (~ 3U0 8560∠-173°  3I0 1881∠-74°) "3ph line A0 0")

    ;трёхфазная линия
    ;К1 при заземлении параллельной цепи
    (const m3 (ZEMQ \(
        (:G 0 11 0,666+i4,5 69700a0deg)
        (:G 0 12 0,666+i4,5 69700a240deg)
        (:G 0 13 0,666+i4,5 69700a120deg)
        (:M (11 12 13 0) (0 22 23 0) {T
            9.333+i31.173   2.853+i15.813   2.853+i15.813   4.965+i24.895 |
            2.853+i15.813   9.333+i31.173   2.853+i15.813   4.965+i24.895 |
            2.853+i15.813   2.853+i15.813   9.333+i31.173   4.965+i24.895 |
            4.965+i24.895   4.965+i24.895   4.965+i24.895   15.04+i62.8   | |})

        )))
;    (print-measurements m3)
    (const (_ _ _ (_ Ua Ia) (_ Ub Ib) (_ Uc Ic)) m3)
    (assertion (~ Ua 58220∠-2°   Ia 2566∠-72°) "4ph line A0 A")
    (assertion (~ Ub 69700∠-120° Ib 0∠0°)      "4ph line A0 B")
    (assertion (~ Uc 69700∠120°  Ic 0∠0°)      "4ph line A0 C")
    (const 3U0 (+ Ua Ub Uc))
    (const 3I0 (+ Ia Ib Ic))
    (assertion (~ 3U0 11670∠-170°  3I0 2566∠-72°) "4ph line A0 0")

    )


(block "Transformer"

    ;трансформатор Yн/D
    (const m1 (ZEMQ \(
        (:G 0 11 0+i10 10000a0deg)
        (:G 0 12 0+i10 10000a240deg)
        (:G 0 13 0+i10 10000a120deg)
        (:M (11 21) (0 23) {T 0+i10_010 0+i10_000 | 0+i10_000 0+i10_010 | |})
        (:M (12 22) (0 21) {T 0+i10_010 0+i10_000 | 0+i10_000 0+i10_010 | |})
        (:M (13 23) (0 22) {T 0+i10_010 0+i10_000 | 0+i10_000 0+i10_010 | |})
        (:Z 21 0 10000)
        (:Z 22 0 10000)
        (:Z 23 0 10000)
        )))
;    (print-measurements m1)
    (const (_ _ _  _ _ _  _ _ _ (_ Ua Ia) (_ Ub Ib) (_ Uc Ic)) m1)
    (assertion (~ Ua 5762∠030°  Ia 1∠030°) "transformer Yn/D idle A")
    (assertion (~ Ub 5762∠270°  Ib 1∠270°) "transformer Yn/D idle B")
    (assertion (~ Uc 5762∠150°  Ic 1∠150°) "transformer Yn/D idle C")

    ;трансформатор Yн/D под 3U0
    (const m2 (ZEMQ \(
        (:G 0 11 0+i10 10000a0deg)
        (:G 0 12 0+i10 7000a240deg)
        (:G 0 13 0+i10 7000a120deg)
        (:M (11 21) (0 23) {T 0+i10_010 0+i10_000 | 0+i10_000 0+i10_010 | |})
        (:M (12 22) (0 21) {T 0+i10_010 0+i10_000 | 0+i10_000 0+i10_010 | |})
        (:M (13 23) (0 22) {T 0+i10_010 0+i10_000 | 0+i10_000 0+i10_010 | |})
        )))
    (const (_ _ _ (_ Ua Ia) _ (_ Ub Ib) _ (_ Uc Ic)) m2)
    (const 3U0 (+ Ua Ub Uc))
    (const 3I0 (+ Ia Ib Ic))
    (const Z0 (/ 3U0 3I0))
;    (print-measurements m2)
;    (log \3U0= (cexp 3U0) '   ' \3I0= (cexp 3I0) '   ' \Z0= (round-3 Z0))
    (assertion (= (round Z0) 0+i20) "transformer Yn/D Z0")

    ;трансформатор Yн/Y под 3U0
    (const m3 (ZEMQ \(
        (:G 0 11 0+i10 10000a0deg)
        (:G 0 12 0+i10 7000a240deg)
        (:G 0 13 0+i10 7000a120deg)
        (:M (11 21) (0 24) {T 0+i10_010 0+i10_000 | 0+i10_000 0+i10_010 | |})
        (:M (12 22) (0 24) {T 0+i10_010 0+i10_000 | 0+i10_000 0+i10_010 | |})
        (:M (13 23) (0 24) {T 0+i10_010 0+i10_000 | 0+i10_000 0+i10_010 | |})
        )))
    (const (_ _ _ (_ Ua Ia) _ (_ Ub Ib) _ (_ Uc Ic)) m3)
    (const 3U0 (+ Ua Ub Uc))
    (const 3I0 (+ Ia Ib Ic))
    (const Z0 (/ 3U0 3I0))
;    (print-measurements m3)
;    (log \3U0= (cexp 3U0) '   ' \3I0= (cexp 3I0) '   ' \Z0= (round-3 Z0))
    (assertion (~ Z0 0+i10_010 0 0) "transformer Yn/Y Z0")
    ;здесь большая погрешность, вероятно связана с влиянием утечек

    )


(block "grid"

    ;трёхфазная линия c двусторонним питанием
    ;К1 при заземлении параллельной цепи
    (const m1 (ZEMQ \(
        (:G 0 11 0,666+i4,5 69700a0deg)
        (:G 0 12 0,666+i4,5 69700a240deg)
        (:G 0 13 0,666+i4,5 69700a120deg)
        (:M (11 12 13 0) (0 22 23 0) {T
            9.333+i31.173   2.853+i15.813   2.853+i15.813   4.965+i24.895 |
            2.853+i15.813   9.333+i31.173   2.853+i15.813   4.965+i24.895 |
            2.853+i15.813   2.853+i15.813   9.333+i31.173   4.965+i24.895 |
            4.965+i24.895   4.965+i24.895   4.965+i24.895   15.04+i62.8   | |})
        (:G 0 0 1,29+i5,55 69000a-5deg)
        (:G 0 22 1,29+i5,55 69000a235deg)
        (:G 0 23 1,29+i5,55 69000a115deg)
        )))
;    (print-measurements m1)
    (const (_ _ _ (_ Ua Ia) (_ Ub Ib) (_ Uc Ic)) m1)
    (assertion (~ Ua 57430∠-2°   Ia 2760∠-70°) "4ph 2p line A0 A")
    (assertion (~ Ub 68450∠-120° Ib 292∠139°)  "4ph 2p line A0 B")
    (assertion (~ Uc 69140∠118°  Ic 646∠116°)  "4ph 2p line A0 C")
    (const 3U0 (+ Ua Ub Uc))
    (const 3I0 (+ Ia Ib Ic))
    (assertion (~ 3U0 8540∠-175°  3I0 1877∠-77°) "4ph 2p line A0 0")

    ;трёхфазная линия с двусторонним питанием
    ;К1 с переходным сопротивлением при заземлении параллельной цепи
    (const m2 (ZEMQ \(
        (:G 0 11 0,666+i4,5 69700a0deg)
        (:G 0 12 0,666+i4,5 69700a240deg)
        (:G 0 13 0,666+i4,5 69700a120deg)
        (:M (11 12 13 0) (21 22 23 0) {T
            9.333+i31.173   2.853+i15.813   2.853+i15.813   4.965+i24.895 |
            2.853+i15.813   9.333+i31.173   2.853+i15.813   4.965+i24.895 |
            2.853+i15.813   2.853+i15.813   9.333+i31.173   4.965+i24.895 |
            4.965+i24.895   4.965+i24.895   4.965+i24.895   15.04+i62.8   | |})
        (:G 0 21 1,29+i5,55 69000a-5deg)
        (:G 0 22 1,29+i5,55 69000a235deg)
        (:G 0 23 1,29+i5,55 69000a115deg)
        (:Z 21 0 50)
        )))
;    (print-measurements m2)
    (const (_ _ _ (_ Ua Ia) (_ Ub Ib) (_ Uc Ic)) m2)
    (assertion (~ Ua 69500∠-2°   Ia 472∠2°)    "4ph 2p line A0R A")
    (assertion (~ Ub 69560∠-121° Ib 244∠-119°) "4ph 2p line A0R B")
    (assertion (~ Uc 69860∠119°  Ic 254∠136°)  "4ph 2p line A0R C")
    (const 3U0 (+ Ua Ub Uc))
    (const 3I0 (+ Ia Ib Ic))
    (assertion (~ 3U0 770∠-105°  3I0 170∠-6°) "4ph 2p line A0R 0")

    )


;repl

(fmt nil \Ok NL)


